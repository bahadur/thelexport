<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                    <li><a href="#"><?php echo ucwords(str_replace("_"," ",$this->router->fetch_class()))?></a><i class="icon-angle-right"></i></li>
                    <li class="active"><?php echo ucwords(str_replace("_"," ",$this->router->fetch_method()))?></li>
                </ul>
            </div>
        </div>
    </div>
</section>