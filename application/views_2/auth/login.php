<section>

    <div class="container">
        <div class="row">
            <div class="log-lg-12">
                <h4>Log In</h4>
                <form id="loginform" action="contact/contact.php" method="post" class="validateform" name="send-contact">
                    <div class="col-lg-12 field">
                        <div class="row">
                            <div class="col-lg-4">

                                <input type="text" name="loginid" placeholder="* Enter your Login Id" data-rule="required" data-msg="Please enter Login Id" />
                                <div class="validation"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 field">
                        <div class="row">
                            <div class="col-lg-4">
                                <input type="password" name="password" placeholder="* Enter Password" data-rule="required" data-msg="Please enter Password" />
                                <div class="validation"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 field">
                        <p>
                            <button class="btn btn-theme margintop10 pull-left" type="submit">Login</button>
                            <span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>