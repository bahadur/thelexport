 <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.easing.1.3.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.fancybox.pack.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.fancybox-media.js"></script>
        <!-- script src="<?php echo base_url() ?>assets/js/google-code-prettify/prettify.js"></script -->
        <script src="<?php echo base_url() ?>assets/js/portfolio/jquery.quicksand.js"></script>
        <script src="<?php echo base_url() ?>assets/js/portfolio/setting.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery.flexslider.js"></script>
        <script src="<?php echo base_url() ?>assets/js/animate.js"></script>
        <script src="<?php echo base_url() ?>assets/js/custom.js"></script>