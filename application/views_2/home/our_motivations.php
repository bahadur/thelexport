<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <p class="lead">
                   In an era of increased competition and globalization it is ever more important for firm to increase efficiency while controlling costs. With team members in Europe and Asia we are able to assists you to provide your clients with the highest standard of service in the most efficient manner possible. 
                   Our solutions are tailored to your problems. For further information please Contact Us.
                </p>
            </div>
            
        </div>
            
    </div>
</section>