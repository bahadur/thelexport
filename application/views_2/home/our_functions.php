
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <p class="lead">
                    In todays global village firms need to redesign their business model to increase efficiency while reducing time and resources consumed. 
                    We provide round the clock comprehensive services tailored to your individual needs so that you can concentrate on being all that you can be while we take care of the rest. 
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                 
                <h3 class="heading">DOCUMENTS SERVICES</h3>
            
            </div>
            <div class="col-lg-3 col-md-3  col-sm-4">
                <h4>E-DOCUMENT REVIEW</h4> 
                <p>Our proprietary software provides you with review and analysis in a secure hosted environment. 
                    By combining the latest technology with our core strengths we provide you with a cost effective and efficient service
                </p>
               
            </div>
            <div class="col-lg-3 col-md-3  col-sm-4">
                <h4>DOCUMENT REVIEW</h4> 
                <p>
                    Our business model and experience enable us to provide you with a comprehensive document review service at the highest attainable standard
                </p>
               
            </div>
            <div class="col-lg-3 col-md-3  col-sm-4">
                <h4>TRANSCRIPTION</h4> 
                <p>Our team realizes that today recording the spoken word is easier than ever but transcribing it is still time consuming. 
                    Therefore our transcription service allows you to outsource your transcription needs to us while you focus on being all that you can be
                </p>
               
            </div>
            <div class="col-lg-3 col-md-3 ">
                <h4>E-DOCUMENT REVIEW</h4> 
                <p>Our proprietary software provides you with review and analysis in a secure hosted environment. 
                    By combining the latest technology with our core strengths we provide you with a cost effective and efficient service
                </p>
               
            </div>
            
        </div>
        <div class="row">
            <div class="solidline"></div>
        </div>
        <div class="row">
            
            <div class="col-lg-12">
                 
                <h3 class="heading">PROJECT SERVICES</h3>
            
            </div>
            <div class="col-lg-4 col-md-4">
                <h4>LEGAL RESEARCH &emp; DRAFTING</h4> 
                <p>Our associates work with leading industry experts to provide you with quick and up to date research on cases and legislation whilst utilizing the latest online tools to research and identify data
                </p>
               
            </div>
            
            <div class="col-lg-4 col-md-4">
                <h4>CASE SUPPORT</h4> 
                <p>Our team understands that litigation support services are an essential component of the litigation process. Therefore we work to ensure that all your litigation support needs are met
                </p>
               
            </div>
            
            <div class="col-lg-4 col-md-4">
                <h4>PROJECT CONTRACT MANAGEMENT</h4> 
                <p>Our world economy has become increasingly networked and we realize that as a result now no business is an island and must contract with others. We provide our clients with complete contract management and support so that you can concentrate on being all that you can be
                </p>
               
            </div>
        </div>
        
        <div class="row">
            <div class="solidline"></div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                 
                <h3 class="heading">INSOURCING SERVICES</h3>
            
            </div>
            
            <div class="col-lg-6 col-md-6  col-sm-6">
                <h4>THE NETWORK</h4> 
                <p>Our network of lawyers and legal services providers connects you with business and legal talents in the industry as and when required by you
                </p>
               
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6">
                <h4>ON SITE INSOURCING </h4> 
                <p>Our on site insourcing service provides you with the flexibility and skills you need to maximize your legal services without the need of hiring additional or temporary staff
                </p>
               
            </div>
        </div>
        
    </div>
    
</section>
    