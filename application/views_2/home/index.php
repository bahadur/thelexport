                <?php $this->load->view("home/carousel") ?>
<section class="callaction">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="big-cta">
                    <div class="cta-text">
                        <h2><span>Law</span> Find our barristers, clerks and staff</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="box">
                            <div class="box-gray aligncenter">
                                <h4>Legal Services</h4>
                                <div class="icon">
                                    <i class="fa fa-desktop fa-3x"></i>
                                </div>
                                <p>
                                    Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                </p>

                            </div>
                            <div class="box-bottom">
                                <a href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="box">
                            <div class="box-gray aligncenter">
                                <h4>Document Services</h4>
                                <div class="icon">
                                    <i class="fa fa-pagelines fa-3x"></i>
                                </div>
                                <p>
                                    Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                </p>

                            </div>
                            <div class="box-bottom">
                                <a href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="box">
                            <div class="box-gray aligncenter">
                                <h4>Business Services</h4>
                                <div class="icon">
                                    <i class="fa fa-edit fa-3x"></i>
                                </div>
                                <p>
                                    Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                </p>

                            </div>
                            <div class="box-bottom">
                                <a href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="box">
                            <div class="box-gray aligncenter">
                                <h4>Research Services</h4>
                                <div class="icon">
                                    <i class="fa fa-code fa-3x"></i>
                                </div>
                                <p>
                                    Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                </p>

                            </div>
                            <div class="box-bottom">
                                <a href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- divider -->
        <div class="row">
            <div class="col-lg-12">
                <div class="solidline">
                </div>
            </div>
        </div>
        <!-- end divider -->
        <!-- Portfolio Projects -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="heading">FEATURED CASE STUDIES</h3>
                <div class="row">
                    <div class="col-lg-3">
                        <h4>GLOBAL CONTRACT REVIEW</h4>
                        <blockquote>
                            <i class="icon-quote-left icon-2x"></i> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </blockquote>
                    </div>
                    <div class="col-lg-3">
                        <h4>DOCUMENT PROCESSING</h4>
                        <blockquote>
                            <i class="icon-quote-left icon-2x"></i> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </blockquote>
                    </div>
                    <div class="col-lg-3">
                        <h4>SUPPORT SERVICES</h4>
                        <blockquote>
                            <i class="icon-quote-left icon-2x"></i> Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </blockquote>
                    </div>
                    <div class="col-lg-3">
                        <h4>BRAND ASSESSMENT</h4>
                        <span class="pullquote-left">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </span>
                        <span class="pullquote-right margintop10">
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. </span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>