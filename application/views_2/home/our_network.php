<section>
    
    <div class="container">
        
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <p class="lead">
                    Our network combines legal and non-legal talent so that our clients can receive tailored made solutions as and when required.
                </p>
                   
            </div>
            <div class="col-lg-6 col-md-6">
                
                <div class="row">
                    <div class="col-lg-12 col-md-12 aligncenter">
                        <h3>Join our Network</h3>
                    </div>
                    <div class="col-lg-6 col-md-6 aligncenter">
                        <a class="btn btn-theme">United Kingdom Applicants</a>
                    </div>
                    <div class="col-lg-6 col-md-6 aligncenter">
                        <a class="btn btn-theme">Non UK Applicants</a>
                    </div>
                         
                </div>
            </div>
            
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="solidline"></div>
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <p class="lead">
                    Register your Organization with our Network
                </p>
                   
            </div>
            <div class="col-lg-6 col-md-6">
                
                <div class="row">
                    
                    <div class="col-lg-6 col-md-6 aligncenter">
                        <a class="btn btn-theme">Organizations in the United Kingdom</a>
                    </div>
                    <div class="col-lg-6 col-md-6 aligncenter">
                        <a class="btn btn-theme">Organizations outside the United Kingdom</a>
                    </div>
                         
                </div>
            </div>
            
        </div>
        
    </div>
</section>