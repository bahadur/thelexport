<section id="featured">
    <!-- start slider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Slider -->
                <div id="main-slider" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="<?php echo base_url() ?>assets/img/slides/1.jpg" alt="" />
                            <div class="flex-caption">
                                <h3>Your need Our Services</h3> 
                                <p>Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urna</p> 
                                <a href="#" class="btn btn-theme">Learn More</a>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo base_url() ?>assets/img/slides/2.jpg" alt="" />
                            <div class="flex-caption">
                                <h3>Your Future Our Focus</h3> 
                                <p>Sodales neque vitae justo sollicitudin aliquet sit amet diam curabitur sed fermentum.</p> 
                                <a href="#" class="btn btn-theme">Learn More</a>
                            </div>
                        </li>
                        <li>
                            <img src="<?php echo base_url() ?>assets/img/slides/3.jpg" alt="" />
                            <div class="flex-caption">
                                <h3>Your Inspiration Our Expertise</h3> 
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit donec mer lacinia.</p> 
                                <a href="#" class="btn btn-theme">Learn More</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- end slider -->
            </div>
        </div>
    </div>	
</section>