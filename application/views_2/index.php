<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Law Firm</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- css -->
        <?php include "include_top.php" ?>
    </head>
    <body>
        <div id="wrapper">
            <!-- start header -->
            <?php include "header.php" ?>
            <?php $this->load->view("breadcrumb") ?>
            <!-- end header -->
            <div class="solidline"></div>
            <?php $this->load->view($page_name) ?>
            <?php include "footer.php" ?>
        </div>
        <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
        <!-- javascript
            ================================================== -->
       <?php include "include_bottom.php" ?>
    </body>
</html>