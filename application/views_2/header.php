<header>
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><span>Preceneus</span></a>
            </div>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">
                    <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "index") ? "class='active'" : "" ?>><a href="<?php echo base_url() ?>">Home</a></li>
                    <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "our_functions") ? "class='active'" : "" ?>><a href="<?php echo base_url() ?>home/our_functions">Our Functions</a></li>
                    <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "our_motivations") ? "class='active'" : "" ?>><a href="<?php echo base_url() ?>home/our_motivations">Our Motivations</a></li>
                    <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "our_networks") ? "class='active'" : "" ?>><a href="<?php echo base_url() ?>home/our_network">Our Networks</a></li>
                    <li <?php echo ($this->router->fetch_class() == "careers" && $this->router->fetch_method() == "index") ? "class='active'" : "" ?>><a href="<?php echo base_url() ?>">Careers</a></li>
                    
                    <!--li <?php //echo (($this->router->fetch_class() == "home" && $this->router->fetch_method() == "who_we_are") || ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "who_we_do")) ? "class='dowpdown active'" : "class='dowpdown'" ?> >
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">About Us</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php //echo base_url() ?>home/who_we_are">Who We Are</a></li>
                            <li><a href="<?php //echo base_url() ?>home/who_we_do">What We Do</a></li>
                        </ul>
                    </li>

                    <li <?php echo ($this->router->fetch_class() == "career" && $this->router->fetch_method() == "index") ? "class='dowpdown active'" : "class='dowpdown'" ?>>
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">People &AMP; Careers</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url()?>peoples/index">Barristers </a></li>
                                <li><a href="<?php echo base_url()?>peoples/index">Arbitrators</a></li>
                                <li><a href="<?php echo base_url()?>peoples/index">Mediators</a></li>
                                <li><a href="<?php echo base_url()?>peoples/index">Clerks</a></li>
                                <li><a href="<?php echo base_url()?>peoples/index">Staffs</a></li>
                                <li><a href="<?php echo base_url()?>peoples/careers">Careers</a></li>
                                
                            </ul>
                    
                    </li-->
                    <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "contact_us") ? "class='active'" : "" ?>><a href="<?php echo base_url() ?>home/contact_us">Contact Us</a></li>
                    <li <?php echo ($this->router->fetch_class() == "auth" && $this->router->fetch_method() == "login") ? "class='active'" : "" ?>><a href="<?php echo base_url() ?>auth/login">Log In</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>