<?php date_default_timezone_set('Asia/Karachi') ?>
<!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        &copy; The Lexport Inc. <?php echo date("Y")?>, All Rights Reserved<br>
                        <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                       
                             
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="#" class="social-icon si-small si-borderless si-facebook">
                                <em class="icon-facebook"></em>
                                
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-twitter">
                                <em class="icon-twitter"></em>
                                <em class="icon-twitter"></em>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-gplus">
                                <em class="icon-gplus"></em>
                                <em class="icon-gplus"></em>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-pinterest">
                                <em class="icon-pinterest"></em>
                               
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-vimeo">
                                <em class="icon-vimeo"></em>
                                
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-github">
                                <em class="icon-github"></em>
                               
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-yahoo">
                                <em class="icon-yahoo"></em>
                                <em class="icon-yahoo"></em>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-linkedin">
                                <em class="icon-linkedin"></em>
                                <em class="icon-linkedin"></em>
                            </a>
                        </div>

                        <div class="clear"></div>

                        <em class="icon-envelope2"></em> info@thelexport.com <span class="middot">&middot;</span> <em class="icon-headphones"></em> +91-11-6541-6369 <span class="middot">&middot;</span> <em class="icon-skype2"></em> thelexport
                    </div>

                </div>

            </div><!-- #copyrights end -->