<div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><em class="icon-reorder"></em></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="<?php echo base_url()?>" class="standard-logo" data-dark-logo="<?php echo base_url()?>assets/images/logo-dark.png"><img src="<?php echo base_url()?>assets/images/logo.png?<?php echo date("YmdHis")?>" alt="The Lexport Logo"></a>
                        <a href="<?php echo base_url()?>" class="retina-logo" data-dark-logo="<?php echo base_url()?>assets/images/logo-dark@2x.png"><img src="<?php echo base_url()?>assets/images/logo@2x.png?<?php echo date("YmdHis")?>" alt="The Lexport Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu" class="dark">

                        <ul>
                            <!--li <?php //echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "index") ? "class='current'" : "" ?>><a href="<?php //echo base_url()?>"><div>Home</div></a></li -->
                          
                            <!--li <?php //echo ($this->router->fetch_class() == "network" && $this->router->fetch_method() == "index") ? "class='current'" : "" ?>><a href="<?php //echo base_url()?>network/index"><div>Our Networks</div></a></li>
                            <li <?php //echo ($this->router->fetch_class() == "career" && $this->router->fetch_method() == "index") ? "class='current'" : "" ?>><a href="<?php //echo base_url()?>career"><div>Career</div></a></li -->
                         
							
							<li <?php echo ($this->router->fetch_class() == "pages" && $this->router->fetch_method() == "contact") ? "class='current'" : "" ?>><a href="<?php echo base_url()?>pages/contact"><div>Contact us</div></a></li>
							 
							<li <?php echo ($this->router->fetch_class() == "pages" && $this->router->fetch_method() == "about") ? "class='current'" : "" ?>><a href="<?php echo base_url()?>pages/about"><div>About us</div></a></li>
                            
							<li <?php echo ($this->router->fetch_class() == "pages" && $this->router->fetch_method() == "faq") ? "class='current'" : "" ?>><a href="<?php echo base_url()?>pages/faq"><div>FAQS</div></a></li>
							 
							<li <?php echo ($this->router->fetch_class() == "pages" && $this->router->fetch_method() == "jobs") ? "class='current'" : "" ?>><a href="<?php echo base_url()?>pages/jobs"><div>Jobs</div></a></li>
                                                        <?php if(!$this->tank_auth->is_logged_in()) {?>
                                                            <li><a href="<?php echo base_url()?>auth"><div>Login</div></a></li>
                                                        <?php }?>
                        </ul>

                        <!-- Top Cart
                        ============================================= -->
                        

                        <!-- Top Search
                        ============================================= -->
                        <div id="top-search">
                            <a href="#" id="top-search-trigger"><em class="icon-search3"></em><em class="icon-line-cross"></em></a>
                            <form action="search.html" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                            </form>
                        </div><!-- #top-search end -->

                    </nav><!-- #primary-menu end -->

                </div>

            </div>