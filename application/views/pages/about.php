
<section id="page-title">

    <div class="container clearfix">
        <h1>About Us</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">About</li>
        </ol>
    </div>

</section><!-- #page-title end -->


<section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <div class="col_one_third">

                        <div class="heading-block fancy-title nobottomborder title-bottom-border">
                            <h4>Why choose <span>Us</span>.</h4>
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quidem minus id omnis, nam expedita, ea fuga commodi voluptas iusto, hic autem deleniti dolores explicabo labore enim repellat earum perspiciatis.</p>

                    </div>

                    <div class="col_one_third">

                        <div class="heading-block fancy-title nobottomborder title-bottom-border">
                            <h4>Our <span>Mission</span>.</h4>
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quidem minus id omnis, nam expedita, ea fuga commodi voluptas iusto, hic autem deleniti dolores explicabo labore enim repellat earum perspiciatis.</p>

                    </div>

                    <div class="col_one_third col_last">

                        <div class="heading-block fancy-title nobottomborder title-bottom-border">
                            <h4>What we <span>Do</span>.</h4>
                        </div>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quidem minus id omnis, nam expedita, ea fuga commodi voluptas iusto, hic autem deleniti dolores explicabo labore enim repellat earum perspiciatis.</p>

                    </div>

                </div>

                <div class="section nomargin">
                    <div class="container clearfix">

                        <div class="col_one_fourth nobottommargin center bounceIn animated" data-animate="bounceIn">
                            <i class="i-plain i-xlarge divcenter icon-line2-directions"></i>
                            <div class="counter counter-lined"><span data-from="100" data-to="846" data-refresh-interval="50" data-speed="2000">846</span>K+</div>
                            <h5>Lines of Codes</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center bounceIn animated" data-animate="bounceIn" data-delay="200">
                            <i class="i-plain i-xlarge divcenter nobottommargin icon-line2-graph"></i>
                            <div class="counter counter-lined"><span data-from="3000" data-to="15360" data-refresh-interval="100" data-speed="2500">15360</span>+</div>
                            <h5>KBs of HTML Files</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center bounceIn animated" data-animate="bounceIn" data-delay="400">
                            <i class="i-plain i-xlarge divcenter nobottommargin icon-line2-layers"></i>
                            <div class="counter counter-lined"><span data-from="10" data-to="408" data-refresh-interval="25" data-speed="3500">408</span>*</div>
                            <h5>No. of Templates</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center col_last bounceIn animated" data-animate="bounceIn" data-delay="600">
                            <i class="i-plain i-xlarge divcenter nobottommargin icon-line2-clock"></i>
                            <div class="counter counter-lined"><span data-from="60" data-to="1200" data-refresh-interval="30" data-speed="2700">1200</span>+</div>
                            <h5>Hours of Coding</h5>
                        </div>

                    </div>
                </div>

                <div class="row common-height clearfix">

                    <div class="col-sm-5 col-padding" style="height: 599px; background: url(http://themes.semicolonweb.com/html/canvas/images/team/3.jpg) 50% 50% / cover no-repeat;"></div>

                    <div class="col-sm-7 col-padding" style="height: 599px;">
                        <div>
                            <div class="heading-block">
                                <span class="before-heading color">CEO &amp; Co-Founder</span>
                                <h3>John Doe</h3>
                            </div>

                            <div class="row clearfix">

                                <div class="col-md-6">
                                    <p>Employment respond committed meaningful fight against oppression social challenges rural legal aid governance. Meaningful work, implementation, process cooperation, campaign inspire.</p>
                                    <p>Advancement, promising development John Lennon, our ambitions involvement underprivileged billionaire philanthropy save the world transform. Carbon rights maintain healthcare emergent, implementation inspire social change solve clean water livelihoods.</p>
                                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                                        <i class="icon-gplus"></i>
                                        <i class="icon-gplus"></i>
                                    </a>
                                </div>

                                <div class="col-md-6">
                                    <ul class="skills">
                                        <li data-percent="80">
                                            <span>Wordpress</span>
                                            <div class="progress skills-animated" style="width: 80%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="80" data-refresh-interval="30" data-speed="1100">80</span>%</div></div>
                                            </div>
                                        </li>
                                        <li data-percent="60">
                                            <span>CSS3</span>
                                            <div class="progress skills-animated" style="width: 60%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="60" data-refresh-interval="30" data-speed="1100">60</span>%</div></div>
                                            </div>
                                        </li>
                                        <li data-percent="90">
                                            <span>HTML5</span>
                                            <div class="progress skills-animated" style="width: 90%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="90" data-refresh-interval="30" data-speed="1100">90</span>%</div></div>
                                            </div>
                                        </li>
                                        <li data-percent="70">
                                            <span>jQuery</span>
                                            <div class="progress skills-animated" style="width: 70%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="70" data-refresh-interval="30" data-speed="1100">70</span>%</div></div>
                                            </div>
                                        </li>
                                        <li data-percent="85">
                                            <span>Ruby</span>
                                            <div class="progress skills-animated" style="width: 85%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="85" data-refresh-interval="30" data-speed="1100">85</span>%</div></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

                <div class="row common-height bottommargin-lg clearfix">

                    <div class="col-sm-7 col-padding" style="height: 599px; background-color: rgb(245, 245, 245);">
                        <div>
                            <div class="heading-block">
                                <span class="before-heading color">Developer &amp; Evangelist</span>
                                <h3>Mary Jane</h3>
                            </div>

                            <div class="row clearfix">

                                <div class="col-md-6">
                                    <p>Employment respond committed meaningful fight against oppression social challenges rural legal aid governance. Meaningful work, implementation, process cooperation, campaign inspire.</p>
                                    <p>Advancement, promising development John Lennon, our ambitions involvement underprivileged billionaire philanthropy save the world transform. Carbon rights maintain healthcare emergent, implementation inspire social change solve clean water livelihoods.</p>
                                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon inline-block si-small si-light si-rounded si-gplus">
                                        <i class="icon-gplus"></i>
                                        <i class="icon-gplus"></i>
                                    </a>
                                </div>

                                <div class="col-md-6">
                                    <ul class="skills">
                                        <li data-percent="80">
                                            <span>Wordpress</span>
                                            <div class="progress skills-animated" style="width: 80%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="80" data-refresh-interval="30" data-speed="1100">80</span>%</div></div>
                                            </div>
                                        </li>
                                        <li data-percent="60">
                                            <span>CSS3</span>
                                            <div class="progress skills-animated" style="width: 60%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="60" data-refresh-interval="30" data-speed="1100">60</span>%</div></div>
                                            </div>
                                        </li>
                                        <li data-percent="90">
                                            <span>HTML5</span>
                                            <div class="progress skills-animated" style="width: 90%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="90" data-refresh-interval="30" data-speed="1100">90</span>%</div></div>
                                            </div>
                                        </li>
                                        <li data-percent="70">
                                            <span>jQuery</span>
                                            <div class="progress skills-animated" style="width: 70%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="70" data-refresh-interval="30" data-speed="1100">70</span>%</div></div>
                                            </div>
                                        </li>
                                        <li data-percent="85">
                                            <span>Ruby</span>
                                            <div class="progress skills-animated" style="width: 85%;">
                                                <div class="progress-percent"><div class="counter counter-inherit counter-instant"><span data-from="0" data-to="85" data-refresh-interval="30" data-speed="1100">85</span>%</div></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="col-sm-5 col-padding" style="height: 599px; background: url(http://themes.semicolonweb.com/html/canvas/images/team/8.jpg) 50% 50% / cover no-repeat;"></div>

                </div>

                <div class="container clearfix">

                    <div class="clear"></div>

                    <div class="heading-block center">
                        <h4>Our Clients</h4>
                    </div>

                    <ul class="clients-grid grid-6 nobottommargin clearfix">
                        <li><a href="http://logofury.com/logo/enzo.php"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/1.png" alt="Clients"></a></li>
                        <li><a href="http://logofury.com"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/2.png" alt="Clients"></a></li>
                        <li><a href="http://logofaves.com/2014/03/grabbt/"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/3.png" alt="Clients"></a></li>
                        <li><a href="http://logofaves.com/2014/01/ladera-granola/"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/4.png" alt="Clients"></a></li>
                        <li><a href="http://logofaves.com/2014/02/hershel-farms/"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/5.png" alt="Clients"></a></li>
                        <li><a href="http://logofury.com/logo/food-fight-radio.php"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/6.png" alt="Clients"></a></li>
                        <li><a href="http://logofury.com"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/7.png" alt="Clients"></a></li>
                        <li><a href="http://logofury.com/logo/up-travel.php"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/8.png" alt="Clients"></a></li>
                        <li><a href="http://logofury.com/logo/caffi-bardi.php"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/9.png" alt="Clients"></a></li>
                        <li><a href="http://logofury.com/logo/bignix-design.php"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/10.png" alt="Clients"></a></li>
                        <li><a href="http://logofury.com/"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/11.png" alt="Clients"></a></li>
                        <li><a href="http://logofury.com/"><img src="http://themes.semicolonweb.com/html/canvas/images/clients/12.png" alt="Clients"></a></li>
                    </ul>

                </div>

                <div class="section footer-stick">

                    <h4 class="uppercase center">What <span>Clients</span> Say?</h4>

         <div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">
                        <div class="flexslider" style="height: 227px;">
                            <div class="slider-wrap">
                                <div class="slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                                    <div class="testi-image">
                                        <a href="#"><img src="http://themes.semicolonweb.com/html/canvas/images/testimonials/3.jpg" alt="Customer Testimonails" draggable="false"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
                                        <div class="testi-meta">
                                            Steve Jobs
                                            <span>Apple Inc.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                                    <div class="testi-image">
                                        <a href="#"><img src="http://themes.semicolonweb.com/html/canvas/images/testimonials/2.jpg" alt="Customer Testimonails" draggable="false"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
                                        <div class="testi-meta">
                                            Collis Ta'eed
                                            <span>Envato Inc.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="slide flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                                    <div class="testi-image">
                                        <a href="#"><img src="http://themes.semicolonweb.com/html/canvas/images/testimonials/1.jpg" alt="Customer Testimonails" draggable="false"></a>
                                    </div>
                                    <div class="testi-content">
                                        <p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum!</p>
                                        <div class="testi-meta">
                                            John Doe
                                            <span>XYZ Inc.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       
						</div>
                    </div>

                </div>

            </div>

        </section>