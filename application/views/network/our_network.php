<!-- Page Title
        ============================================= -->
<section id="page-title">

    <div class="container clearfix">
        <h1>Our Network</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Our Network</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section id="content">

     <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent nobottommargin clearfix">

                        <!-- Posts
                        ============================================= -->
                        <div id="posts" class="small-thumbs">

                            <div class="entry clearfix">
                                
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h2>Join Our Network</h2>
                                    </div>
                                    
                                    <div class="entry-content">
                                        <p>
                                        <p>Our network combines legal and non-legal talent so that our clients can receive tailored made solutions as and when required.</p>
                                                
                                        <a class="more-link" href="<?php echo base_url().'network/register'?>">United Kingdom Applicants</a><br />
                                        <a class="more-link" href="#">United Kingdom Applicants</a>
                                        </p>
                                        

                                        <p>
                                        <p>Register your Organization with our Network.</p>

                                        <a class="more-link" href="#">Organizations in the United Kingdom</a><br />
                                        <a class="more-link" href="#">Organizations outside the United Kingdom</a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                          

                        </div><!-- #posts end -->

                        

                    </div><!-- .postcontent end -->

                    <!-- Sidebar
                    ============================================= -->
                    <div class="sidebar nobottommargin col_last clearfix">
                        <div class="sidebar-widgets-wrap">

                            <div class="widget widget-twitter-feed clearfix">

                                <h4>Twitter Feed</h4>
                                <ul id="sidebar-twitter-list-1" class="iconlist">
                                    <li></li>
                                </ul>

                                <a href="#" class="btn btn-default btn-sm fright">Follow Us on Twitter</a>

                                <script type="text/javascript">
                                    jQuery( function($){
                                        $.getJSON('include/twitter/tweets.php?username=envato', function(tweets){
                                            $("#sidebar-twitter-list-1").html(sm_format_twitter(tweets));
                                        });
                                    });
                                </script>

                            </div>

                            

                            <div class="widget clearfix">

                                <div class="tabs nobottommargin clearfix" id="sidebar-tabs">

                                    <ul class="tab-nav clearfix">
                                        <li><a href="#tabs-1">Popular</a></li>
                                        <li><a href="#tabs-2">Recent</a></li>
                                        <li><a href="#tabs-3"><i class="icon-comments-alt norightmargin"></i></a></li>
                                    </ul>

                                    <div class="tab-container">

                                        <div class="tab-content clearfix" id="tabs-1">
                                            <div id="popular-post-list-sidebar">

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/magazine/small/3.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <div class="entry-title">
                                                            <h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
                                                        </div>
                                                        <ul class="entry-meta">
                                                            <li><i class="icon-comments-alt"></i> 35 Comments</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/magazine/small/2.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <div class="entry-title">
                                                            <h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
                                                        </div>
                                                        <ul class="entry-meta">
                                                            <li><i class="icon-comments-alt"></i> 24 Comments</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/magazine/small/1.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <div class="entry-title">
                                                            <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                        </div>
                                                        <ul class="entry-meta">
                                                            <li><i class="icon-comments-alt"></i> 19 Comments</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-content clearfix" id="tabs-2">
                                            <div id="recent-post-list-sidebar">

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/magazine/small/1.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <div class="entry-title">
                                                            <h4><a href="#">Lorem ipsum dolor sit amet, consectetur</a></h4>
                                                        </div>
                                                        <ul class="entry-meta">
                                                            <li>10th July 2014</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/magazine/small/2.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <div class="entry-title">
                                                            <h4><a href="#">Elit Assumenda vel amet dolorum quasi</a></h4>
                                                        </div>
                                                        <ul class="entry-meta">
                                                            <li>10th July 2014</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/magazine/small/3.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <div class="entry-title">
                                                            <h4><a href="#">Debitis nihil placeat, illum est nisi</a></h4>
                                                        </div>
                                                        <ul class="entry-meta">
                                                            <li>10th July 2014</li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="tab-content clearfix" id="tabs-3">
                                            <div id="recent-post-list-sidebar">

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/icons/avatar.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <strong>John Doe:</strong> Veritatis recusandae sunt repellat distinctio...
                                                    </div>
                                                </div>

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/icons/avatar.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <strong>Mary Jane:</strong> Possimus libero, earum officia architecto maiores....
                                                    </div>
                                                </div>

                                                <div class="spost clearfix">
                                                    <div class="entry-image">
                                                        <a href="#" class="nobg"><img class="img-circle" src="images/icons/avatar.jpg" alt=""></a>
                                                    </div>
                                                    <div class="entry-c">
                                                        <strong>Site Admin:</strong> Deleniti magni labore laboriosam odio...
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <script>
                                    $(function() {
                                        $( "#sidebar-tabs" ).tabs({ show: { effect: "fade", duration: 400 } });
                                    });
                                </script>

                            </div>

                           

                            <div class="widget clearfix">

                                <h4>Tag Cloud</h4>
                                <div class="tagcloud">
                                    <a href="#">general</a>
                                    <a href="#">videos</a>
                                    <a href="#">music</a>
                                    <a href="#">media</a>
                                    <a href="#">photography</a>
                                    <a href="#">parallax</a>
                                    <a href="#">ecommerce</a>
                                    <a href="#">terms</a>
                                    <a href="#">coupons</a>
                                    <a href="#">modern</a>
                                </div>

                            </div>

                        </div>

                    </div><!-- .sidebar end -->

                </div>

            </div>
    
    
    <!--div class="content-wrap">
        

            <div class="container clearfix">
                
                <div clsss="postcontent nobottommargin clearfix">
                    <div id="posts" class="small-thumbs">
                        <div class="entry clearfix">
                            <div class="entry-c">
                                <div class="entry-title">
                                    <h2>Join Our Network</h2>
                                </div>
                                
                                <div class="entry-content">
                                    <p>Our network combines legal and non-legal talent so that our clients can receive tailored made solutions as and when required.</p>
                                    <a class="more-link" href="#">United Kingdom Applicants</a><br />
                                    <a class="more-link" href="#">United Kingdom Applicants</a>
                                    
                                    <p>Register your Organization with our Network.</p>
                                    
                                    <a class="more-link" href="#">Organizations in the United Kingdom</a><br />
                                    <a class="more-link" href="#">Organizations outside the United Kingdom</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="sidebar nobottommargin col_last clearfix">
                    <div class="sidebar-widgets-wrap">
                        <div class="widget widget_links clearfix">
                            <h4>Pages</h4>
                        </div>

                    </div>
                </div>
                     
                
                
                
                
                
                
                
               


            </div>

        </div -->
    
</section>
