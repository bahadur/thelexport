<!-- Page Title
        ============================================= -->
<section id="page-title">

    <div class="container clearfix">
        <h1>Register Network</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Network</a></li>
            <li class="active">Register</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    

                    <div class="col_full nobottommargin">


                        <h3>Join Our Network</h3>

                        <p>Our network combines legal and non-legal talent so that our clients can receive tailored made solutions as and when required.</p>

                        <form id="register-form" name="register-form" class="nobottommargin" action="#" method="post">
                                   
                            

                            <ul id="progressbar">
                                <li>Account Detail</li>
                                <li>Activation</li>
                                <li>Profile Detail</li>
                            </ul>
                            <div class="col_one_third">
                                <label for="register-form-name">Full Name:</label>
                                <input type="text" id="register-form-name" name="register-form-name" value="" class="required form-control input-block-level" />
                            </div>

                            <div class="col_one_third">
                                <label for="register-form-email">Email Address:</label>
                                <input type="text" id="register-form-email" name="register-form-email" value="" class="required form-control input-block-level" />
                            </div>
                            
                            <div class="col_one_third col_last">
                                <label for="register-form-username">Choose a Username:</label>
                                <input type="text" id="register-form-username" name="register-form-username" value="" class="required form-control input-block-level" />
                            </div>

                            
                           

                            <div class="clear"></div>
                            
                             <div class="col_half">
                                <label for="register-form-username">Job Title:</label>
                                <input type="text" id="register-form-job_title" name="register-form-job_title" value="" class="required form-control input-block-level" />
                            </div>
                            
                            <div class="col_half col_last ">
                                <label for="register-form-username">Company:</label>
                                <input type="text" id="register-form-company" name="register-form-company" value="" class="required form-control input-block-level" />
                            </div>
                            
                            
                            
                            <div class="col_full">
                                <h4>Contact no:</h4>
                                
                                <div class="col_one_third">
                                    <label for="register-form-phone">Business:</label>
                                    <input type="text" id="register-form-phone" name="register-form-phone" value="" class="required form-control input-block-level" />
                                </div>
                                
                                <div class="col_one_third">
                                    <label for="register-form-phone">Fax:</label>
                                    <input type="text" id="register-form-phone" name="register-form-phone" value="" class="required form-control input-block-level" />
                                </div>
                                
                                <div class="col_one_third col_last">
                                    <label for="register-form-phone">Home:</label>
                                    <input type="text" id="register-form-phone" name="register-form-phone" value="" class="required form-control input-block-level" />
                                </div>
                                     
                                
                            </div>
                            <div class="clear"></div>
                            
                            <div class="col_one_third">
                                <label for="register-form-phone">Phone:</label>
                                <input type="text" id="register-form-phone" name="register-form-phone" value="" class="required form-control input-block-level" />
                            </div>

                            <div class="clear"></div>

                            <div class="col_half">
                                <label for="register-form-password">Choose Password:</label>
                                <input type="password" id="register-form-password" name="register-form-password" value="" class="required form-control input-block-level" />
                            </div>

                            <div class="col_half col_last">
                                <label for="register-form-repassword">Re-enter Password:</label>
                                <input type="password" id="register-form-repassword" name="register-form-repassword" value="" class="required form-control input-block-level" />
                            </div>

                            <div class="clear"></div>

                            <div class="col_full nobottommargin">
                                <button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
                            </div>

                        </form>


                    </div>

                </div>

            </div>

        </section><!-- #content end -->