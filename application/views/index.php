<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="SemiColonWeb" />

        <!-- Stylesheets
        ============================================= -->
        
        <link href=" http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/dark.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-icons.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/animate.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/magnific-popup.css" type="text/css" />

        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-multiselect.css" type="text/css" />

        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/responsive.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/validationEngine.jquery.css" type="text/css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome-animation.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-datepicker.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-datetimepicker.min.css" type="text/css" />
    
        <!--link rel="stylesheet" href="<?php echo base_url()?>assets/css/calendar.css" type="text/css" / -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fullcalendar.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/fullcalendar.print.css" type="text/css"  media='print' />
        
        

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!--[if lt IE 9]>
            <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->

        <!-- External JavaScripts
        ============================================= -->
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins.js"></script>
       

        <script type="text/javascript" src="<?php echo base_url()?>assets/js/events-data.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap-multiselect.js"></script>

        
        
        <!--script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.calendario.js"></script -->
        <!-- DataTables CSS -->
        <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.css">
        
  
        <!-- DataTables -->
        <script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/fullcalendar.min.js"></script>
        

        <!-- Document Title
        ============================================= -->
        <title><?php echo $page_title?></title>

    </head>
    <body class="stretched">
        <!--div id="top-bar">
            <?php //$this->load->view("topbar");?>
        </div-->
        
        <!-- The Main Wrapper
        ============================================= -->
        <div id="wrapper" class="clearfix">

            <!-- Header
            ============================================= -->
            <header id="header" class="<?php echo ($this->router->fetch_class() == "auth" && $this->router->fetch_method() == "index")?'transparent-header dark':''?> full-header" data-sticky-class="no-sticky">

               <?php $this->load->view("header");?>

            </header>
            <?php  if ($this->tank_auth->is_logged_in()) {	?>
            <!-- Page Sub Menu
        ============================================= -->
            <div id="page-menu">

                <div id="page-menu-wrap">

                    <div class="container clearfix">

                        <div class="menu-title">Just <span>LEXPORT</span> it</div>

                        <nav  id="secondary-menu">
                            <ul>
                                <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "index") ? "class='current'" : "" ?>>
                                    <a href="<?php echo base_url()?>home">Calendar View</a>
                                </li>
                                
                                <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "listview") ? "class='current'" : "" ?>>
                                    <a href="<?php echo base_url()?>home/listview">List View</a>
                                </li>
                                
                                <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "messages") ? "class='current'" : "" ?>>
                                    <a href="<?php echo base_url()?>home/timeline">Messages</a>
                                </li>
                                
                                <li <?php echo ($this->router->fetch_class() == "home" && $this->router->fetch_method() == "options") ? "class='current'" : "" ?> class="dropdown dropdown-submenu">
                                    <a href="<?php echo base_url()?>settings">Settings</b></a>
                                </li>
                                
                                <li>
                                    <a href="<?php echo base_url()?>auth/logout">Logout</a>
                                </li>
                                
                            </ul>
                        </nav>

                    <div id="page-submenu-trigger"><em class="icon-reorder"></em></div>

                    </div>

                </div>

            </div><!-- #page-menu end -->
            <?php } ?>
            
            <!-- Site Content
            ============================================= -->
            
            

                        <?php $this->load->view($page_name) ?>
                

                

            <!-- Footer
            ============================================= -->
            <footer id="footer" class="dark">

                

                    <?php $this->load->view("footer") ?>

               

                

            </footer>

        </div>
    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- Footer Scripts
    ============================================= -->
    <script>
         var base_url = '<?php echo base_url()?>';
    </script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/functions.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validationEngine-en.js" type="text/javascript"></script>
    
    </body>
</html>
