<?php

$form_attributes = array('class' => 'nobottommargin');
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
        'class' => "required form-control input-block-level"
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'login_password',
        
	'size'	=> 30,
        'class' => 'required form-control input-block-level'
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);

?>
<section id="slider" class="slider-parallax full-screen dark" style="background: url(<?php echo base_url() ?>assets/images/landing/landing1.jpg) center;">

            <div class="container vertical-middle clearfix">

                <div class="row">
                    <div class="col-lg-3">&nbsp;</div>
                    <div class="col-lg-6">
                        <div class="well well-lg nobottommargin">
                            <h1 data-animate="fadeInDown" class="center">The Lexport </h1>
                            <p data-animate="fadeInUp" data-delay="400" class="center">Your Legal Network</p>
                            <?php echo form_open($this->uri->uri_string(),$form_attributes); ?>
                            <!--form id="login-form" name="login-form" class="nobottommargin" action="#" method="post" -->
                                <div class="col_full">
                                    <!--label for="login-form-username">User/Email:</label -->
                                    <?php echo form_label($login_label, $login['id']); ?>
                                    <?php echo form_input($login); ?>
                                    <div style="color:red"><?php echo form_error($login['name']); ?></div>
                                    <!-- input type="text" id="login-form-username" value="" class="required form-control input-block-level" / -->
                                </div>

                                <div class="col_full">
                                    <?php echo form_label("Password", $password['id']); ?>
                                    <?php echo form_password($password); ?>
                                    <div style="color:red"><?php echo form_error($password['name']); ?></div>
                                    <!--label for="login-form-password">Password:</label>
                                    <input type="password" id="login-form-password" value="" class="required form-control input-block-level" / -->
                                </div>
                            
                                <div class="col_full">
                                    <?php echo form_checkbox($remember); ?>
                                    <?php echo form_label('Remember me', $remember['id']); ?>

                                </div>

                                <div class="col_full nobottommargin">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <?php echo form_submit(array('class'=>'button button-3d nomargin'),'submit'); ?>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="col-lg-12">
                                            <?php echo anchor('/auth/forgot_password/', 'Forgot password',array('class'=>'fright')); ?>
                                            </div>
                                            <div class="col-lg-12">
                                            <?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/home/register/', 'Register',array('class'=>'fright')); ?>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <!--div class="col_full">
                                    <button class="button button-3d nomargin fright" id="login-form-submit" name="login-form-submit" value="login">Login</button>

                                </div >
                                <div class="col_full">
                                    <a href="#" class="fleft">Don't have Account?</a>
                                    <a href="#" class="fright">Forgot Password?</a>
                                </div-->


                            </form>
                        </div>
                        <div class="col-lg-3">&nbsp;</div>
                    </div>
                </div>

            </div>

        </section>

