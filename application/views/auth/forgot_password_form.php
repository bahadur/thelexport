<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
?>

<section id="page-title">

    <div class="container clearfix">
        <h1>Forget Password</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Authentication</a></li>
            <li class="active">Forget Password</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col_full nobottommargin">
                <h3>Forget Password</h3>
                <p>Enter the information below and we'll send you an email with the next steps to get a new password.</p>
                
                <?php echo form_open($this->uri->uri_string()); ?>
                    <div class="col_half">
                        <?php echo form_label($login_label, $login['id']); ?>
                        <?php echo form_input($login); ?>
                        <div class="alert-danger"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?'&nbsp;'.$errors[$login['name']]:''; ?></div>
                    </div>
                
                    <div class="col_half">
                              <?php 
                              $data = array(
                                'name' => 'reset',
                                'id' => 'reset',
                                'value' => 'true',
                                'type' => 'submit',
                                'content' => 'Get a new password',
                                'class' => 'button button-3d button-black  pull-right margin-bottom-none'
                            );
                              
                              echo form_button($data); ?>
                    </div>
                
                
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
         
    
</section>
