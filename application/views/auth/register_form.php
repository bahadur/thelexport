<section id="page-title">

    <div class="container clearfix">
        <h1>Register | Login</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Authentication</a></li>
            <li class="active">Register | Login</li>
        </ol>
    </div>

</section><!-- #page-title end -->



<section class="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <h3>Don't have an Account? Register Now.</h3>
                
            

            <div class="col_half">
                        <div class="feature-box fbox-outline fbox-dark fbox-effect">
                            <div class="fbox-icon">
                                <a href="<?php echo base_url()?>auth/register/sol_practitioner"><i class="fa fa-user i-alt"></i></a>
                            </div>
                            <h3>Sol practitioner</h3>
                            <p>Canvas included 20+ custom designed Slider Pages with Premium Sliders like Layer, Revolution, Swiper &amp; others.</p>
                        </div>
                    </div>

                    <div class="col_half col_last">
                        <div class="feature-box fbox-outline fbox-dark fbox-effect">
                            <div class="fbox-icon">
                                <a href="<?php echo base_url()?>auth/register/organization"><i class="fa fa-legal i-alt"></i></a>
                            </div>
                            <h3>Organization</h3>
                            <p>If you wish to have your organization registered on our network or are an individual seeking to join our network then please Contact US.</p>
                        </div>
                    </div>


        </div>
    </div>
</section>
<script>
    
    
$(document).ready(function () {
    $( "#forms-registration" ).tabs({ show: { effect: "fade", duration: 400 } });
});
</script>