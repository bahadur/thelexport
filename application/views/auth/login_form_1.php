<?php
$form_attributes = array('class' => 'nobottommargin');
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
        'class' => "required form-control input-block-level"
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'login_password',
        
	'size'	=> 30,
        'class' => 'required form-control input-block-level'
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>

<section id="page-title">

    <div class="container clearfix">
        <h1>Register | Login</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Authentication</a></li>
            <li class="active">Register | Login</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section class="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col_one_third nobottommargin">
                <div class="well well-lg nobottommargin">
                <?php echo form_open($this->uri->uri_string(),$form_attributes); ?>
                    <h3>Login to your Account</h3>
                    
                    <div class="col_full">
                        <?php echo form_label($login_label, $login['id']); ?>
                        <?php echo form_input($login); ?>
                        <div class="danger"><?php echo form_error($login['name']); ?></div>
                        
                    </div>
                    
                    <div class="col_full">
                        <?php echo form_label("Password", $password['id']); ?>
                        <?php echo form_password($password); ?>
                        <div class="danger"><?php echo form_error($password['name']); ?></div>
                    </div>
                    
                    <div class="col_full">
                        <?php echo form_checkbox($remember); ?>
                        <?php echo form_label('Remember me', $remember['id']); ?>
                        
                    </div>
                    
                    <div class="col_full nobottommargin">
                        <div class="row">
                            <div class="col-lg-4">
                                <?php echo form_submit(array('class'=>'button button-3d nomargin'),'submit', 'Let me in'); ?>
                            </div>
                            <div class="col-lg-8">
                                <div class="col-lg-12">
                                <?php echo anchor('/auth/forgot_password/', 'Forgot password',array('class'=>'fright')); ?>
                                </div>
                                <div class="col-lg-12">
                                <?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register',array('class'=>'fright')); ?>
                                    </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    
                    
                <?php echo form_close(); ?>
                </div>
                
                
            </div>
            
            <div class="col_two_third col_last nobottommargin">
        <h3>Don't have an Account? Register Now.</h3>
        <form id="register-form" name="register-form" class="nobottommargin" action="<?php echo base_url().'auth/register'?>" method="post" >
            <div class="col_half">
                <label for="email">Email:</label>
                <input class="form-control validate[required,custom[email]] text-input" type="text" name="email" id="email" placeholder="Email Address" data-validate="Email is required" />
                <div class="danger"><?php echo form_error('email'); ?><?php echo isset($errors['email'])?$errors['email']:''; ?></div>
            </div>
            
            <div class="col_half col_last">
                <label for="username">User Name:</label>
                <input class="form-control validate[required,custom[onlyLetterNumber],maxSize[20],ajax[ajaxUserCallPhp]] text-input" type="text" name="username" id="username" placeholder="username Address" />
                <div class="danger"><?php echo form_error('username'); ?><?php echo isset($errors['username'])?$errors['username']:''; ?></div>
            </div>
            
            <div class="clear"></div>
            
            <div class="col_half">
                <label for="password">Password:</label>
                <input class="form-control validate[required] text-input" type="password" name="password" id="password" placeholder="Password" />
                <div class="danger"><?php echo form_error('password'); ?></div>
            </div>
            
            <div class="col_half col_last">
                <label for="confirm_password">Confirm Password:</label>
                <input class="form-control validate[required,equals[password]] text-input" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" />
                <div class="danger"><?php echo form_error('confirm_password'); ?></div>
            </div>
             
            <div class="clear"></div>
            
            <?php if ($show_captcha_register) {  ?>
                       
            <div class="col_full">
                
                    <?php echo $captcha_html_register; ?>
            </div>
            <div class="clear"></div>
            <div class="col_half">
                    <label>Enter the code exactly as it appears:</label>
                    <input class="form-control validate[required] text-input" type="text" name="captcha" id="captcha" placeholder="Confirmation Code" />
                    <div class="danger"><?php echo form_error('captcha'); ?></div>
                
            </div>
                
            
            <div class="col_half col_last">
                <button type="submit" class="button button-3d button-black  pull-right margin-bottom-none" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
            </div>
            
            <?php } else {  ?>
            <div class="clear"></div>

            <div class="col_full">
                <button type="submit" class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
            </div>
            <?php }?>
        </form>
            
    </div>
        </div>
    </div>
    
    


</section>
         
<script>
    $(document).ready(function () {
       
        
        $("#register-form").validationEngine();

        $("#register-form").bind("jqv.field.result", function(event, field, errorFound, prompText){ console.log(errorFound) })
    });
</script>