<link rel="stylesheet" href="<?php echo base_url()?>assets/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/select2/select2.css">
<script src="<?php echo base_url()?>assets/select2/select2.min.js"></script>
<?php
$form_attributes = array('class' => 'nobottommargin');
$login = array(
    'name' => 'login',
    'id' => 'login',
    'value' => set_value('login'),
    'maxlength' => 80,
    'size' => 30,
    'class' => "required form-control input-block-level"
);
if ($login_by_username AND $login_by_email) {
    $login_label = 'Email or login';
} else if ($login_by_username) {
    $login_label = 'Login';
} else {
    $login_label = 'Email';
}
$password = array(
    'name' => 'password',
    'id' => 'login_password',
    'size' => 30,
    'class' => 'required form-control input-block-level'
);
$remember = array(
    'name' => 'remember',
    'id' => 'remember',
    'value' => 1,
    'checked' => set_value('remember'),
    'style' => 'margin:0;padding:0',
);
$captcha = array(
    'name'  => 'captcha',
    'id'    => 'captcha',
    'maxlength' => 8,
);
?>

<section id="page-title">

    <div class="container clearfix">
        <h1>Register</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Authentication</a></li>
            <li class="active">Register </li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section class="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <form id="register-sol-practiioner-form" name="register-sol-practiioner-form" class="nobottommargin form-horizontal" action="<?php echo base_url() . 'auth/register' ?>" method="post" >
                <div class="col_two_third col_last nobottommargin">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>Sole Practitioner Information</h3>
                            </div>
                        </div>
                        <div class="panel-body">
                            
                            <div class="col_one_third">
                                <label for="f_name">Name:</label>
                                <input class="form-control validate[required] text-input" type="text" name="f_name" id="f_name" placeholder="First Name" data-validate="First Name is required" />
                                <div style="color: red;"><?php echo form_error('f_name'); ?><?php echo isset($errors['f_name']) ? $errors['f_name'] : ''; ?></div>
                            </div>

                            <div class="col_one_third">
                                <label for="m_name">&nbsp;</label>
                                <input class="form-control" type="text" name="m_name" id="m_name" placeholder="Middle Name" />
                                <div style="color: red;"><?php echo form_error('m_name'); ?><?php echo isset($errors['m_name']) ? $errors['m_name'] : ''; ?></div>
                            </div>

                            <div class="col_one_third col_last">
                                <label for="l_name">&nbsp;</label>
                                <input class="form-control validate[required] text-input" type="text" name="l_name" id="l_name" placeholder="Last Name" data-validate="Last Name is required" />
                                <div style="color: red;"><?php echo form_error('l_name'); ?><?php echo isset($errors['l_name']) ? $errors['l_name'] : ''; ?></div>
                            </div>
                            
                            <div class="clear"></div>
                            
                            <div class="col_half">
                                <label>You are a:</label>
                                <select name="legal_qualifications_legal_qualifications_id" id="legal_qualifications_legal_qualifications_id" class="form-control select2" >
                                    <option></option>
                                    <optgroup label="Legal Qualificatoins">
                                    <?php 
                                    foreach ($this->db->get("legal_qualifications")->result() as  $row_qualification) {
                                        echo "<option value='$row_qualification->legal_qualifications_id'>$row_qualification->qualificatoin</option>";
                                    }
                                    ?>
                                    </optgroup>                                   
                                </select>
                            </div>

                            <div class="col_half col_last">
                                <label>Other</label>
                                <input type="text" name="other_legal_qualifications" id="other_legal_qualifications" class="form-control" placeholder="Please Specify" />
                            </div>
                            
                            <div class="clear"></div>
                            
                            <div class="col_one_third">
                                <label>Regulatory Authority:</label>
                                <select name="regulatory_authority_regulatory_authority_id" id="regulatory_authority_regulatory_authority_id" class="form-control select2">
                                    <option></option>
                                    <optgroup label="Regulatory Authorities">
                                    <?php 
                                    foreach ($this->db->get("regulatory_authority")->result() as  $row_regulatory_authority) {
                                        echo "<option value='$row_regulatory_authority->regulatory_authority_id'>$row_regulatory_authority->authority</option>";
                                    }
                                    ?>   
                                    </optgroup>
                                </select>
                            </div>

                            <div class="col_one_third">
                                <label>Other</label>
                                <input type="text" name="other_regulatory_authority" id="other_regulatory_authority" class="form-control" placeholder="Please Specify" />
                            </div>
                                    
                            <div class="col_one_third col_last">
                                <label>Regulalory Authority No</label>
                                <input type="text" name="regulatory_authority_no" id="regulatory_authority_no" class="form-control" placeholder="Authority No" />
                            </div>

                            <div class="clear"></div>
                                            
                            <div class="col_one_third">
                                <label>Date of Qualification:</label>
                                <select name="qualifications_qualification_id" id="qualifications_qualification_id" class="form-control select2">
                                    <option></option>
                                    <optgroup label="Qualification">
                                    <?php 
                                    foreach ($this->db->get("qualifications")->result() as  $row_qualification) {
                                        echo "<option value='$row_qualification->qualification_id'>$row_qualification->qualification</option>";
                                    }
                                    ?>   
                                    </optgroup>
                                </select>
                            </div>

                            <div class="col_one_third">
                                <label>Other</label>
                                <input type="text" name="other_qualification" id="other_qualification" class="form-control" placeholder="Please Specify" />
                            </div>
                                    
                            <div class="col_one_third col_last">
                                <label>Date of Completion:</label>
                                <input type="text" name="date_completion" id="date_completion" class="form-control" placeholder="Date of Completion" />
                            </div>

                            <div class="clear"></div>
                            
                            <div class="col_full col_last">
                                    <label for="address">Website:</label>
                                <input type="text" name="website" id="website" class="form-control" />
                            </div>
                            
                            <div class="clear"></div>
                            <div class="col_full col_last">
                                <label for="address">Address:</label>
                                <textarea name="address" id="address" class="form-control"></textarea>
                            </div>

                            <div class="clear"></div>
                                            
                            <div class="col_half ">
                                <label for="city">City:</label>
                                <input class="form-control text-input" type="text" name="city" id="city" placeholder="City" />
                            </div>

                            <div class="col_half col_last">
                                <label for="country">Country:</label>
                                <input class="form-control text-input" type="text" name="country" id="country" placeholder="Country" />
                            </div>
                                    
                            <div class="col_half ">
                                <label for="contact_no">Contact No:</label>
                                <input class="form-control text-input" type="text" name="contact_no" id="contact_no" placeholder="Phone" />
                            </div>

                            <div class="col_half col_last ">
                                <label for="zip_code">Post/zip Code:</label>
                                <input class="form-control text-input" type="text" name="zip_code" id="zip_code" placeholder="Postal/Zip Code" />
                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>
                
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>Account Information</h3>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col_half">
                                <label for="user">User Name:</label>
                                <!--input class="form-control validate[required,custom[email]] text-input" type="text" name="email" id="email" placeholder="Your Email Address" data-validate="Email is required" --> 
                                <input type="text" class="form-control validate[required]" name="username" id="username" placeholder="Your Email Address" >
                                                    <!--div class="input-group">
                                                        
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" ><span class="seleted_domain">@brickchamber.co.uk</span> <span class="caret"></span></button>
                                                            <ul class="dropdown-menu" role="menu">
                                                                <select id="domains" class="form-control" multiple="">
                                                                    <option value="@brickchamber.co.uk">@brickchamber.co.uk</option>
                                                                    <option value="@gmail.com">@gmail.com</option>
                                                                    <option value="@yahoo.com">@yahoo.com</option>
                                                                    <option value="@hotmail.com">@hotmail.com</option>
                                                                </select>
                                                            </ul>
                                                        </div>
                                                    </div-->



                                <div style="color: red;"><?php echo form_error('username'); ?></div>
                            </div>

                            <div class="col_half col_last">
                                <label for="user">Email:</label>
                                <input type="text" class="form-control validate[required]"  name="email" id="email" placeholder="Your Email Address" >
                                <div style="color: red;"><?php echo form_error('email'); ?></div>
                            </div>
                                                <!--div class="col_half col_last">
                                                    <div class="style-msg infomsg">
                                                        <div class="sb-msg">
                                                            <i class="icon-info-sign"></i>
                                                            <strong>Note:</strong>
                                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                            tempor incididunt ut labore et dolore magna aliqua. 
                                                                
                                                        </div>
                                                    </div>
                                                </div-->
                                    
                            <div class="clear"></div>

                            <div class="col_half ">
                                <label for="password">Password:</label>
                                <input class="form-control validate[required] text-input" type="password" name="password" id="password" placeholder="Password" />
                                <div style="color: red;"><?php echo form_error('password'); ?></div>
                            </div>
                             
                            <div class="col_half col_last">
                                <label for="confirm_password">Confirm Password:</label>
                                <input class="form-control validate[required,equals[password]] text-input" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" />
                                <div class="danger"><?php echo form_error('confirm_password'); ?></div>
                            </div>

                        </div>
                        
                    </div>
            
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>Optional</h3>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="tabs tabs-bordered clearfix" id="contacts">
                                <ul class="tab-nav clearfix">
                                    <li><a href="#primary-contacts">Primary Contact</a></li>
                                    <li><a href="#secondary-contacts">Secondary Contact</a></li>
                                    <li><a href="#alternative-contacts">Alternative Contact (Optional)</a></li>
                                </ul>
                                <div class="tab-container">
                                    <div class="tab-content clearfix" id="primary-contacts">
                                        <h4>Primary Contact Person</h4>
                                        
                                        <div class="col_one_third">
                                            <label for="primary_name">Name:</label>
                                            <input type="text" name="primary_name" id="primary_name" class="form-control text-input" />
                                        </div>
                                         
                                        <div class="col_one_third ">
                                            <label for="primary_position">Position:</label>
                                            <input type="text" name="primary_position" id="primary_position" class="form-control" />
                                        </div>
                                        
                                        <div class="col_one_third col_last">
                                            <label for="primary_email">Contact Email:</label>
                                            <input type="text" name="primary_email" id="primary_email" class="form-control" />
                                        </div>

                                        <div class="clear"></div>

                                        <div class="col_half ">
                                            <label for="primary_workphone">Work Phone:</label>
                                            <input type="text" name="primary_workphone" id="primary_workphone" class="form-control" />
                                        </div>

                                        <div class="col_half col_last">
                                            <label for="primary_mobile">Work Mobile:</label>
                                            <input type="text" name="primary_mobile" id="primary_mobile" class="form-control" />
                                        </div>
                                    
                                    </div>
                                    
                                    <div class="tab-content clearfix" id="secondary-contacts">
                                        <h4>Secondary Contact Person</h4>
                                        
                                        <div class="col_one_third">
                                            <label for="secondary_name">Name:</label>
                                            <input type="text" name="secondary_name" id="secondary_name" class="form-control text-input" />
                                        </div>
                                        <div class="col_one_third ">
                                            <label for="secondary_position">Position:</label>
                                            <input type="text" name="secondary_position" id="secondary_position" class="form-control" />
                                        </div>
                                        
                                        <div class="col_one_third col_last">
                                            <label for="secondary_email">Contact Email:</label>
                                            <input type="text" name="secondary_email" id="secondary_email" class="form-control" />
                                        </div>

                                        <div class="clear"></div>

                                        <div class="col_half ">
                                            <label for="secondary_workphone">Work Phone:</label>
                                            <input type="text" name="secondary_workphone" id="secondary_workphone" class="form-control" />
                                        </div>

                                        <div class="col_half col_last">
                                            <label for="secondary_mobile">Work Mobile:</label>
                                            <input type="text" name="secondary_mobile" id="secondary_mobile" class="form-control" />
                                        </div>
                        
                                    </div>
                                    
                                    <div class="tab-content clearfix" id="alternative-contacts">
                                        <h4>Alternative Contact Person</h4>
                                        <div class="col_one_third">
                                            <label for="alternative_name">Name:</label>
                                            <input type="text" name="alternative_name" id="alternative_name" class="form-control text-input" />
                                        </div>
                    
                                        <div class="col_one_third ">
                                            <label for="alternative_position">Position:</label>
                                            <input type="text" name="alternative_position" id="alternative_position" class="form-control" />
                                        </div>
                                                            
                                        <div class="col_one_third col_last">
                                            <label for="alternative_email">Contact Email:</label>
                                            <input type="text" name="alternative_email" id="alternative_email" class="form-control" />
                                        </div>

                                        <div class="clear"></div>

                                        <div class="col_half ">
                                            <label for="alternative_workphone">Work Phone:</label>
                                            <input type="text" name="alternative_workphone" id="alternative_workphone" class="form-control" />
                                        </div>

                                        <div class="col_half col_last">
                                            <label for="alternative_mobile">Work Mobile:</label>
                                            <input type="text" name="alternative_mobile" id="alternative_mobile" class="form-control" />
                                        </div>
                    
                                    </div>
                                    
                                </div>
                        
                            </div>
         
                        </div>
                    </div>
                    <?php if ($show_captcha_register) { ?>
                    <div class="col_full">
                        <?php echo $captcha_html_register; ?>
                    </div>
        
                    <div class="clear"></div>
        
                    <div class="col_half">
                        <label>Enter the code exactly as it appears:</label>
                        <?php echo form_label('Confirmation Code', $captcha['id']); ?>
                        <?php echo form_input($captcha); ?>
                        <!--input class="form-control validate[required] text-input" type="text" name="captcha" id="captcha" placeholder="Confirmation Code" / -->
                        <div style="color: red;">
                            <?php echo form_error($captcha['name']); ?>
                        </div>
                    </div>

                    <div class="col_half col_last">
                        <input type="hidden" name="profile" value="user_individual_profile" />
                        <button type="submit" class="button button-3d button-black  pull-right margin-bottom-none" id="register-sol-practiioner-form-submit" name="register-sol-practiioner-form-submit" value="register">Register Now</button>
                    </div>

                    <?php } else { ?>
        
                    <div class="clear"></div>

                    <div class="col_full">
                        <input type="hidden" name="profile" value="user_individual_profile" />
                        <button type="submit" class="button button-3d button-black nomargin" id="register-sol-practiioner-form-submit" name="register-sol-practiioner-form-submit" value="register">Register Now</button>
                    </div>
                    <?php } ?>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    
    

    $(document).ready(function () {

         
        
        $( "#register-sol-practiioner-form #contacts" ).tabs({ 
                show: { 
                    effect: "fade", 
                    duration: 400 
                } 
            }
        );

        
        
         

        // $('#register-sol-practiioner-form #date_of_birth').datepicker({
        //     format: "d MM yyyy",
        //     clearBtn: true,
        //     multidate: false,
        //     autoclose: true
        // });
        
        $('#register-sol-practiioner-form #date_completion').datepicker({
            format: "d MM yyyy",
            clearBtn: true,
            multidate: false,
            autoclose: true
        });

        


        $("#register-sol-practiioner-form").validationEngine('attach', { 
            onValidationComplete: function(form, status){ // when validate is on and the form scan is completed
                if (status == true) { // equiv to success everythings is OK
                    // we disable the default from action to make our own action like alert or other function
//                    form.on('submit', function(e) {
//                        e.preventDefault();
//                    });

                    // your function or your action
                    
                    //$("#email").val($("#username").val() + $("#domains option:selected").val());
                   // alert("Successful! Now submitting");
                    return true;
                    //form.submit();
                } else {
                    // we disable the default from action to make our own action like alert or other function
                    form.on('submit', function(e) {
                        e.preventDefault();
                    });

                    // your function or your action

                    alert("Errors! Stopping Here");
                }
            },
            promptPosition : "topLeft", 
            scroll: false
        });

        $("#register-sol-practiioner-form").bind("jqv.field.result", function (event, field, errorFound, prompText) {
            console.log(errorFound)
        });

        

        
        $("#register-sol-practiioner-form #regulatory_authority_regulatory_authority_id").select2({
            placeholder: "Select Regulatory Authority...",
            allowClear: true
        });

        

        $("#register-sol-practiioner-form #qualifications_qualification_id").select2({
            placeholder: "Select Qualification...",
            allowClear: true
        });


        $("#register-sol-practiioner-form #legal_qualifications_legal_qualifications_id").select2({
            placeholder: "Select Legal Qualification...",
            allowClear: true
        });

    });
</script>