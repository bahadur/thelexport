<link rel="stylesheet" href="<?php echo base_url()?>assets/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/select2/select2.css">
<script src="<?php echo base_url()?>assets/select2/select2.min.js"></script>
<?php
$form_attributes = array('class' => 'nobottommargin');
$login = array(
    'name' => 'login',
    'id' => 'login',
    'value' => set_value('login'),
    'maxlength' => 80,
    'size' => 30,
    'class' => "required form-control input-block-level"
);
if ($login_by_username AND $login_by_email) {
    $login_label = 'Email or login';
} else if ($login_by_username) {
    $login_label = 'Login';
} else {
    $login_label = 'Email';
}
$password = array(
    'name' => 'password',
    'id' => 'login_password',
    'size' => 30,
    'class' => 'required form-control input-block-level'
);
$remember = array(
    'name' => 'remember',
    'id' => 'remember',
    'value' => 1,
    'checked' => set_value('remember'),
    'style' => 'margin:0;padding:0',
);
$captcha = array(
    'name'  => 'captcha',
    'id'    => 'captcha',
    'maxlength' => 8,
);
?>
<section id="page-title">

    <div class="container clearfix">
        <h1>Register</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Authentication</a></li>
            <li class="active">Register </li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section class="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <form id="register-organization-form" name="register-organization-form" class="nobottommargin form-horizontal" action="<?php echo base_url() . 'auth/register/organization' ?>" method="post" >
                <div class="col_two_third col_last nobottommargin">
                
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>Organization Information</h3>
                            </div>
                        </div>
                    <div class="panel-body">
                        
                        <div class="col_full col_last">
                            <label for="organization_name">Name of Organization:</label>
                            <input class="form-control validate[required] text-input" type="text" name="organization_name" id="organization_name" placeholder="Organization Name" data-validate="Organization Name is required" />
                            <div style="color: red;"><?php echo form_error('organization_name'); ?><?php echo isset($errors['organization_name']) ? $errors['organization_name'] : ''; ?></div>
                        </div>
           
                        <div class="clear"></div>
                        
                        <div class="col_half">
                            <label>Type of Organization:</label>
                            <select name="organization_types_organization_type_id" id="organization_types_organization_type_id" class="form-control select2" >
                                <option></option>
                                <optgroup label="Organizations">
                                <?php 
                                foreach ($this->db->get("organization_types")->result() as  $row_organization) {
                                    echo "<option value='$row_organization->organization_type_id'>$row_organization->organization_type</option>";
                                }
                                ?>
                                </optgroup>                                   
                            </select>
                        </div>

                        <div class="col_half col_last">
                            <label>Other</label>
                            <input type="text" name="other_organization" id="other_organization" class="form-control" placeholder="Please Specify" />
                        </div>


                        <div class="col_full col_last">
                            <label>Prosecuting Authority</label>
                            <input type="checkbox" name="prosecuting_authority" id="prosecuting_authority" class="form-control" />
                        </div>
                        
                       

                        <div class="col_one_third">
                            <label>Type</label>



                            <select name="prosecuting_authority_type" id="prosecuting_authority_type" class="form-control select2" disabled="disabled" placeholder="Type of Prosecuting Authority">
                                <option></option>
                                <optgroup label="Prosecuting Authority">
                                <?php 
                                foreach ($this->db->get("prosecuting_authority_types")->result() as  $row_prosecuting) {
                                    echo "<option value='$row_prosecuting->prosecuting_authority_id'>$row_prosecuting->prosecuting_authority_type</option>";
                                }
                                ?>
                                </optgroup>
                                <option value="other">Other</option>    
                            </select>
                        </div>
                        
                        <div class="col_one_third">
                            <label>Other</label>
                            <input type="text" name="other_prosecuting_authority_type" id="other_prosecuting_authority_type" class="form-control"  disabled="disabled" />
                        </div>

                        <div class="col_one_third col_last">
                            <label>Region</label>
                            <input type="text" name="region" id="region" class="form-control"  disabled="disabled" />
                        </div>
                        

                        <div class="clear"></div>
                        
                        <div class="col_one_third">
                            <label>Regulatory Authority:</label>
                            <select name="regulatory_authority_regulatory_authority_id" id="regulatory_authority_regulatory_authority_id" class="form-control select2">
                                <option></option>
                                <optgroup label="Regulatory Authorities">
                                <?php 
                                foreach ($this->db->get("regulatory_authority")->result() as  $row_regulatory_authority) {
                                    echo "<option value='$row_regulatory_authority->regulatory_authority_id'>$row_regulatory_authority->authority</option>";
                                }
                                ?>   
                                </optgroup>
                            </select>
                        </div>

                        <div class="col_one_third">
                            <label>Other</label>
                            <input type="text" name="other_regulatory_authority" id="other_regulatory_authority" class="form-control" placeholder="Please Specify" />
                        </div>
                                
                        <div class="col_one_third col_last">
                            <label>Regulalory Authority No</label>
                            <input type="text" name="regulatory_authority_no" id="regulatory_authority_no" class="form-control" placeholder="Authority No" />
                        </div>

                        <div class="clear"></div>
                                        
                        <div class="col_full col_last">
                            <label>Number of Employees:</label>
                            <input type="text" name="no_employess" id="no_employess" class="form-control" placeholder="Number of Employess" />
                        </div>
                        
                        <div class="clear"></div>
                            
                            <div class="col_full col_last">
                                    <label for="address">Website:</label>
                                <input type="text" name="website" id="website" class="form-control" />
                            </div>
                        <div class="clear"></div>

                        <div class="col_full col_last">
                            <label for="address">Address:</label>
                            <textarea name="address" id="address" class="form-control"></textarea>
                        </div>

                        <div class="clear"></div>
                                        
                        <div class="col_half ">
                            <label for="city">City:</label>
                            <input class="form-control text-input" type="text" name="city" id="city" placeholder="City" />
                        </div>

                        <div class="col_half col_last">
                            <label for="country">Country:</label>
                            <input class="form-control text-input" type="text" name="country" id="country" placeholder="Country" />
                        </div>
                                
                        <div class="col_half ">
                            <label for="contact_no">Contact No:</label>
                            <input class="form-control text-input" type="text" name="contact_no" id="contact_no" placeholder="Phone" />
                        </div>

                        <div class="col_half col_last ">
                            <label for="zip_code">Post/zip Code:</label>
                            <input class="form-control text-input" type="text" name="zip_code" id="zip_code" placeholder="Postal/Zip Code" />
                        </div>

                        <div class="clear"></div>
                        
                    </div>
                
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h3>Account Information</h3>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="col_half">
                        <label for="username">Email:</label>
                        <!--input type="text" class="form-control validate[required]" name="username" id="username" placeholder="Your Email Address" -->
                        
                        <div class="input-group">
                            <input class="form-control validate[required] text-input" type="text" name="username" id="username" placeholder="Your Email Address" data-validate="Email is required" style="text-align: right" /> 
                            <input type="hidden" name="email" id="email" /> 
                            <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" ><span class="seleted_domain">@brickchamber.co.uk</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <select id="domains" class="form-control" multiple="">
                                        <option value="@brickchamber.co.uk" selected="selected">@brickchamber.co.uk</option>
                                        <option value="@gmail.com">@gmail.com</option>
                                        <option value="@yahoo.com">@yahoo.com</option>
                                        <option value="@hotmail.com">@hotmail.com</option>
                                    </select>
                                </ul>
                            </div>
                        </div>

                        <div style="color: red;"><?php echo form_error('username'); ?></div>
                    </div>

                    <!--div class="col_half col_last">
                    <label for="user">Email:</label>
                    <input type="text" class="form-control validate[required]"  name="email" id="email" placeholder="Your Email Address" >
                    <div style="color: red;"><?php //echo form_error('email'); ?></div>
                    </div-->
                    <div class="col_half col_last">
                        <div class="style-msg infomsg">
                            <div class="sb-msg">
                                <i class="icon-info-sign"></i>
                                <strong>Note:</strong>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. 
                            </div>
                        </div>
                    </div>
                            
                    <div class="clear"></div>

                    <div class="col_half ">
                        <label for="password">Password:</label>
                        <input class="form-control validate[required] text-input" type="password" name="password" id="password" placeholder="Password" />
                        <div style="color: red;"><?php echo form_error('password'); ?></div>
                    </div>
                    
                    <div class="col_half col_last">
                        <label for="confirm_password">Confirm Password:</label>
                        <input class="form-control validate[required,equals[password]] text-input" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" />
                        <div class="danger"><?php echo form_error('confirm_password'); ?></div>
                    </div>

                </div>
                    
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h3>Optional</h3>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="tabs tabs-bordered clearfix" id="contacts">
                        <ul class="tab-nav clearfix">
                            <li><a href="#primary-contacts">Primary Contact</a></li>
                            <li><a href="#secondary-contacts">Secondary Contact</a></li>
                            <li><a href="#alternative-contacts">Alternative Contact (Optional)</a></li>
                        </ul>
                        <div class="tab-container">
                            <div class="tab-content clearfix" id="primary-contacts">
                                <h4>Primary Contact Person</h4>
                    
                                <div class="col_one_third">
                                    <label for="primary_name">Name:</label>
                                    <input type="text" name="primary_name" id="primary_name" class="form-control text-input" />
                                </div>
                                
                                <div class="col_one_third ">
                                    <label for="primary_position">Position:</label>
                                    <input type="text" name="primary_position" id="primary_position" class="form-control" />
                                </div>
                                
                                <div class="col_one_third col_last">
                                    <label for="primary_email">Contact Email:</label>
                                    <input type="text" name="primary_email" id="primary_email" class="form-control" />
                                </div>

                                <div class="clear"></div>

                                <div class="col_half ">
                                    <label for="primary_workphone">Work Phone:</label>
                                    <input type="text" name="primary_workphone" id="primary_workphone" class="form-control" />
                                </div>

                                <div class="col_half col_last">
                                    <label for="primary_mobile">Work Mobile:</label>
                                    <input type="text" name="primary_mobile" id="primary_mobile" class="form-control" />
                                </div>
                            </div>
                            
                            <div class="tab-content clearfix" id="secondary-contacts">
                                <h4>Secondary Contact Person</h4>
                                    
                                    <div class="col_one_third">
                                        <label for="secondary_name">Name:</label>
                                        <input type="text" name="secondary_name" id="secondary_name" class="form-control text-input" />
                                    </div>
                                    
                                    <div class="col_one_third ">
                                        <label for="secondary_position">Position:</label>
                                        <input type="text" name="secondary_position" id="secondary_position" class="form-control" />
                                    </div>
                                
                                    <div class="col_one_third col_last">
                                        <label for="secondary_email">Contact Email:</label>
                                        <input type="text" name="secondary_email" id="secondary_email" class="form-control" />
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_half ">
                                        <label for="secondary_workphone">Work Phone:</label>
                                        <input type="text" name="secondary_workphone" id="secondary_workphone" class="form-control" />
                                    </div>

                                    <div class="col_half col_last">
                                        <label for="secondary_mobile">Work Mobile:</label>
                                        <input type="text" name="secondary_mobile" id="secondary_mobile" class="form-control" />
                                    </div>
                        
                                </div>
                                
                                <div class="tab-content clearfix" id="alternative-contacts">
                                    <h4>Alternative Contact Person</h4>
                    
                                        <div class="col_one_third">
                                            <label for="alternative_name">Name:</label>
                                            <input type="text" name="alternative_name" id="alternative_name" class="form-control text-input" />
                                        </div>
                                
                                        <div class="col_one_third ">
                                            <label for="alternative_position">Position:</label>
                                            <input type="text" name="alternative_position" id="alternative_position" class="form-control" />
                                        </div>
                                            
                                        <div class="col_one_third col_last">
                                            <label for="alternative_email">Contact Email:</label>
                                            <input type="text" name="alternative_email" id="alternative_email" class="form-control" />
                                        </div>

                                        <div class="clear"></div>

                                        <div class="col_half ">
                                            <label for="alternative_workphone">Work Phone:</label>
                                            <input type="text" name="alternative_workphone" id="alternative_workphone" class="form-control" />
                                        </div>

                                        <div class="col_half col_last">
                                            <label for="alternative_mobile">Work Mobile:</label>
                                            <input type="text" name="alternative_mobile" id="alternative_mobile" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
 
                        </div>
                    </div>


                    <?php if ($show_captcha_register) { ?>

                    <div class="col_full">
                        <?php echo $captcha_html_register; ?>
                    </div>
                    
                    <div class="clear"></div>
                    
                    <div class="col_half">
                        <label>Enter the code exactly as it appears:</label>
                        <?php echo form_label('Confirmation Code', $captcha['id']); ?>
                        <?php echo form_input($captcha); ?>
                        <!--input class="form-control validate[required] text-input" type="text" name="captcha" id="captcha" placeholder="Confirmation Code" / -->
                        <div style="color: red;">
                            <?php echo form_error($captcha['name']); ?>
                        </div>
                    </div>

                    <div class="col_half col_last">
                        <input type="hidden" name="profile" value="organization_profile" />
                        <button type="submit" class="button button-3d button-black  pull-right margin-bottom-none" id="register-organization-form-submit" name="register-organization-form-submit" value="register">Register Now</button>
                    </div>

                    <?php } else { ?>
                    <div class="clear"></div>

                    <div class="col_full">
                        <input type="hidden" name="profile" value="organization_profile" />
                        <button type="submit" class="button button-3d button-black nomargin" id="register-organization-form-submit" name="register-organization-form-submit" value="register">Register Now</button>
                    </div>
                    <?php } ?>
                </div>
            
            </form>
        </div>
    </div>
</section>
<script>
    
   

    $(document).ready(function () {

         
        
        $( "#register-organization-form #contacts" ).tabs({ 
                show: { 
                    effect: "fade", 
                    duration: 400 
                } 
            }
        );

        
        
         

        // $('#register-organization-form #date_of_birth').datepicker({
        //     format: "d MM yyyy",
        //     clearBtn: true,
        //     multidate: false,
        //     autoclose: true
        // });
        
        // $('#register-organization-form #date_completion').datepicker({
        //     format: "d MM yyyy",
        //     clearBtn: true,
        //     multidate: false,
        //     autoclose: true
        // });

        


        $("#register-organization-form").validationEngine('attach', { 
            onValidationComplete: function(form, status){ // when validate is on and the form scan is completed
                if (status == true) { // equiv to success everythings is OK
                    // we disable the default from action to make our own action like alert or other function
//                    form.on('submit', function(e) {
//                        e.preventDefault();
//                    });

                    // your function or your action
                    
                    $("#email").val($("#username").val() + $("#domains option:selected").val());
                    
                    //alert("Successful! Now submitting");
                    return true;
                    //form.submit();
                } else {
                    // we disable the default from action to make our own action like alert or other function
                    form.on('submit', function(e) {
                        e.preventDefault();
                    });

                    // your function or your action

                    alert("Errors! Stopping Here");
                }
            },
            promptPosition : "topLeft", 
            scroll: false
        });

        $("#register-organization-form").bind("jqv.field.result", function (event, field, errorFound, prompText) {
            console.log(errorFound)
        });

        $("#domains").change(function () {
            $("#domains option:selected").each(function () {
                $('button.dropdown-toggle span.seleted_domain').html($(this).html());
            });
        });

        $("#organization_types_organization_type_id").select2({
             allowClear:true,
             placeholder:"Select Organization Type..."
        });
        $("#regulatory_authority_regulatory_authority_id").select2({
            placeholder: "Select Regulatory Authority...",
            allowClear: true
        });

        

        $("#qualifications_qualification_id").select2({
            placeholder: "Select Qualification...",
            allowClear: true
        });


        $("#legal_qualifications_legal_qualifications_id").select2({
            placeholder: "Select Legal Qualification...",
            allowClear: true
        });


        $("#prosecuting_authority_type").select2({
            placeholder: "Type of Prosecuting Authority",
            allowClear: true
        });
        
        $("#prosecuting_authority_type").on("change", function (e) { 
            if($(this).val() == "other"){
                $("#other_prosecuting_authority_type").prop("disabled", false);
            } else {
                $("#other_prosecuting_authority_type").prop("disabled", true);
            }

        });


        $('#prosecuting_authority').change(function(){
            $("#prosecuting_authority_type").prop("disabled", !$(this).is(':checked'));
            $("#region").prop("disabled", !$(this).is(':checked'));
        });

    });
</script>