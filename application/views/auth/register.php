<!-- Page Title
        ============================================= -->
<style>

    #register-form{
        width:80%;
        margin: 50px auto;
       // text-align: center;
        position: relative;
    }

    #register-form fieldset{
        border: 0 none;
        border-radius: 3px;
        box-shadow: 0 0 15px 1px rgba(0,0,0,0,4);
        padding: 20px 30px;
        box-sizing: border-box;
        width: 80%;
        //margin: 0 10%;


    }

    /*
    #register-form input, #register-form textarea{
       // padding: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
        //margin-bottom: 10px;
        width: 100%;
        color: #2c3e50;
        font-size: 13px;
    }*/

    
    

    .fs-title{
        font-size: 15px;
        text-transform: uppercase;
        color: #2c3e50;
        margin-bottom: 10px;
    }

    .fs-subtitle{
        font-weight: normal;
        font-size: 13px;

        color: #666;
        margin-bottom: 20px;
    }
    

   

   
</style>
<section id="page-title">

    <div class="container clearfix">
        <h1>Register Network</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Network</a></li>
            <li class="active">Register</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">



            <div class="col_full nobottommargin">


                <h3>Join Our Network</h3>

                <p>Our network combines legal and non-legal talent so that our clients can receive tailored made solutions as and when required.</p>

                <form id="register-form" name="register-form" class="nobottommargin" action="<?php echo base_url().$this->uri->uri_string()?>" method="post" >

                    <fieldset>
                    <div class="form-group">
                        
                        <div class="input-group">
                            <i class="input-group-addon">@</i>
                            <input class="form-control validate[required,custom[email]] text-input" type="text" name="email" id="email" placeholder="Email Address" data-validate="Email is required" />
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        
                        <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input class="form-control validate[required,custom[onlyLetterNumber],maxSize[20],ajax[ajaxUserCallPhp]] text-input" type="text" name="username" id="username" placeholder="username Address" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        
                        <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input class="form-control validate[required] text-input" type="password" name="password" id="password" placeholder="Password" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        
                        <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input class="form-control validate[required,equals[password]] text-input" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" />
                        </div>
                    </div>
                        
                    <?php if ($captcha_registration) { 
                        if ($use_recaptcha) { ?>
                    <div class="form-group">
                        
                        <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <div id="recaptcha_image"></div>
                        <div>
                            <a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
                            <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                            <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
                        </div>
                        <div>
                            <div class="recaptcha_only_if_image">Enter the words above</div>
                            <div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
                        </div>
                            <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
                            <?php echo form_error('recaptcha_response_field'); ?>
                            <?php echo $recaptcha_html; ?>
                        </div>
                    </div>
                    <?php } else { ?>
                        <div class="form-group">
                            <div class="input-group">
                                
                                <div class="col-md-4">
                                <?php echo $captcha_html; ?>
                                </div>
                                <div class="col-md-8">
                                    <label>Enter the code exactly as it appears:</label>
                                    <input class="form-control validate[required] text-input" type="text" name="captcha" id="captcha" placeholder="Confirmation Code" />
                                    <?php echo form_error('captcha'); ?>
                                </div>
                            </div>
                        </div>
                    <?php } }?>
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </fieldset>

                    

                    



                </form>


            </div>

        </div>

    </div>

</section><!-- #content end -->

<script>
    $(document).ready(function () {
       
        
        $("#register-form").validationEngine();

        $("#register-form").bind("jqv.field.result", function(event, field, errorFound, prompText){ console.log(errorFound) })
        
        
//        $('#register-form').validate({
//            rules: {
//                email: {
//                    
//                    required: true,
//                    email: true
//                    
//                },
//                username: {
//                    minlength: 3,
//                    maxlength: 15,
//                    required: true
//                },
//                password: {
//                    
//                    required: true,
//                    password: true
//                },
//                re_password: {
//                    minlength: 3,
//                    maxlength: 15,
//                    required: true
//                }
//            },
//            highlight: function (element) {
//                $(element).closest('.form-group').addClass('has-error');
//            },
//            unhighlight: function (element) {
//                $(element).closest('.form-group').removeClass('has-error');
//            },
//            errorElement: 'span',
//            errorClass: 'help-block',
//            errorPlacement: function (error, element) {
//                if (element.parent('.input-group').length) {
//                    error.insertAfter(element.parent());
//                } else {
//                    error.insertAfter(element);
//                }
//            }
//        });
    });
</script>