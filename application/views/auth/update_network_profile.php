<style>

    .profile 
    {
        min-height: 355px;
        display: inline-block;
    }
    figcaption.ratings
    {
        margin-top:20px;
    }
    figcaption.ratings a
    {
        color:#f1c40f;
        font-size:11px;
    }
    figcaption.ratings a:hover
    {
        color:#f39c12;
        text-decoration:none;
    }
    .divider 
    {
        border-top:1px solid rgba(0,0,0,0.1);
    }
    
    
    
.fa-refresh-animate {
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
}

@-webkit-keyframes spin2 {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}
    
    
   

</style>
<section id="page-title">

    <div class="container clearfix">
        <h1>Create Profile</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Auth</a></li>
            <li class="active">Create Profile</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section class="content">
    <div class="content-wrap">
        <div class="container clearfix">
            
            <div class="col_two_third">
                <?php if(!empty($message)){ ?>
                <div class="style-msg infomsg">
                    <div class="sb-msg">
                    <i class="icon-info-sign"></i>
                    <strong>Complete your profile! </strong><?php echo $message?>
                    </div>
                </div> 
                <?php } ?>
                <div class="panel panel-default marginbot40">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Basic Information
                        </div>
                    </div>
                    <div class="panel-body">
                        <form id="profile-form" class="marginbot40" role="form">
                             
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <div class="row">
                            <div class="col-md-4"><input type="text" name="f_name" id="f_name" class="form-control  validate[required] text-input" value="<?php echo $profile_data->f_name ?>" placeholder="First Name" /></div>
                            <div class="col-md-4"><input type="text" name="m_name" id="m_name" class="form-control" value="<?php echo $profile_data->m_name ?>" placeholder="Middle Name"  /></div>
                            <div class="col-md-4"><input type="text" name="l_name" id="l_name" class="form-control  validate[required] text-input" value="<?php echo $profile_data->l_name ?>" placeholder="Last Name" /></div>
                            
                        </div>
                        
                    </div>
                
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" id="phone" class="form-control  validate[required] text-input" value="<?php echo $profile_data->phone ?>" placeholder="Year of Call" />
                    </div>
                
               
                    <div class="form-group">
                        <label for="year_of_call">Year of call</label>
                        <input type="text" name="year_of_call" id="year_of_call" class="form-control  validate[required] text-input" value="<?php echo $profile_data->year_of_call ?>" placeholder="Year of Call" />
                    </div>
                            
               
                
               
                    <div class="form-group">
                        <label for="clerk">Clerk</label>
                        <input type="text" name="clerk" id="clerk" class="form-control  validate[required] text-input" value="<?php echo $profile_data->clerk ?>" placeholder="Clerk" />
                    </div>
                
                
               
                    <div class="form-group">
                        <label for="silk">Silk</label>
                        <input type="text" name="silk" id="silk" class="form-control  validate[required] text-input" value="<?php echo $profile_data->silk ?>" placeholder="Silk" />
                    </div>
               
                
                
                
                    <div class="form-group">
                        <label for="description">Summary</label>
                        <textarea name="description" id="description" class="form-control"><?php echo $profile_data->description ?>"</textarea>
                        
                    </div>
                
                
                            
                
                            <button type="submit" name="update" id="upadate" class="btn btn-lg btn-primary pull-right"><span class="fa fa-refresh"></span> Update</button>
                        </form>
                    </div>
                </div>
                     
                
                
               
                
                
                
             </div>
            <div class="col_one_third col_last">
                Right Col
            </div>
                
            
        </div>
    </div>
</section>

<script type="text/javascript">

    jQuery(document).ready(function ($){
        
        $("#profile-form").validationEngine('attach', {

            onValidationComplete: function(form, status){
              
            if(status){
                
                $.ajax({
                    url:'<?php echo base_url()?>ajax/update_profile',
                    type:'POST',
                    data:$("#profile-form").serialize(),
                    
                    success:function(data){
                        
                        $( "<div class='style-msg successmsg'><div class='sb-msg'><i class='icon-thumbs-up'></i>Update successfull</div></div>" ).insertBefore( "#upadate" );
                    }
                });
                 
            }
              
            }  
        });
        
        
        //$("#profile-form").validationEngine();

        //$("#profile-form").bind("jqv.field.result", function(event, field, errorFound, prompText){ console.log(errorFound) })
       
       $('#update').click(function(){
           $("#profile-form").submit();
           
       });
       
       
       
       
    });
    
</script>
    