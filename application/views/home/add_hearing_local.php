<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Login</a></li>
            <li class="active">username</li>
            <li class="active">Add Hearing</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<?php 

//if($this->tank_auth->getUserType() == "user_member_profile" ){ 
if( !$this->tank_auth->hearingPermission(1)){

?>

<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<p>You don't have permission to add Hearing. Please contact to your organization admin.</p>
		</div>
	</div>
</section>



<?php  } else { ?>

<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="col_two_third">
				<form class="form-horizontal nobottommargin" method="post">
						
					
					

					<div class="form-group">
					
						<label for="hearing_date" class="col-sm-3 control-label">Hearing Date:</label>
							
						<div class="input-group input-append date col-sm-5" id="datepicker">
							<input  type="text" class="form-control" name="hearing_date"  value="<?php echo $date?>" />
							<span class="add-on input-group-addon">
				            	<span class="icon-calendar"></span>
				            </span>
						</div>
			
					</div>

					<div class="form-group">
			
						<label for="hearing_time" class="col-sm-3 control-label">Hearing Time:</label>
			                
			            <div class='input-group input-append date col-sm-5' id='timepicker'>
			            	<input type='text' class="form-control" name="hearing_time"  value="<?php echo $date?>" />
			                <span class="add-on input-group-addon">
			                	<span class="icon-time"></span>
			                </span>
			            </div>
			
			        </div>

						
					


					<div class="col_half col_last">
						<label class="reference_no">Reference No:</label>
						<?php $reference_no = $this->db
												->order_by('hearing_id','desc')
												->limit(1)
												->get('hearing')->row(); 
						if(empty($reference_no)){
							
							$ref_no = 1;
						} else {
							$ref_no = ($reference_no->hearing_id) + 1;
						}
							
							
						?>
						<input type="text" class="form-control" name="hearing_id" value="<?php echo sprintf("%04d", $ref_no)?>" disabled="disabled" />
					</div>

					
					
					

					<div class="clear"></div>
					
					<div class="col_half">
						<label class="court_court_id">Name of Court:</label>

						<select class="form-control" name="court_court_id">
							<?php 

								foreach($this->db->get('court')->result() as $court){ 
									echo "<option value='$court->court_id'>$court->court_name</option>";
								}


							?>
							
						</select>

						
					</div>

					<!--div class="col_half col_last">
						<label class="court_court_type_court_type_id">Type of Court:</label>
						
						<select class="form-control" name="court_court_type_court_type_id">
							<?php 

								// foreach($this->db->get('court_type')->result() as $court_type){ 
								// 	echo "<option value='$court_type->court_type_id'>$court_type->type</option>";
								// }


							?>
							
						</select>
						
					</div -->

					<div class="clear"></div>

					<div class="col_half">
						<label class="hearing_type_hearing_type_id">Type of Hearing:</label>
						<select class="form-control" name="hearing_type_hearing_type_id">
							<?php 

								foreach($this->db->get('hearing_type')->result() as $hearing_type){ 
									echo "<option value='$hearing_type->hearing_type_id'>$hearing_type->hearing_type</option>";
								}


							?>
							
						</select>

						
					</div>

					<div class="col_half col_last">
						<label class="estimated_time_hearing">Estimated Time of hearing:</label>
						<input type="text" class="form-control" name="estimated_time_hearing"  />
					</div>

					<div class="clear"></div>

					<div class="col_half">
						<label class="law_area_law_area_id">Area of Law:</label>
						<select class="form-control" name="law_area_law_area_id">
							<?php 

								foreach($this->db->get('law_area')->result() as $law_area){ 
									echo "<option value='$law_area->law_area_id'>$law_area->area_of_law</option>";
								}


							?>
							
						</select>
						
					</div>

					
					<div class="col_half col_last">
						<label class="track_track_id">Track:</label>

						<select class="form-control" name="track_track_id">
							<?php 

								foreach($this->db->get('track')->result() as $track){ 
									echo "<option value='$track->track_id'>$track->track</option>";
								}


							?>
							
						</select>
						
					</div>

					<div class="clear"></div>

					<div class="col_full col_last">
						<label class="comments">Comments:</label>
						<textarea class="form-control" name="comments"></textarea>
						
					</div>
					<div class="clear"></div>
					<div class="col_full">
                    	<button type="submit" class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Add Hearing</button>
                    </div>


				</form>		


			</div>

			<div class="col_one_third col_last">
				
				
			</div>
		</div>
	</div>
</section>
<?php } ?>
<script type="text/javascript">
  $(function() {

    $('#datepicker').datetimepicker({
    	defaultDate: "11/1/2013",
    	disabledDates: [
                    moment("5/25/2015"),
                    new Date(2015, 6 - 1, 21),
                    "05/23/2015 00:53"
                    ],
      	language: 'pt-BR'
    });
	
	$('#timepicker').datetimepicker({
    	format: 'LT'
    });
    
  });
</script>