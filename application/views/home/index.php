<style type="text/css">
.hover-end{padding:0;margin:0;font-size:75%;text-align:center;position:absolute;bottom:0;width:100%;opacity:.8}

.custom_pointer {
    cursor:pointer;
    background-color:#F2FAF4;
    opacity: 0.3;
    
}

.fc-today .fc-unthemed {
    background: rgba(252, 248, 227, 0.22) !important;
}

.fc-content-skeleton {
    z-index: 0 !important;
}
</style>

<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Login</a></li>
            <li class="active">username</li>
            <li class="active">Calendar view</li>
        </ol>
    </div>

</section><!-- #page-title end -->
<section id="content">

    <!--div class="content-wrap">
        <div class="parallax header-stick bottommargin-lg light" style="padding: 60px 0; background-image: url('<?php echo base_url() ?>assets/images/parallax/calendar.jpg'); height: auto;" data-stellar-background-ratio="0.5">
            <div class="container clearfix">

                <div class="events-calendar">
                    <div class="events-calendar-header clearfix">
                        <h2 id="add-hearing" style="cursor:pointer">Add</h2>
                        <h3 class="calendar-month-year">
                            <span id="calendar-month" class="calendar-month"></span>
                            <span id="calendar-year" class="calendar-year"></span>
                            <nav>
                                <span id="calendar-prev" class="calendar-prev"><i class="icon-chevron-left"></i></span>
                                <span id="calendar-next" class="calendar-next"><i class="icon-chevron-right"></i></span>
                                <span id="calendar-current" class="calendar-current" title="Got to current date"><i class="icon-reload"></i></span>
                            </nav>
                        </h3>
                    </div>
                    <div id="calendar" class="fc-calendar-container"></div>
                </div >
                
            </div>
        </div>
    </div-->

    <div class="content-wrap">
        <div class="container clearfix">

            

            <div class="clear"></div>
            
            <div class="col_full col_last nobottommargin">
                
                <div class="col_ful col_last">
                   
                        <button class="btn btn-success" id="add-hearing" >Add</button>
                   
                </div>
                
                <div class="divider divider-center"></div>

                <div id="calendar"></div>
            

            </div>
            <div class="divider divider-center"><i class="icon-circle-blank"></i></div>
              <div class="col_full col_last nobottommargin">

                <div class="row well">
                <h3>Filters</h3>
                <form method="post" action="<?php echo base_url()?>home/index">
                    <div class="form-group col-md-6">

                        <div class="col-md-4">
                            <label>By Court</label>
                        </div>
                        <div class="col-md-8">

                            <select class="form-control" name="court_id">
                                <option value="0" selected >[ Select Court ]</option>
                                <?php 
                                foreach ($this->db->get("court")->result() as $value) { ?>
                                    <option value="<?php echo $value->court_id?>" <?php echo ($this->input->post("court_id") && $this->input->post("court_id") != 0)?'selected':'' ?> ><?php echo $value->court_name?></option>
                                    
                                <?php } ?>
                                
                            </select>
                        </div>



                    </div>

                   <div class="form-group col-md-6">

                        <div class="col-md-4">
                            <label>By Court Type</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control" name="court_type_id">
                                <option value="0" selected >[ Select Court Type]</option>
                                <?php 
                                foreach ($this->db->get("court_type")->result() as $value) { ?>
                                    <option value="<?php echo $value->court_type_id?>" <?php echo ($this->input->post("court_type_id") && $this->input->post("court_type_id") != 0)?'selected':'' ?> ><?php echo $value->type?></option>
                                    
                                <?php } ?>
                            </select>
                        </div>



                    </div>


                    <div class="form-group col-md-6">

                        <div class="col-md-4">
                            <label>By Hearing Type</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control" name="hearing_type_id">
                               <option value="0" selected >[ Select Hearing Type]</option>
                                <?php 
                                foreach ($this->db->get("hearing_type")->result() as $value) { ?>
                                    <option value="<?php echo $value->hearing_type_id?>" <?php echo ($this->input->post("hearing_type_id") && $this->input->post("hearing_type_id") != 0)?'selected':'' ?> ><?php echo $value->hearing_type?></option>
                                    
                                <?php } ?>
                            </select>
                        </div>



                    </div>
                    
                   

                    <div class="form-group col-md-6">

                        <div class="col-md-4">
                            <label>Date</label>
                        </div>
                        <div class="col-md-8">


                            <input type="text" name="hearing_date" id="hearing_date" class="form-control" value="<?php echo ($this->input->post('hearing_date') && $this->input->post('hearing_date') != '')?$this->input->post("hearing_date"):''?>" />

                            <!-- select class="form-control" name="law_area_id">
                               <option value="0" selected >[ Select Area of Law]</option>
                                <?php 
                               // foreach ($this->db->get("law_area")->result() as $value) { ?>
                                    <option value="<?php //echo $value->law_area_id?>" <?php //echo ($this->input->post("law_area_id") && $this->input->post("law_area_id") != 0)?'selected':'' ?> ><?php //echo $value->area_of_law?></option>
                                    
                                <?php //} ?>
                            </select -->
                        </div>



                    </div>

                    
                    <div class="form-group col-md-6">

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Go ... </button>
                            <button type="button" class="btn btn-success" id="clear_filter">Clear </button>
                        </div>




                    </div>

                </form>
                </div>

            </div>      

                    
    </div>


    </div>
</div>

</section>

<script type="text/javascript">
$(document).ready(function() {


    $("#clear_filter").click(function(){

        document.location = '<?php echo base_url()?>';
    });
    $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultDate: '<?php echo date("Y-m-d")?>',
            businessHours: false, 
            editable: false,
            eventLimit: true, 

            events: <?php echo json_encode($hearings);?>,

            dayClick: function(date, jsEvent, view) {

                    

               
                var array = $('#calendar').fullCalendar('clientEvents', function(event){ 
                
            
                    if(event.start.format('YYYY-MM-DD') == date.format('YYYY-MM-DD')){
                    
                        document.location = '<?php echo base_url()?>home/listview/'+date.format('YYYY-MM-DD');
                    } else {
                        document.location = '<?php echo base_url()?>home/add_hearing/'+date.format('YYYY-MM-DD');
                    }
            
            
            

                });
              

            },
            dayRender: function(date, cell) {
               var currentDate = new Date();
                
               var ct =   currentDate.getTime();
               ct = ct - (60 * 60 * 60 * 60  * 6);
               
                   
                if(date >= ct) {
                    // alert("Date: "+date);
                    // alert("Current Date: "+ct);

                   
                    
                    cell.addClass('custom_pointer');
                     

                    

               }
            }

               

            });



    $("#add-hearing").click(function(){
        document.location = '<?php echo base_url()?>home/add_hearing';
    });

    $("#hearing_date").datepicker({
        format: 'yyyy-mm-dd'
    });
});




</script>

