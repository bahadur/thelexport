
<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>
        
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Messages</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            
            <div id="posts" class="post-grid post-masonry post-timeline grid-2 clearfix" >
                <div class="timeline-border"></div>

                <?php 
                foreach ($hearing_messages as $key => $data_array) {
                ?>
                <div class="entry entry-date-section notopmargin"><span><?php echo $key?></span></div>
                
                <?php 
                $i=0;
                foreach ( $data_array as  $row) { 
                    
                ?>
                <div class="entry clearfix <?php echo (($i++ % 2) != 0 )?'alt':''?>">
                   <div class="entry-timeline" style="display:block">
                        <div class="timeline-divider"></div>
                    </div>
                    <div class="entry-image">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                
                                <h3><?php echo sprintf("%04d", $row->hearing_id)?></h3>
                                <a href="<?php echo base_url()?>home/messages/<?php echo $row->hearing_id?>">Messages <span class="badge"><?php echo $row->num_msg?></span></a>
                                <div> <?php echo $row->court_name?>
                                <?php echo $row->type?>
                                </div>

                                
                                <ul class="entry-meta clearfix">
                                    <li><i class="icon-calendar3"></i> <?php   echo $row->hearing_date?></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                        
                </div>
                 <?php } ?>
                
                
            
                <?php } ?>
                </div>
        </div>

    </div>
    
</section>


