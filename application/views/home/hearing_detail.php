<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Login</a></li>
            <li class="active">username</li>
            <li class="active">Hearing Detail</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section>
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col_two_third">
                
                     
                
                <div class="row">
                    <div class="col-lg-4  bottom">
                        <h3>Hearing Starts in:</h3>
                    </div>
                    <div class="col-lg-8">
                        <div id="countdown-example" class="countdown countdown-large"></div>
                    </div>
                </div>

                <div class="divider">
                        <i class="fa fa-circle"></i>
                </div>
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="fancy-title">
                            <h2>Details</h2>
                            <ul class="list-group  bottommargin">
                                <li class="list-group-item"><strong>Reference No:</strong><div class="pull-right"><?php echo sprintf("%04d", $hearings[0]->hearing_id)?></div></li>
                                <!--li><span>Date of Hearing:</span><div class="pull-right"><?php //echo $hearings[0]->hearing_date?></div></li -->
                                <li class="list-group-item"><strong>Hearing Type:</strong><div class="pull-right"><?php echo $hearings[0]->hearing_type?></div></li>
                                <li class="list-group-item"><strong>Time Estimated:</strong><div class="pull-right"><?php echo $hearings[0]->estimated_time_hearing?></div></li>
                                <li class="list-group-item"><strong>Type of Court:</strong><div class="pull-right"><?php echo $hearings[0]->type?></div></li>
                                <!-- li><span>Name of Court:</span><div class="pull-right"><?php //echo $hearings[0]->court_name?></div></li -->
                                <li class="list-group-item"><strong>Area of Law:</strong><div class="pull-right"><?php echo $hearings[0]->area_of_law?></div></li>
                                <li class="list-group-item"><strong>Track:</strong><div class="pull-right"><?php echo $hearings[0]->track?></div></li>
                                <li class="list-group-item"><strong>Comments:</strong><div class="pull-right"><?php echo $hearings[0]->comments?></div></li>
                                
                            </ul>
                        </div>
                    </div>
                    
                    

                    <div class="col-md-6">
                        <div class="fancy-title">
                            <h2>Hearing info</h2>
                            <ul class="list-group bottommargin">
                                <li class="list-group-item">
                                    <i class="fa fa-clock-o"></i>
                                    <?php echo $hearings[0]->hearing_datetime?>
                                </li>
                                
                                <li class="list-group-item">
                                    <i class="fa fa-university"></i>
                                    <?php echo $hearings[0]->court_name?>
                                </li>
                                <li class="list-group-item">
                                    <i class="fa fa-map-marker"></i>
                                    <address>
                                        <strong><?php echo $hearings[0]->court_name?></strong><br />
                                        <?php echo $hearings[0]->address?><br />
                                        <?php echo $hearings[0]->location?>
                                    </address>
                                </li>
                            </ul>
                            
                        </div>
                    </div>


                </div>
                <?php if($this->tank_auth->get_user_id() != $hearings[0]->users_user_id ){ ?>
                <div class="row">
                    
                <?php 

                    $this->db->where('users_user_id',$this->tank_auth->get_user_id());
                    $this->db->where('hearing_hearing_id',$hearings[0]->hearing_id);
                    $query = $this->db->get('hearing_msg');

                    if($query->num_rows() > 0){ ?>
                        <div class="col-md-12">
                            <div class="style-msg infomsg">
                                <div class="sb-msg">
                                    <i class="icon-info-sign"></i>
                                        <strong>Message:</strong>You already sent message.
                                   
                                </div>
                            </div>
                        </div>
                    <?php } else {?>

                    <div class="col-md-12">
                        <?php if( $this->tank_auth->hearingPermission(2)){ ?>
                        <button type="button" class="button button-3d button-black nomargin pull-right" id="send-msg" name="send-msg"  >Send Message</button>
                        <?php } else { ?>
                        <div class="text-success">You don't have permission to send message. Please contact you organization administrator</div>
                        <?php } ?>
                    </div>

                
                <?php }?>
                </div> 
                <?php } ?>
                     
                
            </div>
            <div class="col_one_third col_last ">
                
                    
                <div class="row">
                    <div class="col-md-12">
                        <div class="fancy-title title-bottom-border">
                            <h4>Available Hearing At <span><?php echo $hearings[0]->court_name?></span></h4>
                        </div>
                        
                    </div>

                    <div class="col-md-12">
                        <div class="fancy-title title-bottom-border">
                            <h4>Available Hearing on  <span><?php echo $hearings[0]->hearing_date?> <span> </h4>
                        </div>
                        
                    </div>
                </div>    
                    
                
            </div>
            
            
        </div>
    </div>
</section>




<script>
	jQuery(document).ready( function($){
		var date = '<?php echo $hearings[0]->hearing_js?>';
        var dateSplit = date.split(',');
        var newDate = new Date(dateSplit[0].trim(), (dateSplit[1].trim())-1, dateSplit[2].trim(),dateSplit[3].trim(),dateSplit[4].trim(),dateSplit[5].trim());
                
		$('#countdown-example').countdown({
            until: newDate,
        });


        $("#send-msg").click(function(){


            $.ajax({
                url: '<?php echo base_url()?>ajax/send_message',
                data: {
                    hearing_hearing_id: <?php echo $hearings[0]->hearing_id?>,
                    users_user_id: '<?php echo $this->tank_auth->get_user_id()?>'

                },
                type: 'post',
                success: function(res){
                    if(res.status == 1){
                        alert("Message sent successfully");
                        $("#send-msg").hide();

                    }

                }
            })

        });

	});
</script>      