<!-- Page Title
        ============================================= -->
<section id="page-title">

    <div class="container clearfix">
        <h1>Our Functions</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Our Functions</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section id="content">
    <div class="content-wrap nopadding">
        <div class="section notopborder nomargin">
            <div class="container clearfix">
                
                <p class="lead">
                    In todays global village firms need to redesign their business model to increase efficiency while reducing time and resources consumed. We provide round the clock comprehensive services tailored to your individual needs so that you can concentrate on being all that you can be while we take care of the rest. 
                </p>
                
                
                <div class="divider divider-center">
                    <i class="icon-cloud"></i>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>
                            DOCUMENTS SERVICES
                        </h2>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h3>E-DOCUMENT REVIEW</h3>
                        <p>Our proprietary software provides you with review and analysis in a secure hosted environment. By combining the latest technology with our core strengths we provide you with a cost effective and efficient service </p>
                    </div>
                    <div class="col-md-4">
                        <h3>DOCUMENT REVIEW</h3>
                        <p>Our business model and experience enable us to provide you with a comprehensive document review service at the highest attainable standard  </p>
                    </div>
                    <div class="col-md-4">
                        <h3>TRANSCRIPTION</h3>
                        <p>Our team realizes that today recording the spoken word is easier than ever but transcribing it is still time consuming. Therefore our transcription service allows you to outsource your transcription needs to us while you focus on being all that you can be  </p>
                    </div>
                   
                </div>
                
                <div class="divider">
                    <i class="icon-circle"></i>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <h2>
                            PROJECT SERVICES
                        </h2>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <h3>LEGAL RESEARCH &amp; DRAFTING</h3>
                        <p>Our associates work with leading industry experts to provide you with quick and up to date research on cases and legislation whilst utilizing the latest online tools to research and identify data  </p>
                    </div>
                    <div class="col-md-4">
                        <h3>CASE SUPPORT</h3>
                        <p>Our team understands that litigation support services are an essential component of the litigation process. Therefore we work to ensure that all your litigation support needs are met  </p>
                    </div>
                    <div class="col-md-4">
                        <h3>PROJECT CONTRACT MANAGEMENT</h3>
                        <p>Our world economy has become increasingly networked and we realize that as a result now no business is an island and must contract with others. We provide our clients with complete contract management and support so that you can concentrate on being all that you can be  </p>
                    </div>
                   
                </div>
                
                <div class="divider">
                    <i class="icon-circle"></i>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <h2>
                            INSOURCING SERVICES
                        </h2>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <h3>THE NETWORK</h3>
                        <p>Our network of lawyers and legal services providers connects you with business and legal talents in the industry as and when required by you  </p>
                    </div>
                    <div class="col-md-6">
                        <h3>ON SITE INSOURCING</h3>
                        <p>Our team understands that litigation support services are an essential component of the litigation process. Therefore we work to ensure that all your litigation support needs are met  </p>
                    </div>
                    
                   
                </div>
                
            </div>
        </div>
        
        
    </div>
</section>