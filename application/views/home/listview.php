<?php 

$objdate = new DateTime($date);
$sdate =  $objdate->format('l, F j, Y');

 ?>
<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>
        <span><?php echo $sdate?></span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Login</a></li>
            <li class="active">username</li>
            <li class="active">List View</li>
        </ol>
    </div>

</section><!-- #page-title end -->

 <section id="content">
     <div class="content-wrap">
         <div class="container clearfix">
            
                <div ckass="col_full col_last">
                    <input type="text" name="date" id="filteredDate"  /> <button class="btn btn-primary" id="filterbydate">Go</button>
                    <button class="btn btn-success pull-right" id="add-hearing" >Add</button>
                </div>

                <div class="clear"></div>
            
             <div class="tabs tabs-bordered clearfix" id="hearing-list">
                 <ul class="tab-nav clearfix">
                     <li><a href="#my-requests">My Hearing</a></li>
                     <li><a href="#open-requests">Open Hearing</a></li>
                 </ul>

                 <div class="tab-container">
                    <div class="tab-content clearfix" id="my-requests">
                        <h1>My Requests for Hearing Coverage</h1> 
                        <div class="table-responsive">
                        <table id="my-hearings" class="table">
                            <thead>
                                <tr>
                                    <th>Hearing ID</th>
                                    <th>Date & Time</th>
                                    <th>Court & Location</th>
                                    <th>Area of Law</th>
                                    <th>Hearing Type</th>
                                    <th>Track</th>
                                    <th>Fees</th>
                                    <th>Reference no</th>
                                </tr>
                            </thead>
                         
                           

                             <tfoot>
                                <tr>
                                    <th>Hearing ID</th>
                                    <th>Date & Time</th>
                                    <th>Court & Location</th>
                                    <th>Area of Law</th>
                                    <th>Hearing Type</th>
                                    <th>Track</th>
                                    <th>Fees</th>
                                    <th>Reference no</th>
                                    
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>

                    <div class="tab-content clearfix" id="open-requests">
                        <h1>Open Hearings for Coverage</h1> 
                        <div class="table-responsive">
                        <table id="available-hearings" class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Hearing ID</th>
                                    
                                    <th>Date & Time</th>
                                    <th>Court & Location</th>
                                    <th>Area of Law</th>
                                    <th>Hearing Type</th>
                                    <th>Track</th>
                                    <th>Fees</th>
                                   
                                    <th>Reference no</th>
                                </tr>
                            </thead>
                         
                           

                             <tfoot>
                                <tr>
                                    <th>Hearing ID</th>
                                    
                                    <th>Date & Time</th>
                                    <th>Court & Location</th>
                                    <th>Area of Law</th>
                                    <th>Hearing Type</th>
                                    <th>Track</th>
                                    <th>Fees</th>
                                    
                                    <th>Reference no</th>
                                    
                                </tr>
                            </tfoot>
                           
                        </table>
                    </div>
                    </div>
                </div>

             </div>
         </div>
     </div>
 </section>


						

<script type="text/javascript">
$(document).ready(function() {
    

    $("#filterbydate").click(function(){
        document.location = '<?php echo base_url()?>home/listview/'+$("#filteredDate").val();
    });

     $('#filteredDate').datepicker({
            format: "yyyy-mm-dd",
            clearBtn: true,
            multidate: false,
            autoclose: true
        });
    
    $( "#hearing-list" ).tabs({ show: { effect: "fade", duration: 400 } });
    

    var table_my_hearing = $('#my-hearings').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: '<?php echo base_url()?>ajax/my_hearings',
            data:function(d){
                d.date = '<?php echo $date?>'
            }
        },
        
        aoColumnDefs:[
            {
                aTargets: [0],
                visible: false
            },
            {
                aTargets: [7],
                 mRender: function (data, type, full) {
                    return '<a href="<?php echo base_url()?>home/hearing_detail/'+full[0]+'" >'+full[7]+'-'+full[8]+'-'+full[9]+'</a>';
                 }
            }
        ]

       
    });

   
    

     $('#available-hearings').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: '<?php echo base_url()?>ajax/available_hearings',
            data:function(d){
                d.date = '<?php echo $date?>'
            }
        },

        aoColumnDefs:[
            {
                aTargets: [0],
                visible: false
            },
            {
                aTargets: [7],
                 mRender: function (data, type, full) {
                    return '<a href="<?php echo base_url()?>home/hearing_detail/'+full[0]+'" >'+full[7]+'-'+full[8]+'-'+full[9]+'</a>';
                 }
            }
        ]
            
    });

    $("#add-hearing").click(function(){
        document.location = '<?php echo base_url()?>home/add_hearing';
    });

   

} );
</script>
						