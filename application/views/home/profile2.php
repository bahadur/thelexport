<style>

    .profile 
    {
        min-height: 355px;
        display: inline-block;
    }
    figcaption.ratings
    {
        margin-top:20px;
    }
    figcaption.ratings a
    {
        color:#f1c40f;
        font-size:11px;
    }
    figcaption.ratings a:hover
    {
        color:#f39c12;
        text-decoration:none;
    }
    .divider 
    {
        border-top:1px solid rgba(0,0,0,0.1);
    }
    
    
    
    
    
    
   

</style>

<section id="page-title">

    <div class="container clearfix">
        <h1>Profile</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Profile</li>
        </ol>
    </div>

</section><!-- #page-title end -->


<section class="content">
    <div class="content-wrap">
        <div class="container clearfix">
            
                <div class="row">
                    <div class="col-md-7 col-lg-6">
                        <div class="well profile">
                            <div class="col-sm-12">
                                <div class="col-xs-12 col-sm-4 text-center">
                                    <figure>
                                        <img src="<?php echo base_url() ?>assets/images/author/1.jpg" alt="" class="img-circle img-responsive">
                                        <figcaption class="ratings">
                                            <p>Ratings
                                                <a href="#">
                                                    <span class="fa fa-star"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="fa fa-star-half-empty"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="fa fa-star-o"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="fa fa-star-o"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="fa fa-star-o"></span>
                                                </a> 
                                            </p>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="fancy-title title-bottom-border">
                                        <h3><?php echo $profile->f_name.' '. $profile->m_name. ' ' .$profile->l_name ?><span class="badge pull-right">QC</span></h3>
                                    </div>
                                    <p><strong>Legal Qualification: </strong> <span class="pull-right"><?php echo $profile->legal_qualification?> </span></p>
                                    <p><strong>Qualification: </strong> <span class="pull-right"><?php echo $profile->qualification?> </span></p>
                                    <p><strong>Regulatory Authority: </strong> <span class="pull-right"><?php echo $profile->authority?> </span></p>
                                    <p><strong>Regulatory Authority No: </strong> <span class="pull-right"><?php echo $profile->regulatory_authority_no?></span></p>
                                    <p><strong>Email: </strong> <span class="pull-right"><?php echo $profile->email?> </span></p>
                                    <p><strong>Address: </strong> <span class="pull-right"><?php echo $profile->address?> </span></p>
                                    <p><strong>Contact No: </strong> <span class="pull-right"><?php echo $profile->contact_no?> </span></p>





                                </div>             
                                
                            </div>            
                            
                        </div>                 
                    </div>
                    <div class="col-md-5 col-lg-6">
                        <h3>overview</h3>
                        <blockquote class="quote">
                        <p class="text-justify">
                            Over view here.
                        </p>
                        </blockquote>
                        
                    </div>
                    
                </div>
                
                <!--div class="row">
                    
                    <div class="col-lg-12">
                        <div class="tabs side-tabs tabs-bordered clearfix" id="tab-5">
                            <ul class="tab-nav clearfix">
                                <li><a href="#tab-1">Practice Area</a></li>
                                <li><a href="#tab-2">Publications</a></li>
                                <li><a href="#tab-3">Education & Qualificatoins</a></li>
                                <li><a href="#tab-4">Directory Quotes</a></li>
                                
                            </ul>
                            
                            <div class="tab-container">
                                
                                <div class="tab-content clearfix" id="tab-1" style="min-height:139px">
                                    <div class="accordion clearfix">
                                        <div class="acctitle">
                                            <i class="acc-closed icon-ok-circle"></i>
                                            <i class="acc-open icon-remove-circle"></i>Commercial
                                            
                                        </div>
                                        <div class="acc_content clearfix">
                                            Helen Davies has acted in a wide variety of commercial cases. Her experience (both as an advocate and in an advisory capacity) ranges from various contractual disputes to energy, banking, insurance/reinsurance, financial services and professional negligence.
                                            <h5>Reported cases include:</h5>
                                            <ul>
                                                <li>
                                                    Berezovsky v Abramovich (£6 billion claim for intimidation/breach of trust, acting for successful defendant Roman Abramovich), Commercial Court and Court of Appeal
                                                </li>
                                                <li>
                                                    Berezovsky v Abramovich (£6 billion claim for intimidation/breach of trust, acting for successful defendant Roman Abramovich), Commercial Court and Court of Appeal
                                                </li>
                                                <li>
                                                    Berezovsky v Abramovich (£6 billion claim for intimidation/breach of trust, acting for successful defendant Roman Abramovich), Commercial Court and Court of Appeal
                                                </li>
                                                <li>
                                                    Berezovsky v Abramovich (£6 billion claim for intimidation/breach of trust, acting for successful defendant Roman Abramovich), Commercial Court and Court of Appeal
                                                </li>
                                            </ul>
                                        </div>
                                        
                                        
                                        <div class="acctitle">
                                            <i class="acc-closed icon-ok-circle"></i>
                                            <i class="acc-open icon-remove-circle"></i>EU/Competition
                                            
                                        </div>
                                        <div class="acc_content clearfix">
                                            Having undertaken a stage in EC Commission Competition Directorate, Helen Davies has also acted in a broad range of European and Competition Law cases before a variety of tribunals, including the UK Courts, the CAT and the Competition Commission, as well as the General Court and the CJEU These include:
                                            
                                            <ul>
                                                <li>
                                                    FAPL v Ofcom (appeal by FA Premier League against OFCom's decision to require BSkyB to supply its premium sports channels to other pay TV retailers on regulated terms), CAT
                                                </li>
                                                <li>
                                                    FAPL v Ofcom (appeal by FA Premier League against OFCom's decision to require BSkyB to supply its premium sports channels to other pay TV retailers on regulated terms), CAT
                                                </li>
                                                <li>
                                                    FAPL v Ofcom (appeal by FA Premier League against OFCom's decision to require BSkyB to supply its premium sports channels to other pay TV retailers on regulated terms), CAT
                                                </li>
                                                <li>
                                                    FAPL v Ofcom (appeal by FA Premier League against OFCom's decision to require BSkyB to supply its premium sports channels to other pay TV retailers on regulated terms), CAT
                                                </li>
                                            </ul>
                                        </div>
                                        
                                        <div class="acctitle">
                                            <i class="acc-closed icon-ok-circle"></i>
                                            <i class="acc-open icon-remove-circle"></i>Arbitrary
                                            
                                        </div>
                                        <div class="acc_content clearfix">
                                            Helen Davies has also appeared in numerous commercial arbitrations, both international and domestic. Many of these have involved claims of very substantial value. She also sits as an arbitrator.
                                            
                                            
                                        </div>
                                        
                                        
                                    </div>
                                    
                                </div>
                                
                                <div class="tab-content clearfix" id="tab-2" style="min-height:139px">
                                    Contributor to Brick Court Chambers, Competition Litigation: UK Practice and Procedure
                                    
                                </div>
                                
                                <div class="tab-content clearfix" id="tab-3" style="min-height:139px">
                                    BA Hons Law, Class I (Emmanuel College, Camb)

                                    Stage, European Commission DGIV, October 1993

                                    Member of the B Panel to the Crown, 1999-2008

                                    Appointed a Bencher of Inner Temple 2010
                                    
                                </div>
                                
                                <div class="tab-content clearfix" id="tab-4" style="min-height:139px">
                                    <h5>commercial litigation:</h5>
                                    <ul>
                                        <li>"Highly intelligent and a pleasure to work with." (Chambers & Partners 2015)</li>
                                        <li>"Highly intelligent and a pleasure to work with." (Chambers & Partners 2015)</li>
                                        <li>"Highly intelligent and a pleasure to work with." (Chambers & Partners 2015)</li>
                                        <li>"Highly intelligent and a pleasure to work with." (Chambers & Partners 2015)</li>
                                    </ul>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div -->
            
        </div>
    </div>
</section>
<script>
$(function() {
    $( "#tab-5" ).tabs({ show: { effect: "fade", duration: 400 } });
});
</script>