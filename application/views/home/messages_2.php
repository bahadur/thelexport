
<section id="page-title">

    <div class="container clearfix">
        <h1>Messages</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Messages</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            
            <div id="posts" class="post-grid post-masonry post-timeline grid-2 clearfix" >
                <div class="timeline-border"></div>

                <div class="entry entry-date-section notopmargin"><span>November 2015</span></div>
                 <?php 
                    foreach ($hearing_messages as $row) {
                ?>
                <div class="entry clearfix">
                   <div class="entry-timeline" style="display:block">
                        <div class="timeline-divider"></div>
                    </div>
                    <div class="entry-image">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p><?php echo $row->message?></p>
                                <div class="testi-meta">
                                    <?php echo $row->msg_sender_lname?>, <?php   echo $row->msg_sender_fname?>
                                    <span><?php echo $row->court_name?></span>
                                </div>
                                <ul class="entry-meta clearfix">
                                    <li><i class="icon-calendar3"></i> <?php   echo $row->msg_date?></li>
                                    <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 26</a></li>
                                    <li><a href="#"><i class="icon-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                        
                </div>
                <?php } ?> 
                <div class="entry clearfix alt">
                   <div class="entry-timeline" style="display:block">
                        <div class="timeline-divider"></div>
                    </div>
                    <div class="entry-image">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p><?php echo $row->message?></p>
                                <div class="testi-meta">
                                    <?php echo $row->msg_sender_lname?>, <?php   echo $row->msg_sender_fname?>
                                    <span><?php echo $row->court_name?></span>
                                </div>
                                <ul class="entry-meta clearfix">
                                    <li><i class="icon-calendar3"></i> 17th Nov 2015</li>
                                    <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 26</a></li>
                                    <li><a href="#"><i class="icon-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                        
                </div>
                <div class="entry clearfix">
                   <div class="entry-timeline" style="display:block">
                        <div class="timeline-divider"></div>
                    </div>
                    <div class="entry-image">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <p><?php echo $row->message?></p>
                                <div class="testi-meta">
                                    <?php echo $row->msg_sender_lname?>, <?php   echo $row->msg_sender_fname?>
                                    <span><?php echo $row->court_name?></span>
                                </div>
                                <ul class="entry-meta clearfix">
                                    <li><i class="icon-calendar3"></i> 17th Nov 2015</li>
                                    <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 26</a></li>
                                    <li><a href="#"><i class="icon-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                        
                </div>
            </div>
                
        </div>

    </div>
    
</section>



<section id="content">
    <div class="content-wrap nopadding">
        <div class="section notopborder nomargin">
            <div class="container clearfix">
                <?php 
                    foreach ($hearing_messages as $row) {
                ?>
                <div class="col-md-6 bottommargin">
                    <div class="testimonial">
                                   
                        <div class="testi-content">
                                    <p><?php echo $row->message?></p>
                            <div class="testi-meta">
                                <?php echo $row->msg_sender_lname?>, <?php   echo $row->msg_sender_fname?>
                                <span><?php echo $row->court_name?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
		  </div>
	   </div>
    </div>
</section>