<section id="page-title">

    <div class="container clearfix">
        <h1>Messages</h1>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Messages</li>
        </ol>
    </div>

</section><!-- #page-title end -->


<section id="content">
     <div class="content-wrap">
         <div class="container clearfix">
             <table id="my-messages" class="display" cellspacing="0" width="100%">
                <thead>
                	<tr>
                    	<th>ID</th>
                    	<th>Name</th>
                        <th>Email Address</th>
                        <th>Telephone</th>
                        <th>Address</th>
                        <th>Regulating Authority</th>
                        <th>Regulating Authority Number</th>
                        
                    </tr>
                </thead>
                <tfoot>
                	<tr>
                		<th>ID</th>
                    	<th>Name</th>
                        <th>Email Address</th>
                        <th>Telephone</th>
                        <th>Address</th>
                        <th>Regulating Authority</th>
                        <th>Regulating Authority Number</th>
                                    
					</tr>
                </tfoot>
            	</table>
         </div>
     </div>
 </section>

<script type="text/javascript">
$(document).ready(function() {
    

    var table_my_hearing = $('#my-messages').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            url: '<?php echo base_url()?>ajax/hearing_messages/<?php echo $hearing_id?>',
            type: 'post'
            // data:function(d){
            //     d.hearing_id = '<?php echo $hearing_id?>'
            // }
        }
        ,
        
        aoColumnDefs:[
            {
                aTargets: [0],
                visible: false
            },
            {
                aTargets: [1],
                 mRender: function (data, type, full) {
                    return '<a href="<?php echo base_url()?>home/profile/'+full[0]+'" >'+full[1]+'</a>';
                 }
            }
        ]

       
   
    });
});

</script>