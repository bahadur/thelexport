<section id="page-title">

    <div class="container clearfix">
        <h1>Profile</h1>
        <span>We provide Amazing Solutions</span>
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Profile</li>
        </ol>
    </div>

</section><!-- #page-title end -->


<section class="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col_one_third nobottommargin">
                <div class="well well-lg nobottommargin">
                    
                    <div class="row">
                        <div class="col_one_third nobottommargin">
                            <div class="thumbnail">
                                <img class="img-circle" src="<?php echo base_url()?>assets/images/author/1.jpg" alt="" />
                            </div>
                        </div>
                        <div class="col_two_third col_last nobottommargin">
                            <div class="fancy-title title-bottom-border">
                                
                                <h5><span>Helen Davies </span><span class="badge pull-right">QC</span></h5>
                                <p class="small hidden-sm">Lorem ipsom Lorem ipcom Lorem ipsom Lorem ipcomLorem ipsom Lorem ipcomLorem ipsom Lorem ipcom</p>
                            </div>
                            
                            
                            
                            
                            
                        </div>
                    </div>
                    
                    
                    <div class="row">
                                <div class="col-sm-6">
                                    <small>Year of Call:</small>
                                </div>
                                <div class="col-sm-6">
                                    <h6><small>1999</small></h6>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <small>Direct phone::</small>
                                </div>
                                <div class="col-sm-6">
                                    <h6><small>020 7520 9923</small></h6>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <small>Clerk:</small>
                                </div>
                                <div class="col-sm-6">
                                    <h6><small>Julian Hawes</small></h6>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <small>Silk:</small>
                                </div>
                                <div class="col-sm-6">
                                    <h6><small>2008</small></h6>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <small>Email:</small>
                                </div>
                                <div class="col-sm-6">
                                    <h6><small>julian.hawes@brickcourt.co.uk</small></h6>
                                </div>
                            </div>
                    
                </div>
                
            </div>
                 
        </div>
    </div>
</section>