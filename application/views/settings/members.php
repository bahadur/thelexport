<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li class="active">Members</li>
            <!-- <li class="active">Calendar view</li> -->
        </ol>
    </div>

</section><!-- #page-title end -->




<section>
	
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="tabs tabs-bordered clearfix" id="members_list">

                <?php if(isset($message)) {?>
                <div class="style-msg successmsg">
                    <div class="sb-msg">
                        <i class="icon-thumbs-up"></i>
                        <strong>Message Sent !</strong>
                        <?php echo $message ?>
                    </div>
                </div>
                <?php } ?>

				<ul class="tab-nav clearfix">
                     <li><a href="#my-members">My Members</a></li>
                     <li><a href="#sent-invitations">Sent Invitations</a></li>
                     <li><a href="#invite_memeber">Invite new memeber</a></li>
                 </ul>

                <div class="tab-container">
                		
                	<div class="tab-content clearfix" id="my-members">
                		<h3>My Memebers</h3> 
                        <table id="table-my-members" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>previledges</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $this->db->where("users_organization_id",$this->tank_auth->get_user_id());
                                $this->db->join("user_member_profile","user_member_profile.users_user_id=users.user_id");
                                $result = $this->db->get("users")->result();
                                

                               

                                foreach ($result as $member) { ?>
                                <tr>
                                    <td><?php echo $member->f_name." ".$member->l_name?></td>    
                                    <td><?php echo $member->email?></td>
                                    <td>


                                        <div class='btn-group pull-left'> 
                                        <!-- <button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
                                            Previledges <span class='caret'></span> 
                                        </button>
                                    <ul  class='dropdown-menu dropdown-default pull-right' role='menu'> -->
                                        <?php 
                                        foreach ($previledges as  $value) {
                                                

                                            $this->db->where("previledges_previledge_id", $value->previledge_id);
                                            $this->db->where("user_member_profile_user_profile_id",  $member->user_id);
                                            $result = $this->db->get("previledges_has_user_member_profile");
                                            $assigned = ($result->num_rows() > 0)?"checked":"";

                                        ?>
                                            <input type="checkbox" name="<?php echo $value->previledge_id?>" <?php echo $assigned?>/ disabled="disabled"> <?php echo $value->previledge?>
                                        <!--li> <a href='javascript:void(0)' > <?php echo $value->previledge?> <i class="fa <?php //echo $assigned?>"></i></a> </li> 
                                        <li class='divider'></li -->  
                                        
                                        
                                        <?php    } ?>

                                        <!--li> <a href='javascript:void(0)' onclick='showPreviledgeModal("<?php echo $member->user_id?>")'><strong> Update Previledge</strong> </i></a> </li> 
                                    </ul -->
                                </div>

                                </td>
                                    <td><a href='javascript:void(0)' class="btn"  onclick='showPreviledgeModal("<?php echo $member->user_id?>")' >Update</a></td>
                                </tr>
                                <?php } ?>

						</table>
                	</div>

                	<div class="tab-content clearfix" id="sent-invitations">
						<h3>My Invitation sent</h3> 
                        <table id="table-my-invitations" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th> </th>
                                </tr>
                            </thead> 
                            <tbody>
                                <?php 
                                $this->db->where("invitation_status_invitation_status_id",1);
                                $result = $this->db->get("member_invitations")->result();
                                

                                

                                foreach ($result as $member) { ?>
                                <tr>
                                    <td><?php echo $member->to_name?></td>    
                                    <td><?php echo $member->to_email?></td>
                                    <td>
                                       
                                    </td>    
                                </tr>
                                <?php } ?>
                            </tbody>  
                        </table>

                	</div>

                	<div class="tab-content clearfix" id="invite_memeber">
                		<h3>New Member</h3>
                		<form class="form-horizontal nobottommargin" method="post" action="">
                			
                			<div class="form-group">
                				<label class="form-label col-sm-3">Name:</label>
                				<div class="col-sm-5">
                					<input type="text" name="name" id="name" class="form-control">
                				</div>
                			</div>	

                			<div class="form-group">
                				<label class="form-label col-sm-3">Email:</label>
                				<div class="col-sm-5">
                					<input type="text" name="email" id="email" class="form-control">
                				</div>
                			</div>	

                            


                			<div class="form-group">
                				
                				<div class="col-sm-offset-3 col-sm-5">
                					<button type="submit" class="button button-3d button-black nomargin"> Send Invitation </button>
                				</div>
                			</div>	


                		</form>

                	</div>

                </div>

			</div>
		</div>
	</div>

</section>



<!--  Modal -->

<div class="modal fade" id="modal-edit" data-backdrop="static" aria-hidden="true" style="display: none;"> 
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <h4 class="modal-title">Previleges</h4> 
            </div> 
            <div class="modal-body">
                <div class="row">  </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div> 
        </div> 
    </div> 
</div>

<script type="text/javascript">
$(document).ready(function() {

	$( "#members_list" ).tabs({ show: { effect: "fade", duration: 400 } });


    $("#table-my-members").dataTable();
    $("#table-my-invitations").dataTable();

});

function showPreviledgeModal(userid)

{ 
    
    var stringUrl = "<?php echo base_url().'modals/previledgeEdit/' ?>" + userid; 
    //console.log("string url : " + stringUrl);
    jQuery('#modal-edit').find('.modal-body').html("<div class='row'><image src='<?= base_url().'assets/images/indicator.gif' ?>'/></div>");
    jQuery.ajax({       
        type: 'get', 
        url: stringUrl,
        success: function(data)
        {  
            jQuery('#modal-edit').find('.modal-body').html(data); 
        },
        error :function(jqXHR, textStatus, errorThrown ) {

        }  
    });
    jQuery('#modal-edit').modal('show', {backdrop: 'static'});
} 


</script>