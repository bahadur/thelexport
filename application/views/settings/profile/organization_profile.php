
<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li class="active">Profile</li>
            <!-- <li class="active">Calendar view</li> -->
        </ol>
    </div>

</section><!-- #page-title end -->

<section>

	<div class="content-wrap">
		<div class="container clearfix">
            <form name="profile_form" id="profile_form" method="post">
			<div class="col_two_third col_last">

				<h3>Profile</h3>

				<div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>
                                    Sole Practitioner Information
                                    
                                    <button type="submit" class="btn btn-primary pull-right" id="submit-edit" style="display:none">Submit</button>
                                    
                                    <button type="button" class="btn btn-success pull-right" id="edit-profile">Edit</button>
                                    

                                </h3>

                            </div>
                        </div>
                        <div class="panel-body">
                            
                            <div class="col_half">
                                <label for="f_name">organization_name:</label>
                                <input class="form-control validate[required] text-input" type="text" name="organization_name" id="organization_name" value="<?php echo $profile->organization_name?>" disabled placeholder="First Name" data-validate="Organization Name is Required" />
                                
                            </div>

                            <div class="col_half col_last">
                            <label>Type of Organization:</label>
                            <select name="organization_types_organization_type_id" id="organization_types_organization_type_id" class="form-control select2" disabled>
                                <option></option>
                                <optgroup label="Organizations">
                                <?php 
                                foreach ($this->db->get("organization_types")->result() as  $row_organization) {
                                    $selected = ($profile->organization_types_organization_type_id == $row_organization->organization_type_id)?"selected":"";
                                    echo "<option value='$row_organization->organization_type_id' $selected >$row_organization->organization_type</option>";
                                }
                                ?>
                                </optgroup>                                   
                            </select>
                            </div>

                           

                           
                            
                            <div class="clear"></div>
                            
                            <div class="col_half">
                                <label>Regulatory Authority:</label>
                                <select name="regulatory_authority_regulatory_authority_id" id="regulatory_authority_regulatory_authority_id" class="form-control select2" disabled>
                                    <option></option>
                                    <optgroup label="Regulatory Authorities">
                                    <?php 
                                    foreach ($this->db->get("regulatory_authority")->result() as  $row_regulatory_authority) {
                                        $selected = ($profile->regulatory_authority_regulatory_authority_id == $row_regulatory_authority->regulatory_authority_id)?"selected":"";
                                        echo "<option value='$row_regulatory_authority->regulatory_authority_id' $selected>$row_regulatory_authority->authority</option>";
                                    }
                                    ?>   
                                    </optgroup>
                                </select>
                            </div>

                            
                                    
                            <div class="col_half col_last">
                                <label>Regulalory Authority No</label>
                                <input type="text" name="regulatory_authority_no" id="regulatory_authority_no" value="<?php echo $profile->regulatory_authority_no?>" disabled class="form-control" placeholder="Authority No" />
                            </div>

                            <div class="clear"></div>
                                            
                           
                           
                            <div class="col_full col_last">
                                <label>Number of Employees:</label>
                                <input type="text" name="number_employees" id="number_employees" class="form-control" value="<?php echo $profile->number_employees?>" disabled placeholder="Number of Employees" />
                            </div>

                            <div class="clear"></div>
                            
                            <div class="col_full col_last">
                                    <label for="address">Website:</label>
                                <input type="text" name="website" id="website" value="<?php echo $profile->website?>" disabled class="form-control" />
                            </div>
                            
                            <div class="clear"></div>
                            <div class="col_full col_last">
                                <label for="address">Address:</label>
                                <textarea name="address" id="address" class="form-control" disabled><?php echo $profile->address?></textarea>
                            </div>

                            <div class="clear"></div>
                                            
                            <div class="col_half ">
                                <label for="city">City:</label>
                                <input class="form-control text-input" type="text" name="city" id="city" value="<?php echo $profile->city?>" disabled placeholder="City" />
                            </div>

                            <div class="col_half col_last">
                                <label for="country">Country:</label>
                                <input class="form-control text-input" type="text" name="country" id="country" value="<?php echo $profile->country?>" disabled placeholder="Country" />
                            </div>
                                    
                            <div class="col_half ">
                                <label for="contact_no">Contact No:</label>
                                <input class="form-control text-input" type="text" name="contact_no" id="contact_no" value="<?php echo $profile->contact_no?>" disabled placeholder="Phone" />
                            </div>

                            <div class="col_half col_last ">
                                <label for="zip_code">Post/zip Code:</label>
                                <input class="form-control text-input" type="text" name="zip_code" id="zip_code" value="<?php echo $profile->zip_code?>" disabled placeholder="Postal/Zip Code" />
                            </div>

                            <input type="hidden" name="user_profile_id" id="user_profile_id" value="<?php echo $profile->user_profile_id?>" />

                           
                        </div>
                    </div>

			</div>
             </form>
		</div>
	</div>
</section>

<script type="text/javascript">
$(document).ready(function() { 

    $("#edit-profile").click(function(){


        $("input, select, textarea").removeAttr("disabled");
        
        $(this).text("Cancel");
        $("#submit-edit").show();



    });

});
</script>