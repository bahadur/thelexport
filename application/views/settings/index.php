<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Settings</a></li>
        </ol>
    </div>

</section><!-- #page-title end -->


<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			
			<div class="col_two_third col_last">
				<h3>Settings</h3>
				<ul class="list-group">
                    
                    <li class="list-group-item"><a href="<?php echo base_url()?>settings/profile">Profile</a></li>
                    <li class="list-group-item"><a href="<?php echo base_url()?>settings/alerts">Alerts</a></li>
                    <li class="list-group-item"><a href="<?php echo base_url()?>auth/change_password">Change Password</a></li>
                    <?php if($this->tank_auth->getUserType() == "organization_profile" ){ ?>
                        <li class="list-group-item"><a href="<?php echo base_url()?>settings/memebers">Memebers</a></li>
                    <?php } ?>
                    
                </ul>
			</div>

		</div>
	</div>
</section>