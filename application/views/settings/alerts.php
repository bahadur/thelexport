
<section id="page-title">

    <div class="container clearfix">
        <h1><?php echo $this->tank_auth->getFullName()?></h1>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li class="active">Alerts</li>
            <!-- <li class="active">Calendar view</li> -->
        </ol>
    </div>

</section><!-- #page-title end -->

<section>
	
	<div class="content-wrap">
		<div class="container clearfix">
			
			<div class="col_two_third col_last">
				<h3>Alerts</h3>
					
					
					<table class="table table-bordered" id="table-alerts">
						<thead>
							
							<tr>
								<th>Alert ID</th>
								<th>Court</th>
								<th>Date</th>
								<th>Action</th>

							</tr>

						</thead>

						<tfoot>
							
							<tr>
								<th>Alert ID</th>
								<th>Court</th>
								<th>Date</th>
								<th>Action</th>

							</tr>

						</tfoot>
					</table>

				<form class="form form-horizontal"  id="form-add-filter">

					
					
					
						<h3>Add Alert</h3>
						<div class="form-group">
							<label class="control-label col-sm-1">Court</label>
							<div class="col-sm-5">
								<select name="court_id" id="court_id" class="form-control"  >
									
									<?php 
									$court_ids = explode("|",$notification['court_id']);

									foreach ($this->db->get('court')->result() as $court_row) { 
										$selected = in_array($court_row->court_id, $court_ids)?"selected":"";
									?>
									<option value="<?php echo $court_row->court_id?>" <?php echo $selected?> ><?php echo $court_row->court_name?></option>

									<?php } ?>

								</select>
								
								
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-1">Date</label>
							<div class="col-sm-5">
								
								<input type="text" class="form-control" name="date" id="date"  value="<?php //echo $notification['date']?>" placeholder="All Dates" />

							</div>
						</div>

					

					<div class="form-group">
						<div class="col-sm-offset-1 col-sm-3">
	                    	<button type="submit" class="button button-3d button-black nomargin" name="update" value="register">Update</button>
	                    </div>
                    </div>

				<form>
			</div>

		</div>
	</div>
</section>

<script type="text/javascript">
$(document).ready(function() { 
	
	$("#date").datepicker({
		format: "yyyy-mm-dd"
	});

	var table_alert = $("#table-alerts").dataTable({
		serverSide: true,
		processing: true,
		ajax: '<?php echo base_url()?>settings/alerts_json',
		dom:'ltip',
		oLanguage: {
        	sEmptyTable: 'No alert has been set'
    	},
		columnDefs: [ 
        {
           targets: 0,
           visible:false
           
         },
        {
           targets: 3,
           render: function ( data, type, full ) {
                return '<a href="<?php echo base_url()?>settings/alert_delete/'+full[0]+'" class="btn btn-default">Delete</a>';
            }
         } ]
	});

	$("#form-add-filter").submit(function(e){
		e.preventDefault();
		$.ajax({
			url: '<?php echo base_url()?>settings/alerts',
			type:'post',
			data:$(this).serialize(),
			success: function(response){
            	if(response.status == 1)
            		table_alert.fnDraw();

        	}
		});

		

	});




	

	



});
</script>