<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peoples extends CI_Controller {
    
          function __construct()
	{
		parent::__construct();

	
	}
	
	public function index()
	{
           $page_data['page_name']  = 'peoples/index';
           $page_data['page_title'] = 'Peoples';
            
            $this->load->view("index",$page_data);
	}
        
        public function careers(){
           $page_data['page_name']  = 'peoples/careers';
           $page_data['page_title'] = 'Careers';
            
            $this->load->view("index",$page_data);
        }
}