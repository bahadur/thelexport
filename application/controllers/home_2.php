<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
          function __construct()
	{
		parent::__construct();
                
                $this->load->library('tank_auth');
                $this->lang->load('tank_auth');
                
	}
	
	public function index()
	{
            if (!$this->tank_auth->is_logged_in()) {	
                
                redirect('/auth/login/');
            }
           
            $query = $this->db->query("select 
                                hearing_id,
                                court.court_name,
                                DATE_FORMAT(hearing_date,'%Y-%m-%dT%H:%i:%s') as f_date,
                                hearing_date, 
                                users_user_id
                                
                                from hearing
                                INNER JOIN court ON hearing.court_court_id = court.court_id
                                where hearing_date > NOW()");
            $arr_h = array();


            
            foreach($query->result() as $row) {
               
               $todate=date_create($row->hearing_date);
               $end_date = date_add($todate,date_interval_create_from_date_string("4 hours"));
               
                $arr_h[] = array("id"    => $row->hearing_id,
                                 "title" => $row->court_name,
                                 "url"   => base_url()."home/hearing_detail/".md5($row->hearing_id),
                                 "start" => $row->f_date,
                                 "end"   => date_format($end_date,"Y-m-d\TH:i:s")
                            );
            }


            $query = $this->db->query(" 
                                    SELECT
                                hearing_msg.msg_date, 
                                hearing.hearing_date, 
                                hearing.estimated_time_hearing, 
                                messages.message, 
                                court.court_name, 
                                hearing.hearing_id,
                                profile2.f_name as msg_sender_fname,
                                profile2.l_name  as msg_sender_lname
                                FROM
                                hearing_msg
                                INNER JOIN messages ON messages.message_id = hearing_msg.messages_message_id
                                INNER JOIN hearing ON hearing_msg.hearing_hearing_id = hearing.hearing_id
                                INNER JOIN users ON hearing.users_user_id = users.user_id
                                INNER JOIN court ON hearing.court_court_id = court.court_id 
                                INNER JOIN users as user2 ON hearing_msg.users_user_id = user2.user_id
                                INNER JOIN user_network_profiles profile2 ON user2.user_id = profile2.users_user_id 
                                WHERE users.user_id = ".$this->tank_auth->get_user_id()."
                                AND hearing.hearing_date > NOW()");


           
           $page_data['hearings'] =  $arr_h;
           
           $page_data['page_name']  = 'home/index';
           $page_data['page_title'] = 'Home';
            
           $this->load->view("index",$page_data);
	}
        
    function add_hearing(){
        
        if (!$this->tank_auth->is_logged_in()) {    
                
                redirect('/auth/login/');
            }
           

        if($this->input->post()){

        
            $data = array(
                            
                            'hearing_date' => $this->input->post('hearing_date'),
                            'comments' => $this->input->post('comments'),
                            'estimated_time_hearing' => $this->input->post('estimated_time_hearing'),
                            'court_court_id' => $this->input->post('court_court_id'),
                            'hearing_type_hearing_type_id' => $this->input->post('hearing_type_hearing_type_id'),
                            'law_area_law_area_id' => $this->input->post('law_area_law_area_id'),
                            'track_track_id'=> $this->input->post('track_track_id'),
                            'users_user_id' => $this->tank_auth->get_user_id()
                            );

            $this->db->insert('hearing',$data);
            redirect('home/listview');

        }



        $page_data['page_name']  = 'home/add_hearing';
        $page_data['page_title'] = 'Add Hearing';
        $this->load->view("index",$page_data);
    }    
    function hearing_detail($hearing_id){
            if (!$this->tank_auth->is_logged_in()) {	
                
                redirect('/auth/login/');
            }
           //DATE_FORMAT(hearing.hearing_date,'%H:%i:%s') as hearing_time,
            $query = $this->db->query("SELECT
                                    hearing.hearing_id,
                                    hearing.reference_no,
                                    DATE_FORMAT(hearing.hearing_date,'%d %b %Y %h:%i %p') as hearing_datetime,
                                    DATE_FORMAT(hearing.hearing_date,'%d %b %Y') as hearing_date,
                                    
                                    DATE_FORMAT(hearing.hearing_date,'%Y, %m, %d, %H, %i, %s') as hearing_js,
                                    estimated_time_hearing,
                                    hearing_type.hearing_type,
                                    court_type.type,
                                    court.court_name,
                                    court.address,
                                    court.location,
                                    law_area.area_of_law,
                                    track.track,
                                    hearing.comments,
                                    users_user_id

                                    FROM
                                    hearing
                                    INNER JOIN users ON hearing.users_user_id = users.user_id
                                    INNER JOIN court ON hearing.court_court_id = court.court_id
                                    INNER JOIN court_type ON court.court_id = court_type.court_type_id
                                    INNER JOIN law_area ON hearing.law_area_law_area_id = law_area.law_area_id
                                    INNER JOIN track ON hearing.track_track_id = track.track_id
                                    INNER JOIN hearing_type ON hearing.hearing_type_hearing_type_id = hearing_type.hearing_type_id
                                    where md5(hearing.hearing_id) = '".$hearing_id."'");
            
           $page_data['hearings'] = $query->result();
           
           $page_data['page_name']  = 'home/hearing_detail';
           $page_data['page_title'] = 'Hearing Detail';
            
           $this->load->view("index",$page_data);
        }

        
        
        public function profile(){
            $page_data['page_name']  = 'home/profile2';
            $page_data['page_title'] = 'Home | Profile';
            
            $this->load->view("index",$page_data);
        }
        
        public function our_functions(){
           $page_data['page_name']  = 'home/our_functions';
           $page_data['page_title'] = 'Home | Our Functions';
            
            $this->load->view("index",$page_data);
        }
        
        public function our_motivations(){
            $page_data['page_name']  = 'home/our_motivations';
           $page_data['page_title'] = 'Home | Our Motivations';
            
            $this->load->view("index",$page_data);
        }
        
       
        
        
        
        
        public function contact_us(){
            $page_data['page_name']  = 'home/contact_us';
           $page_data['page_title'] = 'Home';
            
            $this->load->view("index",$page_data);
        }
        
        function _show_message($message)
        	{
                    
        		$this->session->set_flashdata('login_message', $message);
                        
        		redirect('/auth/');
        	}
	   function listview($date = ""){
        if (!$this->tank_auth->is_logged_in()) {    
                
                redirect('/auth/login/');
            }
        
        $page_data['date']=$date;
        $page_data['page_name']  = 'home/listview';
        $page_data['page_title'] = 'Home';
            
        $this->load->view("index",$page_data);
        }
		
		function messages(){

            if (!$this->tank_auth->is_logged_in()) {    
                
                redirect('/auth/login/');
            }

            $hearing_messages = array();
            $date_query = $this->db->query(" 
                                SELECT
                                DATE_FORMAT(hearing_msg.msg_date,'%M %Y') as msg_month, 

                                hearing_msg.msg_date
                                
                                FROM
                                hearing_msg
                                INNER JOIN messages ON messages.message_id = hearing_msg.messages_message_id
                                INNER JOIN hearing ON hearing_msg.hearing_hearing_id = hearing.hearing_id
                                INNER JOIN users ON hearing.users_user_id = users.user_id
                                INNER JOIN court ON hearing.court_court_id = court.court_id 
                                INNER JOIN users as user2 ON hearing_msg.users_user_id = user2.user_id
                                INNER JOIN user_network_profiles profile2 ON user2.user_id = profile2.users_user_id 
                                WHERE hearing_msg.users_user_id = ".$this->tank_auth->get_user_id()."
                                AND hearing.hearing_date > NOW()");
            
           //print_r($date_query->result()); die;
            foreach($date_query->result() as $row_date){

                $hearing_messages[$row_date->msg_month] = array();
                


                $query = $this->db->query(" 
                                SELECT
                                hearing_msg.msg_date, 
                                hearing.hearing_date, 
                                hearing.estimated_time_hearing, 
                                messages.message, 
                                court.court_name, 
                                hearing.hearing_id,
                                profile2.f_name as msg_sender_fname,
                                profile2.l_name  as msg_sender_lname
                                FROM
                                hearing_msg
                                INNER JOIN messages ON messages.message_id = hearing_msg.messages_message_id
                                INNER JOIN hearing ON hearing_msg.hearing_hearing_id = hearing.hearing_id
                                INNER JOIN users ON hearing.users_user_id = users.user_id
                                INNER JOIN court ON hearing.court_court_id = court.court_id 
                                INNER JOIN users as user2 ON hearing_msg.users_user_id = user2.user_id
                                INNER JOIN user_network_profiles profile2 ON user2.user_id = profile2.users_user_id 
                                WHERE hearing_msg.users_user_id = ".$this->tank_auth->get_user_id()."
                                and DATE_FORMAT(hearing_msg.msg_date,'%M %Y') = '".$row_date->msg_month."'
                                AND hearing.hearing_date > NOW()");
                foreach ($query->result() as $row) {
                    
                    $hearing_messages[$row_date->msg_month][] = $row;
                }
               
            }
    
            //print_r($hearing_messages);die;
            $page_data['hearing_messages'] = $hearing_messages;

            $page_data['page_name']  = 'home/messages';
            $page_data['page_title'] = 'Home';
            
            $this->load->view("index",$page_data);
        }
}

