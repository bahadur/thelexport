<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
    
    function __construct(){
		parent::__construct();
                
        $this->load->library('tank_auth');
        $this->lang->load('tank_auth');
         if (!$this->tank_auth->is_logged_in()) {	
                
        	redirect('/auth/login/');
        }
                
	}


	function index(){

		$page_data['page_name']  = 'settings/index';
        $page_data['page_title'] = 'Settings';
	    $this->load->view("index",$page_data);

	}


	function alerts(){



		if($this->input->post()){

			$alert_data = array(
							"court_court_id" => $this->input->post("court_id"),
							"date" => $this->input->post("date"),
							"users_user_id" => $this->tank_auth->get_user_id()

							);

					
					$this->db->insert("alerts",$alert_data);

			header('Content-Type: application/json');

			echo json_encode(array("status"=>1));
			die;


		} 

		
		//$notification = $this->db->where("users_user_id", $this->tank_auth->get_user_id())->get("alert");

		// if($notification->num_rows() > 0){
		// 	$not_row = $notification->row();
		// 	$page_data['notification'] = array("court_id" => $not_row->court_id, "date"=>$not_row->date, "user_id" => $not_row->users_user_id);

		// } else {
		// 	$page_data['notification'] = array("court_id" => 0, "date"=>'', "user_id" => 0);			
		// }
		//print_r($page_data['notification']); die;

		// $page_data['settings'] = $this->db->select('notificatoin')
		// 							->where("user_id",$this->tank_auth->get_user_id())
		// 							->get('users')
		// 							->row();

		$page_data['page_name']  = 'settings/alerts';
        $page_data['page_title'] = 'Settings | Alerts';

	    $this->load->view("index",$page_data);

	}

	function alert_delete($alert_id){

		$this->db->where("alert_id", $alert_id);
		$this->db->delete("alerts");

		

		redirect("settings/alerts");

	}

	function alerts_json(){

		header('Content-Type: application/json');
		
		


		$this->db->select("SQL_CALC_FOUND_ROWS * ", false);
		//$this->db->join("court","alert.court_court_id = court.court_id");
		$this->db->where("users_user_id",$this->tank_auth->get_user_id());
		


		
		
		if ( isset($request['start']) && $request['length'] != -1 ) {
			
			$this->db->limit(intval($request['length']),intval($request['start']));
			//$limit = "LIMIT ".intval($request['start']).", ".intval($request['length']);
		}

		$rs = $this->db->get("view_alerts")->result();


		$resFilterLength = $this->db->query( 
			"SELECT FOUND_ROWS() num_rows"
		)->result();

		$recordsFiltered = $resFilterLength[0]->num_rows;


		$resTotalLength = $this->db->query( 
			"SELECT COUNT(alert_id) count
			 FROM   view_alerts
			 WHERE users_user_id = ".$this->tank_auth->get_user_id())->result();
		$recordsTotal = $resTotalLength[0]->count;
		$alerts = array();
		foreach ($rs as $value) {
			
			$alerts[] = array($value->alert_id, $value->court_name, $value->date);
		}

		echo json_encode(array("draw"=>$this->input->get("draw"),"recordsTotal" => $recordsTotal, "recordsFiltered"=>$recordsFiltered, "data" => $alerts));

		


	}


	function memebers(){


		if ($message = $this->session->flashdata('message')) {
			$page_data['message'] = $message;
		}

		if($this->input->post()){


			if($this->input->post('action') == "update_previledge"){

				$insert_prv = array();
				$this->db->delete("previledges_has_user_member_profile", array("user_member_profile_user_profile_id" =>  $this->input->post("user_member_profile_user_profile_id") ));
				
				if( $this->input->post('previledges_previledge_id')  ){
				
				
				
					foreach($this->input->post('previledges_previledge_id') as $p_ids){
						$insert_prv[] = array(	"previledges_previledge_id" => $p_ids,
												"user_member_profile_user_profile_id" => $this->input->post("user_member_profile_user_profile_id"));
					$this->db->insert("previledges_has_user_member_profile", array(	"previledges_previledge_id" => $p_ids,
																					"user_member_profile_user_profile_id" => $this->input->post("user_member_profile_user_profile_id")));
					}

				}


				$this->_show_message("Previledge has been updated.","memebers");
			} else {

			$insert_data = array(
				"to_name" 			=> $this->input->post('name'),
				"to_email" 			=> $this->input->post('email'),
				"users_user_id" 	=> $this->tank_auth->get_user_id(),
				"invitation_date" 	=> date("Y-m-d H:i:s"),
				"invitation_status_invitation_status_id" => 1
				);

			$this->db->insert('member_invitations', $insert_data);

			$insert_id = $this->db->insert_id();
			



			$data["from_name"] = $this->tank_auth->getFullName();
			$data["to_name"] = $this->input->post('name');
			$data["invite_id"] = $insert_id;
			$data['site_name'] = $this->config->item('website_name', 'tank_auth');
			 
			$this->_send_email($this->input->post('email'), $data);


			$this->_show_message("Invitation has been send","memebers");

			//redirect("settings/memebers");
			}
		}

		$page_data["previledges"] = $this->db->get("previledges")->result();

		$page_data['page_name']  = 'settings/members';
        $page_data['page_title'] = 'Settings | Members';
	    $this->load->view("index",$page_data);		
	}


	function profile(){
		
		$user_profile = $this->tank_auth->getUserType();

		
		if($this->input->post()){

			switch ($user_profile) {
				case 'user_individual_profile':
				
						
						//users_user_id
						
						$profile_update = array(
						'f_name' 										=> $this->input->post('f_name'),
						'm_name' 										=> $this->input->post('m_name'),
						'l_name' 										=> $this->input->post('l_name'),
						'legal_qualifications_legal_qualifications_id' 	=> $this->input->post('legal_qualifications_legal_qualifications_id'),
						'regulatory_authority_regulatory_authority_id' 	=> $this->input->post('regulatory_authority_regulatory_authority_id'),
						'regulatory_authority_no' 						=> $this->input->post('regulatory_authority_no'),
						'website' 										=> $this->input->post('website'),
						'address' 										=> $this->input->post('address'),
						'city' 											=> $this->input->post('city'),
						'country' 										=> $this->input->post('country'),
						'zip_code' 										=> $this->input->post('zip_code'),
						'contact_no' 									=> $this->input->post('contact_no'),
						'qualifications_qualification_id' 				=> $this->input->post('qualifications_qualification_id'),
						'date_completion' 								=> $this->input->post('date_completion'));
						
						
						


					break;

				case 'organization_profile':
					
				
					


						$profile_update = array(
						'organization_name' 							=> $this->input->post('organization_name'),
						'organization_types_organization_type_id' 		=> $this->input->post('organization_types_organization_type_id'),
						'regulatory_authority_regulatory_authority_id' 	=> $this->input->post('regulatory_authority_regulatory_authority_id'),
						'regulatory_authority_no' 						=> $this->input->post('regulatory_authority_no'),
						'number_employees' 								=> $this->input->post('number_employees'),
						'website' 										=> $this->input->post('website'),
						'address' 										=> $this->input->post('address'),
						'city' 											=> $this->input->post('city'),
						'country' 										=> $this->input->post('country'),
						'zip_code' 										=> $this->input->post('zip_code'),
						'contact_no' 									=> $this->input->post('contact_no'));
												
						
						

				break;

				case 'user_member_profile':
					



						$profile_update = array(
						'f_name' 										=> $this->input->post('f_name'),
						'm_name' 										=> $this->input->post('m_name'),
						'l_name' 										=> $this->input->post('l_name'),
						'legal_qualifications_legal_qualifications_id' 	=> $this->input->post('legal_qualifications_legal_qualifications_id'),
						'regulatory_authority_regulatory_authority_id' 	=> $this->input->post('regulatory_authority_regulatory_authority_id'),
						'regulatory_authority_no' 						=> $this->input->post('regulatory_authority_no'),
						'website' 										=> $this->input->post('website'),
						'address' 										=> $this->input->post('address'),
						'city' 											=> $this->input->post('city'),
						'country' 										=> $this->input->post('country'),
						'zip_code' 										=> $this->input->post('zip_code'),
						'contact_no' 									=> $this->input->post('contact_no'),
						'qualifications_qualification_id' 				=> $this->input->post('qualifications_qualification_id'),
						'date_completion' 								=> $this->input->post('date_completion'));
						
						
						

				break;
				
				
			}

			$this->db->where('user_profile_id', $this->input->post('user_profile_id'));
			$this->db->where("users_user_id",$this->tank_auth->get_user_id());
			$this->db->update($user_profile,$profile_update);
			
			redirect('settings/profile');
			
		}


		$page_data['page_name']  = 'settings/profile/'.$user_profile;
        $page_data['page_title'] = 'Settings | Profile';



        $this->db->where("users_user_id",$this->tank_auth->get_user_id());
        $page_data['profile'] = $this->db->get($user_profile)->row();

       
	    $this->load->view("index",$page_data);		
	}


	function _send_email($email, $data){

            $this->load->library('email');
            $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
            $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
            $this->email->to($email);
            $this->email->subject(sprintf($this->lang->line('invitation_subject'), $data["from_name"]));
            $this->email->message($this->load->view('email/invitation-html', $data, TRUE));
            $this->email->set_alt_message($this->load->view('email/invitation-text', $data, TRUE));
            $this->email->send();
    }


    function _show_message($message, $redirect)
	{

		$this->session->set_flashdata('message', $message);

		redirect("settings/$redirect");
	}


}