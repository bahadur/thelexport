<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
          function __construct()
	{
		parent::__construct();
                
                $this->load->library('tank_auth');
                $this->lang->load('tank_auth');
                
	}
	
	public function index()
	{
           // echo md5(1); die;
            if (!$this->tank_auth->is_logged_in()) {	
                
                redirect('/auth/login/');
            }

            
           
            
            $this->db->select("
                                count(*) total_hearings,
                                hearing_id,
                                court.court_name,
                                DATE_FORMAT(hearing_date,'%Y-%m-%dT%H:%i:%s') as f_date,
                                DATE_FORMAT(hearing_date,'%Y-%m-%d') as g_date,
                                hearing_date,
                                users_user_id", false);
            $this->db->join("court","hearing.court_court_id = court.court_id");
            $this->db->where("hearing_date > NOW()");
            
            if($this->input->post("court_id") && $this->input->post("court_id") != 0){
                $this->db->where("hearing.court_court_id",$this->input->post("court_id"));
            }

            if($this->input->post("court_type_id")  && $this->input->post("court_type_id") != 0 ){
                $this->db->where("court.court_type_court_type_id",$this->input->post("court_type_id"));
            }

            if($this->input->post("hearing_type_id") && $this->input->post("hearing_type_id") != 0 ){
                $this->db->where("hearing.hearing_type_hearing_type_id",$this->input->post("hearing_type_id"));
            }


            if($this->input->post("hearing_date") && $this->input->post("hearing_date") != ""){
                $this->db->like("hearing.hearing_date",$this->input->post("hearing_date"));
            }

            $this->db->group_by("g_date");
            $arr_h = array();
            foreach($this->db->get('hearing')->result() as $row) {
               
               $todate=date_create($row->hearing_date);
               $end_date = date_add($todate,date_interval_create_from_date_string("4 hours"));
               
               $this->db->where("users_user_id", $this->tank_auth->get_user_id());
               $this->db->where("hearing_hearing_id", $row->hearing_id);
               $num_rows = $this->db->get('hearing_available_for')->num_rows();

               if($row->users_user_id == $this->tank_auth->get_user_id() || $num_rows > 0){

               

               $arr_h[] = array(
                                "title" =>$row->total_hearings." Hearing(s)",
                                "url" =>  base_url()."home/listview/".$row->g_date,
                                "start" =>$row->g_date,
                                //"end" =>date_format($end_date,"Y-m-d\TH:i:s")
                                    );
                }
            }
            
           
            $this->db->select(' 
                                hearing_id,
                                count(*) my_hearings, 
                                date_format(hearing.hearing_date, "%Y-%m-%d") as g_date',false);
        $this->db->join("court","hearing.court_court_id = court.court_id");
        $this->db->join("law_area","hearing.law_area_law_area_id = law_area.law_area_id");
        $this->db->join("hearing_type","hearing.hearing_type_hearing_type_id = hearing_type.hearing_type_id");
        $this->db->join("track","hearing.track_track_id = track.track_id");
        if($this->input->get('date') != ""){
            $this->db->like('hearing_date', $this->input->get('date'),'after');    
        }
        $this->db->where('users_user_id', $this->tank_auth->get_user_id());
        $this->db->where('hearing.hearing_date > NOW()');


         if($this->input->post("court_id") && $this->input->post("court_id") != 0){
                $this->db->where("hearing.court_court_id",$this->input->post("court_id"));
            }

            if($this->input->post("court_type_id")  && $this->input->post("court_type_id") != 0 ){
                $this->db->where("court.court_type_court_type_id",$this->input->post("court_type_id"));
            }

            if($this->input->post("hearing_type_id") && $this->input->post("hearing_type_id") != 0 ){
                $this->db->where("hearing.hearing_type_hearing_type_id",$this->input->post("hearing_type_id"));
            }


            if($this->input->post("hearing_date") && $this->input->post("hearing_date") != ""){
                $this->db->like("hearing.hearing_date",$this->input->post("hearing_date"));
            }
        

        $this->db->group_by("g_date");
        $hearing_data = array();
        foreach($this->db->get('hearing')->result() as $hearings){
           $arr_h[] = array(
                                "title" =>$hearings->my_hearings." my Hearing(s)",
                                "color" => "green",
                                "url" =>  base_url()."home/listview/".$hearings->g_date,
                                "start" =>$hearings->g_date,
                                
                                    );

                
        }


        // $m_query = $this->db->query("
        //                 SELECT
        //                 count(*) my_messages,
        //                 DATE_FORMAT(hearing_msg.msg_date,'%Y-%m-%d') m_date
        //                 FROM
        //                 hearing_msg
        //                 INNER JOIN hearing ON hearing_msg.hearing_hearing_id = hearing.hearing_id
        //                 where hearing.users_user_id = ".$this->tank_auth->get_user_id());


        $this->db->select("
                        count(*) my_messages,
                        DATE_FORMAT(hearing.hearing_date,'%Y-%m-%d') m_date", false);

        $this->db->join("hearing","hearing_msg.hearing_hearing_id = hearing.hearing_id");

        $this->db->where("hearing.users_user_id",$this->tank_auth->get_user_id());
        $this->db->where('hearing.hearing_date > NOW()');

            if($this->input->post("court_id") && $this->input->post("court_id") != 0){
                $this->db->where("hearing.court_court_id",$this->input->post("court_id"));
            }

            // if($this->input->post("court_type_id")  && $this->input->post("court_type_id") != 0 ){
            //     $this->db->where("court.court_type_court_type_id",$this->input->post("court_type_id"));
            // }

            if($this->input->post("hearing_type_id") && $this->input->post("hearing_type_id") != 0 ){
                $this->db->where("hearing.hearing_type_hearing_type_id",$this->input->post("hearing_type_id"));
            }


            if($this->input->post("hearing_date") && $this->input->post("hearing_date") != ""){
                $this->db->like("hearing.hearing_date",$this->input->post("hearing_date"));
            }

        $this->db->group_by("m_date");

        foreach($this->db->get("hearing_msg")->result() as $row) {
               
              
              
               if($row->my_messages > 0){
                $arr_h[] = array(
                                "title" =>$row->my_messages." Message(s)",
                                "url" =>  base_url()."home/timeline/",
                                "color" => "red",
                                "start" =>$row->m_date,
                                //"end" =>date_format($end_date,"Y-m-d\TH:i:s")
                                );
                }
            }

         


           
           $page_data['hearings'] =  $arr_h;
           
           $page_data['page_name']  = 'home/index';
           $page_data['page_title'] = 'Home';
            
           $this->load->view("index",$page_data);
	}


    function register(){
         $page_data['page_name']  = 'auth/register_form';
           $page_data['page_title'] = 'Register';
            
           $this->load->view("index",$page_data);
    }
        
    function add_hearing($date=""){
        
        if (!$this->tank_auth->is_logged_in()) {    
                
                redirect('/auth/login/');
            }
           

        if($this->input->post()){

        
            

            $data = array(
                            
                            'hearing_date' => $this->input->post('hearing_date'),
                            'comments' => $this->input->post('comments'),
                            'estimated_time_hearing' => $this->input->post('estimated_time_hearing'),
                            'court_court_id' => $this->input->post('court_court_id'),
                            'hearing_type_hearing_type_id' => $this->input->post('hearing_type_hearing_type_id'),
                            'law_area_law_area_id' => $this->input->post('law_area_law_area_id'),
                            'track_track_id'=> $this->input->post('track_track_id'),
                            'users_user_id' => $this->tank_auth->get_user_id()
                            );

            $this->db->insert('hearing',$data);

            $hearing_id = $this->db->insert_id();

            $user_available = array();
            foreach($this->input->post('available_for') as $userid){

                $user_available = array("users_user_id"=>$userid,
                                          "hearing_hearing_id" => $hearing_id);

                $this->db->insert('hearing_available_for',$user_available);
            }



            $data['site_name'] = $this->config->item('website_name', 'tank_auth');

            //$this->db->select("email");
            //$this->db->where("notificatoin",1);

           
            $hearing_date = explode(" ",  $this->input->post('hearing_date'));


            $this->db->join("users","alerts.users_user_id = users.user_id");
            $this->db->where("users_user_id != ",$this->tank_auth->get_user_id());
            $this->db->where("court_court_id",$this->input->post('court_court_id'));
            $this->db->like("date",$hearing_date[0]);

            foreach ($this->db->get("alerts")->result() as $alerts) {
                   
                
                        
                        $data['username'] = $alerts->username;
                        $this->_send_email($alerts->email, $data);

               

            }   



            redirect('home/listview');

        }

        


        $page_data['date']  = $date;

        $this->db->select("users.user_id, CONCAT(user_individual_profile.f_name,' ',user_individual_profile.l_name,' ',user_individual_profile.l_name) as user_name", false);
        $this->db->join('user_individual_profile','users.user_id = user_individual_profile.users_user_id');
        $this->db->where("users.user_id !=", $this->tank_auth->get_user_id());

        $users = $this->db->get('users')->result();


        $this->db->select("users.user_id,organization_profile.organization_name as user_name");
        $this->db->join('organization_profile','users.user_id = organization_profile.users_user_id');
        $this->db->where("users.user_id !=", $this->tank_auth->get_user_id());

        $users = $query = array_merge($users, $this->db->get('users')->result());


        $this->db->select("users.user_id,CONCAT(user_member_profile.f_name,' ',user_member_profile.l_name,' ',user_member_profile.l_name) as user_name",false);
        $this->db->join('user_member_profile','users.user_id = user_member_profile.users_user_id');
        $this->db->where("users.user_id !=", $this->tank_auth->get_user_id());

        $users = $query = array_merge($users, $this->db->get('users')->result());

        $page_data['user_names'] = $users;

        $page_data['page_name']  = 'home/add_hearing';
        $page_data['page_title'] = 'Add Hearing';
        $this->load->view("index",$page_data);
    }    



    function _send_email($email, $data){

            $this->load->library('email');
            $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
            $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
            $this->email->to($email);
            $this->email->subject(sprintf($this->lang->line('new_hearing_added'), $this->config->item('website_name', 'tank_auth')));
            $this->email->message($this->load->view('email/hearing_added-html', $data, TRUE));
            $this->email->set_alt_message($this->load->view('email/hearing_added-txt', $data, TRUE));
            $this->email->send();
    }


    function hearing_detail($hearing_id = ""){
            if (!$this->tank_auth->is_logged_in()) {	
                
                redirect('/auth/login/');
            }
            if(empty($hearing_id)){
                redirect('home/listview');
            }
           //DATE_FORMAT(hearing.hearing_date,'%H:%i:%s') as hearing_time,
            $query = $this->db->query("SELECT
                                    hearing.hearing_id,
                                    hearing.reference_no,
                                    DATE_FORMAT(hearing.hearing_date,'%d %b %Y %h:%i %p') as hearing_datetime,
                                    DATE_FORMAT(hearing.hearing_date,'%d %b %Y') as hearing_date,
                                    
                                    DATE_FORMAT(hearing.hearing_date,'%Y, %m, %d, %H, %i, %s') as hearing_js,
                                    estimated_time_hearing,
                                    hearing_type.hearing_type,
                                    court_type.type,
                                    court.court_name,
                                    court.address,
                                    court.location,
                                    law_area.area_of_law,
                                    track.track,
                                    hearing.comments,
                                    users_user_id

                                    FROM
                                    hearing
                                    INNER JOIN users ON hearing.users_user_id = users.user_id
                                    INNER JOIN court ON hearing.court_court_id = court.court_id
                                    INNER JOIN court_type ON court.court_id = court_type.court_type_id
                                    INNER JOIN law_area ON hearing.law_area_law_area_id = law_area.law_area_id
                                    INNER JOIN track ON hearing.track_track_id = track.track_id
                                    INNER JOIN hearing_type ON hearing.hearing_type_hearing_type_id = hearing_type.hearing_type_id
                                    where md5(hearing.hearing_id) = '".$hearing_id."'");
            
           $page_data['hearings'] = $query->result();
           
           $page_data['page_name']  = 'home/hearing_detail';
           $page_data['page_title'] = 'Hearing Detail';
            
           $this->load->view("index",$page_data);
        }

        
        
        public function profile($user_id){
            
            $this->db->select('
                                users.email,
                                user_individual_profile.f_name,
                                user_individual_profile.m_name,
                                user_individual_profile.l_name,
                                legal_qualifications.qualificatoin as legal_qualification,
                                regulatory_authority.authority,
                                user_individual_profile.regulatory_authority_no,
                                qualifications.qualification,
                                user_individual_profile.address,
                                user_individual_profile.contact_no');
            $this->db->join("user_individual_profile", "users.user_id = user_individual_profile.users_user_id");
            $this->db->join("legal_qualifications", "user_individual_profile.legal_qualifications_legal_qualifications_id = legal_qualifications.legal_qualifications_id");
            $this->db->join("regulatory_authority", "user_individual_profile.regulatory_authority_regulatory_authority_id = regulatory_authority.regulatory_authority_id");
            $this->db->join("qualifications", "user_individual_profile.qualifications_qualification_id = qualifications.qualification_id");

            $this->db->where("users.user_id", $user_id);

            $page_data['profile'] = $this->db->get('users')->row();
            $page_data['page_name']  = 'home/profile2';
            $page_data['page_title'] = 'Home | Profile';
            
            $this->load->view("index",$page_data);
        }
        
        public function our_functions(){
           $page_data['page_name']  = 'home/our_functions';
           $page_data['page_title'] = 'Home | Our Functions';
            
            $this->load->view("index",$page_data);
        }
        
        public function our_motivations(){
            $page_data['page_name']  = 'home/our_motivations';
           $page_data['page_title'] = 'Home | Our Motivations';
            
            $this->load->view("index",$page_data);
        }
        
       
        
        
        
        
        public function contact_us(){
            $page_data['page_name']  = 'home/contact_us';
           $page_data['page_title'] = 'Home';
            
            $this->load->view("index",$page_data);
        }
        
        function _show_message($message)
        	{
                    
        		$this->session->set_flashdata('login_message', $message);
                        
        		redirect('/auth/');
        	}
	   function listview($date = ""){
        if (!$this->tank_auth->is_logged_in()) {    
                
                redirect('/auth/login/');
            }
        
        $page_data['date']=$date;
        $page_data['page_name']  = 'home/listview';
        $page_data['page_title'] = 'Home';
            
        $this->load->view("index",$page_data);
        }

        function messages($hearing_id){
            if (!$this->tank_auth->is_logged_in()) {    
                
                redirect('/auth/login/');
            }



            $page_data['hearing_id'] = $hearing_id;
            $page_data['page_name']  = 'home/messages';
            $page_data['page_title'] = 'Home';
            
            $this->load->view("index",$page_data);

        }
		
		function timeline(){

            if (!$this->tank_auth->is_logged_in()) {    
                
                redirect('/auth/login/');
            }

            $hearing_messages = array();
            $date_query = $this->db->query(" 
                                
                                SELECT 
                                DATE_FORMAT(hearing_date,'%M %Y') as msg_month,
                                hearing_date

                                from hearing 
                                where hearing.users_user_id = ".$this->tank_auth->get_user_id()."
                                AND hearing.hearing_date > NOW()");
            
           //print_r($date_query->result()); die;

            foreach($date_query->result() as $row_date){

                $hearing_messages[$row_date->msg_month] = array();
                


                $query = $this->db->query(" 
                                SELECT
                                hr1.hearing_id,
                                hr1.hearing_date,
                                court.court_name,
                                court_type.type,
                                law_area.area_of_law,

                                (SELECT
                                    count(*) as 'num_msg'
                                    FROM
                                    hearing_msg
                                    INNER JOIN hearing ON hearing_msg.hearing_hearing_id = hearing.hearing_id
                                    INNER JOIN users ON hearing.users_user_id = users.user_id
                                    WHERE users.user_id = ".$this->tank_auth->get_user_id()."
                                    and hearing.hearing_id = hr1.hearing_id
                                ) as 'num_msg'
                                FROM
                                users
                                INNER JOIN hearing hr1 ON users.user_id = hr1.users_user_id
                                INNER JOIN court ON hr1.court_court_id = court.court_id
                                INNER JOIN court_type ON court.court_type_court_type_id = court_type.court_type_id
                                INNER JOIN law_area ON hr1.law_area_law_area_id = law_area.law_area_id
                                WHERE users.user_id = ".$this->tank_auth->get_user_id()."
                                and DATE_FORMAT(hr1.hearing_date,'%M %Y') = '".$row_date->msg_month."'
                                AND hr1.hearing_date > NOW()");
                foreach ($query->result() as $row) {
                    
                    $hearing_messages[$row_date->msg_month][] = $row;
                }
               
            }
    
            //print_r($hearing_messages);die;
            $page_data['hearing_messages'] = $hearing_messages;

            $page_data['page_name']  = 'home/timeline';
            $page_data['page_title'] = 'Home';
            
            $this->load->view("index",$page_data);
        }


        
}

