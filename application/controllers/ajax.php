<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
    
    function __construct()
	{
		parent::__construct();
                
                $this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
        header("content-type: application/json");
	}
        
    function _remap($method, $params){
        switch($method){
            case 'ajaxValidateFieldUser':
                if (!$this->input->is_ajax_request()) {
                    redirect('404');
                } else {
                    $this->_ajaxValidateFieldUser();
                }
                break;
            case 'update_profile':
                 if (!$this->input->is_ajax_request()) {
                    redirect('404');
                } else {
                    if(!$this->tank_auth->is_profile()){
                        $this->_create_profile();
                    } else {
                        $this->_update_profile();
                    }
                    
                }
            case 'my_hearings':
             if (!$this->input->is_ajax_request()) {
                    redirect('404');
                } else {
                    
                    $this->_my_hearings();
                    
                }
            break;
            case 'available_hearings':
            if (!$this->input->is_ajax_request()) {
                    redirect('404');
                } else {
                    
                    $this->_available_hearings();
                    
                }
            break;

            case 'send_message':
            if (!$this->input->is_ajax_request()) {
                    redirect('404');
                } else {
                    
                    $this->_send_message();
                    
                }
            break;

            case 'hearing_messages':
                if (!$this->input->is_ajax_request()) {
                    redirect('404');
                } else {
                    
                    $this->_hearing_messages($params);
                    
                }
            break;


        }
    }


    function _hearing_messages($params){
       

        
        $this->db->select('
                        users.user_id,
                        user_individual_profile.f_name,
                        user_individual_profile.m_name,
                        user_individual_profile.l_name,
                        users.email,
                        user_individual_profile.contact_no,
                        user_individual_profile.address,
                        regulatory_authority.authority,
                        user_individual_profile.regulatory_authority_no');
        $this->db->join("users","hearing_msg.users_user_id = users.user_id");
        $this->db->join("user_individual_profile","users.user_id = user_individual_profile.users_user_id");
        $this->db->join("regulatory_authority","user_individual_profile.regulatory_authority_regulatory_authority_id = regulatory_authority.regulatory_authority_id");
        
       
        
         $this->db->where('hearing_msg.hearing_hearing_id', $params[0]);
         
        $message_data = array();
        foreach($this->db->get('hearing_msg')->result() as $hearings){
            $message_data[] = array(    
                                        $hearings->user_id,
                                        $hearings->f_name . ' '. $hearings->m_name.' '.$hearings->l_name,
                                        $hearings->email,
                                        $hearings->contact_no,
                                        $hearings->address,
                                        $hearings->authority,
                                        $hearings->regulatory_authority_no
                                       );

                
         }
         
        


        echo json_encode(array("draw"=>0,
                "recordsTotal" =>2,
                "recordsFiltered" => 2,
                "data"=> $message_data));
    }

    function _send_message(){

        $data = array();
        foreach ($this->input->post() as $key => $value) {
            $data[$key] = $value;
        }
        $data['messages_message_id'] = 1;
        $data['msg_date'] = date('Y-m-d H:i:s');

        $this->db->insert('hearing_msg', $data);

        echo json_encode(array('status'=>1));


    }

    function _available_hearings(){

        $this->db->select(' hearing.hearing_id,
                            court.court_id,
                            date_format(hearing.hearing_date,"%d %b %Y %h:%i %p") as datetime,
                            date_format(hearing.hearing_date,"%d/%b/%Y") as ref_date,
                            concat(court.court_name,"<br /><small>",court.location,"</small>") as court,
                            law_area.area_of_law,
                            hearing_type.hearing_type,
                            track.track,
                            users_user_id,
                            \'fees\'',false);
        $this->db->join("court","hearing.court_court_id = court.court_id");
        $this->db->join("law_area","hearing.law_area_law_area_id = law_area.law_area_id");
        $this->db->join("hearing_type","hearing.hearing_type_hearing_type_id = hearing_type.hearing_type_id");
        $this->db->join("track","hearing.track_track_id = track.track_id");
        $this->db->where('users_user_id !=', $this->tank_auth->get_user_id());
        
        $this->db->where('hearing.hearing_date > NOW()');
         if($this->input->get('date') != ""){
            $this->db->like('hearing_date', $this->input->get('date'),'after');    
        }
        $hearing_data = array();
        foreach($this->db->get('hearing')->result() as $hearings){


            $this->db->where("users_user_id", $this->tank_auth->get_user_id());
            $this->db->where("hearing_hearing_id", $hearings->hearing_id);
            $num_rows = $this->db->get('hearing_available_for')->num_rows();

            if($hearings->users_user_id == $this->tank_auth->get_user_id() || $num_rows > 0){


                $hearing_data[] = array(    md5($hearings->hearing_id),
                                            $hearings->datetime,
                                            $hearings->court,
                                            $hearings->area_of_law,
                                            $hearings->hearing_type,
                                            $hearings->track,
                                            $hearings->fees,
                                            sprintf("%04d", $hearings->court_id),
                                            $hearings->ref_date,
                                            sprintf("%04d", $hearings->hearing_id));
            }

                
        }
        


        echo json_encode(array("draw"=>0,
                "recordsTotal" =>2,
                "recordsFiltered" => 2,
                "data"=> $hearing_data));


    }


    function _my_hearings(){

        $this->db->select(' hearing.hearing_id,
                            court.court_id,
                            date_format(hearing.hearing_date,"%d %b %Y %h:%i %p") as datetime,
                            date_format(hearing.hearing_date,"%d/%b/%Y") as ref_date,
                            concat(court.court_name,"<br /><small>",court.location,"</small>") as court,
                            law_area.area_of_law,
                            hearing_type.hearing_type,
                            track.track,
                            \'fees\'',false);
        $this->db->join("court","hearing.court_court_id = court.court_id");
        $this->db->join("law_area","hearing.law_area_law_area_id = law_area.law_area_id");
        $this->db->join("hearing_type","hearing.hearing_type_hearing_type_id = hearing_type.hearing_type_id");
        $this->db->join("track","hearing.track_track_id = track.track_id");
        if($this->input->get('date') != ""){
            $this->db->like('hearing_date', $this->input->get('date'),'after');    
        }
        $this->db->where('users_user_id', $this->tank_auth->get_user_id());
         $this->db->where('hearing.hearing_date > NOW()');
        $hearing_data = array();
        foreach($this->db->get('hearing')->result() as $hearings){
            $hearing_data[] = array(    md5($hearings->hearing_id),
                                        $hearings->datetime,
                                        $hearings->court,
                                        $hearings->area_of_law,
                                        $hearings->hearing_type,
                                        $hearings->track,
                                        $hearings->fees,
                                        sprintf("%04d", $hearings->court_id),
                                        $hearings->ref_date,
                                        sprintf("%04d", $hearings->hearing_id));

                
        }
        


        echo json_encode(array("draw"=>0,
                "recordsTotal" =>2,
                "recordsFiltered" => 2,
                "data"=> $hearing_data));


    }

    function _ajaxValidateFieldUser(){
        $table_name = 'users';
        $table_name = $this->config->item('db_table_prefix', 'tank_auth').$table_name;
	
        
        $validateValue=$_REQUEST['fieldValue'];
        $validateId=$_REQUEST['fieldId'];


        $validateError= "This username is already taken";
        $validateSuccess= "This username is available";



                /* RETURN VALUE */
                $arrayToJs = array();
                $arrayToJs[0] = $validateId;
            
            $this->db->where("username",$validateValue);
            $query= $this->db->get($table_name);
            
            
            
        if(!$query->num_rows() == 1){		// validate??
                $arrayToJs[1] = true;			// RETURN TRUE
                echo json_encode($arrayToJs);			// RETURN ARRAY WITH success
        }else{
                for($x=0;$x<1000000;$x++){
                        if($x == 990000){
                                $arrayToJs[1] = false;
                                echo json_encode($arrayToJs);		// RETURN ARRAY WITH ERROR
                        }
                }

        }
            
    }
    
    function _update_profile(){
         $this->form_validation->set_rules('f_name', 'First Name', 'trim|required|xss_clean');
	$this->form_validation->set_rules('l_name', 'Last Name', 'trim|required|xss_clean');
	$this->form_validation->set_rules('year_of_call', 'Year of Call', 'trim|required|xss_clean');
        $this->form_validation->set_rules('clerk', 'Clerk', 'trim|required|xss_clean');
        $this->form_validation->set_rules('silk', 'Silk', 'trim|required|xss_clean');
        $data['errors'] = array();
        
        if($this->form_validation->run()){
            
            if($this->tank_auth->update_profile()){
                echo json_encode(array("inserted"));
            }
        }
    }
    
    
    function _create_profile(){
        $this->form_validation->set_rules('f_name', 'First Name', 'trim|required|xss_clean');
	$this->form_validation->set_rules('l_name', 'Last Name', 'trim|required|xss_clean');
	$this->form_validation->set_rules('year_of_call', 'Year of Call', 'trim|required|xss_clean');
        $this->form_validation->set_rules('clerk', 'Clerk', 'trim|required|xss_clean');
        $this->form_validation->set_rules('silk', 'Silk', 'trim|required|xss_clean');
        $data['errors'] = array();
        
        if($this->form_validation->run()){
            
            if($this->tank_auth->insert_profile()){
                echo json_encode(array("inserted"));
            }
        }
            
//        } else {
//            $errors = $this->tank_auth->get_error_message();
//            
//                foreach ($errors as $k => $v)	{
//                    $data['errors'][$k] = $this->lang->line($v['msg']);
//                }
//            echo json_encode($errors);
//        }
        
    }
    
    
}