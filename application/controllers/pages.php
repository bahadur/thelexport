<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
    
          function __construct()
	{
		parent::__construct();
                
                $this->load->library('tank_auth');
                $this->lang->load('tank_auth');
	}
	
	public function index()
	{
           
           
           redirect('pages/about');
	}
        
        
      
        
        public function about(){
            $page_data['page_name']  = 'pages/about';
            $page_data['page_title'] = 'Home | About';
            
            $this->load->view("index",$page_data);
        }
        
      public function contact(){
            $page_data['page_name']  = 'pages/contact';
            $page_data['page_title'] = 'Home | Contact us';
            
            $this->load->view("index",$page_data);
        }
		
		 public function faq(){
            $page_data['page_name']  = 'pages/faq';
            $page_data['page_title'] = 'Home | FAQS';
            
            $this->load->view("index",$page_data);
        }
		
		public function jobs(){
            $page_data['page_name']  = 'pages/jobs';
            $page_data['page_title'] = 'Home | JOBS';
            
            $this->load->view("index",$page_data);
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */