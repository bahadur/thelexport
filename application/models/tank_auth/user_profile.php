<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users Profile
 *
 * This model represents user profile data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Bahadur
 */

class User_profile extends CI_Model{
    private $table_name			= 'users';                                      // user accounts
    private $profile_table_name         = 'user_network_profiles';                      // user profiles
    
    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Karachi');
        $ci = & get_instance();
        $this->table_name               = $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
        $this->profile_table_name	= $ci->config->item('db_table_prefix', 'tank_auth').$this->profile_table_name;
    }
    
    function getFullName(){
        //print_r($this->session->userdata('user_profile'));
        $profile = $this->session->userdata('user_profile');    
        if($this->session->userdata('type_id') == 1)
            return $profile->l_name.', '.$profile->f_name.' '.$profile->m_name;
        elseif($this->session->userdata('type_id') == 2)
            return $profile->organization_name;

    }
    


    function is_profile($user_id){
        
        $this->db->where('users_user_id', $user_id);
	
		$query = $this->db->get($this->profile_table_name);
                if ($query->num_rows() > 0) {
                    return true;
                } else {
                    return false;
                }
                    
//		if ($query->num_rows() > 0) {
//                    $row = $query->result();
//                    if($row->name == null){
//                        return false;
//                    } else {
//                        return true;
//                    }
//                }
                    
    }
    
    function getProfileDetails($user_id, $type){
        $this->db->where('users_user_id',$user_id);
        return $this->db->get($type)->row();
        
    }
    
    
    function udate_profile(){
        $insertdate = $this->input->post();

        $this->db->where('users_user_id',$this->tank_auth->get_user_id());
        if($this->db->update($this->profile_table_name, $insertdate)){
            return true;
        } else {
            return false;
        }
    }
    
    function insert_profile(){
       
        $insertdate = $this->input->post();

        $insertdate['users_user_id'] = $this->tank_auth->get_user_id();
        
        //$insertdate['users_roles_role_id'] = $this->tank_auth->getUserTypeId($this->tank_auth->profile_type($this->tank_auth->get_user_id()));
        
        if($this->db->insert($this->profile_table_name, $insertdate)){
            return true;
        } else {
            return false;
        }
        
        
    }
}
?>