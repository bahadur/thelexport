<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Roles
 *
 * Modified by bahadur
 *
 * @package	Tank_auth
 * @author	Bahadur
 */
class Roles extends CI_Model
{
	private $table_name			= 'roles';
	

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Karachi');
		$ci =& get_instance();
		$this->roles_table_name		= $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
		
	}

	/**
	 * Get user data for auto-logged in user.
	 * Return NULL if given key or user ID is invalid.
	 *
	 * @param	int
	 * @param	string
	 * @return	object
	 */
	function getRoleId($role)
	{
		$this->db->select($this->roles_table_name.'.role_id');
		$this->db->where($this->roles_table_name.'.description', $role);
		
		$query = $this->db->get($this->roles_table_name);
		if ($query->num_rows() == 1) {
                    $row = $query->row();
                    return $row->role_id;
                
                }
		return NULL;
	}

	

	
}

/* End of file user_autologin.php */
/* Location: ./application/models/auth/roles.php */