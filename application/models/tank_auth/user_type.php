<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Roles
 *
 * Modified by bahadur
 *
 * @package	Tank_auth
 * @author	Bahadur
 */
class User_Type extends CI_Model
{
	private $type_table_name			= 'user_type';
    private $users_table_name			= 'users';
	

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Karachi');
		$ci =& get_instance();
		$this->type_table_name		= $ci->config->item('db_table_prefix', 'tank_auth').$this->type_table_name;
                $this->users_table_name		= $ci->config->item('db_table_prefix', 'tank_auth').$this->users_table_name;
		
	}

	/**
	 * Get user data for auto-logged in user.
	 * Return NULL if given key or user ID is invalid.
	 *
	 * @param	int
	 * @param	string
	 * @return	object
         * Author:      Bahadur oad
	 */
	function getUserTypeId($type)
	{

		$this->db->select($this->type_table_name.'.user_type_id');
		$this->db->where($this->type_table_name.'.type', $type);
		
		$query = $this->db->get($this->type_table_name);
        
		if ($query->num_rows() == 1) {
                    $row = $query->row();
                    return $row->user_type_id;
                
                }
		return NULL;
	}
        
        function getUserType($type_id)
	{
            $this->db->where('user_type_id', $type_id);
            //$this->db->join('users', 'user_type.user_type_id = users.user_type_user_type_id');
            $query = $this->db->get($this->type_table_name);
           
		if ($query->num_rows() > 0){
                    $rs = $query->result();
           
                    return $rs[0]->type;
                }
		return NULL;
	}
        /**
         * Get Profile type
         * 
         * @return boolean
         * 
         * Author: Bahadur oad
         */
        

	

	
}

/* End of file user_autologin.php */
/* Location: ./application/models/auth/roles.php */