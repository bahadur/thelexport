<?php

// Errors
$lang['auth_incorrect_password'] = array('title'=>'Incorrect password', 'msg' => 'Incorrect password');
$lang['auth_incorrect_login'] = array('title'=>'Incorrect login', 'msg' => 'Incorrect login');
$lang['auth_incorrect_email_or_username'] = array('title'=>'Login or email doesn\'t exist', 'msg' => 'Login or email doesn\'t exist');
$lang['auth_email_in_use'] = array('title'=>'Email is already in used', 'msg' => 'Email is already used by another user. Please choose another email.');
$lang['auth_username_in_use'] = array('title'=>'Username already exists', 'msg' => 'Username already exists. Please choose another username.');
$lang['auth_current_email'] = array('title'=>'This is your current email', 'msg' => 'This is your current email');
$lang['auth_incorrect_captcha'] = array('title'=>'Confirmation code not match', 'msg' => 'Your confirmation code does not match the one in the image.');
$lang['auth_captcha_expired'] = array('title'=>'Confirmation code has expired', 'msg' => 'Your confirmation code has expired. Please try again.');

// Notifications
$lang['auth_message_logged_out'] = array('title'=>'Logout', 'msg' => 'You have been successfully logged out.');
$lang['auth_message_registration_disabled'] = array('title'=>'Registration is disabled.', 'msg' => 'Registration is disabled.');
$lang['auth_message_registration_completed_1'] = array('title'=>'Registered', 'msg' => 'You have successfully registered. Check your email address to activate your account.');
$lang['auth_message_registration_completed_2'] = array('title'=>'Successfully Registered', 'msg' => 'You have successfully registered.');
$lang['auth_message_activation_email_sent'] = array('title'=>'Activation email', 'msg' => 'A new activation email has been sent to %s. Follow the instructions in the email to activate your account.');
$lang['auth_message_activation_completed'] = array('title'=>'Account Activated.', 'msg' => 'Your account has been successfully activated.');
$lang['auth_message_activation_failed'] = array('title'=>'activation code is incorrect', 'msg' => 'The activation code you entered is incorrect or expired.');
$lang['auth_message_password_changed'] = array('title'=>'Password changed.', 'msg' => 'Your password has been successfully changed.');
$lang['auth_message_new_password_sent'] = array('title'=>'Crating new password', 'msg' => 'An email with instructions for creating a new password has been sent to you.');
$lang['auth_message_new_password_activated'] = array('title'=>'Crating new password', 'msg' => 'You have successfully reset your password');
$lang['auth_message_new_password_failed'] = array('title'=>'Activation key is incorrect', 'msg' => 'Your activation key is incorrect or expired. Please check your email again and follow the instructions.');
$lang['auth_message_new_email_sent'] = array('title'=>'Confirmation Email', 'msg' => 'A confirmation email has been sent to %s. Follow the instructions in the email to complete this change of email address.');
$lang['auth_message_new_email_activated'] = array('title'=>'Email address changed', 'msg' => 'You have successfully changed your email');
$lang['auth_message_new_email_failed'] = array('title'=>'Activation key is incorrect', 'msg' => 'Your activation key is incorrect or expired. Please check your email again and follow the instructions.');
$lang['auth_message_banned'] = array('title'=>'You are banned.', 'msg' => 'You are banned.');
$lang['auth_message_unregistered'] = array('title'=>'Your account has been deleted...', 'msg' => 'Your account has been deleted...');

// Email subjects
$lang['auth_subject_welcome'] = 'Welcome to %s!';
$lang['auth_subject_activate'] = 'Welcome to %s!';
$lang['auth_subject_forgot_password'] = 'Forgot your password on %s?';
$lang['auth_subject_reset_password'] = 'Your new password on %s';
$lang['auth_subject_change_email'] = 'Your new email address on %s';


/* End of file tank_auth_lang.php */
/* Location: ./application/language/english/tank_auth_lang.php */