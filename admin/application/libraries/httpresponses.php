<?php 


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HttpResponses
{
  
  function __construct()
  { 
    $this->_ci =& get_instance(); 
  }

  public function respondJson($message, $statusCode) {
    $this->_ci->output->set_status_header($statusCode);
    $this->_ci->output
    ->set_content_type('application/json')
    ->set_output($message);
  }

  public function respond($message, $statusCode) {
    $this->_ci->output->set_status_header($statusCode);
    $this->_ci->output 
    ->set_output($message);
  }

  public function respondJsonify($message, $statusCode) {
    $this->_ci->output->set_status_header($statusCode);
    $this->_ci->output
    ->set_content_type('application/json')
    ->set_output(json_encode($message));
  }
}