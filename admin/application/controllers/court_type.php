<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class court_type extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
         function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth');
	}
        
        public function index()
	{
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
            else
                redirect(base_url() . 'court_type/court_type_list', 'refresh');
	}
        
       public function court_type_list(){
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');

            if($this->input->post("courttypeedit")){

            	$id= $this->input->post("id");

           $data = array(
                            
                            'type' => $this->input->post('courttypeedit')
                            );
           $this->db->where('court_type_id', $id);
		   $this->db->update('court_type', $data);
		   redirect(base_url() . 'court_type/court_type_list/');
		 //	print_r($data);
		 	//die();

       }
           if($this->input->post("courttype")){

        
            $data = array(
                            
                            'type' => $this->input->post('courttype')
                 
                            );

            $this->db->insert('court_type',$data);
			redirect(base_url() . 'court_type/court_type_list/');
			}
		  
		    $page_data['page_name']  = 'court_type/court_type_list';
            $page_data['page_title'] = 'Court Types';
            
            $this->load->view("index",$page_data);
        }
      
	  
	  
		 public function court_type_del($id)
	   {
	   		//echo $id;
	 		if ($this->db->_error_number() == 1451){
			echo "aaaaaaa";
			}else{
			$this->db->where('court_type_id', $id);
 			$this->db->delete('court_type'); 
 			redirect(base_url() . 'court_type/court_type_list/');
			}
	   		
	   }

		 public function court_type_edit(){
         //edit
		 	if($this->input->post()){

		 	echo $id;
		 	$data = array(
                            
                            'type' => $this->input->post('courttype')
                 
                            );
		 	print_r($data);
		 	die();
									}
        }
		
		
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */