<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class law_area extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
         function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth');
	}
        
        public function index()
	{
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
            else
                redirect(base_url() . 'law_area/law_area_list', 'refresh');
	}
        
       public function law_area_list(){
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
      

       if($this->input->post("law_area_edit")){

            	$id= $this->input->post("id");

           $data = array(
                            
                            'area_of_law' => $this->input->post('law_area_edit')
                            );
           $this->db->where('law_area_id', $id);
		   $this->db->update('law_area', $data);
		   redirect(base_url() . 'law_area/law_area_list/');
		 //	print_r($data);
		 	//die();

       }


		     if($this->input->post("lawareaname")){

        
            $data = array(
                            
                            'area_of_law' => $this->input->post('lawareaname')
                 
                            );

            $this->db->insert('law_area',$data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Hearing Type Record is Successfully Added!</div>');

			//die();
			redirect(base_url() . 'law_area/law_area_list/');
			}
			

		    $page_data['page_name']  = 'law_area/law_area_list';
            $page_data['page_title'] = 'Law Area';
            
            $this->load->view("index",$page_data);
        }
      
	  
	   public function law_area_del($id)
	   {
	   		//echo $id;
	 		
			$this->db->where('law_area_id', $id);
 			$this->db->delete('law_area'); 
 			redirect(base_url() . 'law_area/law_area_list/');
	   		
	   }

 
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */