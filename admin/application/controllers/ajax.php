<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
                header('Content-Type: application/json');
	}
        
        function index(){
            
        }
        
        function tasks(){
            
          
            $rs = $this->db->query( 
			"SELECT SQL_CALC_FOUND_ROWS `task`,`username`,FROM_UNIXTIME(`create_date`,'%a, %b %d %Y') as 'create_date', `status`
			FROM `tasks`
            INNER JOIN `users` ON `tasks`.`client_id` = `users`.`id`
            llimit 10"
			 
			 
		)->result();
            
            $data = array();
            foreach($rs as $row){
                $data[] = array($row->task,$row->username,$row->create_date,$row->status);
            }
            
            $resFilterLength = $this->db->query( 
			"SELECT FOUND_ROWS() num_rows"
		)->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
			"SELECT COUNT(`rowid`) count
			 FROM   `tasks`"
		)->result();
		$recordsTotal = $resTotalLength[0]->count;
            
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }
		
		function courtList(){
            
            $where="";
            $order ="";
            $limit ="";
            $start =0;
            $total = "";
//echo $_GET['order'];

             if ($_GET['length']!="") 
             {
            $limit = $_GET['length'];
            $start = $_GET['start'];
            
             }

            if ($_GET['order'][0]['column']!="") {
               
         
            if($_GET['order'][0]['column']==1)
            {

                $order = "order by court_name ASC";
            } elseif ($_GET['order'][0]['column']==2) {
                 $order = "order by address ASC";
            }else
            {
                 $order = "order by location ASC";
            }
               }
          if($_GET['search']['value']!="")
          {

            $where = "where court_name like '%".$_GET['search']['value']."%' OR address like '%".$_GET['search']['value']."%' OR location like '%".$_GET['search']['value']."%'";

          }
			//"SELECT court.court_id,court.court_name, court.address, court.location FROM  court $where $order  limit $start,$limit"
            $rs = $this->db->query("
              SELECT
              court.court_name,
              court.court_id,
              court.address,
              court.location,
              count(hearing.court_court_id) as 'total_hearings'
              FROM
              court
              Left JOIN hearing ON court.court_id = hearing.court_court_id
              $where 
              GROUP BY court.court_id
              $order  limit $start,$limit"
			 
		)->result();
          
            $data = array();
            foreach($rs as $row){
                $data[] = array($row->total_hearings, $row->court_id,$row->court_name,$row->address,$row->location);
            }
            
            $resFilterLength = $this->db->query( 
			"SELECT FOUND_ROWS() num_rows"
		)->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
			"SELECT  COUNT(DISTINCT court.court_id ) count
			 FROM   `court`"
		)->result();
		$recordsTotal = $resTotalLength[0]->count;
    // echo $_GET['order'][0]['column'];
          //  print_r($_GET['length']);
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }
		



		function courtType(){
             $where="";
            $order ="";
            $limit ="";
            $start =0;
            $total = "";
//echo $_GET['order'];

             if ($_GET['length']!="") 
             {
            $limit = $_GET['length'];
            $start = $_GET['start'];
            
             }

            if ($_GET['order'][0]['column']!="") {
               
         
            if($_GET['order'][0]['column']==0)
            {

                $order = "order by type ASC";
            } 
               }
          if($_GET['search']['value']!="")
          {

            $where = "where type like '%".$_GET['search']['value']."%' ";

          }

          
            $rs = $this->db->query( 
			//"SELECT court_type_id,type FROM court_type $where $order  limit $start,$limit"
			"SELECT
			court_type_id,
			court_type.type,
			count(court.court_type_court_type_id) AS TotalCourtType
			FROM
			court_type
			$where
			LEFT JOIN court ON court_type.court_type_id = court.court_type_court_type_id
			group by court_type.type
			$order  limit $start,$limit"

			 
		)->result();
            //echo   $this->db->last_query();
           // print_r($_GET['start']);

            $data = array();
            foreach($rs as $row){
                $data[] = array($row->TotalCourtType,$row->court_type_id,$row->type);
            }
            
            $resFilterLength = $this->db->query( 
			"SELECT FOUND_ROWS() num_rows")->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
			"SELECT COUNT(DISTINCT court_type.court_type_id)  count 
			 FROM court_type"
		)->result();
		$recordsTotal = $resTotalLength[0]->count;
            
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }
		
		
		function hearing_type(){
            
                $where="";
            $order ="";
            $limit ="";
            $start ="";
            $total = "";
//echo $_GET['order'];

             if ($_GET['length']!="") 
             {
            $limit = $_GET['length'];
            $start = $_GET['start'];
            
             }

            if ($_GET['order'][0]['column']!="") {
               
         
            if($_GET['order'][0]['column']==0)
            {

                $order = "order by hearing_type";
            } 
               }
          if($_GET['search']['value']!="")
          {

            $where = "where hearing_type like '%".$_GET['search']['value']."%' ";

          }

            $rs = $this->db->query( 
			//"SELECT hearing_type_id,hearing_type FROM hearing_type $where $order ASC limit $start,$limit"
			   "SELECT 
			    hearing_type.hearing_type_id,
				hearing_type.hearing_type,
				count(hearing.hearing_type_hearing_type_id) AS HearingTotal
				FROM
				hearing_type
				$where 
				LEFT JOIN hearing ON hearing_type.hearing_type_id = hearing.hearing_type_hearing_type_id
				GROUP BY 
				hearing_type.hearing_type
				$order ASC limit $start,$limit"

			 
		)->result();
            
            $data = array();
            foreach($rs as $row){
                $data[] = array($row->HearingTotal,$row->hearing_type_id,$row->hearing_type);
               
            }
            
            $resFilterLength = $this->db->query( 
			"SELECT FOUND_ROWS() num_rows"
		)->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
			"SELECT COUNT(DISTINCT hearing_type_id)  count 
			 FROM hearing_type"
		)->result();
		$recordsTotal = $resTotalLength[0]->count;
            
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }

        function law_area(){
            
            $where="";
            $order ="";
            $limit ="";
            $start ="";
            $total = "";
//echo $_GET['order'];

             if ($_GET['length']!="") 
             {
            $limit = $_GET['length'];
            $start = $_GET['start'];
            
             }

            if ($_GET['order'][0]['column']!="") {
               
         
            if($_GET['order'][0]['column']==0)
            {

                $order = "order by area_of_law";
            } 
               }
          if($_GET['search']['value']!="")
          {

            $where = "where area_of_law like '%".$_GET['search']['value']."%'";

          }
            $rs = $this->db->query( 
     // "SELECT law_area.law_area_id, law_area.area_of_law FROM law_area $where $order ASC limit $start,$limit"
	 
	 			"SELECT 
			    law_area.law_area_id,
				law_area.area_of_law,
				count(hearing.law_area_law_area_id) AS LawAreaTotal
				FROM
				law_area
				$where 
				LEFT JOIN hearing ON law_area.law_area_id = hearing.law_area_law_area_id
				GROUP BY 
				law_area.law_area_id
				$order ASC limit $start,$limit"
				
       
       
    )->result();
         // echo   $this->db->last_query();
            $data = array();
            foreach($rs as $row){
                $data[] = array($row->LawAreaTotal,$row->law_area_id,$row->area_of_law);
            }
            
            $resFilterLength = $this->db->query( 
      "SELECT FOUND_ROWS() num_rows"
    )->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
      "SELECT  COUNT(DISTINCT law_area.law_area_id ) count
       FROM   `law_area`"
    )->result();
    $recordsTotal = $resTotalLength[0]->count;
    // echo $_GET['order'][0]['column'];
          //  print_r($_GET['length']);
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }


         function user_admin(){
            
            $where="";
            $order ="";
            $limit ="";
            $start ="";
            $total = "";
//echo $_GET['order'];

             if ($_GET['length']!="") 
             {
            $limit = $_GET['length'];
            $start = $_GET['start'];
            
             }

            if ($_GET['order'][0]['column']!="") {
               
         
            if($_GET['order'][0]['column']==1)
            {

                $order = "order by users.username ASC";
            } 

            if($_GET['order'][0]['column']==2)
            {

                $order = "order by hearing.hearing_id ASC";
            }
               }
          if($_GET['search']['value']!="")
          {

            $where = "where users.username like '%".$_GET['search']['value']."%' AND users.roles_role_id=3";

          }else{ $where = "where users.roles_role_id=3 and user_type_user_type_id=2";}

            $rs = $this->db->query("SELECT users.user_id,users.username,COUNT(hearing.hearing_id) as TotalHearing FROM users INNER JOIN user_type ON user_type.user_type_id = users.user_type_user_type_id LEFT JOIN hearing ON hearing.users_user_id = users.user_id $where AND hearing.hearing_date > CURDATE()  GROUP BY users.username $order  limit $start,$limit")->result();
         // echo   $this->db->last_query();
            $data = array();
            foreach($rs as $row){
                $data[] = array($row->user_id,$row->username,$row->TotalHearing);
            }
           // print_r($data);
            $resFilterLength = $this->db->query( 
      "SELECT FOUND_ROWS() num_rows"
    )->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
      "SELECT  COUNT(DISTINCT users.user_id) count
       FROM   `users`"
    )->result();
    $recordsTotal = $resTotalLength[0]->count;
    // echo $_GET['order'][0]['column'];
          //  print_r($_GET['length']);
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }
    
  function user_individual(){
            
            $where="";
            $order ="";
            $limit ="";
            $start ="";
            $total = "";
//echo $_GET['order'];

             if ($_GET['length']!="") 
             {
            $limit = $_GET['length'];
            $start = $_GET['start'];
            
             }

            if ($_GET['order'][0]['column']!="") {
               
         
            if($_GET['order'][0]['column']==1)
            {

                $order = "order by users.username ASC";
            } 

            if($_GET['order'][0]['column']==2)
            {

                $order = "order by hearing.hearing_id ASC";
            }
               }
          if($_GET['search']['value']!="")
          {

            $where = "where users.username like '%".$_GET['search']['value']."%' AND users.roles_role_id=1";

          }else{ $where = "where users.roles_role_id=3 and user_type_user_type_id=1";}

            $rs = $this->db->query("SELECT users.user_id,users.username,COUNT(hearing.hearing_id) as TotalHearing FROM users INNER JOIN user_type ON user_type.user_type_id = users.user_type_user_type_id LEFT JOIN hearing ON hearing.users_user_id = users.user_id $where AND hearing.hearing_date > CURDATE()  GROUP BY users.username $order limit $start,$limit")->result();
          //echo   $this->db->last_query();
            $data = array();
            foreach($rs as $row){
                $data[] = array($row->user_id,$row->username,$row->TotalHearing);
            }
           // print_r($data);
            $resFilterLength = $this->db->query( 
      "SELECT FOUND_ROWS() num_rows"
    )->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
      "SELECT  COUNT(DISTINCT users.user_id) count
       FROM   `users`"
    )->result();
    $recordsTotal = $resTotalLength[0]->count;
    // echo $_GET['order'][0]['column'];
          //  print_r($_GET['length']);
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }
    


    function user_others(){
            
            $where="";
            $order ="";
            $limit ="";
            $start ="";
            $total = "";
//echo $_GET['order'];

             if ($_GET['length']!="") 
             {
            $limit = $_GET['length'];
            $start = $_GET['start'];
            
             }

            if ($_GET['order'][0]['column']!="") {
               
         
            if($_GET['order'][0]['column']==1)
            {

                $order = "order by users.username ASC";
            } 

            if($_GET['order'][0]['column']==2)
            {

                $order = "order by hearing.hearing_id ASC";
            }
               }
          if($_GET['search']['value']!="")
          {

            $where = "where users.username like '%".$_GET['search']['value']."%' AND users.roles_role_id =3";

          }else{ $where = "where users.roles_role_id=3 and user_type_user_type_id=3";}

            $rs = $this->db->query("SELECT users.user_id,users.username,COUNT(hearing.hearing_id) as TotalHearing FROM users INNER JOIN user_type ON user_type.user_type_id = users.user_type_user_type_id LEFT JOIN hearing ON hearing.users_user_id = users.user_id $where  AND hearing.hearing_date > CURDATE() GROUP BY users.username $order limit $start,$limit")->result();
          //echo   $this->db->last_query();
            $data = array();
            foreach($rs as $row){
                $data[] = array($row->user_id,$row->username,$row->TotalHearing);
            }
           // print_r($data);
            $resFilterLength = $this->db->query( 
      "SELECT FOUND_ROWS() num_rows"
    )->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
      "SELECT  COUNT(DISTINCT users.user_id) count
       FROM   `users`"
    )->result();
    $recordsTotal = $resTotalLength[0]->count;
    // echo $_GET['order'][0]['column'];
          //  print_r($_GET['length']);
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }
    

    function track_list(){
             $where="";
            $order ="";
            $limit ="";
            $start =0;
            $total = "";
//echo $_GET['order'];

             if ($_GET['length']!="") 
             {
            $limit = $_GET['length'];
            $start = $_GET['start'];
            
             }

            if ($_GET['order'][0]['column']!="") {
               
         
            if($_GET['order'][0]['column']==0)
            {

                $order = "order by track ASC";
            } 
               }
          if($_GET['search']['value']!="")
          {

            $where = "where track like '%".$_GET['search']['value']."%' ";

          }

          
            $rs = $this->db->query( 
      "SELECT track_id,track FROM track $where $order  limit $start,$limit"
       
       
    )->result();
            //echo   $this->db->last_query();
           // print_r($_GET['start']);

            $data = array();
            foreach($rs as $row){
                $data[] = array($row->track_id,$row->track);
            }
            
            $resFilterLength = $this->db->query( 
      "SELECT FOUND_ROWS() num_rows")->result();
            $recordsFiltered = $resFilterLength[0]->num_rows;
            
            $resTotalLength = $this->db->query( 
      "SELECT COUNT(DISTINCT track_id)  count 
       FROM track"
    )->result();
    $recordsTotal = $resTotalLength[0]->count;
            
            echo json_encode( array('sEcho'=>$_GET['draw'],
                            'iTotalRecords' => intval( $recordsTotal ),
                            'iTotalDisplayRecords'=>intval( $recordsFiltered ),
                            'aaData'=>$data));
        }
    
    
}