<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usersall extends CI_Controller {

	
    function __construct(){
    	
		parent::__construct();

		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
		if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
                
	}
        
    function index(){
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
            else
                redirect(base_url() . 'usersall/user_admin_list', 'refresh');
	}
        
    function user_admin_list(){
            
     
      if($this->input->post("user_admin_edit")){

            	$id= $this->input->post("id");

           $data = array(
                            'username' => $this->input->post('user_admin_edit'),
                            'roles_role_id' => $this->input->post('role_id')
                             );
           $this->db->where('user_id', $id);
		   $this->db->update('users', $data);
		  print_r($data);
		// echo "edit";
		// 	die();
		   redirect(base_url() . 'usersall/user_admin_list');
		}

		    $page_data['page_name']  = 'usersall/user_admin_list';
            $page_data['page_title'] = 'User List';
            
            $this->load->view("index",$page_data);
        }
      
	function user_others(){
           
      
		    $page_data['page_name']  = 'usersall/user_others';
            $page_data['page_title'] = 'User List';
            
            $this->load->view("index",$page_data);
        }

        function user_individual(){
           
      
		    $page_data['page_name']  = 'usersall/user_individual';
            $page_data['page_title'] = 'User individual List';
            
            $this->load->view("index",$page_data);
        }

	function user_del($id){
	   		//echo $id;
	 		
			$this->db->where('user_id', $id);
 			$this->db->delete('users'); 
 			redirect(base_url() . 'usersall/user_admin_list/');
	   		
	}
	
}
