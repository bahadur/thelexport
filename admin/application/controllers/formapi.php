<?php
class formapi extends CI_Controller {

  function __construct() {

    parent::__construct(); 
    $this->load->library('Datatables');
    $this->load->library('httpresponses'); 

  }
 

  
  public function courtList(){

   $this->datatables->set_database("default");
    

      if($this->input->is_ajax_request()){


      
      

      $this->datatables->select("
      court.court_id,
      count(hearing.court_court_id) as 'total_hearings',
      court.court_name,
      court.address,
      court.location

      ", false)
      ->join('hearing','court.court_id = hearing.court_court_id','Left')
      ->from('`court`')
      ->group_by("court.court_id");
      
      


      
      

       echo $this->datatables->generate();
      


    }

  }




}
