<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modals extends CI_Controller {

         function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth');
	}
       
function law_area($id)
      {
      // echo $id;
		  $query = $this->db->query("select * from law_area where law_area_id =".$id);
//print_r($query->result());

		  $page_data['law_area'] = $query->row();
		 //  $page_data['page_name']  = 'modals/court_type';
          $page_data['page_title'] = 'Law Area Detail';
            
        $this->load->view("modals/law_area",$page_data);

      }


      function court_type($id)
      {
      // echo $id;
		  $query = $this->db->query("select * from court_type where court_type_id =".$id);
//print_r($query->result());

		  $page_data['CourtType'] = $query->row();
		 //  $page_data['page_name']  = 'modals/court_type';
          $page_data['page_title'] = 'Court Type Detail';
            
        $this->load->view("modals/court_type",$page_data);

      }
		
function hearing_type($id)
      {
      // echo $id;
		  $query = $this->db->query("select * from hearing_type where hearing_type_id =".$id);
//print_r($query->result());

		  $page_data['HearingType'] = $query->row();
		 //  $page_data['page_name']  = 'modals/court_type';
          $page_data['page_title'] = 'Hearing Type Detail';
            
        $this->load->view("modals/hearing_type",$page_data);

      }
		function court_list($id)
      {
      // echo $id;
		  $query = $this->db->query("select * from court where court_id =".$id);
//print_r($query->result());

		  $page_data['Court'] = $query->row();
		 //  $page_data['page_name']  = 'modals/court_type';
          $page_data['page_title'] = 'Court List Detail';
            
        $this->load->view("modals/court_list",$page_data);

      }


      function user_admin_list($id)
      {
      // echo $id;
      $query = $this->db->query("SELECT users.user_id,users.username,roles.description FROM users INNER JOIN roles ON roles.role_id = users.roles_role_id where users.user_id=".$id);
//print_r($query->result());

      $page_data['useradmin'] = $query->row();
     //  $page_data['page_name']  = 'modals/court_type';
          $page_data['page_title'] = 'Users List Detail';
            
        $this->load->view("modals/user_admin_list",$page_data);

      }
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */