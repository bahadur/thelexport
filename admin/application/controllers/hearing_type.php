<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class hearing_type extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
         function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth');
	}
        
        public function index()
	{
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
            else
                redirect(base_url() . 'hearing_type/hearing_type_list', 'refresh');
	}
        
       public function hearing_type_list(){
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
       if($this->input->post("hearing_type_edit")){

            	$id= $this->input->post("id");

           $data = array(
                            
                            'hearing_type' => $this->input->post('hearing_type_edit')
                            );
           $this->db->where('hearing_type_id', $id);
		   $this->db->update('hearing_type', $data);
		   redirect(base_url() . 'hearing_type/hearing_type_list/');
		 //	print_r($data);
		 	//die();

       }


		     if($this->input->post("hearingtype")){

        
            $data = array(
                            
                            'hearing_type' => $this->input->post('hearingtype')
                 
                            );

            $this->db->insert('hearing_type',$data);
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Hearing Type Record is Successfully Added!</div>');

			//die();
			redirect(base_url() . 'hearing_type/hearing_type_list/');
			}
			

		    $page_data['page_name']  = 'hearing_type/hearing_type_list';
            $page_data['page_title'] = 'Court Types';
            
            $this->load->view("index",$page_data);
        }
      
	  
	   public function hearing_type_del($id)
	   {
	   		//echo $id;
	 		
			$this->db->where('hearing_type_id', $id);
 			$this->db->delete('hearing_type'); 
 			redirect(base_url() . 'hearing_type/hearing_type_list/');
	   		
	   }

 public function hearing_type_edit($id)
	   {
	   		echo $id;
	 		
		//	$this->db->where('hearing_type_id', $id);
 		//	$this->db->update('hearing_type'); 
 		//	redirect(base_url() . 'hearing_type/hearing_type_list/');
	   		die();
	   }
		
		 public function hearing_type_add(){
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
          
		  
        if($this->input->get()){

        
           print_r($this->input->post());
           die();
			}
		  
		 
        }
		
		
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */