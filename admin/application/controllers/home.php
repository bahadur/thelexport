<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
         function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth');
	}
        
        public function index()
	{
             if(!$this->tank_auth->is_logged_in())
                 redirect(base_url() . 'auth/login/');
             else
               redirect(base_url() . 'home/dashboard', 'refresh');
	}
        
        public function dashboard(){
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
            $page_data['page_name']  = 'admin/dashboard';
            $page_data['page_title'] = 'admin_dashboard';
            
            $this->load->view("index",$page_data);
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */