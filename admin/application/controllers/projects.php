<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
         function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth');
	}
        
        public function index()
	{
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
            else
                redirect(base_url() . 'projects/project_list', 'refresh');
	}
        
        public function project_list(){
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
            $page_data['page_name']  = 'project_list';
            $page_data['page_title'] = 'Project List';
            
            $this->load->view("index",$page_data);
        }
        
        
        public function upload_doc(){
            $this->load->library('DocxConversion');
            
            $fileName = $_FILES['userfile']['name'];
            $tmpName  = $_FILES['userfile']['tmp_name'];
            $fileSize = $_FILES['userfile']['size'];
            $fileType = $_FILES['userfile']['type'];
            move_uploaded_file($tmpName, 'assets/docs/'.$fileName);
            //echo $fileType;die;
            $docObj = new DocxConversion('assets/docs/'.$fileName);
            
            $content =   $docObj->convertToText();
//            $fp      = fopen($tmpName, 'r');
//            $content = fread($fp, filesize($tmpName));
//            $content = addslashes($content);
//            fclose($fp);

            if(!get_magic_quotes_gpc())
            {
                $fileName = addslashes($fileName);
            }
            
            $data = array("name" => $fileName,
                            "type" => $fileType,
                            "size" => $fileSize,
                            "content" => $content);
            $this->db->insert("documents",$data);
            
            
            

            echo "<br>File $fileName uploaded<br>";
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */