<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function index() {
        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url(), 'refresh');
            
        $this->login();
        //$this->load->view("login");
    }

    public function login() {
        
        if ($this->session->userdata('admin_login') == 1)
                redirect(base_url(), 'refresh');
        $this->load->view("login");
    }

    public function ajax_login() {

        $response = array();

        $userid = $_POST['userid'];
        $password = $_POST['password'];
        $response['submitted_data'] = $_POST;

        $login_status = $this->validate_login($userid, $password);
      
        $response['login_status'] = $login_status;
        if ($login_status == 'success') {
            $response['redirect_url'] = '';
        }

        //Replying ajax request with validation response
        echo json_encode($response);
    }

    public function validate_login($userid = '', $password = '') {

        if ($userid == "admin" && $password == "admin123") {
            $this->session->set_userdata('admin_login', '1');
            $this->session->set_userdata('admin_id', '1');
            $this->session->set_userdata('name', 'admin');
            $this->session->set_userdata('login_type', 'admin');
            return "success";
        } else {
            return "invalid";
        }
    }
    
    function logout()
    {
        $this->session->unset_userdata();
        $this->session->sess_destroy();
        $this->session->set_flashdata('logout_notification', 'logged_out');
        redirect(base_url() , 'refresh');
    }

}
