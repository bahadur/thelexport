<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Court extends CI_Controller {

	
	
         function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth');
	}
        
        public function index()
	{
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
            else
                redirect(base_url() . 'court/court_list', 'refresh');
	}
        
        public function court_list(){
            if(!$this->tank_auth->is_logged_in())
                redirect(base_url() . 'auth/login/');
          


            if($this->input->post("courtlistnameedit")){

            	$id= $this->input->post("id");

           $data = array(
                            
                            'court_name' => $this->input->post('courtlistnameedit'),
                            'address' => $this->input->post('courtlistadd'),
                            'location' => $this->input->post('courtlistloc'),
                            'court_type_court_type_id' => $this->input->post('court_type_id'),
                            'updated_at'               => date("Y-m-d H:i:s"),
                            'updated_by'               => $this->tank_auth->get_user_id(),
                            );
           $this->db->where('court_id', $id);
		   $this->db->update('court', $data);
		 // print_r($data);
		// echo "edit";
		// 	die();
		   redirect(base_url() . 'court/court_list/');
		 	

       }

		  if($this->input->post("courtlistname")){

        
            $data = array(
                            
                            'court_name'                => $this->input->post('courtlistname'),
                            'address'                   => $this->input->post('courtlistadd'),
                            'location'                  => $this->input->post('courtlistloc'),
                            'court_type_court_type_id'  => $this->input->post('court_type_id'),
                            'status'                    => 1,
                            'created_at'                => date("Y-m-d H:i:s"),
                            'created_by'                => $this->tank_auth->get_user_id(),

                 
                            );

            $this->db->insert('court',$data);
           // print_r($data);
           // echo "done";
		 //	die();
			redirect(base_url() . 'court/court_list/');
			}
			
		    $page_data['page_name']  = 'court/court_list';
            $page_data['page_title'] = 'Court List';
            
            $this->load->view("index",$page_data);
        }
        
       
        

        public function court_list_del($id)
        {
           
           $this->db->where('court_id', $id);
 			$this->db->delete('court'); 
 			
			redirect(base_url() . 'court/court_list/');
		}
		  
		public function court_list_edit($id)
        {
           
           //edit Update
        	//echo $id;
        	/*
        	$data = array(
                            
                            'court_name' => $this->input->post('courtlistname'),
                            'address' => $this->input->post('courtlistadd'),
                            'location' => $this->input->post('courtlistloc'),
                            'court_type_court_type_id' => $this->input->post('court_type_id'),
                         );

			$this->db->where('id',$id);
			$this->db->update('login_table',$data);

 			redirect(base_url() . 'court/court_list/');
 			*/
 			
		}
		
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */