<!-- DATA TABLES -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/select2/select2-bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/select2/select2.css">
<script src="<?php echo base_url()?>assets/select2/select2.min.js"></script>

<div class="row">
    
    <div class="col-xs-12">
                <div class="box-header">
                   <h3 class="box-title">Tasks from clients</h3>
                 </div>
        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">

        <li class="active"><a href="#view" data-toggle="tab">Court List View</a></li>
        <li class><a href="#Add" data-toggle="tab">Add Court List</a></li>
        </ul>

      <div class="tab-content"> 
                <div class="tab-pane active" id="view">

                     <div class="box-body table-responsive">
                              <table id="table_tasks" class="table table-bordered table-striped">
                                       <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Total Hearings</th>
                                                <th>Court Name</th>
                                                <th>Address</th>
                                                <th>Location</th>
                                                <th>Action</th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Total Hearings</th>
                                                <th>Court Name</th>
                                                <th>Address</th>
                                                <th>Location</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                    </div><!-- /.box-body -->
                 </div>
        <div class="tab-pane" id="Add">
          <div class="box-body">
                        
            <form class="nobottommargin" method="post">
              
              <div class="row">
                <div class="col-md-4">

                  <div class="form-group">
                     
                      <label for="exampleInputEmail1">Select Court Type</label>
                      <select class="form-control select2" name="court_type_id" id="court_type_id">
                      <?php 
                      foreach($this->db->get('court_type')->result() as $court){ 
                        echo "<option value='$court->court_type_id'>$court->type</option>";
                      }
                      ?>
                      </select>
                    
                  </div>

                  <div class="form-group">
                    
                      <label for="exampleInputEmail1">Name</label>
                      <input type="text" class="form-control" placeholder="" name="courtlistname">
                    
                  </div>

                  <div class="form-group">
                     
                      <label for="exampleInputEmail1">Address</label>
                      <input type="text" class="form-control" placeholder="" name="courtlistadd">
                    
                  </div>
         
                  <div class="form-group">
                    
                      <label for="exampleInputEmail1">Location</label>
                      <input type="text" class="form-control" placeholder="" name="courtlistloc">
                      <input type="hidden" name="addcourt" value="addcourt">
                    
                  </div>
         
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" value="register" id="register-form-submit" name="register-form-submit">Submit</button>
                  </div>   
                </div>

                <div class="col-md-8">
                  <!--
                  <input id="gadres" type="text" placeholder="Type a place name" class="pure-input-2-3" required="">
                  <button title="Find Lat &amp; Long" class="pure-button pure-button-primary">Find</button>

                  <input type="text" name="lat" id="lat" placeholder="lat coordinate">
                  <input type="text" name="lng" id="lng" placeholder="long coordinate">
                  <div id="latlongmap"></div>
                  <div id="latlngspan"></div>
                  <div id="dms-lat"></div>
                  <div id="mlat"></div>
                  -->
                  
                </div>

              </div>      
            </form>

                
                
        </div>
      </div>

    </div>

     
            
 
<!-- DATA TABES SCRIPT -->
<script src="<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">



    var map;
    var geocoder;
    var marker;
    var infowindow;

    function initialize() {
        var latlng = new google.maps.LatLng(1.10, 1.10);
        var myOptions = {
            zoom: 2,
            center: latlng,
            panControl: true,
            scrollwheel: false,
            scaleControl: true,
            overviewMapControl: true,
            overviewMapControlOptions: { opened: true },
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        map = new google.maps.Map(document.getElementById("latlongmap"),
                myOptions);
        geocoder = new google.maps.Geocoder();
        marker = new google.maps.Marker({
            position: latlng,
            map: map
        });

        map.streetViewControl = false;
        infowindow = new google.maps.InfoWindow({
            content: "(1.10, 1.10)"
        });

        google.maps.event.addListener(map, 'click', function(event) {
            marker.setPosition(event.latLng);

            var yeri = event.latLng;

            var latlongi = "(" + yeri.lat().toFixed(6) + ", " +yeri.lng().toFixed(6) + ")";

            infowindow.setContent(latlongi);

            document.getElementById('lat').value = yeri.lat().toFixed(6);
            document.getElementById('lng').value = yeri.lng().toFixed(6);
            document.getElementById('latlngspan').innerHTML =  latlongi;
            
            document.getElementById('coordinatesurl').value = 'http://www.latlong.net/c/?lat='
                    + yeri.lat().toFixed(6) + '&long='
                    + yeri.lng().toFixed(6);
            document.getElementById('coordinateslink').innerHTML = '&lt;a href="http://www.latlong.net/c/?lat='
                        + yeri.lat().toFixed(6) + '&amp;long='
                        + yeri.lng().toFixed(6) + '" target="_blank"'+ '&gt;(' 
                        + yeri.lat().toFixed(6) + ', '
                        + yeri.lng().toFixed(6) + ')&lt;/a&gt;';
                
            dec2dms();
        });


        google.maps.event.addListener(map, 'mousemove', function(event) {
            var yeri = event.latLng;
            document.getElementById("mlat").innerHTML = "(" + yeri.lat().toFixed(6) + ", " +yeri.lng().toFixed(6)+ ")";
        });

    }

    function codeAddress() {
        var address = document.getElementById("gadres").value;
        if (address == '') {
            alert("Address can not be empty!");
            return;
        }
        geocoder.geocode({'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                document.getElementById('lat').value = results[0].geometry.location.lat().toFixed(6);
                document.getElementById('lng').value = results[0].geometry.location.lng().toFixed(6);
                var latlong = "(" + results[0].geometry.location.lat().toFixed(6) + ", " +
                        +results[0].geometry.location.lng().toFixed(6)+ ")";

                document.getElementById('latlngspan').innerHTML =  latlong;
                document.getElementById('coordinatesurl').value = 'http://www.latlong.net/c/?lat='
                        + results[0].geometry.location.lat().toFixed(6) + '&long='
                        + results[0].geometry.location.lng().toFixed(6);
                
                document.getElementById('coordinateslink').innerHTML = '&lt;a href="http://www.latlong.net/c/?lat='
                        + results[0].geometry.location.lat().toFixed(6) + '&amp;long='
                        + results[0].geometry.location.lng().toFixed(6) + '" target="_blank"'+ '&gt;(' 
                        + results[0].geometry.location.lat().toFixed(6) + ', '
                        + results[0].geometry.location.lng().toFixed(6) + ')&lt;/a&gt;';

                marker.setPosition(results[0].geometry.location);
                map.setZoom(16);
                infowindow.setContent(latlong);

                if (infowindow) {
                    infowindow.close();
                }

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });

                infowindow.open(map, marker);

                dec2dms();
            } else {
                alert("Lat and long cannot be found.");
            }
        });
    }

    function dec2dms( )
    {
        var mylat = document.getElementById("lat").value;
        var mylng = document.getElementById("lng").value;
        var scriptUr1 = "dec2dms.php?lat=" + mylat;
        var scriptUr2 = "dec2dms.php?long=" + mylng;
        $.ajax({
            url: scriptUr1,
            type: 'get',
            dataType: 'html',
            async: true,
            success: function(data) {
                result = data;
                $('#dms-lat').html(result);
            }
        });
        $.ajax({
            url: scriptUr2,
            type: 'get',
            dataType: 'html',
            async: true,
            success: function(data) {
                result = data;
                $('#dms-lng').html(result);
            }
        });

    }

    function loadScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
       // script.src = 'https://maps.googleapis.com/maps/api/js?sensor=false&' +
        //        'callback=initialize';
        script.src = 'https://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize';
        document.body.appendChild(script);
    }

    window.onload = loadScript;






$(function() {
    $('#table_tasks').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax:{
          url: '<?php echo base_url()?>formapi/courtList/',
          type: 'post'
        },
        
        columns: [           
          { 
            data: "court_id" 
          },
          { 
            data: "total_hearings" 
          },
          { 
            data: "court_name" 
          },
          { 
            data: "address" 
          },
          { 
            data: "location" 
          },
          { 
            data: "action" 
          }
        ],

        columnDefs: [ 
          {
            "targets": 0,
            "visible":false
          },
          {
            "targets": 1,
            "visible":false,
            "searchable": false
          },
          {
            "searchable": false,
            "targets": 5,
            "render": function(data, type, full) {
                
               

                var ret =  '<a href="javascript:;" onclick="showAjaxModal('+full['court_id']+');" class="btn btn-default">Edit</a>';
                if(full['total_hearings'] > 0){
                  ret += ' | <a href="javascript:void(0);" onclick="deleteDate('+full['court_id']+')" class="btn btn-default" disabled>Delete</a>';
                  ret += '<p class="text-warning">Can not delete. This court has child records in hearings</p>';
                }
                else{
                  ret += ' | <a href="javascript:void(0);" onclick="deleteDate('+full['court_id']+')" class="btn btn-default">Delete</a> |';
                }
                return ret;
            }
          }
        ]
    });
});
</script>


<script type="text/javascript">
    var url="<?php echo base_url();?>";
    function deleteDate(id){
    //  alert('');
       var r=confirm("Do you want to delete this?")
        if (r==true)
          window.location = url+"court/court_list_del/"+id;
        else
          return false;
        } 
</script>


<script type="text/javascript">
    function showAjaxModal(id)
{
jQuery('#myModal').modal('show', {backdrop: 'static'});

$.ajax({
url: "<?php echo base_url()?>modals/court_list/"+id,
success: function(response)
{
jQuery('#myModal .modal-body').html(response);
}
});
}
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
     <form class="form-horizontal nobottommargin" method="post">
    
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Court Details Edit</h4>
      </div>
      <div class="modal-body">
    
      
      </div>
      <div class="modal-footer">
        
      </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">

$(document).ready(function () {

  $("#court_type_id").select2({
    placeholder: "Type of Court",
    allowClear: true
  });
  
});
</script>