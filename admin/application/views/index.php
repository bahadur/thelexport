<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>The Lexport | <?php echo ucwords($this->router->fetch_class());?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <?php include 'include_top.php';?>
       <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
         <?php include 'include_bottom.php';?>
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <?php include('header.php') ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include('navigator.php') ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo ucwords(str_replace("_"," ",$this->router->fetch_method()));?>
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo ucwords($this->router->fetch_class());?></a></li>
                        <li class="active"><?php echo ucwords(str_replace("_"," ",$this->router->fetch_method()));?></li>
                    </ol>
                </section>

                <!-- Main content -->

                <section class="content">
                <?php include ($page_name.'.php') ?>
                </section><!-- /.content -->
                
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        

    </body>
</html>
