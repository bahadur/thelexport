<!-- DATA TABLES -->
<script src="//cdn.datatables.net/responsive/1.0.3/js/dataTables.responsive.js"></script>
<link href="<?php echo base_url() ?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="//cdn.datatables.net/responsive/1.0.3/css/dataTables.responsive.css" rel="stylesheet" type="text/css" />

<div class="row">
    
 <div class="col-xs-12">
                <div class="box-header">
                   <h3 class="box-title">Tasks from clients</h3>
                 </div>
        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
   <li class="active"><a href="#individual" data-toggle="tab">Individual Users</a></li>
        <li class><a href="#admin" data-toggle="tab">Organization Users</a></li>
        <li class><a href="#user" data-toggle="tab">Member Users</a></li>

        </ul>
      <div class="tab-content"> 
                <div class="tab-pane" id="admin">
                                <div class="box-body table-responsive">
                                    <table id="table_tasks1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                         
                                               <th>User ID</th>     
                                                <th>User Name</th>  
                                                <th>Number of Hearings</th> 
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                           
                                               <th>User ID</th>     
                                                <th>User Name</th>  
                                                <th>Number of Hearings</th>  
                                                <th>Action</th> 
                                            </tr>

                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
 
 <div class="tab-pane" id="user">
                                <div class="box-body table-responsive">
                                    <table id="table_tasks2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                              
                                                <th>User ID</th>     
                                                <th>User Name</th>  
                                                <th>Number of Hearings</th> 
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                           
                                                <th>User ID</th>     
                                                <th>User Name</th>  
                                                <th>Number of Hearings</th>  
                                                <th>Action</th> 
                                            </tr>

                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->

                            <div class="tab-pane active" id="individual">
                                <div class="box-body table-responsive">
                                    <table id="table_tasks3" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                              
                                                <th>User ID</th>     
                                                <th>User Name</th>  
                                                <th>Number of Hearings</th> 
                                                <th>Action</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                           
                                                <th>User ID</th>     
                                                <th>User Name</th>  
                                                <th>User Type</th>  
                                                <th>Action</th> 
                                            </tr>

                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
</div>

<!-- DATA TABES SCRIPT -->
<script src="<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>


<script type="text/javascript">
$(function() {
    $('#table_tasks1').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax:'<?php echo base_url()?>ajax/user_admin/',
        "columnDefs": [ 
{
           "targets": 0,
           "visible":false
           
         },
        {
           "targets": 3,
           "render": function ( data, type, full ) {
                return '<a href="<?php echo base_url()?>usersall/user_del/'+full[0]+'" class="btn btn-default">Delete</a> | <a href="javascript:;" onclick="showAjaxModal('+full[0]+');" class="btn btn-default">Edit</a>';
             }
         } ]
    });

       $('#table_tasks2').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax:'<?php echo base_url()?>ajax/user_others/',
        "columnDefs": [ 
{
           "targets": 0,
           "visible":false
           
         },
        {
           "targets": 3,
           "render": function ( data, type, full ) {
                return '<a href="<?php echo base_url()?>userall/user_del/'+full[0]+'" class="btn btn-default">Delete</a> | <a href="javascript:;" onclick="showAjaxModal('+full[0]+');" class="btn btn-default">Edit</a>';
             }
         } ]
    });

       $('#table_tasks3').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax:'<?php echo base_url()?>ajax/user_individual/',
        "columnDefs": [ 
{
           "targets": 0,
           "visible":false
           
         },
        {
           "targets": 3,
           "render": function ( data, type, full ) {
                return '<a href="<?php echo base_url()?>usersall/user_del/'+full[0]+'" class="btn btn-default">Delete</a> | <a href="javascript:;" onclick="showAjaxModal('+full[0]+');" class="btn btn-default">Edit</a>';
             }
         } ]
    });
},

$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
  
})


);


</script>
<script type="text/javascript">
    function showAjaxModal(id)
{
jQuery('#myModal').modal('show', {backdrop: 'static'});

$.ajax({
url: "<?php echo base_url()?>modals/user_admin_list/"+id,
success: function(response)
{
jQuery('#myModal .modal-body').html(response);
}
});
}
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
     <form class="form-horizontal nobottommargin" method="post">
    
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">User Edit</h4>
      </div>
      <div class="modal-body">
    
      
      </div>
      <div class="modal-footer">
       
        
      </div>
      </form>
    </div>
  </div>