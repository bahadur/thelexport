<aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url()?>assets/img/avatar5.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $this->tank_auth->get_username()?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        
                        <li <?php echo ($this->router->fetch_class() == "home")?"class='active'":""?>>
                            <a href="<?php echo base_url()?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                       
                         <li class="treeview">
                            <a href="#">
                            <i class="fa fa-university"></i>
                            <span>Court</span>
                            <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url()?>court/court_list">
                                <i class="fa fa-circle-o"></i>Courts</a></li>
                                  <li><a href="<?php echo base_url()?>court_type/court_type_list"><i class="fa fa-circle-o"></i>Court Types</a></li>
                                <li><a href="<?php echo base_url()?>hearing_type/hearing_type_list"><i class="fa fa-circle-o"></i>Hearing Types</a></li>
                                 <li><a href="<?php echo base_url()?>law_area/law_area_list"><i class="fa fa-laptop"></i><span>Law Area</span></a>
                            
                        </li>
                                
                            </ul>
                        </li>
                       
                         <li>
                            <a href="<?php echo base_url()?>tracks/track_list">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Tracks</span>
                                
                            </a>
                            
                        </li>
                       <li>
                            <a href="<?php echo base_url()?>usersall/user_admin_list/">
                                <i class="fa fa-laptop"></i>
                                <span>User Management</span>
                                
                            </a>
                            
                        </li>

                       
                        
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>