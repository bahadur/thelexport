    <?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
        'class' => 'form-control',
        'placeholder' => 'User ID'
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
        'class' => 'form-control',
        'placeholder'=>'Password'
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember')
	
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Law | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Sign In</div>
            
            <?php echo form_open($this->uri->uri_string()); ?>
                <div class="body bg-gray">
                    <div class="form-group">
                        <?php echo form_input($login); ?>
                    </div>
                    <div class="form-group">
                        <?php echo form_password($password); ?>
                        
                    </div>          
                    <div class="form-group">
                        <?php echo form_checkbox($remember); ?> <?php echo form_label('Remember me', $remember['id']); ?>
                    </div>
                </div>
                <div class="footer">                                                               
                    <?php echo form_submit( array('value'=>'Sign me in', 'class'=>'btn bg-olive btn-block')); ?>
                    <!--button type="button" class="btn bg-olive btn-block" id="signin">Sign me in</button-->  

                    <p><?php echo anchor('/auth/forgot_password/', 'Forgot password'); ?></p>

                    <?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Register'); ?>
                </div>
            <?php echo form_close(); ?>

            <div class="margin text-center">
                <span>Sign in using social networks</span>
                <br/>
                <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
                <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>

            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>

<script>
    $(document).ready(function () {
        baseurl = '<?php echo base_url() ?>';
        $("#signin").click(function () {
            $.ajax({
                url: baseurl + 'auth/ajax_login',
                method: 'POST',
                dataType: 'json',
                data: {
                    userid: $("input#userid").val(),
                    password: $("input#password").val(),
                },
                error:function(e){
                    console.log(e);
                },
                success: function (response)
                {   
                    
                    // Login status [success|invalid]
                    var login_status = response.login_status;

                    // Form is fully completed, we update the percentage
                    //neonLogin.setPercentage(100);


                    // We will give some time for the animation to finish, then execute the following procedures	
                   
                        // If login is invalid, we store the 
                        if (login_status == 'invalid')
                        {
                            //$(".login-page").removeClass('logging-in');
                            //neonLogin.resetProgressBar(true);
                        }
                        else
                        if (login_status == 'success')
                        {
                            // Redirect to login page
                           
                                var redirect_url = baseurl;

                                if (response.redirect_url && response.redirect_url.length)
                                {
                                    redirect_url = response.redirect_url;
                                }

                                window.location.href = redirect_url;
                            
                        }

                   
                }
            });
        });
    });
</script>