<div class="row">
    <div class="col-lg-12">
        <form role="form" action="<?php echo base_url()?>projects/upload_doc"  method="post" enctype="multipart/form-data">
            <div class="box-body">
                
                
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file"  name="userfile" id="userfile" class="form-control"  > 
                    <input type="hidden" name="MAX_FILE_SIZE" value="2000000">
                    <p class="help-block">Example block-level help text here.</p>
                </div>
                
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>