<!-- DATA TABLES -->
<script src="//cdn.datatables.net/responsive/1.0.3/js/dataTables.responsive.js"></script>
<link href="<?php echo base_url() ?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="//cdn.datatables.net/responsive/1.0.3/css/dataTables.responsive.css" rel="stylesheet" type="text/css" />

<div class="row">
    
    <div class="col-lg-12">
        <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Tasks from clients</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="table_tasks" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Task</th>
                                                <th>client</th>
                                                <th>Create On</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Task</th>
                                                <th>client</th>
                                                <th>Create On</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
    </div>
</div>

<!-- DATA TABES SCRIPT -->
<script src="<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
$(function() {
    $('#table_tasks').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax:'<?php echo base_url()?>ajax/tasks/'
    });
});
</script>