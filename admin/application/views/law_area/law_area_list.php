<!-- DATA TABLES -->
<script src="//cdn.datatables.net/responsive/1.0.3/js/dataTables.responsive.js"></script>
<link href="<?php echo base_url() ?>assets/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="//cdn.datatables.net/responsive/1.0.3/css/dataTables.responsive.css" rel="stylesheet" type="text/css" />

<div class="row">
    
 <div class="col-xs-12">
                <div class="box-header">
                   <h3 class="box-title">Tasks from clients</h3>
                 </div>
        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">

        <li class="active"><a href="#view" data-toggle="tab">Law Area View</a></li>
        <li class><a href="#Add" data-toggle="tab">Add Law Area</a></li>
        </ul>
      <div class="tab-content"> 
                <div class="tab-pane active" id="view">
                                <div class="box-body table-responsive">
                                    <table id="table_tasks" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
											<th>Law Area Total</th>  
                                            <th>ID</th>  
                                                <th>Law Area</th>     
                                                <th>Action</th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
											<th>Law Area Total</th> 
                                            <th>ID</th>  
                                                 <th>Law Area</th>
                                                 <th>Action</th>  
                                            </tr>

                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
 
    <div class="tab-pane" id="Add">
            <div class="box-body table-responsive">
                           
              <form class="form-horizontal nobottommargin" method="post">
                    
         <div class="form-group">
         <div class="col-md-4"> 
         <label for="exampleInputEmail1">Add Law Area</label>
          <input type="text" class="form-control" placeholder="" name="lawareaname">
     </div>
</div>
     <div class="box-footer">
         <button type="submit" class="btn btn-primary" value="register" id="register-form-submit" name="register-form-submit">Submit</button>
     </div>  
</form>
    
    </div>
</div>
</div>

<!-- DATA TABES SCRIPT -->
<script src="<?php echo base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>


<script type="text/javascript">
$(function() {
    $('#table_tasks').dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax:'<?php echo base_url()?>ajax/law_area/',
        "columnDefs": [ 
{
           "targets": 0,
           "visible":false
           
         },
		 {
           "targets": 1,
           "visible":false
           
         },
        {
           "targets": 3,
           "render": function ( data, type, full ) {
              
				
				    var ret =  '<a href="javascript:;" onclick="showAjaxModal('+full[1]+');" class="btn btn-default">Edit</a>';
                if(full[0] > 0){
                  ret += ' | <a href="javascript:void(0);" onclick="deleteDate('+full[1]+')" class="btn btn-default" disabled>Delete</a>';
                  ret += '<p class="text-warning">Can not delete. This court has child records in hearings</p>';
                }
                else{
                  ret += ' | <a href="javascript:void(0);" onclick="deleteDate('+full[1]+')" class="btn btn-default">Delete</a> |';
                }
                return ret;
				
				
             }
         } ]
    });
},

$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
  
})


);


</script>

<script type="text/javascript">
    var url="<?php echo base_url();?>";
    function deleteDate(id){
    //  alert('');
       var r=confirm("Do you want to delete this?")
        if (r==true)
          window.location = url+"law_area/law_area_del/"+id;
        else
          return false;
        } 
</script>

<script type="text/javascript">
    function showAjaxModal(id)
{
jQuery('#myModal').modal('show', {backdrop: 'static'});

$.ajax({
url: "<?php echo base_url()?>modals/law_area/"+id,
success: function(response)
{
jQuery('#myModal .modal-body').html(response);
}
});
}
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
     <form class="form-horizontal nobottommargin" method="post">
    
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Law Area</h4>
      </div>
      <div class="modal-body">
    
      
      </div>
      <div class="modal-footer">
        
      </div>
      </form>
    </div>
  </div>