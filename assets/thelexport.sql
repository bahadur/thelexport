/*
Navicat MySQL Data Transfer

Source Server         : new 195
Source Server Version : 50173
Source Host           : 192.168.0.195:3306
Source Database       : thelexport

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2015-07-14 12:06:48
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `alert`
-- ----------------------------
DROP TABLE IF EXISTS `alert`;
CREATE TABLE `alert` (
  `alert_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_user_id` int(11) NOT NULL,
  `court_court_id` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  PRIMARY KEY (`alert_id`,`users_user_id`,`court_court_id`),
  KEY `fk_alert_users_idx` (`users_user_id`),
  KEY `fk_alert_court1_idx` (`court_court_id`),
  CONSTRAINT `fk_alert_court1` FOREIGN KEY (`court_court_id`) REFERENCES `court_OLD` (`court_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alert_users` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of alert
-- ----------------------------
INSERT INTO `alert` VALUES ('1', '16', '1', '2015-06-18');
INSERT INTO `alert` VALUES ('2', '16', '4', '2015-06-24');
INSERT INTO `alert` VALUES ('3', '16', '4', '2015-06-25');
INSERT INTO `alert` VALUES ('4', '16', '6', '2015-06-26');
INSERT INTO `alert` VALUES ('5', '16', '7', '2015-06-23');
INSERT INTO `alert` VALUES ('6', '16', '5', '2015-06-30');
INSERT INTO `alert` VALUES ('7', '16', '3', '2015-06-26');
INSERT INTO `alert` VALUES ('8', '16', '6', '2015-06-28');
INSERT INTO `alert` VALUES ('9', '16', '6', '2015-06-12');

-- ----------------------------
-- Table structure for `contact_persons`
-- ----------------------------
DROP TABLE IF EXISTS `contact_persons`;
CREATE TABLE `contact_persons` (
  `contacts_id` int(10) unsigned zerofill NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `work_telephone` varchar(100) DEFAULT NULL,
  `mobile_no` varchar(100) DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  `contact_type_contact_type_id` int(11) NOT NULL,
  PRIMARY KEY (`contacts_id`,`contact_type_contact_type_id`),
  KEY `fk_contact_persons_users1_idx` (`users_user_id`),
  KEY `fk_contact_persons_contact_type1_idx` (`contact_type_contact_type_id`),
  CONSTRAINT `fk_contact_persons_contact_type1` FOREIGN KEY (`contact_type_contact_type_id`) REFERENCES `contact_type` (`contact_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contact_persons_users1` FOREIGN KEY (`users_user_id`) REFERENCES `preceneus`.`users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contact_persons
-- ----------------------------

-- ----------------------------
-- Table structure for `contact_type`
-- ----------------------------
DROP TABLE IF EXISTS `contact_type`;
CREATE TABLE `contact_type` (
  `contact_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`contact_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contact_type
-- ----------------------------
INSERT INTO `contact_type` VALUES ('1', 'Primary');
INSERT INTO `contact_type` VALUES ('2', 'Secondary');
INSERT INTO `contact_type` VALUES ('3', 'Alternative');

-- ----------------------------
-- Table structure for `court`
-- ----------------------------
DROP TABLE IF EXISTS `court`;
CREATE TABLE `court` (
  `court_id` int(11) NOT NULL AUTO_INCREMENT,
  `court_name` varchar(200) NOT NULL,
  `address` mediumtext,
  `write_address` mediumtext,
  `email` varchar(200) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `location` varchar(200) NOT NULL,
  `court_type_court_type_id` int(11) NOT NULL,
  `lat` decimal(11,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remarks` mediumtext,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`court_id`,`court_type_court_type_id`),
  KEY `fk_court_court_type_idx` (`court_type_court_type_id`),
  KEY `court_id` (`court_id`),
  CONSTRAINT `court_ibfk_1` FOREIGN KEY (`court_type_court_type_id`) REFERENCES `court_type` (`court_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of court
-- ----------------------------
INSERT INTO `court` VALUES ('1', 'Aberdeen Tribunal Hearing Centre', 'Mezzanine Floor Atholl House 84-88 Guild Street Aberdeen Aberdeenshire AB11 6LT', '(ET) 3rd Floor Eagle Building\r\n215 Bothwell Street\r\nGlasgow', 'aberdeenet@hmcts.gsi.gov.uk', '01224 593 137', '01224 593 138', 'Aberdeenshire', '1', '57.14445639', '-2.09971170', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:42:51', '0');
INSERT INTO `court` VALUES ('2', 'Abergavenny Magistrates\' Court', 'Tudor Street\r\nAbergavenny\r\nMonmouthshire\r\nNP7 5DL', 'Gwent Magistrates\' Court\r\nThe Law Courts\r\nFaulkner Road\r\nNewport\r\n', 'GW-adminenq@hmcts.gsi.gov.uk', '01633 261300', '0870 739 4319', 'Monmouthshire', '1', '51.82175899', '-3.02319557', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:43:19', '0');
INSERT INTO `court` VALUES ('3', 'Aberystwyth Justice Centre', 'Aberystwyth Justice Centre\r\nY Lanfa\r\nTrefechan\r\nAberystwyth\r\nCeredigion\r\nSY23 1AS', '', 'enquiries@aberystwyth.countycourt.gsi.gov.uk', '01970 621 250', '0870 761 7656', 'Ceredigion', '1', '52.40906996', '-4.08709983', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:43:51', '0');
INSERT INTO `court` VALUES ('4', 'Accrington County Court', 'Bradshawgate House\r\n1 Oak Street\r\nAccrington\r\nLancashire\r\nBB5 1EQ\r\n', 'Burnley County Court\r\nThe Law Courts\r\nHammerton Street\r\nBurnley\r\nLancashire\r\nBB11 1XD', 'enquiries@burnley.countycourt.gsi.gov.uk', '01254 237 490,', '0870 739 4210', 'Lancashire', '1', '53.75146550', '-2.36172454', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:44:00', '0');
INSERT INTO `court` VALUES ('5', 'Accrington Magistrates\' Court', 'East Lancashire Magistrates\' Court\r\nThe Law Courts\r\nManchester Road\r\nAccrington\r\nLancashire\r\nBB5 2BH', 'Accrington Magistrates\' Court\r\nThe Court House\r\nNorthgate\r\nBlackburn\r\nLancashire\r\nBB2 1AA', 'ln-blackburnmcenq@hmcts.gsi.gov.uk', '01254 687500', '01254 687500', 'Lancashire', '1', '53.74912812', '-2.35932376', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:44:26', '0');
INSERT INTO `court` VALUES ('6', 'Administrative Court', 'Administrative Court Office\r\nThe Royal Courts of Justice\r\nStrand\r\nLondon\r\nWC2A 2LL', '', 'administrativecourtoffice.generaloffice@hmcts.x.gsi.gov.uk', '020 7947 6655', '020 7947 6802', 'London', '1', '51.51414311', '-0.11374170', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:44:36', '0');
INSERT INTO `court` VALUES ('7', 'Admiralty and Commercial Court', 'Rolls Building\r\n7 Rolls Buildings\r\nFetter Lane\r\nLondon\r\nEC4A 1NL', '', 'comct.issue@hmcts.gsi.gov.uk', '020 7947 6112', '  0870 761 7725', 'London', '1', '51.51589485', '-0.10967644', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:44:57', '0');
INSERT INTO `court` VALUES ('8', 'Aldershot and Farnham County Court and Family Court', 'Aldershot County Court and Family Court Hearing Centre\r\n84-86 Victoria Road\r\nAldershot\r\nHampshire\r\nGU11 1SS', '', 'aldershot.court.enquiries@hmcts.gsi.gov.uk', '01252 796 800', '0870 324 0155', 'Hampshire', '1', '51.24836472', '-0.75954956', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:12', '0');
INSERT INTO `court` VALUES ('9', 'Aldershot Magistrates\' Court', 'The Court House\r\nCivic Centre\r\nAldershot\r\nHampshire\r\nGU11 1NY', '', '', '', '', '', '1', null, null, '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:23', '0');
INSERT INTO `court` VALUES ('10', 'Aldershot Social Security and Child Support Tribunal', 'Arcade Chambers\r\nThe Arcade\r\nLittle Wellington Street\r\nAldershot\r\nHampshire\r\nGU11 1EE\r\n', 'HM Courts & Tribunals Service Sutton\r\nCopthall House\r\n9 The Pavement\r\nGrove Road\r\nSutton\r\nLondon\r\nSM1 1DA', 'sscsa-sutton@hmcts.gsi.gov.uk', '0300 123 1142', '020 8652 2381', 'Hampshire', '1', '51.24890770', '-0.76321698', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:22', '0');
INSERT INTO `court` VALUES ('11', 'Aldridge Magistrates\' Court', '', '', '', '', '', '', '1', null, null, '1', 'This court or tribunal is no longer in service.', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:25', '0');
INSERT INTO `court` VALUES ('12', 'Alton Magistrates\' Court', '', '', '', '', '', '', '1', null, null, '1', 'This court or tribunal is no longer in service.', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:25', '0');
INSERT INTO `court` VALUES ('13', 'Altrincham County Court and Family Court', 'Trafford Courthouse\r\nAshton Lane\r\nSale\r\nCheshire\r\nM33 7NR', 'Altrincham County Court and Family Court Hearing Centre', 'enquiries@altrincham.countycourt.gsi.gov.uk', '0161 975 4760 ', '01264 347 894', 'Cheshire', '1', '53.42576900', '-2.32577200', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:40', '0');
INSERT INTO `court` VALUES ('14', 'Amersham Law Courts', 'King George V Road\r\nAmersham\r\nBuckinghamshire\r\nHP6 5AJ', '', 'Enquiries@Aylesbury.Crowncourt.gsi.gov.uk', '01296 434 401', '01264 785079', 'Buckinghamshire', '1', '51.67472300', '-0.60283700', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:43:12', '0');
INSERT INTO `court` VALUES ('15', 'Andover Magistrates\' Court', '', '', '', '', '', '', '1', null, null, '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:26', '0');
INSERT INTO `court` VALUES ('16', 'Avon and Somerset Central Accounts Department', 'HMCTS\r\nCentral Accounts Dept\r\nP.O. Box 480\r\nWeston-Super-Mare\r\nAvon\r\nBS23 9BE', '', 'av.qet@hmcts.gsi.gov.uk', '01934 528 528', '01934 528 528', 'Avon', '1', '51.34387288', '-2.95700855', '1', 'This court or tribunal is no longer in service.', '2015-07-02 00:00:00', '0', '2015-07-06 18:43:40', '0');
INSERT INTO `court` VALUES ('17', 'Avon & Somerset Fixed Penalty Office', '', '', '', '', '', '', '1', null, null, '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:27', '0');
INSERT INTO `court` VALUES ('18', 'Aylesbury County Court and Family Court', 'Walton Street\r\nAylesbury\r\nBuckinghamshire\r\nHP21 7QZ', 'Aylesbury County Court and Family Court Hearing Centre\r\nWalton Street\r\nAylesbury\r\nBuckinghamshire\r\nHP21 7QZ\r\n', 'enquiries@Aylesbury.countycourt.gsi.gov.uk', '01296 554 327', '01264 785026', 'Buckinghamshire', '1', '51.81078059', '-0.80817086', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:44:05', '0');
INSERT INTO `court` VALUES ('19', 'Aylesbury Crown Court', 'County Hall\r\nMarket Square\r\nAylesbury\r\nBuckinghamshire\r\nHP20 1XD', '', 'Enquiries@Aylesbury.Crowncourt.gsi.gov.uk', '01296 434 401', '01264 785079', 'Buckinghamshire', '1', '51.81566527', '-0.81203185', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:44:20', '0');
INSERT INTO `court` VALUES ('20', 'Aylesbury Magistrates\' Court and Family Court', 'Walton Street\r\nAylesbury\r\nBuckinghamshire\r\nHP21 7QZ', 'Milton Keynes Magistrates\' Court and Family Court Hearing Centre\r\n301 Silbury Boulevard\r\nWitan Gate East\r\nCentral Milton Keynes\r\nBuckinghamshire\r\nMK9 2AJ', '', '01908 451 145', '01264 785107', 'Buckinghamshire', '1', '51.81077983', '-0.80817120', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:44:31', '0');
INSERT INTO `court` VALUES ('21', 'Ayr Social Security and Child Support Tribunal', 'Russell House\r\nKing Street\r\nAyr\r\nAyrshire\r\nKA8 0BD\r\n', 'Wellington House\r\n134-136 Wellington Street\r\nGlasgow\r\nG2 2XL\r\n', 'enquiries-cicap@hmcts.gsi.gov.uk', '0141 354 8400', '01264 347 981', 'Ayrshire', '1', '55.46644863', '-4.62492495', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:44:37', '0');
INSERT INTO `court` VALUES ('23', 'Banbury County Court\r\n', 'The Court House\r\nWarwick Road\r\nBanbury\r\nOxfordshire\r\nOX16 2AW ', '', 'enquiries@banbury.countycourt.gsi.gov.uk \r\n', '01295 452 090 \r\n', '01264 785029 \r\n', 'Oxfordshire\r\n', '1', '52.06393067', '-1.34088575', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court` VALUES ('24', 'Banbury Fixed Penalty Office\r\n', '', '', '', '', '', '', '1', '0.00000000', '0.00000000', '1', 'This court or tribunal is no longer in service.\r\n', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court` VALUES ('25', 'Banbury Magistrates\' Court and Family Court\r\n', 'The Court House\r\nWarwick Road\r\nBanbury\r\nOxfordshire\r\nOX16 2AW ', 'Oxford Magistrates Court and Family Court Hearing Centre\r\nSpeedwell Street\r\nOxford\r\nOxfordshireOX1 1RZ ', ' TV-OxfordMCEnq@hmcts.gsi.gov.uk \r\n', '01865 448 020 \r\n', '01264 785088 \r\n', 'Oxfordshire\r\n', '1', '52.06392984', '-1.34088575', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court` VALUES ('26', 'Bankruptcy Court (Central London)\r\n', 'Thomas More Building\r\nRoyal Courts of Justice\r\nStrand\r\nLondon\r\nLondon\r\nWC2A 2LL ', '\r\n', 'RCJBankCLCCDJHearings@hmcts.gsi.gov.uk \r\n', '020 7947 6448 , 020 7947 6863 , 020 7947 6441 , 020  7947 6839 \r\n', '0870 761 7695 \r\n', 'London\r\n', '1', '51.51414311', '-0.11374170', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court` VALUES ('27', 'Bankruptcy Court (High Court)\r\n', 'Rolls Building\r\n7 Rolls Buildings\r\nFetter Lane\r\nLondon\r\nEC4A 1NL ', '', 'rolls.filemanagement@hmcts.gsi.gov.uk, RCJCompanies.Orders@hmcts.gsi.gov.uk', ' 020 8343 4272 \r\n', '0870 324 0201 \r\n', 'London\r\n', '1', '51.60043679', '-0.19560673', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court` VALUES ('28', 'Barkingside Magistrates\' Court\r\n', '850 Cranbrook Road\r\nBarkingside\r\nIlford\r\nEssex\r\nIG6 1HW ', '', 'gl-barkingsidemcenq@hmcts.gsi.gov.uk \r\n', '020 8437 6525 \r\n', '0870 739 4187 \r\n', 'Essex\r\n', '1', '51.58771250', '-0.19560673', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court` VALUES ('29', 'Barnet Civil and Family Courts Centre\r\n', 'St Marys Court\r\nRegents Park Road\r\nFinchley Central\r\nLondon\r\nN3 1BQ ', 'Hearing Centre\r\nSt Marys Court\r\nRegents Park Road\r\nFinchley Central\r\nLondon\r\nN3 1BQ ', 'enquiries@barnet.countycourt.gsi.gov.uk \r\n', '020 8343 4272 \r\n', ' 0870 324 0201 \r\n', 'London\r\n', '1', '51.60043679', '-0.19560673', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court` VALUES ('30', 'Barnsley Law Courts\r\n', 'The Court House\r\nWestgate\r\nBarnsley\r\nSouth Yorkshire\r\nS70 2HW ', 'The Court House\r\nWestgate\r\nBarnsley\r\nSouth Yorkshire\r\nS70 2DW ', 'enquiries@barnsley.countycourt.gsi.gov.uk \r\n', '01226 320 000 \r\n', '01264 347 930 \r\n', 'South Yorkshire\r\n', '1', '53.55468102', '-1.48437529', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court` VALUES ('31', 'Barnsley Social Security and Child Support Tribunal', 'Wellington House\r\nWellington Street\r\nBarnsley\r\nSouth Yorkshire\r\nS70 1WA ', 'York House\r\n31 York Place\r\nLeeds\r\nWest Yorkshire\r\nLS1 2ED ', 'sscsa-leeds@hmcts.gsi.gov.uk \r\n', ' 0300 123 1142 \r\n', '  0113 389 6001 \r\n', 'South Yorkshire\r\n', '1', '53.55085460', '-1.48082956', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('32', 'Barnstaple County Court and Family Court\r\n', 'Barnstaple County Court and Family Court hearing centre\r\n8th Floor\r\nThe Law Courts\r\nCivic Centre\r\nBarnstaple\r\nDevon\r\nEX31 1DX ', '', 'hearings@barnstaple.countycourt.gsi.gov.uk \r\n', '01271 340 410 \r\n', '0870 324 0129 \r\n', 'Devon\r\n', '1', '51.08134299', '-1.48082956', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('33', 'Barnstaple Crown Court\r\n', 'The Law Courts\r\nCivic Centre\r\nBarnstaple\r\nDevon\r\nEX31 1DX ', 'Exeter Combined Court\r\nSouthernhay Gardens\r\nExeter\r\nDevon\r\nEX1 1UH ', '  exeter.enquiries@exeter.crowncourt.gsi.gov.uk \r\n', '01392 415 330 \r\n', '', 'Devon\r\n', '1', '51.08134299', '-4.06565858', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('34', 'Barnstaple Magistrates\' Court - North and East Devon\r\n', 'The Law Courts\r\nCivic Centre\r\nBarnstaple\r\nDevon\r\nEX31 1DX ', '', 'DE-Barnstaplemclist@hmcts.gsi.gov.uk \r\n', '01271 340 410 \r\n', '0870 324 0128 \r\n', 'Devon\r\n', '1', '51.08134243', '-4.06565858', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('35', 'Barrow-in-Furness County Court and Family Court\r\n', 'Barrow Law Courts\r\nAbbey Road\r\nBarrow in Furness\r\nCumbria\r\nLA14 5QX ', 'Barrow-in-Furness County Court and Family Court Hearing Centre\r\nBarrow Law Courts\r\nAbbey Road\r\nBarrow in Furness\r\nCumbria\r\nLA14 5QX ', ' enquiries.barrowcountycourt@hmcts.gsi.gov.uk \r\n', '  01229 840 370 \r\n', '0870 739 4409 \r\n', 'Cumbria\r\n', '1', '54.11652236', '-3.22757743', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('36', 'Barrow-in-Furness Magistrates\' Court\r\n', 'Abbey Road\r\nBarrow in Furness\r\nCumbria\r\nLA14 5QX ', '', 'cumbria.south.magistrates@hmcts.gsi.gov.uk \r\n', '01229 820 161 \r\n', '0870 739 4409 \r\n', 'Cumbria\r\n', '1', '54.11652098', '-3.22757705', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('37', 'Basildon Combined Court\r\n', 'The Gore\r\nBasildon\r\nEssex\r\nSS14 2EW ', '', 'enquiries@basildon.crowncourt.gsi.gov.uk, enquiries@basildon.countycourt.gsi.gov.uk', '0344 892 4000 \r\n', '01268 458 100 \r\n', 'Essex\r\n', '1', '51.57257094', '0.45480719', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('38', 'Basildon Magistrates\' Court and Family Court\r\n', 'The Court House\r\nGreat Oaks\r\nBasildon\r\nEssex\r\nSS14 1EH ', 'Basildon Magistrates\' Court and Family Court Hearing Centre\r\nCourt Administration Centre\r\nP.O. Box 10754\r\nChelmsford\r\nEssex\r\nCM1 9PZ ', 'esosprey@hmcts.gsi.gov.uk \r\n', '01245 313 300 \r\n', '0870 324 0091 \r\n', 'Essex\r\n', '1', '51.57177086', '0.45822771', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('39', 'Basingstoke County Court and Family Court\r\n', 'Basingstoke County Court and Family Court hearing centre\r\nThe Court House\r\nLondon Road\r\nBasingstoke\r\nHampshire\r\nRG21 4AB ', '', 'Basingstoke.court.enquiries@hmcts.gsi.gov.uk \r\n', '01256 318 200 \r\n', '  01264 785170 \r\n', 'Hampshire\r\n', '1', '51.26331533', '-1.08317824', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('40', 'Basingstoke Magistrates\' Court\r\n', 'The Court House\r\nLondon Road\r\nBasingstoke\r\nHampshire\r\nRG21 4AB ', '', 'ha.basmags@hmcts.gsi.gov.uk \r\n', '01256 318 200 \r\n', '', 'Hampshire\r\n', '1', '51.26331471', '-1.08317850', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('41', 'Bath County Court and Family Court\r\n', 'Bath County Court and Family Court hearing centre\r\nThe Law Courts\r\nNorth Parade Road\r\nBath\r\nSomerset\r\nBA1 5AF ', 'Bath County Court and Family Court\r\nP.O. Box 4302\r\nNorth Parade Road\r\nBath\r\nSomerset\r\nBA1 0LF ', 'av-bathcounty@hmcts.gsi.gov.uk \r\n', '01225 476 730 \r\n', '0161 743 4023 \r\n', 'Somerset\r\n', '1', '51.38081360', '-2.35428380', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('42', 'Bath Magistrates\' Court\r\n', 'North Parade Road\r\nBath\r\nSomerset\r\nBA1 5AF', '', 'av-bath.mc@hmcts.gsi.gov.uk \r\n', '01225 46 3281 \r\n', '0870 324 0189 \r\n', 'Somerset\r\n', '1', '51.38114337', '-2.35205908', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('43', 'Bedford and Mid Beds Magistrates\' Court and Family Court\r\n', 'Shire Hall\r\n3 St Paul\'s Square\r\nBedford\r\nBedfordshire\r\nMK40 1SQ ', 'Luton & South Bedfordshire Magistrates\' Court and Family Hearing Centre\r\nStuart Street\r\nLuton\r\nBedfordshire\r\nLU1 5BL ', ' bd-lutonmcenq@hmcts.gsi.gov.uk \r\n', '01234 319 100 \r\n', ' 01234 319 114 \r\n', 'Bedfordshire\r\n', '1', '52.13502903', '-0.46788032', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('44', 'Bedford County Court and Family Court\r\n', 'Shire Hall\r\n3 St Paul\'s Square\r\nBedford\r\nBedfordshire\r\nMK40 1SQ ', 'Bedford County Court and Family Court Hearing Centre\r\nP.O. Box 1405\r\n3 St Paul\'s Square\r\nBedford\r\nBedfordshire\r\nMK40 9DN ', 'enquiries@bedford.countycourt.gsi.gov.uk \r\n', '0300 123 5577 \r\n', '0161 743 4023 \r\n', 'Bedfordshire\r\n', '1', '52.13502903', '-0.46788032', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('45', 'Belmarsh Magistrates\' Court\r\n', '4 Belmarsh Road\r\nThamesmead\r\nLondon\r\n', 'Belmarsh Magistrates\' Court\r\n9 Blackheath Road\r\nLondon\r\nSE10 8PE ', '\r\nGL-CAO.correspond@hmcts.gsi.gov.uk \r\n', '0300 790 9901 \r\n', '', 'Thamesmead\r\n', '1', '51.49600000', '0.09176900', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('46', 'Berwick upon Tweed Magistrates\' Court\r\n', '40 Church Street\r\nBerwick upon Tweed\r\nNorthumberland\r\nTD15 1DX ', '', 'NO_Berwick@hmcts.gsi.gov.uk \r\n', '0191 2327326 \r\n', '55.769863568', 'Northumberland\r\n', '1', '55.76986357', '-2.00188691', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('47', 'Beverley Magistrates\' Court and Family Court\r\n', 'Beverley Magistrates\' Court and Family Court Hearing Centre\r\nThe Court House\r\nChampney Road\r\nBeverley\r\nEast Yorkshire\r\nHU17 9EJ ', '', 'hu-beverleymcadmin@hmcts.gsi.gov.uk \r\n', '  01482 861 607 \r\n', '0870 739 4398 \r\n', 'East Yorkshire\r\n', '1', '53.84052313', '-0.42866900', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('48', 'Bexleyheath Social Security and Child Support Tribunal\r\n', '40-46 Avenue Road\r\nBexleyheath\r\nKent\r\nDA7 4EG ', 'HMCTS Sutton\r\nCopthall House\r\n9 The Pavement\r\nGrove Road\r\nSutton\r\nLondon\r\nSM1 1DA ', 'sscsa-sutton@hmcts.gsi.gov.uk \r\n', ' 0300 123 1142 \r\n', '020 8652 2381 \r\n', 'Kent\r\n', '1', '51.46281921', '0.13255203', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court` VALUES ('49', 'Bexley Magistrates\' Court and Family Court\r\n', 'Norwich Place\r\nBexleyheath\r\nKent\r\nDA6 7ND ', 'Bexley Magistrates\' Court\r\nThe Court house\r\n1 London Road\r\nBromley\r\nKent\r\nBR1 1RA  Opening hours\r\n', 'gl-bromleymcenq@hmcts.gsi.gov.uk \r\n', '020 8437 3585 \r\n', '0870 324 0223 \r\n', 'Kent\r\n', '1', '51.45518276', '0.14790024', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('50', 'Bicester Magistrates\' Court and Family Court\r\n', 'Waverley House\r\nQueens Avenue\r\nBicester\r\nOxfordshire\r\nOX26 2NZ ', 'Oxford Magistrates Court and Family Court Hearing Centre\r\nSpeedwell Street\r\nOxford\r\nOxfordshire\r\nOX1 1RZ ', ' TV-OxfordMCEnq@hmcts.gsi.gov.uk \r\n', '01865 448 020 \r\n', '  01865 448 024 \r\n', 'Oxfordshire\r\n', '1', '51.89919171', '0.14790024', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('51', 'Birkenhead County Court and Family Court\r\n', '76 Hamilton Street\r\nBirkenhead\r\nMerseyside\r\nCH41 5EN ', 'Birkenhead County Court and Family Hearing Centre\r\n76 Hamilton Street\r\nBirkenhead\r\nMerseyside\r\nCH41 5EN ', 'civil@birkenhead.countycourt.gsi.gov.uk \r\n', '0151 666 5800 \r\n', '01264 785 146 \r\n', 'Merseyside\r\n', '1', '53.39137809', '-3.01662367', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('52', 'Birmingham Civil and Family Justice Centre\r\n', 'Birmingham Civil and Family Justice Hearing Centre\r\nPriory Courts\r\n33 Bull Street\r\nBirmingham\r\nWest Midlands\r\nB4 6DS ', '', 'enquiries@birmingham.countycourt.gsi.gov.uk', '0300 123 1751 \r\n', '01264 785131 \r\n', 'West Midlands', '1', '52.48166041', '-1.89552897', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('53', 'Birmingham Crown Court\r\n', 'Queen Elizabeth II Law Courts\r\n1 Newton Street\r\nBirmingham\r\nWest Midlands\r\nB4 7NA ', '', 'birmingham.enquiries@birmingham.crowncourt.gsi.gov.uk \r\n', '0121 681 3300, 0121 681 3339 \r\n', '0870 324 0273 \r\n', 'West Midlands\r\n', '1', '52.48308860', '-1.89415616', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('54', 'Birmingham District Probate Registry\r\n', 'Birmingham District Probate Registry\r\n', 'The Priory Courts\r\n33 Bull Street\r\nBirmingham\r\nWest Midlands\r\nB4 6DU ', 'BirminghamDPRsolicitorsenquiries@hmcts.gsi.gov.uk, ', '0121 681 3400, 0121 681 3401 \r\n', '', 'West Midlands\r\n', '1', '52.48166136', '-1.89552894', '1', 'Fax not available.', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('55', 'Birmingham Employment Tribunal\r\n', 'Centre City Tower\r\n5-7 Hill Street\r\nBirmingham\r\nWest Midlands\r\nB5 4UU ', '', 'BirminghamET@hmcts.gsi.gov.uk \r\n', '0121 600 7780 \r\n', '01264 347 999 \r\n', 'West Midlands\r\n', '1', '52.47622408', '-1.89851583', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('56', 'Birmingham Immigration and Asylum Chamber (First Tier Tribunal)\r\n', 'Sheldon Court\r\n1 Wagon Lane\r\nSheldon\r\nBirmingham\r\nWest Midlands\r\nB26 3DU ', '', 'customer.service@hmcts.gsi.gov.uk \r\n', '0845 600 0877 \r\n', '0870 739 5792 \r\n', 'West Midlands\r\n', '1', '0.00000000', '0.00000000', '1', 'Co-ordinates not available.', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('57', 'Birmingham Magistrates\' Court\r\n', 'Victoria Law Courts\r\nCorporation Street\r\nBirmingham\r\nWest Midlands\r\nB4 6QA ', '', 'WM-CustomerServicesBirmingham@hmcts.gsi.gov.uk \r\n', '0121 212 6600 \r\n', '0870 324 0274 \r\n', 'West Midlands\r\n', '1', '52.48373578', '-1.89405153', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('58', 'Birmingham Social Security and Child Support Tribunal\r\n', 'Administrative Support Centre \r\nPO Box 14620\r\nBirmingham\r\nWest Midlands\r\nB16 6FR', '', '', '0300 123 1142 \r\n', '01264 347 983 \r\n', 'West Midlands\r\n', '1', '0.00000000', '0.00000000', '1', 'Email/co-ordinates not available.', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('59', 'Birmingham Youth Court\r\n', 'Steelhouse Lane\r\nBirmingham\r\nWest Midlands\r\nB4 6BJ ', '', 'WM-CustomerServicesBirmingham@hmcts.gsi.gov.uk \r\n', '0121 212 6600 \r\n', '0121 212 6771 \r\n', 'West Midlands\r\n', '1', '52.48335097', '-1.89496539', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('60', 'Blackburn County Court and Family Court\r\n', '64 Victoria Street\r\nBlackburn\r\nLancashire\r\nBB1 6DJ ', 'Blackburn County Court and Family Court Hearing Centre\r\n64 Victoria Street\r\nBlackburn\r\nLancashire\r\nBB1 6DJ ', 'enquiries@blackburn.countycourt.gsi.gov.uk \r\n', '01254 299 840 \r\n', '0870 324 0328 \r\n', 'Lancashire\r\n', '1', '53.74989980', '-2.48248176', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('61', 'Blackburn Magistrates\' Court\r\n', 'The Court House\r\nNorthgate\r\nBlackburn\r\nLancashire\r\nBB2 1AA ', '', '  ln-blackburnmcenq@hmcts.gsi.gov.uk \r\n', '01254 687 500 \r\n', '0870 739 4254 \r\n', 'Lancashire\r\n', '1', '53.74982981', '-2.48613549', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('62', 'Blackfriars Crown Court\r\n', '1-15 Pocock Street\r\nLondon\r\nSE1 OBT ', '', 'blackfriars.enquiries@blackfriars.crowncourt.gsi.gov.uk \r\n', '020 7922 5800 \r\n', ' 0870 324 0146 \r\n', 'London\r\n', '1', '51.50223900', '-0.09993400', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('63', 'Blackpool County Court and Family Court\r\n', 'The Law Courts\r\nChapel Street\r\nBlackpool\r\nLancashire\r\nFY1 5RJ ', 'Blackpool County Court and Family Court Hearing Centre\r\nThe Law Courts\r\nChapel Street\r\nBlackpool\r\nLancashire\r\nFY1 5RJ ', 'enquiries@blackpool.countycourt.gsi.gov.uk \r\n', '01253 754 020 \r\n', '01264 347 891 \r\n', 'Lancashire\r\n', '1', '53.81173283', '-3.05298421', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('64', 'Blackpool Magistrates\' Court\r\n', 'Civic Centre\r\nChapel Street\r\nBlackpool\r\nLancashire\r\nFY1 5DQ ', '', ' ln-blackpoolmcenq@hmcts.gsi.gov.uk \r\n', '01253 757 000 \r\n', '08703 240203 \r\n', 'Lancashire\r\n', '1', '53.81173283', '-3.05298421', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('65', 'Blackwood Civil and Family Court\r\n', '8 Hall Street\r\nBlackwood\r\nSouth Wales\r\nNP12 1NY ', '', ' Blackwood.Enquiries@hmcts.gsi.gov.uk \r\n', '01495 238 200 \r\n', '0870 324 0167 \r\n', 'South Wales\r\n', '1', '51.66653055', '-3.19293370', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court` VALUES ('66', 'Bodmin County Court and Family Court\r\n', 'Bodmin County Court and Family Court hearing centre\r\nThe Law Courts\r\nLaunceston Road\r\nBodmin\r\nCornwall\r\nPL31 2AL ', 'Bodmin County Court and Family Court hearing centre\r\nThe Law Courts\r\nLaunceston Road\r\nBodmin\r\nCornwall\r\nPL31 2AL ', 'enquiriesbodmincc@hmcts.gsi.gov.uk \r\n', '01208 261 580 \r\n', '01208 77255 \r\n', 'Cornwall', '1', '50.46952713', '-4.70886473', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('67', 'Bodmin Magistrates\' Court\r\n', 'Launceston Road\r\nBodmin\r\nCornwall\r\nPL31 2AL ', '', 'cornwall-admin@hmcts.gsi.gov.uk \r\n', '01208 262 700 \r\n', '01208 269 965 \r\n', 'Cornwall', '1', '50.46952713', '-4.70886473', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('68', 'Bodmin Probate Sub-Registry\r\n', 'Magistrates\' Courts\r\nLaunceston Road\r\nBodmin\r\nCornwall\r\nPL31 2AL ', 'The Civil Justice Centre\r\n2 Redcliff Street\r\nBristol\r\nBS1 6GR ', 'bristoldprolicitorsenquiries@hmcts.gsi.gov.uk, bristoldprenquiries@hmcts.gsi.gov.uk \r\n', '01208 261 581 \r\n', '', 'Cornwall\r\n', '1', '50.46952752', '-4.70886549', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('69', 'Bolton County Court and Family Court\r\n', 'The Law Courts\r\nBlackhorse Street\r\nBolton\r\nGreater Manchester\r\nBL1 1SU ', 'Bolton County Court and Family Court Hearing Centre\r\nThe Law Courts\r\nBlackhorse Street\r\nBolton\r\nGreater Manchester\r\nBL1 1SU ', 'enquiries@bolton.countycourt.gsi.gov.uk \r\n', '01204 392 881 \r\n', '01264 347908 \r\n', 'Greater Manchester\r\n', '1', '53.57820434', '-2.43368898', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('70', 'Bolton Crown Court\r\n', 'The Law Courts\r\nBlackhorse Street\r\nBolton\r\nGreater Manchester\r\nBL1 1SU ', '', '\r\nenquiries@bolton.crowncourt.gsi.gov.uk \r\n', '01204 392 881 \r\n', '01204 363 204 \r\n', 'Greater Manchester\r\n', '1', '53.57820434', '-2.43368898', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('71', 'Bolton Magistrates\' Court\r\n', 'The Courts\r\nLe Mans Crescent\r\nBolton\r\nGreater Manchester\r\nBL1 1UA ', '', 'GM-BoltonMcAdmin@hmcts.gsi.gov.uk \r\n', '01204 558 200 \r\n', '0870 324 0133 \r\n', 'Greater Manchester\r\n', '1', '53.57870600', '-2.43167028', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('72', 'Bolton Social Security and Child Support Tribunal\r\n', 'Ground Floor\r\nBayley House\r\nSt Georges Square\r\nBolton\r\nGreater Manchester\r\nBL1 2HB ', '36 Dale Street\r\nLiverpool\r\nMerseyside\r\nL2 5UZ ', 'sscsa-liverpool@hmcts.gsi.gov.uk', '0300 123 1142 \r\n', '0151 243 1441 \r\n', 'Greater Manchester\r\n', '1', '53.58296338', '-2.42752967', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('73', 'Boston County Court and Family Court\r\n', 'Boston County Court and Family Court hearing centre\r\n55 Norfolk Street\r\nBoston\r\nLincolnshire\r\nPE21 6PE ', '', 'bostonenquiries@hmcts.gsi.gov.uk \r\n', '01205 366 080 \r\n', '0161 743 4023 \r\n', 'Lincolnshire\r\n', '1', '52.98305915', '-0.02498226', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('74', 'Boston Magistrates\' Court\r\n', 'Boston Court House\r\n55 Norfolk Street\r\nBoston\r\nLincolnshire\r\nPE21 6PE ', 'The Court House\r\n358 High Street\r\nLincoln\r\nLincolnshire\r\nLN5 7QA ', 'Li-lincolnmcadmin@hmcts.gsi.gov.uk \r\n', '01522 528218 \r\n', '08703240256 \r\n', 'Lincolnshire\r\n', '1', '52.98306023', '-0.02498167', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('75', 'Bournemouth and Poole County Court and Family Court\r\n', 'Bournemouth and Poole County Court and Family Court hearing centre\r\nCourts of Justice\r\nDeansleigh Road\r\nBournemouth\r\nDorset\r\nBH7 7DS ', '', 'enquiries@bournemouth.countycourt.gsi.gov.uk \r\n', '01202 502800 \r\n', '01202 502 801 \r\n', '', '1', '50.74795173', '-1.81626555', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('76', 'Bournemouth Crown Court\r\n', 'Courts of Justice\r\nDeansleigh Road\r\nBournemouth\r\nDorset\r\nBH7 7DS ', '', 'enquiries@bournemouth.crowncourt.gsi.gov.uk \r\n', '01202 502 800 \r\n', '0870 7394490 \r\n', 'Dorset\r\n', '1', '50.74795221', '-1.81626549', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('77', 'Bournemouth Magistrates\' Court\r\n', 'The Law Courts\r\nStafford Road\r\nBournemouth\r\nDorset\r\nBH1 1JH ', '', 'bmthmc@hmcts.gsi.gov.uk \r\n', '01202 745 309 \r\n', '', 'Dorset\r\n', '1', '50.72311299', '-1.86887825', '1', 'Fax service not available.', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('78', 'Bow County Court and Family Court\r\n', 'Hearing Centre \r\n96 Romford Road\r\nStratford\r\nLondon\r\nE15 4EG ', '96 Romford Road\r\nStratford\r\nLondon\r\nE15 4EG ', 'civil@bow.countycourt.gsi.gov.uk, family@bow.countycourt.gsi.gov.uk \r\n\r\n', '020 8536 5200', '0870 324 0188 \r\n', 'London\r\n', '1', '51.54291793', '0.01085347', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('79', 'Bradford and Keighley Magistrates\' Court and Family Court\r\n', 'Bradford and Keighley Magistrates\' Court and Family Court Hearing Centre\r\nThe Tyrls\r\nBradford\r\nWest Yorkshire\r\nBD1 1LA ', '', 'wy-bradfordmags@hmcts.gsi.gov.uk \r\n', '01274 390 111 \r\n', '0870 739 4466 \r\n', 'West Yorkshire\r\n', '1', '53.79176752', '-1.75417576', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('80', 'Bradford Combined Court Centre\r\n', 'Bradford Law Courts\r\nExchange Square\r\nDrake Street\r\nBradford\r\nWest Yorkshire\r\nBD1 1JA ', '', 'uiries@bradford.countycourt.gsi.gov.uk \r\n', '01274 840 274 \r\n', '01264 347 975 \r\n', 'West Yorkshire\r\n', '1', '53.79274522', '-1.75417576', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('81', 'Bradford Tribunal Hearing Centre\r\n', 'Phoenix House\r\nRushton Avenue\r\nThornbury\r\nBradford\r\nWest Yorkshire\r\nBD3 7BH ', '', '', '0300 123 1711 \r\n', '0870 739 4452 \r\n', 'West Yorkshire\r\n', '1', '53.80101962', '-1.71132104', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('82', 'Brecon Law Courts\r\n', 'Brecon Law Courts\r\nCambrian Way\r\nBrecon\r\nPowys\r\nLD3 7HR ', 'Merthyr Tydfil Combined Courts\r\nThe Law Courts\r\nGlebeland Place\r\nMerthyr Tydfil\r\nSouth Wales\r\nCF47 8BH ', 'sw-merthyrmcenq@hmcts.gsi.gov.uk \r\n', '01685 727 600 \r\n', '0870 324 0338 \r\n', 'Powys', '1', '51.94481593', '-3.38199382', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('83', 'Brentford County Court and Family Court\r\n', 'Alexandra Road\r\nHigh Street\r\nBrentford\r\nMiddlesex\r\nTW8 0JJ ', 'Hearing Centre\r\nAlexandra Road\r\nHigh Street\r\nBrentford\r\nMiddlesex\r\nTW8 0JJ ', 'enquiries@Brentford.countycourt.gsi.gov.uk \r\n', '  020 8231 8940 \r\n', '0870 739 5909 \r\n', 'Middlesex\r\n', '1', '51.48428615', '-0.30435566', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('84', 'Bridgend Law Courts\r\n', 'The Law Courts\r\nSunnyside\r\nBridgend\r\nSouth Wales\r\nCF31 4AJ ', '', 'EnquiriesBridgendLawCourts@hmcts.gsi.gov.uk \r\n', '01656 673 833 \r\n', '0870 739 5940 \r\n', 'South Wales\r\n', '1', '51.50425487', '-3.58271768', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court` VALUES ('85', 'Bridlington Magistrates\' Court and Family Court\r\n', 'Bridlington Magistrates\' Court and Family Court Hearing Centre\r\nThe Court House\r\nQuay Road\r\nBridlington\r\nEast Yorkshire\r\nYO16 4EJ ', '', 'bridlington.mc@cawitnessservice.cjsm.net \r\n', '01482 861 607 \r\n', '0870 739 4398 \r\n', 'East Yorkshire\r\n', '1', '54.08510300', '-0.20009500', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:21:16', '0');
INSERT INTO `court` VALUES ('86', 'Brighton County Court\r\n', 'William Street\r\nBrighton\r\nEast Sussex\r\nBN2 0RF ', '', 'enquiries@brighton.countycourt.gsi.gov.uk, hearings@brighton.countycourt.gsi.gov.uk, bailiffs@brighton.countycourt.gsi.gov.uk, e-filing@brighton.countycourt.gsi.gov.uk \r\n', '01273 674 421 (Enquiries), 01273 388 421 (Appointments)\r\n\r\n', '  0870 324 0319 \r\n', 'East Sussex\r\n', '1', '50.82301576', '-0.13522827', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:21:16', '0');
INSERT INTO `court` VALUES ('87', 'Brighton District Probate Registry\r\n', 'William Street\r\nBrighton\r\nEast Sussex\r\nBN2 0RF ', '', 'BrightonDPRenquiries@hmcts.gsi.gov.uk, BrightonDPRsolicitorsenquiries@hmcts.gsi.gov.uk \r\n', '01273 573 510 \r\n', '01273 625 845 \r\n', 'East Sussex\r\n', '1', '50.82301625', '-0.13522774', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:21:17', '0');
INSERT INTO `court` VALUES ('88', 'Brighton Magistrates\' Court\r\n', 'The Law Courts\r\nEdward Street\r\nBrighton\r\nEast Sussex\r\nBN2 0LG ', 'Brighton Magistrates\' Court Administration Centre\r\nThe Law Courts\r\nEdward Street\r\nBrighton\r\nEast Sussex\r\nBN2 0LG ', 'ss-sussexadmin@hmcts.gsi.gov.uk (Enquiries), sussexfamily@hmcts.gsi.gov.uk (Family queries)\r\n\r\n', '01273 670 888 (Enquiries), 01273 811333 (Family queries) \r\n\r\n', '0870 761 7669 \r\n', 'East Sussex\r\n', '1', '50.82323469', '-0.13429665', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:21:17', '0');
INSERT INTO `court` VALUES ('89', 'Caernarfon County Court and Family Court\r\n', 'Caernarfon County Court and Family Court Hearing Centre\r\nLlanberis Road\r\nCaernarfon\r\nGwynedd\r\nLL55 2DF ', '', 'Enquiries:  enquiries@caernarfon.countycourt.gsi.gov.uk \r\nFamily queries:  family@caernarfon.countycourt.gsi.gov.uk \r\nSmall claims mediation:  scmenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01286 684 600 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\n', 'Money claims fax:  0161 743 4023 \r\nFax:  01264 347958', 'Gwynedd', '1', '53.14042912', '-4.26433494', '1', 'Money claims\r\nHousing possession\r\nBankruptcy\r\nHigh court\r\nDomestic violence\r\nChildren\r\nDivorce\r\nAdoption\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:13:16', '0');
INSERT INTO `court` VALUES ('90', 'Caernarfon Crown Court\r\n', 'Caernarfon Criminal Justice Centre\r\nLlanberis Road\r\nCaernarfon\r\nGwynedd\r\nLL55 2DF ', '', 'listing@mold.crowncourt.gsi.gov.uk \r\n', 'Enquiries:  01352 707 340 \r\nListing:  01352 707 389', 'Fax:  0870 324 0321 \r\n', 'Gwynedd', '1', '53.14043024', '-4.26433562', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:13:16', '0');
INSERT INTO `court` VALUES ('91', 'Caernarfon Magistrates\' Court and Family Court\r\n', 'Caernarfon Criminal Justice Centre\r\nLlanberis Road\r\nCaernarfon\r\nGwynedd\r\nLL55 2DF ', '', 'Enquiries:  Nw-caernarfonmcenq@hmcts.gsi.gov.uk \r\nListing:  NW-Llandudnomclist@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01286 669 700 \r\nFine queries:  01633 645 112 \r\nPay a fine:  0300 790 9980 \r\nFixed penalties:  01639 605 784 \r\nListing:  01492 871 333 \r\n', 'Fax:  0870 739 4384 \r\n', 'Gwynedd', '1', '53.14043024', '-4.26433562', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:13:16', '0');
INSERT INTO `court` VALUES ('92', 'Caernarfon Probate Sub- Registry\r\n', 'The Criminal Justice Centre\r\nLlanberis Road\r\nCaernarfon\r\nGwynedd\r\nLL55 2DF ', 'Cardiff Probate Registry of Wales\r\n3rd Floor\r\nCardiff Magistrates\' Court\r\nFitzalan Place\r\nCardiff\r\nSouth Wales\r\nCF24 0RZ ', '\r\nEnquiries:  CaernarfonPSR@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01286 669 755 \r\nProbate helpline:  0300 123 1072 \r\n', '', 'Gwynedd', '1', '53.14043024', '-4.26433562', '1', 'Probate\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:13:16', '0');
INSERT INTO `court` VALUES ('93', 'Caerphilly Magistrates\' Court \r\n', 'The Court House\r\nMountain Road\r\nCaerphilly\r\nGlamorgan\r\nCF83 1HG ', 'Faulkner Road\r\nNewport Magistrates\' Court \r\nThe Law Courts\r\nFaulkner Road\r\nNewport\r\nGwent\r\nNP20 4PR ', 'Child maintenance:  gw-gwentmaintenance@hmcts.gsi.gov.uk \r\nEnquiries:  GW-adminenq@hmcts.gsi.gov.uk \r\nLegal aid:  Gwent-MC-LegalAid@hmcts.gsi.gov.uk \r\nListing:  gw.gwentmclisting@hmcts.gsi.gov.uk \r\nPo', 'Enquiries:  01633 261300 \r\nFine queries:  01633 645 112 \r\nPay a fine:  0300 790 9980 \r\nFixed penalties:  01639 605 784 \r\n', 'Fax:  0870 739 4319 \r\n', 'Glamorgan\r\n', '1', '51.56950068', '-3.21738759', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:13:16', '0');
INSERT INTO `court` VALUES ('94', 'Calderdale (Halifax) Magistrates\' Court and Family Court\r\n', 'Calderdale Magistrates\' Court and Family Court Hearing Centre\r\nHarrison Road\r\nHalifax\r\nWest Yorkshire\r\nHX1 2AN', 'Calderdale Magistrates\' Court and Family Court Hearing Centre\r\nP.O.Box 32\r\nHarrison Road\r\nHalifax\r\nWest Yorkshire\r\nHX1 2AN ', 'Enquiries:  wy-halifaxmags@hmcts.gsi.gov.uk \r\nWitness service:  calderdale.mc@cawitnessservice.cjsm.net \r\n', 'Enquiries:  01422 360 695 \r\nFine queries:  0113 307 6600 \r\nWitness service:  03003321148', 'Fax:  08707 394450 \r\n', 'West Yorkshire\r\n', '1', '53.72017965', '-1.86333645', '1', 'Crime\r\nChildren\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:13:16', '0');
INSERT INTO `court` VALUES ('95', 'Camberwell Green Magistrates\' Court\r\n', '15 D\'Eynsford Road\r\nCamberwell Green\r\nLondon\r\nSE5 7UP ', '', 'Enquiries:  gl-southgroupmcenq@hmcts.gsi.gov.uk \r\nFine queries:  LCCCCollectionUnit@hmcts.gsi.gov.uk \r\nListing:  gl-southgroupmclist@hmcts.gsi.gov.uk \r\nWitness service:  camberwellgreen.mc@cawitnessse', 'Enquiries:  020 7805 9851, 020 7805 9863, 020 7805 9860 \r\nFine queries:  020 7556 8500 \r\nPay a fine:  0300 790 9901 \r\nWitness service:  0300 332 1149 \r\n', '', 'London\r\n', '1', '51.47377700', '-0.09654322', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('96', 'Cambridge County Court and Family Court\r\n', '197 East Road\r\nCambridge\r\nCambridgeshire\r\nCB1 1BA ', 'Cambridge County Court and Family Court Hearing Centre\r\n197 East Road\r\nCambridge\r\nCambridgeshire\r\nCB1 1BA ', 'Enquiries:  cambridgecountyenquiries@hmcts.gsi.gov.uk \r\nFamily queries:  cambridgecountyfamily@hmcts.gsi.gov.uk \r\n', 'Enquiries:  0344 892 4000 \r\nClaims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372', 'Fax:  0870 761 7687, Money claims fax:  0161 743 4023 \r\n \r\n', 'Cambridgeshire\r\n', '1', '52.20371259', '0.13206539', '1', 'Divorce\r\nHigh court\r\nAdoption\r\nChildren\r\nMoney claims\r\nBankruptcy\r\nHousing possession\r\nDomestic violence\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('97', 'Cambridge Crown Court\r\n', 'The Court House\r\n83 East Road\r\nCambridge\r\nCambridgeshire\r\nCB1 1BT ', '', '\r\nEnquiries:  enquiries@cambridge.crowncourt.gsi.gov.uk', 'Enquiries:  01223 488 321 \r\nJury service:  01223 488 303 \r\nWitness service:  01223 488 378 \r\n', 'Fax:  01264 785012 \r\n', 'Cambridgeshire\r\n', '1', '52.20690001', '0.13679912', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('98', 'Cambridge Magistrates\' Court\r\n', '12 St Andrews Street\r\nCambridge\r\nCambridgeshire\r\nCB2 3AX ', 'The Court House\r\nBridge Street\r\nPeterborough\r\nCambridgeshire\r\nPE1 1ED ', 'Enquiries:  cb-enquiries@hmcts.gsi.gov.uk \r\nFine queries:  cens.warrants@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01223 376000 \r\nFine queries:  01502 501060, 01702 283 830 \r\n', 'Fax:  01264 785085 \r\n', 'Cambridgeshire\r\n', '1', '52.20419997', '0.12199038', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('99', 'Cannock Magistrates\' Court\r\n', 'The Court House\r\nWolverhampton Road\r\nCannock\r\nStaffordshire\r\nWS11 1AT ', 'Southern Courts and Administration Centre\r\nSouth Walls\r\nStafford\r\nStaffordshire\r\nST16 3DW ', '', 'Enquiries:  01785 275 700 \r\nFine queries:  01889 503 500 \r\nWitness service:  0300 3321000 \r\n', 'Fax:  0870 739 4179 \r\n', 'Staffordshire\r\n', '1', '52.68471852', '-2.03565241', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('100', 'Canterbury Combined Court Centre\r\n', 'Canterbury Combined Court\r\nThe Law Courts\r\nChaucer Road\r\nCanterbury\r\nKent\r\nCT1 1ZA ', 'Canterbury Combined Court and Family Court Hearing Centre\r\nThe Law Courts\r\nChaucer Road\r\nCanterbury\r\nKent\r\nCT1 1ZA ', 'Bailiffs:  bailiffs@canterbury.countycourt.gsi.gov.uk \r\nCounty Court:  enquiries@canterbury.countycourt.gsi.gov.uk \r\nCrown Court:  generaloffice@canterbury.crowncourt.gsi.gov.uk \r\nCrown Court listing:', 'Enquiries:  01227 819200 \r\nAppointments:  01227 819267 \r\n Small claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1057 \r\n', 'Crown Court fax:  01227 819329 \r\nCounty Court fax:  01264 347904 \r\nFamily fax:  01264 347904 \r\n', 'Kent\r\n', '1', '51.28197738', '1.09495687', '1', 'Adoption\r\nHousing possession\r\nHigh court\r\nDomestic violence\r\nChildren\r\nCrime\r\nBankruptcy\r\nMoney claims\r\nDivorce\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('101', 'Canterbury Magistrates\' Court\r\n', 'Broad Street\r\nCanterbury\r\nKent\r\nCT1 2UE ', 'Canterbury Magistrates\' Court and Family Court Hearing Centre\r\nPencester Road\r\nDover\r\nKent\r\nCT16 1BS ', 'Listing:  eastkentmc@hmcts.gsi.gov.uk', 'Enquiries:  01304 218 600 \r\nFine queries:  01622 680 070 \r\n', 'Fax:  0870 739 5848 \r\n', 'Kent\r\n', '1', '51.27870306', '1.08577331', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('102', 'Cardiff and the Vale Magistrates Court', 'Cardiff and the Vale Magistrates Court \r\nFitzalan Place\r\nCardiff\r\nSouth Wales\r\nCF24 0RZ ', '', 'Enquiries:  SW-cardiffmcenq@hmcts.gsi.gov.uk \r\n', 'Enquiries:  029 2046 3040 \r\nFixed penalties:  01639 605 784 \r\nFine queries:  01633 645 112 \r\nPay a fine:  0300 790 9980 \r\n', 'Fax:  0870 324 0236 \r\n', 'South Wales\r\n', '1', '51.48211358', '-3.16648335', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('103', 'Cardiff Civil and Family Justice Centre', 'Cardiff Civil and Family Justice Centre\r\n2 Park Street\r\nCardiff\r\nSouth Wales\r\nCF10 1ET ', '', 'Enquiries:  enquiries@cardiff.countycourt.gsi.gov.uk \r\nPersonal support unit:  Cardiff@thepsu.org.uk \r\nSmall claims mediation:  scmenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  029 2037 6400 \r\nListing:  029 2037 6412 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\nPersonal support unit:  029 2034 3685 \r\n', 'Fax:  01264 347948 \r\nBailiffs fax:  01264 347950 \r\nChildren cases fax:  01264 347952 \r\nCivil fax:  01264 347951 \r\nCivil listing fax:  01264 347943 \r\nDivorce fax:  01264 347959 \r\nMoney claims fax:  016', 'South Wales\r\n', '1', '51.47824079', '-3.17969010', '1', 'Forced marriage\r\nHigh court\r\nAdoption\r\nChildren\r\nBankruptcy\r\nDivorce\r\nDomestic violence\r\nCivil partnership\r\nHousing possession\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('104', 'Cardiff Crown Court\r\n', 'The Law Courts\r\nCathays Park\r\nCardiff\r\nSouth Wales\r\nCF10 3PG ', '', 'Enquiries:  listing@cardiff.crowncourt.gsi.gov.uk \r\n', 'Enquiries:  02920 678730 \r\n', 'Fax:  0870 3240261 \r\n', 'South Wales\r\n', '1', '51.48455641', '-3.18025637', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('105', 'Cardiff Probate Registry of Wales\r\n', 'Cardiff Probate Registry of Wales\r\n3rd Floor\r\nCardiff Magistrates Court\r\nFitzalan Place\r\nCardiff\r\nSouth Wales\r\nCF24 0RZ ', '', 'Enquiries:  CardiffDPRsolicitorsenquiries@hmcts.gsi.gov.uk, CardiffDPRenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  029 2047 4373 \r\nProbate helpline:  0300 123 1072 \r\n', 'Fax:  029 2045 6411 \r\n', 'South Wales\r\n', '1', '51.48211425', '-3.16648369', '1', 'Probate\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('106', 'Cardiff Social Security and Child Support Tribunal\r\n', 'Eastgate House\r\nNewport Road\r\nCardiff\r\nSouth Wales\r\nCF24 0YP ', '', 'Enquiries:  cardiff-sscsa@hmcts.gsi.gov.uk \r\n', 'Enquiries:  0300 123 1142 \r\n', 'Fax:  0870 739 4438 \r\n', 'South Wales\r\n', '1', '0.00000000', '0.00000000', '1', 'Social security', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('107', 'Care Standards Tribunal\r\n', '1st Floor\r\nDarlington Magistrates\' Court\r\nParkgate\r\nDarlington\r\nCo. Durham\r\nDL1 1RU ', '', '', 'Enquiries:  01325 289 350 \r\n', 'Fax:  01325 289 395 \r\n', 'Co. Durham', '1', '0.00000000', '0.00000000', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('108', 'Carlisle Combined Court\r\n', 'Courts of Justice\r\nEarl Street\r\nCarlisle\r\nCumbria\r\nCA1 1DJ ', '', 'Bailiffs:  bailiffs@carlisle.countycourt.gsi.gov.uk \r\nCivil listing:  hearings@carlisle.countycourt.gsi.gov.uk \r\nCriminal queries:  enquiries@carlisle.crowncourt.gsi.gov.uk \r\nCrown Court listing:  lis', 'County Court:  01228 882 140 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\nCrown Court:  01228 882 120 \r\nWitness service:  0300 332 1157 \r\n', 'Fax:  0870 324 0259,  Money claims fax:  0161 743 4023 \r\n\r\n', 'Cumbria\r\n', '1', '54.89355895', '-2.93124755', '1', 'Divorce\r\nHousing possession\r\nAdoption\r\nHigh court\r\nBankruptcy\r\nDomestic violence\r\nChildren\r\nCrime\r\nMoney claims\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:07', '0');
INSERT INTO `court` VALUES ('109', 'Carlisle Magistrates\' Court\r\n', 'The Court House\r\nRickergate\r\nCarlisle\r\nCumbria\r\nCA3 8QH ', '', 'Enquiries:  cumbria.north.magistrates@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01228 518 800, 01228 518 834 \r\nFine queries:  01539 720 478 \r\nWitness service:  0300 332 1158 \r\n', 'Fax:  01228 518 844 \r\n', 'Cumbria\r\n', '1', '54.89751772', '-2.93553374', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:08', '0');
INSERT INTO `court` VALUES ('110', 'Carlisle Probate Sub-Registry\r\n', 'Newcastle-Upon-Tyne District Probate Registry\r\nNewcastle DPR\r\nNo 1 Waterloo Square\r\nNewcastle-upon-Tyne\r\nTyne & Wear\r\nNE1 4DR ', '', 'Enquiries:  NewcastleDPRenquiries@hmcts.gsi.gov.uk, NewcastleDPRsolicitorsenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  019 1 211 2170 \r\nProbate helpline:  0300 123 1072 \r\n', '', 'Tyne & Wear\r\n', '1', '0.00000000', '0.00000000', '1', 'Probate\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:08', '0');
INSERT INTO `court` VALUES ('111', 'Carmarthen County Court and Family Court\r\n', 'Carmarthen County Court and Family Court Hearing Centre\r\nThe Hearing Centre\r\nHill House\r\nPicton Terrace\r\nCarmarthen\r\nCarmarthenshire\r\nSA31 3BT ', '', 'Enquiries:  enquiries@carmarthen.countycourt.gsi.gov.uk \r\nSmall claims mediation:  scmenquiries@hmcts.gsi.gov.uk', 'Enquiries:  01267 226780 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\nBailiffs:  01267 226788', 'Fax:  0870 7617684, Money claims fax:  0161 743 4023 \r\n \r\n', 'Carmarthenshire\r\n', '1', '51.85497771', '-4.31789400', '1', 'Divorce\r\nBankruptcy\r\nDomestic violence\r\nChildren\r\nHousing possession\r\nMoney claims\r\nHigh court\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:08', '0');
INSERT INTO `court` VALUES ('112', 'Carmarthen Probate Sub-Registry\r\n', 'Carmarthen Hearing Centre\r\nHill House\r\nPicton Terrace\r\nCarmarthen\r\nCarmarthenshire\r\nSA31 3BT ', '', 'Enquiries:  CarmarthenPSR@hmcts.gsi.gov.uk, CarmarthenPSR@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01267 226781 \r\nProbate helpline:  0300 123 1072 \r\n', '', 'Carmarthenshire\r\n', '1', '51.85497848', '-4.31789467', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:08', '0');
INSERT INTO `court` VALUES ('113', 'Central Buckinghamshire Magistrates\' Court and Family Court\r\n', 'Walton Street\r\nAylesbury\r\nBuckinghamshire\r\nHP21 7QZ ', 'Central Buckinghamshire Magistrates\' Court and Family Court Hearing Centre\r\nWalton Street\r\nAylesbury\r\nBuckinghamshire\r\nHP21 7QZ ', '', 'Enquiries:  01296 554 350 \r\n', '', 'Buckinghamshire\r\n', '1', '51.81078059', '-0.80817086', '1', 'Divorce\r\nDomestic violence\r\nAdoption\r\nChildren\r\nCrime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:08', '0');
INSERT INTO `court` VALUES ('114', 'Central Criminal Court\r\n', 'Old Bailey\r\nLondon\r\nEC4M 7EH ', '', 'ccc.cso@hmcts.gsi.gov.uk, results@central.crowncourt.gsi.gov.uk, enquiries@central.crowncourt.gsi.gov.uk \r\nCase progression:  caseprogression@central.crowncourt.gsi.gov.uk \r\nListing:  listing@central.', 'Enquiries:  020 7248 3277 \r\n', 'Fax:  0870 324 0234, 0870 324 0239 \r\n', 'London\r\n', '1', '51.51549205', '-0.10199667', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:08', '0');
INSERT INTO `court` VALUES ('115', 'Central Family Court\r\n', 'Central Family Court\r\nFirst Avenue House\r\n42-49 High Holborn\r\nLondon\r\nWC1V 6NP ', '', '\r\nCfc.decreeabsolute@hmcts.gsi.gov.uk, Cfc.fru@hmcts.gsi.gov.uk \r\nAdoption:  Cfc.adoptions@hmcts.gsi.gov.uk \r\nCare cases:  Cfc.publiclaw@hmcts.gsi.gov.uk \r\nChildren cases:  Cfc.privatelaw@hmcts.gsi.go', 'Enquiries:  0207 421 8594 \r\n', 'Fax:  0870 739 5973 \r\n', 'London\r\n', '1', '0.00000000', '0.00000000', '1', 'Divorce\r\nChildren\r\nForced marriage\r\nCivil partnership\r\nAdoption\r\nDomestic violence\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:08', '0');
INSERT INTO `court` VALUES ('116', 'Centralised Attachment of Earnings Payments (CAPS)\r\n', '5th Floor\r\nSt Katharine\'s House\r\n21-27 St Katharine\'s Street\r\nNorthampton\r\nNorthamptonshire\r\nNN1 2LH ', '', 'Enquiries:  Caps@hmcts.gsi.gov.uk \r\n', 'Enquiries:  0300 123 1058, 01604 619 435 \r\n', 'Fax:  0870 324 0089 \r\n', 'Northamptonshire\r\n', '1', '52.23730180', '-0.89996546', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:16:08', '0');
INSERT INTO `court` VALUES ('117', 'Central London County Court\r\n', '5th Floor\r\nSt Katharine\'s House\r\n21-27 St Katharine\'s Street\r\nNorthampton\r\nNorthamptonshire\r\nNN1 2LH ', '', 'Enquiries:  Caps@hmcts.gsi.gov.uk \r\n', 'Enquiries:  0300 123 1058, 01604 619 435 \r\n', 'Fax:  0870 324 0089 \r\n', 'Northamptonshire\r\n', '1', '52.23730180', '-0.89996546', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:03', '0');
INSERT INTO `court` VALUES ('118', 'Central London County Court (Bankruptcy)\r\n', 'Room WB10 West Block\r\nRoyal Courts of Justice\r\nStrand\r\nLondon\r\nWC2A 2LL ', '', 'RCJBankCLCCDJHearings@hmcts.gsi.gov.uk \r\n', 'Enquiries:  020 7947 6441, 020 7947 6812 , 020 7947 6839, 020 7947 6078, 020 7947 6863, 020 7947 6448 \r\nClerks:  020 7947 6187, 020 7947 7387 \r\n', 'Fax:  0870 761 7695 \r\n', 'London\r\n', '1', '0.00000000', '0.00000000', '1', 'Money claims\r\nHousing possession\r\nBankruptcy\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:03', '0');
INSERT INTO `court` VALUES ('119', 'Central London Employment Tribunal\r\n', 'Victory House\r\n30-34 Kingsway\r\nLondon\r\nWC2B 6EX \r\n', '', '', 'Enquiries:  020 7273 8603 \r\nListing:  020 7273 8593 \r\n', '', 'London\r\n', '1', '51.51581584', '-0.11874543', '1', 'Employment\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:03', '0');
INSERT INTO `court` VALUES ('120', 'Chancery Division\r\n', 'The Rolls Building\r\n7 Rolls Buildings\r\nFetter Lane\r\nLondon\r\nEC4A 1NL ', '', 'Chancery issue:  chancery.issue@hmcts.gsi.gov.uk \r\nChancery judges listing:  rcjchancery.judgeslisting@hmcts.gsi.gov.uk \r\nChancery masters appointments:  chancery.mastersappointments@hmcts.gsi.gov.uk ', 'Issue:  020 7947 7783 \r\nMasters:  020 7947 7391 \r\nListing:  020 7947 7717, 020 7947 6690 \r\nOrders and accounts:  020 7947 6322, 020 7947 6733 \r\n', 'Issue fax:  0870 761 7719, Masters fax:  0870 761 7720, Listing fax:  0870 739 5869', 'London\r\n', '1', '51.51589485', '-0.10967644', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('121', 'Chatham Magistrates\' Court and Family Court', 'The Court House\r\nThe Brook\r\nChatham\r\nKent\r\nME4 4JZ ', 'Chatham Magistrates\' Court and Family Court Hearing Centre\r\nThe Court House\r\nThe Brook\r\nChatham\r\nKent\r\nME4 4JZ ', '', 'Enquiries:  01634 830 232 \r\nFine queries:  01622 680 070 \r\nWitness service:  01634 830 232 Ext 200 \r\n', 'Fax:  01634 847 400 \r\n', 'Kent\r\n', '1', '51.38422918', '0.52688236', '1', 'Crime\r\nAdoption\r\nChildren\r\nDomestic violence\r\nDivorce\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('122', 'Chelmsford County and Family Court\r\n', 'Priory Place\r\nNew London Road\r\nChelmsford\r\nEssex\r\nCM2 0PP ', 'Chelmsford County and Family Court Hearing Centre\r\nPriory Place\r\nNew London Road\r\nChelmsford\r\nEssex\r\nCM2 0PP ', 'Enquiries:  enquiries@chelmsford.countycourt.gsi.gov.uk \r\nFamily queries:  family@chelmsford.countycourt.gsi.gov.uk \r\n', 'Enquiries:  0344 892 4000 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\n \r\n', 'Civil fax:  01264 347923, Money claims fax:  0161 743 4023 \r\nFamily fax:  01264 347922', 'Essex\r\n', '1', '51.73018922', '0.47130543', '1', 'Adoption\r\nHousing possession\r\nBankruptcy\r\nDivorce\r\nMoney claims\r\nChildren\r\nDomestic violence\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('123', 'Chelmsford Crown Court\r\n', 'P.O. Box 9\r\nNew Street\r\nChelmsford\r\nEssex\r\nCM1 1EL ', '', '', 'Enquiries:  012 45 603 000 \r\n', 'Fax:  0870 739 4375 \r\n', 'Essex\r\n', '1', '51.73721306', '0.47599066', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('124', 'Chelmsford Magistrates\' Court and Family Court\r\n', '10 New Street\r\nChelmsford\r\nEssex\r\nCM1 1NT ', 'Chelmsford Magistrates\' Court and Family Court Hearing Centre\r\nCourt Administration Centre\r\nP.O. Box 10754\r\nChelmsford\r\nEssex\r\nCM1 9PZ ', 'Enquiries:  esosprey@hmcts.gsi.gov.uk \r\nFamily queries:  family@chelmsford.countycourt.gsi.gov.uk \r\n', 'Enquiries:  01245 313 300 \r\nFamily queries:  0844 892 4000 \r\nFine queries:  01553 778730, 01702 283830, 01603 230403 \r\n', 'Fax:  0870 324 0091 \r\n', 'Essex\r\n', '1', '51.73590042', '0.47255856', '1', 'Divorce\r\nDomestic violence\r\nAdoption\r\nChildren\r\nCrime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('125', 'Cheltenham Magistrates\' Court\r\n', 'Cheltenham Courthouse\r\nSt Georges Road\r\nCheltenham\r\nGloucestershire\r\nGL50 3PF ', 'HMCTS Gloucestershire\r\nPO Box 9051\r\nGloucester\r\nGloucestershire\r\nGL1 2XG ', 'Admin:  gs-glosmcadmin@hmcts.gsi.gov.uk \r\nEnforcement:  gs-glosmcenforcement@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01452 334 400 \r\nWitness service:  0300 332 1000 \r\nWitness care unit:  01452 754 517 \r\n', 'Fax:  0870 324 0171 \r\n', 'Gloucestershire\r\n', '1', '51.89909930', '-2.08343350', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('126', 'Cheshire and Merseyside Central Payment and Enforcement Centre\r\n', 'P.O. Box 104\r\nRuncorn\r\nCheshire\r\nWA7 2GE', 'P.O. Box 104\r\nRuncorn\r\nCheshire\r\nWA7 2GE ', 'Ch-Me-arrestwarrants@hmcts.gsi.gov.uk \r\nAccounts:  Ch-Me-accounts@hmcts.gsi.gov.uk \r\nFine queries:  Ch-Me-fines@hmcts.gsi.gov.uk \r\n', 'Enquiries:  0845 200 2078 \r\nFine queries:  01928 703324 \r\nPay a fine:  0300 790 9901 \r\n', '', 'Cheshire', '1', '53.32577679', '-2.69446647', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('127', 'Chester Civil and Family Justice Centre\r\n', 'Trident House\r\nLittle St John Street\r\nChester\r\nCheshire\r\nCH1 1SN ', '', 'Civil queries:  civil@chester.countycourt.gsi.gov.uk \r\nFamily queries:  family@chester.countycourt.gsi.gov.uk \r\nMediation:  scmenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01244 404 200 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\n', 'Fax:  0870 324 0311,  Money claims fax:  0161 743 4023 \r\n\r\n', 'Cheshire', '1', '53.18859317', '-2.88701554', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('128', 'Chester Crown Court\r\n', 'The Castle\r\nChester\r\nCheshire\r\nCH1 2AN ', '', 'Enquiries:  chester.enquiries@chester.crowncourt.gsi.gov.uk \r\n', 'Enquiries:  01244 317606 \r\nWitness service:  0300 332 1164 \r\n', '', 'Cheshire', '1', '53.18600546', '-2.89182608', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('129', 'Chesterfield Magistrates Court, County Court and Family Court\r\n', 'Chesterfield Magistrates Court,County Court and Family Court hearing centre\r\nTapton Lane\r\nChesterfield\r\nChesterfield\r\nDerbyshire\r\nS41 7TW ', '', 'Family queries:  db-family@hmcts.gsi.gov.uk \r\nPost court:  db-chfmpostcourt@hmcts.gsi.gov.uk \r\nPre court:  db-chfmprecourt@hmcts.gsi.gov.uk \r\n', 'Pay a fine:  0300 790 9901 \r\nMagistrates\' pre court fax:  0870 324 0065 \r\nMagistrates\' post court fax:  0870 324 0059 \r\nWitness care unit:  01246 522 569 \r\nWitness service:  0300 3321000 \r\nFine querie', '', 'Derbyshire\r\n', '1', '53.23695969', '-1.42374370', '1', 'Adoption\r\nCrime\r\nMoney claims\r\nChildren\r\nDomestic violence\r\nHigh court\r\nBankruptcy\r\nDivorce\r\nHousing possession\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('130', 'Chester-le-Street Magistrates\' Court\r\n', 'Newcastle Road\r\nChester-le-Street\r\nCo. Durham\r\nDH3 3UA ', '', '\r\nListing:  du-mclisting@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01913 870 700 \r\nFine queries:  01642 261631, 01642 261643 \r\nListing:  01913 870 705 \r\n', 'Fax:  0870 739 4479 \r\n', 'Co. Durham\r\n', '1', '54.86138716', '-1.57302151', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('131', 'Chester Magistrates\' Court\r\n', 'Grosvenor Street\r\nChester\r\nCheshire\r\nCH1 2XA ', '', 'Enquiries:  customerenquirieschestermc@hmcts.gsi.gov.uk \r\nFine queries:  Ch-Me-fines@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01244 405 790 \r\nPay a fine:  0300 790 9901 \r\nFine queries:  01928 703324 \r\nWitness service:  0300 332 1165 \r\n', 'Fax:  0870 324 0207 \r\n', '', '1', '53.18763789', '-2.89350621', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('132', 'Chester Probate Sub-Registry\r\n', 'Chester Civil Justice Centre\r\nTrident House\r\nLittle John Street\r\nChester\r\nCheshire\r\nCH1 1SN ', '2nd Floor East Wing\r\nQE11 Law Courts\r\nDerby Square\r\nLiverpool\r\nMerseyside\r\nL2 1XA ', 'Enquiries:  liverpooldprsolicitorsenquiries@hmcts.gsi.gov.uk , liverpooldprenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  0151 236 8264 \r\nProbate helpline:  0845 30 20 900 \r\nProbate helpline:  0300 123 1072', 'Fax:  0870 324 0207 \r\n', 'Cheshire', '1', '53.18859317', '-2.88701554', '1', 'Probate\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('133', 'Chichester Combined Court Centre\r\n', 'Southgate\r\nChichester\r\nWest Sussex\r\nPO19 1SX ', 'Chichester Combined Court and Family Court Hearing Centre\r\nSouthgate\r\nChichester\r\nWest Sussex\r\nPO19 1SX ', 'County Court:  enquiries@chichester.countycourt.gsi.gov.uk \r\nCrown Court:  enquiries@chichester.crowncourt.gsi.gov.uk \r\nFamily queries:  ss-ccfamchichester@hmcts.gsi.gov.uk \r\nWitness service:  chiches', 'Appointments:  01243 520 702 \r\nBailiffs:  01243 520 701 \r\nCivil and family:  01243 520 700 \r\nCrown Court:  01243 520 742 \r\nJury service:  01243 520 743 \r\nSmall claims mediation:  01604 795511 \r\nWitnes', 'County Court fax:  0870 324 0244, \r\nCrown Court fax:  01264 785011,\r\nMoney claims fax:  0161 743 4023 \r\n', 'West Sussex\r\n', '1', '50.83255373', '-0.77987560', '1', 'High court\r\nCrime\r\nMoney claims\r\nHousing possession\r\nAdoption\r\nDivorce\r\nDomestic violence\r\nChildren\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('134', 'Chichester Magistrates\' Court\r\n', '6 Market Avenue\r\nChichester\r\nWest Sussex\r\nPO19 1YE ', 'Sussex Magistrates Administration Centre\r\nEdward Street\r\nBrighton\r\nEast Sussex\r\nBN2 0LG ', 'Case progression:  ss-sussexcpo@hmcts.gsi.gov.uk \r\nEnquiries:  ss-sussexadmin@hmcts.gsi.gov.uk \r\nFine queries:  ss-easternfmcenq@hmcts.gsi.gov.uk \r\nLegal aid:  BirminghamCAT@legalaid.gsi.gov.uk \r\nList', 'Enquiries:  01273 670888 \r\nLegal aid:  01245 313391 \r\nFine queries:  01622 680070 \r\nPay a fine:  0300 790 9901 \r\n', 'Fax:  0870 761 7669 \r\n', 'West Sussex\r\n', '1', '50.83325142', '-0.77951660', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('135', 'Chippenham and Trowbridge Law Courts\r\n', 'Chippenham Law Courts\r\nPewsham Way\r\nChippenham\r\nWiltshire\r\nSN15 3BF ', '', 'County Court:  chipp&trowcty@hmcts.gsi.gov.uk \r\nFamily queries:  swindon.cty.fly@hmcts.gsi.gov.uk \r\nMagistrates\' court:  wi-nwwiltsmcadmin@hmcts.gsi.gov.uk \r\nSmall claims mediation:  scmenquiries@hmct', 'Enquiries:  01249 463 473 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\n\r\nFamily queries:  01793 690 500 \r\nFine queries:  01633 645 112 \r\nWitness service', 'Fax:  01249 466 246, Money claims fax:  0161 743 4023 \r\n', 'Wiltshire\r\n', '1', '51.45272554', '-2.11118137', '1', 'Domestic violence\r\nHousing possession\r\nCrime\r\nMoney claims\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('136', 'Chippenham Magistrates\' Court\r\n', '', '', '', '', '', '', '1', '0.00000000', '0.00000000', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:04', '0');
INSERT INTO `court` VALUES ('137', 'Chorley Magistrates\' Court\r\n', 'The Court House\r\nSt Thomas\'s Square\r\nChorley\r\nLancashire\r\nPR7 1RZ ', 'Preston Magistrates\' Court\r\nPO Box 52\r\nLawson Street\r\nPreston\r\nLancashire\r\nPR1 2QT ', 'Enquiries:  LN-prestonMCEnq@hmcts.gsi.gov.uk \r\nFamily queries:  prestonfcissue@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01772 208 000 \r\nFamily queries:  01772 272 887 \r\nFine queries:  01282 610 000 \r\nWitness service:  0300 332 1169 \r\n', 'Fax:  0870 324 0190 \r\n', 'Lancashire\r\n', '1', '53.65403054', '-2.63460954', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('138', 'City of London Magistrates\' Court\r\n', 'Visit us:\r\n1 Queen Victoria Street\r\nLondon\r\nEC4N 4XY ', '181 Marylebone Road\r\nLondon\r\nNW1 5BR ', 'Enquiries:  westminster.mc@hmcts.gsi.gov.uk \r\nFine queries:  LCCCCollectionUnit@hmcts.gsi.gov.uk \r\nWitness service:  cityoflondon.mc@cawitnessservice.cjsm.net \r\n', 'Enquiries:  020 3126 3050 \r\nFine queries:  020 7556 8500 \r\nDisabled access:  020 3126 3050 \r\nWitness service:  0300 332 1170', 'Fax:  0870 761 7629 \r\n', 'London\r\n', '1', '51.51298700', '-0.09009700', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('139', 'Claim Production Centre (CPC)\r\n', '', '', '', '', '', '', '1', '0.00000000', '0.00000000', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('140', 'Clerkenwell and Shoreditch County Court and Family Court\r\n', 'The Gee Street Courthouse\r\n29-41 Gee Street\r\nLondon\r\nEC1V 3RE ', 'Hearing Centre\r\nThe Gee Street Courthouse\r\n29-41 Gee Street\r\nLondon\r\nEC1V 3RE ', 'Telephone.hearings@clerkenwellandshoreditch.countycourt.gsi.gov.uk, hearings@clerkenwellandshoreditch.countycourt.gsi.gov.uk \r\nBailiffs:  Bailiffs@clerkenwellandshoreditch.countycourt.gsi.gov.uk \r\nEnq', 'Enquiries:  020 7250 7200 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\nMoney claims fax:  0161 743 4023 \r\nBailiffs:  020 7250 7151, 020 7250 7152 \r\n', 'Fax:  0870 761 7688 \r\n', 'London\r\n', '1', '51.52460542', '-0.09779758', '1', 'Domestic violence\r\nHousing possession\r\nMoney claims\r\nDivorce\r\nChildren\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('141', 'Cleveland Central Enforcement Unit\r\n', 'CD CEU\r\nPO BOX 529\r\nMiddlesbrough\r\nCleveland\r\nTS1 9EJ ', '', 'Enquiries:  CL-MbroMCCFines@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01642 240301 \r\n', '', 'Cleveland\r\n', '1', '0.00000000', '0.00000000', '1', '', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('142', 'Colchester County Court and Family Court\r\n', 'Falkland House\r\n25 Southway\r\nColchester\r\nEssex\r\nCO3 3EG ', 'Colchester County Court and Family Court Hearing Centre\r\nFalkland House\r\n25 Southway\r\nColchester\r\nEssex\r\nCO3 3EG ', 'Enquiries:  enquiries@colchester.countycourt.gsi.gov.uk \r\n', 'Enquiries:  0344 892 4000 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\n', 'Fax:  01264 347924, Money claims fax:  0161 743 4023 \r\n\r\n', 'Essex\r\n', '1', '51.88726848', '0.89398718', '1', 'Bankruptcy\r\nHousing possession\r\nDomestic violence\r\nMoney claims\r\nHigh court\r\nChildren\r\nDivorce\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('143', 'Colchester Magistrates\' Court\r\n', 'St Botolphs Circus\r\nMagdelen Street\r\nColchester\r\nEssex\r\nCO2 7EF ', '', 'Enquiries:  esosprey@hmcts.gsi.gov.uk \r\nFamily queries:  family@chelmsford.countycourt.gsi.gov.uk \r\n', 'Enquiries:  01245 313 300 \r\nFamily queries:  0844 892 4000 \r\nFine queries:  01603 230 403, 01702 283 830, 01553 778 730 \r\n', 'Fax:  0870 324 0091 \r\n', 'Essex\r\n', '1', '51.88647858', '0.90413913', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('144', 'Consett Magistrates\' Court\r\n', 'Ashdale Road\r\nConsett\r\nCo. Durham\r\nDH8 6LY ', 'Newcastle Road\r\nChester-le-Street\r\nCo. Durham\r\nDH3 3UA ', 'Listing:  du-mclistingnth@hmcts.gsi.gov.uk \r\nWitness service:  consett.mc@cawitnessservice.cjsm.net \r\n', 'Enquiries:  01913 870 700 \r\nListing:  01913 870 705 \r\nWitness service:  03003321172 \r\nWitness care unit:  01913 752 936 \r\nFine queries:  01642 261631, 01642 261643 \r\n', 'Fax:  0870 739 4479 \r\n', 'Co. Durham\r\n', '1', '54.85756129', '-1.83028760', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('145', 'Conwy and Colwyn County Court and Family Court\r\n', 'Court hearings:\r\nConwy Road\r\nLlandudno\r\nConwy\r\nLL30 1GA ', 'Rhyl County Court and Family Court Hearing Centre\r\nThe Courthouse\r\nClwyd Street\r\nRhyl\r\nDenbighshire\r\nLL18 3LA ', 'Enquiries:  enquiries@rhyl.countycourt.gsi.gov.uk \r\nFamily queries:  family@rhyl.countycourt.gsi.gov.uk \r\nSmall claims mediation:  scmenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01745 352 940 \r\nSmall claims mediation:  01604 795511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\n', 'Fax:  0870 739 4189, Money claims fax:  0161 743 4023 \r\n\r\n', 'Conwy\r\n', '1', '53.32020769', '-3.82327748', '1', 'Housing possession\r\nMoney claims\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('146', 'Corby Magistrates\' Court\r\n', 'The Court House\r\nElizabeth Street\r\nCorby\r\nNorthamptonshire\r\nNN17 1SH ', 'Regent\'s Pavillion\r\nSummerhouse Road\r\nMoulton Park\r\nNorthampton\r\nNorthamptonshire\r\nNN3 6AS ', 'Enquiries:  NH-Northantscourtenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01604 497 000 \r\nFine queries:  01604 497 040 \r\nWitness service:  0300 3321000 \r\n', 'Fax:  01604 497 010 \r\n', 'Northamptonshire\r\n', '1', '52.48782710', '-0.69904302', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('147', 'County Court Business Centre (CCBC)\r\n', 'St. Katharine\'s House\r\n21-27 St. Katharine\'s Street\r\nNorthampton\r\nNorthamptonshire\r\nNN1 2LH ', '', 'tec@hmcts.gsi.gov.uk, mcol@hmcts.gsi.gov.uk, ccbcclaimants@hmcts.gsi.gov.uk, ccbcdefendants@hmcts.gsi.gov.uk \r\n', 'Bulk claims:  01604 619 400, 0300 123 1056 \r\nCounty Court Money Claims Centre:  0300 123 1057, 01604 619 402 \r\nTraffic enforcement:  01604 619 450, 0300 123 1059 \r\nCentralised attachment of earnings: ', 'Bulk claims fax:  0870 324 0166, Money claims fax:  01604 619 526, Traffic enforcement fax:  0870 324 0092, Centralised attachment of earnings fax:  0870 324 0089  \r\n', 'Northamptonshire\r\n', '1', '0.00000000', '0.00000000', '1', 'Housing possession\r\nMoney claims\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('148', 'County Court Money Claims Centre (CCMCC)\r\n', 'County Court Money Claims Centre\r\nPO Box 527\r\nSalford\r\nGreater Manchester\r\nM5 0BY ', '', 'Enquiries:  ccmcccustomerenquiries@hmcts.gsi.gov.uk \r\nFiling and records:  ccmcce-filing@hmcts.gsi.gov.uk \r\n', 'Enquiries:  0300 123 1372 \r\n', 'Fax:  0161 743 4023', 'Greater Manchester', '1', '0.00000000', '0.00000000', '1', 'Money claims', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('149', 'Court of Appeal Civil Division\r\n', 'Room E307\r\nRoyal Courts of Justice\r\nStrand\r\nLondon\r\nWC2A 2LL ', '', 'civilappeals.cmsb@hmcts.gsi.gov.uk, civilappeals.cmsc@hmcts.gsi.gov.uk, civilappeals.listing@hmcts.gsi.gov.uk, civilappeals.associates@hmcts.gsi.gov.uk, civilappeals.registry@hmcts.gsi.gov.uk, civilap', 'Enquiries:  020 7947 7677 \r\nRegistry:  020 7947 7121 \r\nAssociates:  020 7947 6879 \r\nCase progression (A):  020 7947 6139 \r\nCase progression (B):  020 7947 7828 \r\nCase progression (C):  020 7947 6096 \r', '', 'London\r\n', '1', '51.51414311', '-0.11374170', '1', 'Fax service/Area of law not available.', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('150', 'Court of Protection\r\n', 'PO Box 70185\r\nFirst Avenue House\r\n42-49 High Holborn\r\nLondon\r\nWC1A 9JA ', '', '', 'Enquiries:  0300 456 4600 \r\n', '', 'London\r\n', '1', '51.51843600', '-0.11451600', '1', 'E-mail service/Area of law not available.', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('151', 'Coventry Combined Court Centre\r\n', '140 Much Park Street\r\nCoventry\r\nWest Midlands\r\nCV1 2SN ', '', '\r\nCoventryFamilyReports@hmcts.gsi.gov.uk \r\nBailiffs:  Bailiffs@Coventry.CountyCourt.gsi.gov.uk \r\nCase progression:  listingandcaseprogression@warwick.crowncourt.gsi.gov.uk \r\nChildren cases:  Children@', 'Civil and family:  0300 123 5577 \r\nSmall claims mediation:  01604 795 511 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\nCriminal queries:  01926 682 411, 01926 429 133 \r\nFax:  0247 652 0443 \r\nW', 'Fax:  0247 652 0443, Money claims fax:  0161 743 4023 \r\n\r\n', 'West Midlands', '1', '52.40558365', '-1.50643964', '1', 'Domestic violence\r\nMoney claims\r\nCrime\r\nAdoption\r\nChildren\r\nBankruptcy\r\nDivorce\r\nHousing possession\r\nHigh court\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('152', 'Coventry Magistrates\' Court\r\n', 'Little Park Street\r\nCoventry\r\nWest Midlands\r\nCV1 2SQ ', '', 'Case progression:  wm-caseprogressioncoventry@hmcts.gsi.gov.uk \r\nFamily queries:  family@coventry.county.gsi.gov.uk \r\nListing:  WM-ListingCoventry@hmcourts-service.gsi.gov.uk \r\n', 'Enquiries:  02476 630 666 \r\nFamily queries:  0300 123 5577 \r\nFine queries:  0121 212 7820 \r\nWitness service:  0300 3321000 \r\n', 'Fax:  0870 324 0298 \r\n', 'West Midlands\r\n', '1', '52.40583226', '-1.50762739', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('153', 'Crawley Magistrates\' Court\r\n', 'The Law Courts\r\nWoodfield Road\r\nCrawley\r\nWest Sussex\r\nRH10 8BF', 'Sussex Magistrates Administration Centre\r\nEdward Street\r\nBrighton\r\nEast Sussex\r\nBN2 0LG ', 'Case progression:  ss-sussexcpo@hmcts.gsi.gov.uk \r\nEnquiries:  ss-sussexadmin@hmcts.gsi.gov.uk \r\nFine queries:  ss-easternfmcenq@hmcts.gsi.gov.uk \r\nLegal aid:  es-legalaid@hmcts.gsi.gov.uk \r\nListing: ', 'Enquiries:  01273 670888 \r\nLegal aid:  0121 232 5667 \r\nFine queries:  01622 680070 \r\nPay a fine:  0300 790 9901 \r\n', 'Fax:  0870 761 7669 \r\n', 'West Sussex\r\n', '1', '51.11736799', '-0.18107009', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:05', '0');
INSERT INTO `court` VALUES ('154', 'Crewe County Court and Family Court\r\n', 'The Law Courts\r\nCivic Centre\r\nCrewe\r\nCheshire\r\nCW1 2DP ', 'Crewe County Court and Family Court Hearing Centre\r\nThe Law Courts\r\nCivic Centre\r\nCrewe\r\nCheshire\r\nCW1 2DP ', 'Civil queries:  CH-crewectycivil@hmcts.gsi.gov.uk \r\nFamily queries:  CH-crewectyfamily@hmcts.gsi.gov.uk \r\nMediation:  scmenquiries@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01270 539 300 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\nSmall claims mediation:  01604 795511 \r\nMoney claims fax:  0161 743 4023 \r\n', 'Fax:  0870 761 7649', 'Cheshire', '1', '53.09766649', '-2.43872475', '1', 'Divorce\r\nChildren\r\nBankruptcy\r\nMoney claims\r\nHigh court\r\nDomestic violence\r\nHousing possession\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:06', '0');
INSERT INTO `court` VALUES ('155', 'Crewe (South Cheshire) Magistrates\' Court', 'The Law Courts\r\nCivic Centre\r\nCrewe\r\nCheshire\r\nCW1 2DT ', '', 'Enquiries:  CH-CreweMCAdmin@hmcts.gsi.gov.uk \r\nFine queries:  Ch-Me-fines@hmcts.gsi.gov.uk \r\n', 'Enquiries:  01270 655 920 \r\nFine queries:  01928 703324 \r\nPay a fine:  0300 790 9901 \r\nWitness service:  0300 332 1177 \r\n', 'Fax:  0870 761 7661 ', 'Cheshire\r\n', '1', '53.09766649', '-2.43872475', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:06', '0');
INSERT INTO `court` VALUES ('156', 'Croydon County Court and Family Court\r\n', 'The Law Courts\r\nAltyre Road\r\nCroydon\r\nSurrey\r\nCR9 5AB ', '', 'Bailiffs:  bailiffs@croydon.countycourt.gsi.gov.uk \r\nEnquiries:  enquiries@croydon.countycourt.gsi.gov.uk \r\nFamily queries:  family@croydon.countycourt.gsi.gov.uk \r\nFiling and records:  e-filing@croyd', 'Money claims fax:  0161 743 4023 \r\nCounty Court Money Claims Centre:  0300 123 1372 \r\nCivil and family:  0300 123 5577 \r\n', 'Fax:  01264 347989 \r\n', 'Surrey\r\n', '1', '51.37333754', '-0.09214917', '1', 'Money claims\r\nHigh court\r\nDivorce\r\nBankruptcy\r\nHousing possession\r\nDomestic violence\r\nChildren\r\nAdoption\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:06', '0');
INSERT INTO `court` VALUES ('157', 'Croydon Crown Court\r\n', 'The Law Courts\r\nAltyre Road\r\nCroydon\r\nSurrey\r\nCR9 5AB ', '', 'results@croydon.crowncourt.gsi.gov.uk \r\nEnquiries:  enquiries@croydon.crowncourt.gsi.gov.uk \r\n', 'Enquiries:  020 8410 4700 \r\n', 'Fax:  0870 324 0179 \r\n', 'Surrey\r\n', '1', '51.37333818', '-0.09214863', '1', 'Crime\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:06', '0');
INSERT INTO `court` VALUES ('158', 'Croydon Employment Tribunal\r\n', 'Montague Court\r\n101 London Road\r\nCroydon\r\nSurrey\r\nCR0 2RF ', '', 'londonsouthet@hmcts.gsi.gov.uk', 'Enquiries:  0208 667 9131 \r\n', 'Fax:  020 8649 9470 \r\n', 'Surrey\r\n', '1', '51.37968126', '-0.10457420', '1', 'Employment\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:06', '0');
INSERT INTO `court` VALUES ('159', 'Croydon Magistrates\' Court and Family Court\r\n', 'Barclay Road\r\nCroydon\r\nSurrey\r\nCR9 3NG ', '15 D\'Eynsford Road\r\nCamberwell Green\r\nLondon\r\nSE5 7UP ', 'Enquiries:  gl-southgroupmcenq@hmcts.gsi.gov.uk \r\nFine queries:  LCCCCollectionUnit@hmcts.gsi.gov.uk \r\nWitness service:  croydon.mc@cawitnessservice.cjsm.net \r\n', 'Enquiries:  020 7805 9851, 020 7805 9860 \r\nFine queries:  020 7556 8500 \r\nPay a fine:  0300 790 9901 \r\nLegal aid:  020 7805 9851, 020 7805 9860 \r\nWitness service:  0300 332 1179 \r\n', '', 'Surrey\r\n', '1', '51.37239683', '-0.09398374', '1', 'Crime\r\nChildren\r\n', '2015-07-06 00:00:00', '0', '2015-07-11 14:18:06', '0');

-- ----------------------------
-- Table structure for `court_copy`
-- ----------------------------
DROP TABLE IF EXISTS `court_copy`;
CREATE TABLE `court_copy` (
  `court_id` int(11) NOT NULL AUTO_INCREMENT,
  `court_name` varchar(200) NOT NULL,
  `address` mediumtext,
  `write_address` mediumtext,
  `email` varchar(200) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `location` varchar(200) NOT NULL,
  `court_type_court_type_id` int(11) NOT NULL,
  `lat` decimal(11,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remarks` mediumtext,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`court_id`,`court_type_court_type_id`),
  KEY `fk_court_court_type_idx` (`court_type_court_type_id`),
  KEY `court_id` (`court_id`),
  CONSTRAINT `court_copy_ibfk_1` FOREIGN KEY (`court_type_court_type_id`) REFERENCES `court_type` (`court_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of court_copy
-- ----------------------------
INSERT INTO `court_copy` VALUES ('1', 'Aberdeen Tribunal Hearing Centre', 'Mezzanine Floor Atholl House 84-88 Guild Street Aberdeen Aberdeenshire AB11 6LT', '(ET) 3rd Floor Eagle Building\r\n215 Bothwell Street\r\nGlasgow', 'aberdeenet@hmcts.gsi.gov.uk', '01224 593 137', '01224 593 138', 'Aberdeenshire', '1', '57.14445639', '-2.09971170', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:42:51', '0');
INSERT INTO `court_copy` VALUES ('2', 'Abergavenny Magistrates\' Court', 'Tudor Street\r\nAbergavenny\r\nMonmouthshire\r\nNP7 5DL', 'Gwent Magistrates\' Court\r\nThe Law Courts\r\nFaulkner Road\r\nNewport\r\n', 'GW-adminenq@hmcts.gsi.gov.uk', '01633 261300', '0870 739 4319', 'Monmouthshire', '1', '51.82175899', '-3.02319557', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:43:19', '0');
INSERT INTO `court_copy` VALUES ('3', 'Aberystwyth Justice Centre', 'Aberystwyth Justice Centre\r\nY Lanfa\r\nTrefechan\r\nAberystwyth\r\nCeredigion\r\nSY23 1AS', '', 'enquiries@aberystwyth.countycourt.gsi.gov.uk', '01970 621 250', '0870 761 7656', 'Ceredigion', '1', '52.40906996', '-4.08709983', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:43:51', '0');
INSERT INTO `court_copy` VALUES ('4', 'Accrington County Court', 'Bradshawgate House\r\n1 Oak Street\r\nAccrington\r\nLancashire\r\nBB5 1EQ\r\n', 'Burnley County Court\r\nThe Law Courts\r\nHammerton Street\r\nBurnley\r\nLancashire\r\nBB11 1XD', 'enquiries@burnley.countycourt.gsi.gov.uk', '01254 237 490,', '0870 739 4210', 'Lancashire', '1', '53.75146550', '-2.36172454', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:44:00', '0');
INSERT INTO `court_copy` VALUES ('5', 'Accrington Magistrates\' Court', 'East Lancashire Magistrates\' Court\r\nThe Law Courts\r\nManchester Road\r\nAccrington\r\nLancashire\r\nBB5 2BH', 'Accrington Magistrates\' Court\r\nThe Court House\r\nNorthgate\r\nBlackburn\r\nLancashire\r\nBB2 1AA', 'ln-blackburnmcenq@hmcts.gsi.gov.uk', '01254 687500', '01254 687500', 'Lancashire', '1', '53.74912812', '-2.35932376', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:44:26', '0');
INSERT INTO `court_copy` VALUES ('6', 'Administrative Court', 'Administrative Court Office\r\nThe Royal Courts of Justice\r\nStrand\r\nLondon\r\nWC2A 2LL', '', 'administrativecourtoffice.generaloffice@hmcts.x.gsi.gov.uk', '020 7947 6655', '020 7947 6802', 'London', '1', '51.51414311', '-0.11374170', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:44:36', '0');
INSERT INTO `court_copy` VALUES ('7', 'Admiralty and Commercial Court', 'Rolls Building\r\n7 Rolls Buildings\r\nFetter Lane\r\nLondon\r\nEC4A 1NL', '', 'comct.issue@hmcts.gsi.gov.uk', '020 7947 6112', '  0870 761 7725', 'London', '1', '51.51589485', '-0.10967644', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:44:57', '0');
INSERT INTO `court_copy` VALUES ('8', 'Aldershot and Farnham County Court and Family Court', 'Aldershot County Court and Family Court Hearing Centre\r\n84-86 Victoria Road\r\nAldershot\r\nHampshire\r\nGU11 1SS', '', 'aldershot.court.enquiries@hmcts.gsi.gov.uk', '01252 796 800', '0870 324 0155', 'Hampshire', '1', '51.24836472', '-0.75954956', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:12', '0');
INSERT INTO `court_copy` VALUES ('9', 'Aldershot Magistrates\' Court', 'The Court House\r\nCivic Centre\r\nAldershot\r\nHampshire\r\nGU11 1NY', '', '', '', '', '', '1', null, null, '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:23', '0');
INSERT INTO `court_copy` VALUES ('10', 'Aldershot Social Security and Child Support Tribunal', 'Arcade Chambers\r\nThe Arcade\r\nLittle Wellington Street\r\nAldershot\r\nHampshire\r\nGU11 1EE\r\n', 'HM Courts & Tribunals Service Sutton\r\nCopthall House\r\n9 The Pavement\r\nGrove Road\r\nSutton\r\nLondon\r\nSM1 1DA', 'sscsa-sutton@hmcts.gsi.gov.uk', '0300 123 1142', '020 8652 2381', 'Hampshire', '1', '51.24890770', '-0.76321698', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:22', '0');
INSERT INTO `court_copy` VALUES ('11', 'Aldridge Magistrates\' Court', '', '', '', '', '', '', '1', null, null, '1', 'This court or tribunal is no longer in service.', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:25', '0');
INSERT INTO `court_copy` VALUES ('12', 'Alton Magistrates\' Court', '', '', '', '', '', '', '1', null, null, '1', 'This court or tribunal is no longer in service.', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:25', '0');
INSERT INTO `court_copy` VALUES ('13', 'Altrincham County Court and Family Court', 'Trafford Courthouse\r\nAshton Lane\r\nSale\r\nCheshire\r\nM33 7NR', 'Altrincham County Court and Family Court Hearing Centre', 'enquiries@altrincham.countycourt.gsi.gov.uk', '0161 975 4760 ', '01264 347 894', 'Cheshire', '1', '53.42576900', '-2.32577200', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:40', '0');
INSERT INTO `court_copy` VALUES ('14', 'Amersham Law Courts', 'King George V Road\r\nAmersham\r\nBuckinghamshire\r\nHP6 5AJ', '', 'Enquiries@Aylesbury.Crowncourt.gsi.gov.uk', '01296 434 401', '01264 785079', 'Buckinghamshire', '1', '51.67472300', '-0.60283700', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:43:12', '0');
INSERT INTO `court_copy` VALUES ('15', 'Andover Magistrates\' Court', '', '', '', '', '', '', '1', null, null, '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:26', '0');
INSERT INTO `court_copy` VALUES ('16', 'Avon and Somerset Central Accounts Department', 'HMCTS\r\nCentral Accounts Dept\r\nP.O. Box 480\r\nWeston-Super-Mare\r\nAvon\r\nBS23 9BE', '', 'av.qet@hmcts.gsi.gov.uk', '01934 528 528', '01934 528 528', 'Avon', '1', '51.34387288', '-2.95700855', '1', 'This court or tribunal is no longer in service.', '2015-07-02 00:00:00', '0', '2015-07-06 18:43:40', '0');
INSERT INTO `court_copy` VALUES ('17', 'Avon & Somerset Fixed Penalty Office', '', '', '', '', '', '', '1', null, null, '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 15:45:27', '0');
INSERT INTO `court_copy` VALUES ('18', 'Aylesbury County Court and Family Court', 'Walton Street\r\nAylesbury\r\nBuckinghamshire\r\nHP21 7QZ', 'Aylesbury County Court and Family Court Hearing Centre\r\nWalton Street\r\nAylesbury\r\nBuckinghamshire\r\nHP21 7QZ\r\n', 'enquiries@Aylesbury.countycourt.gsi.gov.uk', '01296 554 327', '01264 785026', 'Buckinghamshire', '1', '51.81078059', '-0.80817086', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:44:05', '0');
INSERT INTO `court_copy` VALUES ('19', 'Aylesbury Crown Court', 'County Hall\r\nMarket Square\r\nAylesbury\r\nBuckinghamshire\r\nHP20 1XD', '', 'Enquiries@Aylesbury.Crowncourt.gsi.gov.uk', '01296 434 401', '01264 785079', 'Buckinghamshire', '1', '51.81566527', '-0.81203185', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:44:20', '0');
INSERT INTO `court_copy` VALUES ('20', 'Aylesbury Magistrates\' Court and Family Court', 'Walton Street\r\nAylesbury\r\nBuckinghamshire\r\nHP21 7QZ', 'Milton Keynes Magistrates\' Court and Family Court Hearing Centre\r\n301 Silbury Boulevard\r\nWitan Gate East\r\nCentral Milton Keynes\r\nBuckinghamshire\r\nMK9 2AJ', '', '01908 451 145', '01264 785107', 'Buckinghamshire', '1', '51.81077983', '-0.80817120', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:44:31', '0');
INSERT INTO `court_copy` VALUES ('21', 'Ayr Social Security and Child Support Tribunal', 'Russell House\r\nKing Street\r\nAyr\r\nAyrshire\r\nKA8 0BD\r\n', 'Wellington House\r\n134-136 Wellington Street\r\nGlasgow\r\nG2 2XL\r\n', 'enquiries-cicap@hmcts.gsi.gov.uk', '0141 354 8400', '01264 347 981', 'Ayrshire', '1', '55.46644863', '-4.62492495', '1', '', '2015-07-02 00:00:00', '0', '2015-07-06 18:44:37', '0');
INSERT INTO `court_copy` VALUES ('23', 'Banbury County Court\r\n', 'The Court House\r\nWarwick Road\r\nBanbury\r\nOxfordshire\r\nOX16 2AW ', '', 'enquiries@banbury.countycourt.gsi.gov.uk \r\n', '01295 452 090 \r\n', '01264 785029 \r\n', 'Oxfordshire\r\n', '1', '52.06393067', '-1.34088575', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court_copy` VALUES ('24', 'Banbury Fixed Penalty Office\r\n', '', '', '', '', '', '', '1', '0.00000000', '0.00000000', '1', 'This court or tribunal is no longer in service.\r\n', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court_copy` VALUES ('25', 'Banbury Magistrates\' Court and Family Court\r\n', 'The Court House\r\nWarwick Road\r\nBanbury\r\nOxfordshire\r\nOX16 2AW ', 'Oxford Magistrates Court and Family Court Hearing Centre\r\nSpeedwell Street\r\nOxford\r\nOxfordshireOX1 1RZ ', ' TV-OxfordMCEnq@hmcts.gsi.gov.uk \r\n', '01865 448 020 \r\n', '01264 785088 \r\n', 'Oxfordshire\r\n', '1', '52.06392984', '-1.34088575', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court_copy` VALUES ('26', 'Bankruptcy Court (Central London)\r\n', 'Thomas More Building\r\nRoyal Courts of Justice\r\nStrand\r\nLondon\r\nLondon\r\nWC2A 2LL ', '\r\n', 'RCJBankCLCCDJHearings@hmcts.gsi.gov.uk \r\n', '020 7947 6448 , 020 7947 6863 , 020 7947 6441 , 020  7947 6839 \r\n', '0870 761 7695 \r\n', 'London\r\n', '1', '51.51414311', '-0.11374170', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court_copy` VALUES ('27', 'Bankruptcy Court (High Court)\r\n', 'Rolls Building\r\n7 Rolls Buildings\r\nFetter Lane\r\nLondon\r\nEC4A 1NL ', '', 'rolls.filemanagement@hmcts.gsi.gov.uk, RCJCompanies.Orders@hmcts.gsi.gov.uk', ' 020 8343 4272 \r\n', '0870 324 0201 \r\n', 'London\r\n', '1', '51.60043679', '-0.19560673', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court_copy` VALUES ('28', 'Barkingside Magistrates\' Court\r\n', '850 Cranbrook Road\r\nBarkingside\r\nIlford\r\nEssex\r\nIG6 1HW ', '', 'gl-barkingsidemcenq@hmcts.gsi.gov.uk \r\n', '020 8437 6525 \r\n', '0870 739 4187 \r\n', 'Essex\r\n', '1', '51.58771250', '-0.19560673', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court_copy` VALUES ('29', 'Barnet Civil and Family Courts Centre\r\n', 'St Marys Court\r\nRegents Park Road\r\nFinchley Central\r\nLondon\r\nN3 1BQ ', 'Hearing Centre\r\nSt Marys Court\r\nRegents Park Road\r\nFinchley Central\r\nLondon\r\nN3 1BQ ', 'enquiries@barnet.countycourt.gsi.gov.uk \r\n', '020 8343 4272 \r\n', ' 0870 324 0201 \r\n', 'London\r\n', '1', '51.60043679', '-0.19560673', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court_copy` VALUES ('30', 'Barnsley Law Courts\r\n', 'The Court House\r\nWestgate\r\nBarnsley\r\nSouth Yorkshire\r\nS70 2HW ', 'The Court House\r\nWestgate\r\nBarnsley\r\nSouth Yorkshire\r\nS70 2DW ', 'enquiries@barnsley.countycourt.gsi.gov.uk \r\n', '01226 320 000 \r\n', '01264 347 930 \r\n', 'South Yorkshire\r\n', '1', '53.55468102', '-1.48437529', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:30', '0');
INSERT INTO `court_copy` VALUES ('31', 'Barnsley Social Security and Child Support Tribunal', 'Wellington House\r\nWellington Street\r\nBarnsley\r\nSouth Yorkshire\r\nS70 1WA ', 'York House\r\n31 York Place\r\nLeeds\r\nWest Yorkshire\r\nLS1 2ED ', 'sscsa-leeds@hmcts.gsi.gov.uk \r\n', ' 0300 123 1142 \r\n', '  0113 389 6001 \r\n', 'South Yorkshire\r\n', '1', '53.55085460', '-1.48082956', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('32', 'Barnstaple County Court and Family Court\r\n', 'Barnstaple County Court and Family Court hearing centre\r\n8th Floor\r\nThe Law Courts\r\nCivic Centre\r\nBarnstaple\r\nDevon\r\nEX31 1DX ', '', 'hearings@barnstaple.countycourt.gsi.gov.uk \r\n', '01271 340 410 \r\n', '0870 324 0129 \r\n', 'Devon\r\n', '1', '51.08134299', '-1.48082956', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('33', 'Barnstaple Crown Court\r\n', 'The Law Courts\r\nCivic Centre\r\nBarnstaple\r\nDevon\r\nEX31 1DX ', 'Exeter Combined Court\r\nSouthernhay Gardens\r\nExeter\r\nDevon\r\nEX1 1UH ', '  exeter.enquiries@exeter.crowncourt.gsi.gov.uk \r\n', '01392 415 330 \r\n', '', 'Devon\r\n', '1', '51.08134299', '-4.06565858', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('34', 'Barnstaple Magistrates\' Court - North and East Devon\r\n', 'The Law Courts\r\nCivic Centre\r\nBarnstaple\r\nDevon\r\nEX31 1DX ', '', 'DE-Barnstaplemclist@hmcts.gsi.gov.uk \r\n', '01271 340 410 \r\n', '0870 324 0128 \r\n', 'Devon\r\n', '1', '51.08134243', '-4.06565858', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('35', 'Barrow-in-Furness County Court and Family Court\r\n', 'Barrow Law Courts\r\nAbbey Road\r\nBarrow in Furness\r\nCumbria\r\nLA14 5QX ', 'Barrow-in-Furness County Court and Family Court Hearing Centre\r\nBarrow Law Courts\r\nAbbey Road\r\nBarrow in Furness\r\nCumbria\r\nLA14 5QX ', ' enquiries.barrowcountycourt@hmcts.gsi.gov.uk \r\n', '  01229 840 370 \r\n', '0870 739 4409 \r\n', 'Cumbria\r\n', '1', '54.11652236', '-3.22757743', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('36', 'Barrow-in-Furness Magistrates\' Court\r\n', 'Abbey Road\r\nBarrow in Furness\r\nCumbria\r\nLA14 5QX ', '', 'cumbria.south.magistrates@hmcts.gsi.gov.uk \r\n', '01229 820 161 \r\n', '0870 739 4409 \r\n', 'Cumbria\r\n', '1', '54.11652098', '-3.22757705', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('37', 'Basildon Combined Court\r\n', 'The Gore\r\nBasildon\r\nEssex\r\nSS14 2EW ', '', 'enquiries@basildon.crowncourt.gsi.gov.uk, enquiries@basildon.countycourt.gsi.gov.uk', '0344 892 4000 \r\n', '01268 458 100 \r\n', 'Essex\r\n', '1', '51.57257094', '0.45480719', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('38', 'Basildon Magistrates\' Court and Family Court\r\n', 'The Court House\r\nGreat Oaks\r\nBasildon\r\nEssex\r\nSS14 1EH ', 'Basildon Magistrates\' Court and Family Court Hearing Centre\r\nCourt Administration Centre\r\nP.O. Box 10754\r\nChelmsford\r\nEssex\r\nCM1 9PZ ', 'esosprey@hmcts.gsi.gov.uk \r\n', '01245 313 300 \r\n', '0870 324 0091 \r\n', 'Essex\r\n', '1', '51.57177086', '0.45822771', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('39', 'Basingstoke County Court and Family Court\r\n', 'Basingstoke County Court and Family Court hearing centre\r\nThe Court House\r\nLondon Road\r\nBasingstoke\r\nHampshire\r\nRG21 4AB ', '', 'Basingstoke.court.enquiries@hmcts.gsi.gov.uk \r\n', '01256 318 200 \r\n', '  01264 785170 \r\n', 'Hampshire\r\n', '1', '51.26331533', '-1.08317824', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('40', 'Basingstoke Magistrates\' Court\r\n', 'The Court House\r\nLondon Road\r\nBasingstoke\r\nHampshire\r\nRG21 4AB ', '', 'ha.basmags@hmcts.gsi.gov.uk \r\n', '01256 318 200 \r\n', '', 'Hampshire\r\n', '1', '51.26331471', '-1.08317850', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('41', 'Bath County Court and Family Court\r\n', 'Bath County Court and Family Court hearing centre\r\nThe Law Courts\r\nNorth Parade Road\r\nBath\r\nSomerset\r\nBA1 5AF ', 'Bath County Court and Family Court\r\nP.O. Box 4302\r\nNorth Parade Road\r\nBath\r\nSomerset\r\nBA1 0LF ', 'av-bathcounty@hmcts.gsi.gov.uk \r\n', '01225 476 730 \r\n', '0161 743 4023 \r\n', 'Somerset\r\n', '1', '51.38081360', '-2.35428380', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('42', 'Bath Magistrates\' Court\r\n', 'North Parade Road\r\nBath\r\nSomerset\r\nBA1 5AF', '', 'av-bath.mc@hmcts.gsi.gov.uk \r\n', '01225 46 3281 \r\n', '0870 324 0189 \r\n', 'Somerset\r\n', '1', '51.38114337', '-2.35205908', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('43', 'Bedford and Mid Beds Magistrates\' Court and Family Court\r\n', 'Shire Hall\r\n3 St Paul\'s Square\r\nBedford\r\nBedfordshire\r\nMK40 1SQ ', 'Luton & South Bedfordshire Magistrates\' Court and Family Hearing Centre\r\nStuart Street\r\nLuton\r\nBedfordshire\r\nLU1 5BL ', ' bd-lutonmcenq@hmcts.gsi.gov.uk \r\n', '01234 319 100 \r\n', ' 01234 319 114 \r\n', 'Bedfordshire\r\n', '1', '52.13502903', '-0.46788032', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('44', 'Bedford County Court and Family Court\r\n', 'Shire Hall\r\n3 St Paul\'s Square\r\nBedford\r\nBedfordshire\r\nMK40 1SQ ', 'Bedford County Court and Family Court Hearing Centre\r\nP.O. Box 1405\r\n3 St Paul\'s Square\r\nBedford\r\nBedfordshire\r\nMK40 9DN ', 'enquiries@bedford.countycourt.gsi.gov.uk \r\n', '0300 123 5577 \r\n', '0161 743 4023 \r\n', 'Bedfordshire\r\n', '1', '52.13502903', '-0.46788032', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('45', 'Belmarsh Magistrates\' Court\r\n', '4 Belmarsh Road\r\nThamesmead\r\nLondon\r\n', 'Belmarsh Magistrates\' Court\r\n9 Blackheath Road\r\nLondon\r\nSE10 8PE ', '\r\nGL-CAO.correspond@hmcts.gsi.gov.uk \r\n', '0300 790 9901 \r\n', '', 'Thamesmead\r\n', '1', '51.49600000', '0.09176900', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('46', 'Berwick upon Tweed Magistrates\' Court\r\n', '40 Church Street\r\nBerwick upon Tweed\r\nNorthumberland\r\nTD15 1DX ', '', 'NO_Berwick@hmcts.gsi.gov.uk \r\n', '0191 2327326 \r\n', '55.769863568', 'Northumberland\r\n', '1', '55.76986357', '-2.00188691', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('47', 'Beverley Magistrates\' Court and Family Court\r\n', 'Beverley Magistrates\' Court and Family Court Hearing Centre\r\nThe Court House\r\nChampney Road\r\nBeverley\r\nEast Yorkshire\r\nHU17 9EJ ', '', 'hu-beverleymcadmin@hmcts.gsi.gov.uk \r\n', '  01482 861 607 \r\n', '0870 739 4398 \r\n', 'East Yorkshire\r\n', '1', '53.84052313', '-0.42866900', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('48', 'Bexleyheath Social Security and Child Support Tribunal\r\n', '40-46 Avenue Road\r\nBexleyheath\r\nKent\r\nDA7 4EG ', 'HMCTS Sutton\r\nCopthall House\r\n9 The Pavement\r\nGrove Road\r\nSutton\r\nLondon\r\nSM1 1DA ', 'sscsa-sutton@hmcts.gsi.gov.uk \r\n', ' 0300 123 1142 \r\n', '020 8652 2381 \r\n', 'Kent\r\n', '1', '51.46281921', '0.13255203', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:31', '0');
INSERT INTO `court_copy` VALUES ('49', 'Bexley Magistrates\' Court and Family Court\r\n', 'Norwich Place\r\nBexleyheath\r\nKent\r\nDA6 7ND ', 'Bexley Magistrates\' Court\r\nThe Court house\r\n1 London Road\r\nBromley\r\nKent\r\nBR1 1RA  Opening hours\r\n', 'gl-bromleymcenq@hmcts.gsi.gov.uk \r\n', '020 8437 3585 \r\n', '0870 324 0223 \r\n', 'Kent\r\n', '1', '51.45518276', '0.14790024', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('50', 'Bicester Magistrates\' Court and Family Court\r\n', 'Waverley House\r\nQueens Avenue\r\nBicester\r\nOxfordshire\r\nOX26 2NZ ', 'Oxford Magistrates Court and Family Court Hearing Centre\r\nSpeedwell Street\r\nOxford\r\nOxfordshire\r\nOX1 1RZ ', ' TV-OxfordMCEnq@hmcts.gsi.gov.uk \r\n', '01865 448 020 \r\n', '  01865 448 024 \r\n', 'Oxfordshire\r\n', '1', '51.89919171', '0.14790024', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('51', 'Birkenhead County Court and Family Court\r\n', '76 Hamilton Street\r\nBirkenhead\r\nMerseyside\r\nCH41 5EN ', 'Birkenhead County Court and Family Hearing Centre\r\n76 Hamilton Street\r\nBirkenhead\r\nMerseyside\r\nCH41 5EN ', 'civil@birkenhead.countycourt.gsi.gov.uk \r\n', '0151 666 5800 \r\n', '01264 785 146 \r\n', 'Merseyside\r\n', '1', '53.39137809', '-3.01662367', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('52', 'Birmingham Civil and Family Justice Centre\r\n', 'Birmingham Civil and Family Justice Hearing Centre\r\nPriory Courts\r\n33 Bull Street\r\nBirmingham\r\nWest Midlands\r\nB4 6DS ', '', 'enquiries@birmingham.countycourt.gsi.gov.uk', '0300 123 1751 \r\n', '01264 785131 \r\n', 'West Midlands', '1', '52.48166041', '-1.89552897', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('53', 'Birmingham Crown Court\r\n', 'Queen Elizabeth II Law Courts\r\n1 Newton Street\r\nBirmingham\r\nWest Midlands\r\nB4 7NA ', '', 'birmingham.enquiries@birmingham.crowncourt.gsi.gov.uk \r\n', '0121 681 3300, 0121 681 3339 \r\n', '0870 324 0273 \r\n', 'West Midlands\r\n', '1', '52.48308860', '-1.89415616', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('54', 'Birmingham District Probate Registry\r\n', 'Birmingham District Probate Registry\r\n', 'The Priory Courts\r\n33 Bull Street\r\nBirmingham\r\nWest Midlands\r\nB4 6DU ', 'BirminghamDPRsolicitorsenquiries@hmcts.gsi.gov.uk, ', '0121 681 3400, 0121 681 3401 \r\n', '', 'West Midlands\r\n', '1', '52.48166136', '-1.89552894', '1', 'Fax not available.', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('55', 'Birmingham Employment Tribunal\r\n', 'Centre City Tower\r\n5-7 Hill Street\r\nBirmingham\r\nWest Midlands\r\nB5 4UU ', '', 'BirminghamET@hmcts.gsi.gov.uk \r\n', '0121 600 7780 \r\n', '01264 347 999 \r\n', 'West Midlands\r\n', '1', '52.47622408', '-1.89851583', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('56', 'Birmingham Immigration and Asylum Chamber (First Tier Tribunal)\r\n', 'Sheldon Court\r\n1 Wagon Lane\r\nSheldon\r\nBirmingham\r\nWest Midlands\r\nB26 3DU ', '', 'customer.service@hmcts.gsi.gov.uk \r\n', '0845 600 0877 \r\n', '0870 739 5792 \r\n', 'West Midlands\r\n', '1', '0.00000000', '0.00000000', '1', 'Co-ordinates not available.', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('57', 'Birmingham Magistrates\' Court\r\n', 'Victoria Law Courts\r\nCorporation Street\r\nBirmingham\r\nWest Midlands\r\nB4 6QA ', '', 'WM-CustomerServicesBirmingham@hmcts.gsi.gov.uk \r\n', '0121 212 6600 \r\n', '0870 324 0274 \r\n', 'West Midlands\r\n', '1', '52.48373578', '-1.89405153', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('58', 'Birmingham Social Security and Child Support Tribunal\r\n', 'Administrative Support Centre \r\nPO Box 14620\r\nBirmingham\r\nWest Midlands\r\nB16 6FR', '', '', '0300 123 1142 \r\n', '01264 347 983 \r\n', 'West Midlands\r\n', '1', '0.00000000', '0.00000000', '1', 'Email/co-ordinates not available.', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('59', 'Birmingham Youth Court\r\n', 'Steelhouse Lane\r\nBirmingham\r\nWest Midlands\r\nB4 6BJ ', '', 'WM-CustomerServicesBirmingham@hmcts.gsi.gov.uk \r\n', '0121 212 6600 \r\n', '0121 212 6771 \r\n', 'West Midlands\r\n', '1', '52.48335097', '-1.89496539', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('60', 'Blackburn County Court and Family Court\r\n', '64 Victoria Street\r\nBlackburn\r\nLancashire\r\nBB1 6DJ ', 'Blackburn County Court and Family Court Hearing Centre\r\n64 Victoria Street\r\nBlackburn\r\nLancashire\r\nBB1 6DJ ', 'enquiries@blackburn.countycourt.gsi.gov.uk \r\n', '01254 299 840 \r\n', '0870 324 0328 \r\n', 'Lancashire\r\n', '1', '53.74989980', '-2.48248176', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('61', 'Blackburn Magistrates\' Court\r\n', 'The Court House\r\nNorthgate\r\nBlackburn\r\nLancashire\r\nBB2 1AA ', '', '  ln-blackburnmcenq@hmcts.gsi.gov.uk \r\n', '01254 687 500 \r\n', '0870 739 4254 \r\n', 'Lancashire\r\n', '1', '53.74982981', '-2.48613549', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('62', 'Blackfriars Crown Court\r\n', '1-15 Pocock Street\r\nLondon\r\nSE1 OBT ', '', 'blackfriars.enquiries@blackfriars.crowncourt.gsi.gov.uk \r\n', '020 7922 5800 \r\n', ' 0870 324 0146 \r\n', 'London\r\n', '1', '51.50223900', '-0.09993400', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('63', 'Blackpool County Court and Family Court\r\n', 'The Law Courts\r\nChapel Street\r\nBlackpool\r\nLancashire\r\nFY1 5RJ ', 'Blackpool County Court and Family Court Hearing Centre\r\nThe Law Courts\r\nChapel Street\r\nBlackpool\r\nLancashire\r\nFY1 5RJ ', 'enquiries@blackpool.countycourt.gsi.gov.uk \r\n', '01253 754 020 \r\n', '01264 347 891 \r\n', 'Lancashire\r\n', '1', '53.81173283', '-3.05298421', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('64', 'Blackpool Magistrates\' Court\r\n', 'Civic Centre\r\nChapel Street\r\nBlackpool\r\nLancashire\r\nFY1 5DQ ', '', ' ln-blackpoolmcenq@hmcts.gsi.gov.uk \r\n', '01253 757 000 \r\n', '08703 240203 \r\n', 'Lancashire\r\n', '1', '53.81173283', '-3.05298421', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('65', 'Blackwood Civil and Family Court\r\n', '8 Hall Street\r\nBlackwood\r\nSouth Wales\r\nNP12 1NY ', '', ' Blackwood.Enquiries@hmcts.gsi.gov.uk \r\n', '01495 238 200 \r\n', '0870 324 0167 \r\n', 'South Wales\r\n', '1', '51.66653055', '-3.19293370', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:32', '0');
INSERT INTO `court_copy` VALUES ('66', 'Bodmin County Court and Family Court\r\n', 'Bodmin County Court and Family Court hearing centre\r\nThe Law Courts\r\nLaunceston Road\r\nBodmin\r\nCornwall\r\nPL31 2AL ', 'Bodmin County Court and Family Court hearing centre\r\nThe Law Courts\r\nLaunceston Road\r\nBodmin\r\nCornwall\r\nPL31 2AL ', 'enquiriesbodmincc@hmcts.gsi.gov.uk \r\n', '01208 261 580 \r\n', '01208 77255 \r\n', 'Cornwall', '1', '50.46952713', '-4.70886473', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('67', 'Bodmin Magistrates\' Court\r\n', 'Launceston Road\r\nBodmin\r\nCornwall\r\nPL31 2AL ', '', 'cornwall-admin@hmcts.gsi.gov.uk \r\n', '01208 262 700 \r\n', '01208 269 965 \r\n', 'Cornwall', '1', '50.46952713', '-4.70886473', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('68', 'Bodmin Probate Sub-Registry\r\n', 'Magistrates\' Courts\r\nLaunceston Road\r\nBodmin\r\nCornwall\r\nPL31 2AL ', 'The Civil Justice Centre\r\n2 Redcliff Street\r\nBristol\r\nBS1 6GR ', 'bristoldprolicitorsenquiries@hmcts.gsi.gov.uk, bristoldprenquiries@hmcts.gsi.gov.uk \r\n', '01208 261 581 \r\n', '', 'Cornwall\r\n', '1', '50.46952752', '-4.70886549', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('69', 'Bolton County Court and Family Court\r\n', 'The Law Courts\r\nBlackhorse Street\r\nBolton\r\nGreater Manchester\r\nBL1 1SU ', 'Bolton County Court and Family Court Hearing Centre\r\nThe Law Courts\r\nBlackhorse Street\r\nBolton\r\nGreater Manchester\r\nBL1 1SU ', 'enquiries@bolton.countycourt.gsi.gov.uk \r\n', '01204 392 881 \r\n', '01264 347908 \r\n', 'Greater Manchester\r\n', '1', '53.57820434', '-2.43368898', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('70', 'Bolton Crown Court\r\n', 'The Law Courts\r\nBlackhorse Street\r\nBolton\r\nGreater Manchester\r\nBL1 1SU ', '', '\r\nenquiries@bolton.crowncourt.gsi.gov.uk \r\n', '01204 392 881 \r\n', '01204 363 204 \r\n', 'Greater Manchester\r\n', '1', '53.57820434', '-2.43368898', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('71', 'Bolton Magistrates\' Court\r\n', 'The Courts\r\nLe Mans Crescent\r\nBolton\r\nGreater Manchester\r\nBL1 1UA ', '', 'GM-BoltonMcAdmin@hmcts.gsi.gov.uk \r\n', '01204 558 200 \r\n', '0870 324 0133 \r\n', 'Greater Manchester\r\n', '1', '53.57870600', '-2.43167028', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('72', 'Bolton Social Security and Child Support Tribunal\r\n', 'Ground Floor\r\nBayley House\r\nSt Georges Square\r\nBolton\r\nGreater Manchester\r\nBL1 2HB ', '36 Dale Street\r\nLiverpool\r\nMerseyside\r\nL2 5UZ ', 'sscsa-liverpool@hmcts.gsi.gov.uk', '0300 123 1142 \r\n', '0151 243 1441 \r\n', 'Greater Manchester\r\n', '1', '53.58296338', '-2.42752967', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('73', 'Boston County Court and Family Court\r\n', 'Boston County Court and Family Court hearing centre\r\n55 Norfolk Street\r\nBoston\r\nLincolnshire\r\nPE21 6PE ', '', 'bostonenquiries@hmcts.gsi.gov.uk \r\n', '01205 366 080 \r\n', '0161 743 4023 \r\n', 'Lincolnshire\r\n', '1', '52.98305915', '-0.02498226', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('74', 'Boston Magistrates\' Court\r\n', 'Boston Court House\r\n55 Norfolk Street\r\nBoston\r\nLincolnshire\r\nPE21 6PE ', 'The Court House\r\n358 High Street\r\nLincoln\r\nLincolnshire\r\nLN5 7QA ', 'Li-lincolnmcadmin@hmcts.gsi.gov.uk \r\n', '01522 528218 \r\n', '08703240256 \r\n', 'Lincolnshire\r\n', '1', '52.98306023', '-0.02498167', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('75', 'Bournemouth and Poole County Court and Family Court\r\n', 'Bournemouth and Poole County Court and Family Court hearing centre\r\nCourts of Justice\r\nDeansleigh Road\r\nBournemouth\r\nDorset\r\nBH7 7DS ', '', 'enquiries@bournemouth.countycourt.gsi.gov.uk \r\n', '01202 502800 \r\n', '01202 502 801 \r\n', '', '1', '50.74795173', '-1.81626555', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('76', 'Bournemouth Crown Court\r\n', 'Courts of Justice\r\nDeansleigh Road\r\nBournemouth\r\nDorset\r\nBH7 7DS ', '', 'enquiries@bournemouth.crowncourt.gsi.gov.uk \r\n', '01202 502 800 \r\n', '0870 7394490 \r\n', 'Dorset\r\n', '1', '50.74795221', '-1.81626549', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('77', 'Bournemouth Magistrates\' Court\r\n', 'The Law Courts\r\nStafford Road\r\nBournemouth\r\nDorset\r\nBH1 1JH ', '', 'bmthmc@hmcts.gsi.gov.uk \r\n', '01202 745 309 \r\n', '', 'Dorset\r\n', '1', '50.72311299', '-1.86887825', '1', 'Fax service not available.', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('78', 'Bow County Court and Family Court\r\n', 'Hearing Centre \r\n96 Romford Road\r\nStratford\r\nLondon\r\nE15 4EG ', '96 Romford Road\r\nStratford\r\nLondon\r\nE15 4EG ', 'civil@bow.countycourt.gsi.gov.uk, family@bow.countycourt.gsi.gov.uk \r\n\r\n', '020 8536 5200', '0870 324 0188 \r\n', 'London\r\n', '1', '51.54291793', '0.01085347', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('79', 'Bradford and Keighley Magistrates\' Court and Family Court\r\n', 'Bradford and Keighley Magistrates\' Court and Family Court Hearing Centre\r\nThe Tyrls\r\nBradford\r\nWest Yorkshire\r\nBD1 1LA ', '', 'wy-bradfordmags@hmcts.gsi.gov.uk \r\n', '01274 390 111 \r\n', '0870 739 4466 \r\n', 'West Yorkshire\r\n', '1', '53.79176752', '-1.75417576', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('80', 'Bradford Combined Court Centre\r\n', 'Bradford Law Courts\r\nExchange Square\r\nDrake Street\r\nBradford\r\nWest Yorkshire\r\nBD1 1JA ', '', 'uiries@bradford.countycourt.gsi.gov.uk \r\n', '01274 840 274 \r\n', '01264 347 975 \r\n', 'West Yorkshire\r\n', '1', '53.79274522', '-1.75417576', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('81', 'Bradford Tribunal Hearing Centre\r\n', 'Phoenix House\r\nRushton Avenue\r\nThornbury\r\nBradford\r\nWest Yorkshire\r\nBD3 7BH ', '', '', '0300 123 1711 \r\n', '0870 739 4452 \r\n', 'West Yorkshire\r\n', '1', '53.80101962', '-1.71132104', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('82', 'Brecon Law Courts\r\n', 'Brecon Law Courts\r\nCambrian Way\r\nBrecon\r\nPowys\r\nLD3 7HR ', 'Merthyr Tydfil Combined Courts\r\nThe Law Courts\r\nGlebeland Place\r\nMerthyr Tydfil\r\nSouth Wales\r\nCF47 8BH ', 'sw-merthyrmcenq@hmcts.gsi.gov.uk \r\n', '01685 727 600 \r\n', '0870 324 0338 \r\n', 'Powys', '1', '51.94481593', '-3.38199382', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('83', 'Brentford County Court and Family Court\r\n', 'Alexandra Road\r\nHigh Street\r\nBrentford\r\nMiddlesex\r\nTW8 0JJ ', 'Hearing Centre\r\nAlexandra Road\r\nHigh Street\r\nBrentford\r\nMiddlesex\r\nTW8 0JJ ', 'enquiries@Brentford.countycourt.gsi.gov.uk \r\n', '  020 8231 8940 \r\n', '0870 739 5909 \r\n', 'Middlesex\r\n', '1', '51.48428615', '-0.30435566', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('84', 'Bridgend Law Courts\r\n', 'The Law Courts\r\nSunnyside\r\nBridgend\r\nSouth Wales\r\nCF31 4AJ ', '', 'EnquiriesBridgendLawCourts@hmcts.gsi.gov.uk \r\n', '01656 673 833 \r\n', '0870 739 5940 \r\n', 'South Wales\r\n', '1', '51.50425487', '-3.58271768', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:19:33', '0');
INSERT INTO `court_copy` VALUES ('85', 'Bridlington Magistrates\' Court and Family Court\r\n', 'Bridlington Magistrates\' Court and Family Court Hearing Centre\r\nThe Court House\r\nQuay Road\r\nBridlington\r\nEast Yorkshire\r\nYO16 4EJ ', '', 'bridlington.mc@cawitnessservice.cjsm.net \r\n', '01482 861 607 \r\n', '0870 739 4398 \r\n', 'East Yorkshire\r\n', '1', '54.08510300', '-0.20009500', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:21:16', '0');
INSERT INTO `court_copy` VALUES ('86', 'Brighton County Court\r\n', 'William Street\r\nBrighton\r\nEast Sussex\r\nBN2 0RF ', '', 'enquiries@brighton.countycourt.gsi.gov.uk, hearings@brighton.countycourt.gsi.gov.uk, bailiffs@brighton.countycourt.gsi.gov.uk, e-filing@brighton.countycourt.gsi.gov.uk \r\n', '01273 674 421 (Enquiries), 01273 388 421 (Appointments)\r\n\r\n', '  0870 324 0319 \r\n', 'East Sussex\r\n', '1', '50.82301576', '-0.13522827', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:21:16', '0');
INSERT INTO `court_copy` VALUES ('87', 'Brighton District Probate Registry\r\n', 'William Street\r\nBrighton\r\nEast Sussex\r\nBN2 0RF ', '', 'BrightonDPRenquiries@hmcts.gsi.gov.uk, BrightonDPRsolicitorsenquiries@hmcts.gsi.gov.uk \r\n', '01273 573 510 \r\n', '01273 625 845 \r\n', 'East Sussex\r\n', '1', '50.82301625', '-0.13522774', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:21:17', '0');
INSERT INTO `court_copy` VALUES ('88', 'Brighton Magistrates\' Court\r\n', 'The Law Courts\r\nEdward Street\r\nBrighton\r\nEast Sussex\r\nBN2 0LG ', 'Brighton Magistrates\' Court Administration Centre\r\nThe Law Courts\r\nEdward Street\r\nBrighton\r\nEast Sussex\r\nBN2 0LG ', 'ss-sussexadmin@hmcts.gsi.gov.uk (Enquiries), sussexfamily@hmcts.gsi.gov.uk (Family queries)\r\n\r\n', '01273 670 888 (Enquiries), 01273 811333 (Family queries) \r\n\r\n', '0870 761 7669 \r\n', 'East Sussex\r\n', '1', '50.82323469', '-0.13429665', '1', '', '2015-07-06 00:00:00', '0', '2015-07-06 19:21:17', '0');

-- ----------------------------
-- Table structure for `court_OLD`
-- ----------------------------
DROP TABLE IF EXISTS `court_OLD`;
CREATE TABLE `court_OLD` (
  `court_id` int(11) NOT NULL AUTO_INCREMENT,
  `court_name` varchar(200) NOT NULL,
  `address` mediumtext,
  `location` varchar(200) NOT NULL,
  `court_type_court_type_id` int(11) NOT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`court_id`,`court_type_court_type_id`),
  KEY `fk_court_court_type_idx` (`court_type_court_type_id`),
  KEY `court_id` (`court_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of court_OLD
-- ----------------------------
INSERT INTO `court_OLD` VALUES ('1', 'Abergavenny Magistrates\' Court', 'Tudor Street\r\nAbergavenny\r\nMonmouthshire\r\nNP7 5DL', 'Monmouthshire', '1', null, null, '1', '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0');
INSERT INTO `court_OLD` VALUES ('3', 'Accrington County Court', 'Bradshawgate House1 Oak StreetAccringtonLancashireBB5 1EQ', 'Lancashire', '6', null, null, '1', '0000-00-00 00:00:00', '0', '2015-07-02 13:23:48', '17');
INSERT INTO `court_OLD` VALUES ('4', 'Accrington Magistrates\' Court', 'East Lancashire Magistrates\' Court\r\nThe Law Courts\r\nManchester Road\r\nAccrington\r\nLancashire\r\nBB5 2BH', 'Lancashire', '2', null, null, '1', '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0');
INSERT INTO `court_OLD` VALUES ('5', 'Admiralty and Commercial Court', 'Rolls Building\r\n7 Rolls Buildings\r\nFetter Lane\r\nLondon\r\nEC4A 1NL', 'London', '2', null, null, '1', '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0');
INSERT INTO `court_OLD` VALUES ('6', 'Aldershot and Farnham County Court and Family Court', 'Aldershot County Court and Family Court Hearing Centre\r\n84-86 Victoria Road\r\nAldershot\r\nHampshire\r\nGU11 1SS', 'Hampshire', '3', null, null, '1', '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0');
INSERT INTO `court_OLD` VALUES ('7', 'Aldershot Magistrates\' Court', 'The Court House\r\nCivic Centre\r\nAldershot\r\nHampshire\r\nGU11 1NY', 'Hampshire', '4', null, null, '1', '0000-00-00 00:00:00', '0', '0000-00-00 00:00:00', '0');
INSERT INTO `court_OLD` VALUES ('43', 'fdsadf', 'sadf', 'asdf', '2', null, null, '1', '2015-07-02 13:12:36', '17', '2015-07-02 13:12:36', '0');

-- ----------------------------
-- Table structure for `court_type`
-- ----------------------------
DROP TABLE IF EXISTS `court_type`;
CREATE TABLE `court_type` (
  `court_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  PRIMARY KEY (`court_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of court_type
-- ----------------------------
INSERT INTO `court_type` VALUES ('1', 'Magistrates Court');
INSERT INTO `court_type` VALUES ('2', 'Crown Court');
INSERT INTO `court_type` VALUES ('3', 'Country Court');
INSERT INTO `court_type` VALUES ('4', 'High Court');
INSERT INTO `court_type` VALUES ('6', 'Employment Tribunal');
INSERT INTO `court_type` VALUES ('176', 'test');

-- ----------------------------
-- Table structure for `documents`
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `type` varchar(225) NOT NULL,
  `size` int(11) NOT NULL,
  `insert_date` datetime DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`document_id`),
  KEY `fk_documents_users1_idx` (`users_user_id`),
  CONSTRAINT `fk_documents_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of documents
-- ----------------------------

-- ----------------------------
-- Table structure for `documents_has_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `documents_has_keywords`;
CREATE TABLE `documents_has_keywords` (
  `documents_document_id` int(11) NOT NULL,
  `keywords_keyword_id` int(11) NOT NULL,
  PRIMARY KEY (`documents_document_id`,`keywords_keyword_id`),
  KEY `fk_documents_has_keywords_keywords1_idx` (`keywords_keyword_id`),
  KEY `fk_documents_has_keywords_documents_idx` (`documents_document_id`),
  CONSTRAINT `fk_documents_has_keywords_documents` FOREIGN KEY (`documents_document_id`) REFERENCES `documents` (`document_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_documents_has_keywords_keywords1` FOREIGN KEY (`keywords_keyword_id`) REFERENCES `keywords` (`keyword_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of documents_has_keywords
-- ----------------------------

-- ----------------------------
-- Table structure for `hearing`
-- ----------------------------
DROP TABLE IF EXISTS `hearing`;
CREATE TABLE `hearing` (
  `hearing_id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_no` varchar(100) NOT NULL,
  `hearing_date` datetime NOT NULL,
  `comments` mediumtext,
  `estimated_time_hearing` varchar(20) DEFAULT NULL,
  `court_court_id` int(11) NOT NULL,
  `hearing_type_hearing_type_id` int(11) NOT NULL,
  `law_area_law_area_id` int(11) NOT NULL,
  `track_track_id` int(11) NOT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`hearing_id`,`court_court_id`,`hearing_type_hearing_type_id`,`law_area_law_area_id`,`track_track_id`,`users_user_id`),
  KEY `fk_hearing_court1_idx` (`court_court_id`),
  KEY `fk_hearing_hearing_type1_idx` (`hearing_type_hearing_type_id`),
  KEY `fk_hearing_law_area1_idx` (`law_area_law_area_id`),
  KEY `fk_hearing_track1_idx` (`track_track_id`),
  KEY `fk_hearing_users1_idx` (`users_user_id`),
  CONSTRAINT `hearing_ibfk_1` FOREIGN KEY (`court_court_id`) REFERENCES `court_OLD` (`court_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `hearing_ibfk_2` FOREIGN KEY (`hearing_type_hearing_type_id`) REFERENCES `hearing_type` (`hearing_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `hearing_ibfk_3` FOREIGN KEY (`law_area_law_area_id`) REFERENCES `law_area` (`law_area_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `hearing_ibfk_4` FOREIGN KEY (`track_track_id`) REFERENCES `track` (`track_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `hearing_ibfk_5` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hearing
-- ----------------------------
INSERT INTO `hearing` VALUES ('1', '', '2015-05-22 15:23:49', 'This is just testing', '4', '3', '2', '2', '1', '31');
INSERT INTO `hearing` VALUES ('2', '', '2015-05-15 14:21:37', 'Second hearing for test', '5', '5', '3', '2', '1', '17');
INSERT INTO `hearing` VALUES ('3', '', '2015-04-22 16:24:18', 'Hearing for testing.', '5', '5', '3', '4', '1', '31');
INSERT INTO `hearing` VALUES ('4', '', '2015-05-14 16:31:38', 'thadasdasdsd', '5', '5', '2', '2', '1', '31');
INSERT INTO `hearing` VALUES ('5', '', '2015-05-20 12:00:00', 'This is simple hearing', '4', '4', '3', '2', '1', '32');
INSERT INTO `hearing` VALUES ('6', '', '2015-05-30 11:17:13', 'Testing', '4', '4', '2', '2', '1', '32');
INSERT INTO `hearing` VALUES ('7', '', '2015-05-15 18:42:57', 'This hearing added my organization', '5', '1', '2', '2', '1', '31');
INSERT INTO `hearing` VALUES ('8', '', '2015-04-23 18:39:24', '', '2', '4', '3', '2', '1', '31');
INSERT INTO `hearing` VALUES ('9', '', '2015-06-01 18:39:24', '', '2', '4', '3', '2', '1', '16');
INSERT INTO `hearing` VALUES ('10', '', '2015-04-28 18:41:27', '', '3', '4', '3', '4', '1', '16');
INSERT INTO `hearing` VALUES ('11', '', '2015-04-28 18:41:27', '', '3', '4', '3', '4', '1', '31');
INSERT INTO `hearing` VALUES ('12', '', '2015-04-28 18:41:27', '', '3', '4', '3', '4', '1', '16');
INSERT INTO `hearing` VALUES ('13', '', '2015-04-28 18:41:27', '', '3', '4', '3', '4', '1', '33');
INSERT INTO `hearing` VALUES ('14', '', '2015-04-28 18:41:27', '', '3', '4', '3', '4', '1', '33');
INSERT INTO `hearing` VALUES ('15', '', '2015-07-09 18:06:53', 'This is testing for user for', '5', '6', '3', '2', '1', '16');
INSERT INTO `hearing` VALUES ('16', '', '2015-07-22 13:08:34', '', '4', '4', '3', '5', '1', '16');
INSERT INTO `hearing` VALUES ('17', '', '2015-07-24 18:09:50', '', '4', '6', '1', '4', '1', '16');

-- ----------------------------
-- Table structure for `hearing_available_for`
-- ----------------------------
DROP TABLE IF EXISTS `hearing_available_for`;
CREATE TABLE `hearing_available_for` (
  `users_user_id` int(11) NOT NULL,
  `hearing_hearing_id` int(11) NOT NULL,
  PRIMARY KEY (`users_user_id`,`hearing_hearing_id`),
  KEY `fk_users_has_hearing_hearing1_idx` (`hearing_hearing_id`),
  KEY `fk_users_has_hearing_users1_idx` (`users_user_id`),
  CONSTRAINT `fk_users_has_hearing_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_hearing_hearing1` FOREIGN KEY (`hearing_hearing_id`) REFERENCES `hearing` (`hearing_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hearing_available_for
-- ----------------------------
INSERT INTO `hearing_available_for` VALUES ('27', '17');
INSERT INTO `hearing_available_for` VALUES ('31', '17');

-- ----------------------------
-- Table structure for `hearing_msg`
-- ----------------------------
DROP TABLE IF EXISTS `hearing_msg`;
CREATE TABLE `hearing_msg` (
  `hearing_msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_date` datetime NOT NULL,
  `users_user_id` int(11) NOT NULL,
  `hearing_hearing_id` int(11) NOT NULL,
  `messages_message_id` int(11) NOT NULL,
  PRIMARY KEY (`hearing_msg_id`,`users_user_id`,`hearing_hearing_id`,`messages_message_id`),
  KEY `fk_hearing_msg_users1_idx` (`users_user_id`),
  KEY `fk_hearing_msg_hearing1_idx` (`hearing_hearing_id`),
  KEY `fk_hearing_msg_messages1_idx` (`messages_message_id`),
  CONSTRAINT `fk_hearing_msg_hearing1` FOREIGN KEY (`hearing_hearing_id`) REFERENCES `hearing` (`hearing_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_hearing_msg_messages1` FOREIGN KEY (`messages_message_id`) REFERENCES `messages` (`message_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_hearing_msg_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hearing_msg
-- ----------------------------
INSERT INTO `hearing_msg` VALUES ('1', '2015-04-29 11:20:13', '16', '1', '1');
INSERT INTO `hearing_msg` VALUES ('2', '2015-04-24 11:20:42', '16', '2', '1');
INSERT INTO `hearing_msg` VALUES ('3', '2015-05-04 11:25:19', '16', '5', '1');
INSERT INTO `hearing_msg` VALUES ('4', '2015-05-04 11:27:20', '16', '7', '1');
INSERT INTO `hearing_msg` VALUES ('5', '2015-04-13 12:31:25', '27', '1', '1');
INSERT INTO `hearing_msg` VALUES ('6', '2015-04-18 13:20:08', '31', '3', '1');

-- ----------------------------
-- Table structure for `hearing_type`
-- ----------------------------
DROP TABLE IF EXISTS `hearing_type`;
CREATE TABLE `hearing_type` (
  `hearing_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `hearing_type` varchar(200) NOT NULL,
  PRIMARY KEY (`hearing_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hearing_type
-- ----------------------------
INSERT INTO `hearing_type` VALUES ('1', 'Application');
INSERT INTO `hearing_type` VALUES ('2', 'Leave for Appeal');
INSERT INTO `hearing_type` VALUES ('3', 'Case Management');
INSERT INTO `hearing_type` VALUES ('4', 'Closing Arguements');
INSERT INTO `hearing_type` VALUES ('25', 'PCMHS');
INSERT INTO `hearing_type` VALUES ('26', 'Sentencing');
INSERT INTO `hearing_type` VALUES ('27', 'Opening Arguments');
INSERT INTO `hearing_type` VALUES ('28', 'Arguments');
INSERT INTO `hearing_type` VALUES ('29', 'Evidencing');
INSERT INTO `hearing_type` VALUES ('30', 'Examination in Chief');
INSERT INTO `hearing_type` VALUES ('31', 'Cross Examination');
INSERT INTO `hearing_type` VALUES ('32', 'Costs');
INSERT INTO `hearing_type` VALUES ('33', 'Appeal');
INSERT INTO `hearing_type` VALUES ('34', 'Award');
INSERT INTO `hearing_type` VALUES ('35', 'Miscellaneous other hearing');

-- ----------------------------
-- Table structure for `invitation_status`
-- ----------------------------
DROP TABLE IF EXISTS `invitation_status`;
CREATE TABLE `invitation_status` (
  `invitation_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`invitation_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of invitation_status
-- ----------------------------
INSERT INTO `invitation_status` VALUES ('1', 'Invited');
INSERT INTO `invitation_status` VALUES ('2', 'Accepted');
INSERT INTO `invitation_status` VALUES ('3', 'Declined');

-- ----------------------------
-- Table structure for `keywords`
-- ----------------------------
DROP TABLE IF EXISTS `keywords`;
CREATE TABLE `keywords` (
  `keyword_id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(45) NOT NULL,
  PRIMARY KEY (`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of keywords
-- ----------------------------

-- ----------------------------
-- Table structure for `law_area`
-- ----------------------------
DROP TABLE IF EXISTS `law_area`;
CREATE TABLE `law_area` (
  `law_area_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_of_law` varchar(200) NOT NULL,
  PRIMARY KEY (`law_area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of law_area
-- ----------------------------
INSERT INTO `law_area` VALUES ('1', 'Transfer of Property');
INSERT INTO `law_area` VALUES ('2', 'Actual Bodily Harm');
INSERT INTO `law_area` VALUES ('3', 'Grievous Bodily Harm');
INSERT INTO `law_area` VALUES ('4', 'Unfair Terms of Contract');
INSERT INTO `law_area` VALUES ('5', 'Implied Terms of Contract');

-- ----------------------------
-- Table structure for `legal_qualifications`
-- ----------------------------
DROP TABLE IF EXISTS `legal_qualifications`;
CREATE TABLE `legal_qualifications` (
  `legal_qualifications_id` int(11) NOT NULL AUTO_INCREMENT,
  `qualificatoin` varchar(100) NOT NULL,
  PRIMARY KEY (`legal_qualifications_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of legal_qualifications
-- ----------------------------
INSERT INTO `legal_qualifications` VALUES ('1', 'Barrister');
INSERT INTO `legal_qualifications` VALUES ('2', 'Solicitor');
INSERT INTO `legal_qualifications` VALUES ('3', 'Silicitor Advocate');
INSERT INTO `legal_qualifications` VALUES ('4', 'Clerk');
INSERT INTO `legal_qualifications` VALUES ('5', 'Agent');

-- ----------------------------
-- Table structure for `login_attempts`
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) NOT NULL,
  `login` varchar(50) NOT NULL,
  `time` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for `member_invitations`
-- ----------------------------
DROP TABLE IF EXISTS `member_invitations`;
CREATE TABLE `member_invitations` (
  `invitation_id` int(11) NOT NULL AUTO_INCREMENT,
  `to_name` varchar(100) NOT NULL,
  `to_email` varchar(100) NOT NULL COMMENT 'Invitation to email address',
  `users_user_id` int(11) NOT NULL COMMENT 'Invitation from Organization user ID',
  `invitation_date` datetime NOT NULL,
  `accepted_date` datetime DEFAULT NULL,
  `invitation_status_invitation_status_id` int(11) NOT NULL,
  PRIMARY KEY (`invitation_id`,`invitation_status_invitation_status_id`),
  KEY `fk_invitation_users1_idx` (`users_user_id`),
  KEY `fk_member_invitations_invitation_status1_idx` (`invitation_status_invitation_status_id`),
  CONSTRAINT `fk_invitation_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_member_invitations_invitation_status1` FOREIGN KEY (`invitation_status_invitation_status_id`) REFERENCES `invitation_status` (`invitation_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member_invitations
-- ----------------------------
INSERT INTO `member_invitations` VALUES ('4', 'Imdad Ali', 'iadaudpota@gmail.com', '31', '2015-04-25 13:17:04', null, '2');
INSERT INTO `member_invitations` VALUES ('5', 'Saqib Ali', 'saqibsra@gmail.com', '31', '2015-05-01 07:07:12', '2015-05-01 07:22:12', '2');

-- ----------------------------
-- Table structure for `messages`
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('1', 'I would like to cover hearing request.');

-- ----------------------------
-- Table structure for `organization_profile`
-- ----------------------------
DROP TABLE IF EXISTS `organization_profile`;
CREATE TABLE `organization_profile` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(225) NOT NULL,
  `regulatory_authority_regulatory_authority_id` int(11) NOT NULL,
  `regulatory_authority_no` varchar(100) NOT NULL,
  `number_employees` varchar(50) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `address` text,
  `city` varchar(225) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip_code` varchar(100) DEFAULT NULL,
  `contact_no` varchar(100) DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  `organization_types_organization_type_id` int(11) NOT NULL,
  `prosecuting_authority` int(2) NOT NULL DEFAULT '0',
  `prosecuting_authority_prosecuting_authority_id` int(11) NOT NULL DEFAULT '0',
  `region` varchar(225) DEFAULT NULL,
  `primary_contact_name` varchar(100) NOT NULL,
  `primary_contact_position` varchar(100) NOT NULL,
  `primary_contact_email` varchar(100) NOT NULL,
  `primary_contact_workphone` varchar(100) NOT NULL,
  `primary_contact_mobile` varchar(100) NOT NULL,
  `secondary_contact_name` varchar(100) DEFAULT NULL,
  `secondary_contact_position` varchar(100) DEFAULT NULL,
  `secondary_contact_email` varchar(100) DEFAULT NULL,
  `secondary_contact_workphone` varchar(100) DEFAULT NULL,
  `secondary_contact_mobile` varchar(100) DEFAULT NULL,
  `alternative_contact_name` varchar(100) DEFAULT NULL,
  `alternative_contact_position` varchar(100) DEFAULT NULL,
  `alternative_contact_email` varchar(100) DEFAULT NULL,
  `alternative_contact_workphone` varchar(100) DEFAULT NULL,
  `alternative_contact_mobile` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_profile_id`,`regulatory_authority_regulatory_authority_id`,`organization_types_organization_type_id`,`prosecuting_authority_prosecuting_authority_id`),
  KEY `fk_user_individual_profile_users1_idx` (`users_user_id`),
  KEY `fk_user_individual_profile_regulatory_authority1_idx` (`regulatory_authority_regulatory_authority_id`),
  KEY `fk_organization_profile_organization_types1_idx` (`organization_types_organization_type_id`),
  KEY `fk_prosecuting_authority_types1_idx` (`prosecuting_authority_prosecuting_authority_id`) USING BTREE,
  CONSTRAINT `fk_organization_profile_organization_types1` FOREIGN KEY (`organization_types_organization_type_id`) REFERENCES `organization_types` (`organization_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prosecuting_authority_types10` FOREIGN KEY (`prosecuting_authority_prosecuting_authority_id`) REFERENCES `prosecuting_authority_types` (`prosecuting_authority_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_individual_profile_regulatory_authority10` FOREIGN KEY (`regulatory_authority_regulatory_authority_id`) REFERENCES `regulatory_authority` (`regulatory_authority_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_individual_profile_users10` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of organization_profile
-- ----------------------------
INSERT INTO `organization_profile` VALUES ('1', 'LZ Law', '1', '8552', '50', 'lzlaw.com', 'Karachi', 'karachi', 'Pakistan', '14545', '021488', '31', '2', '0', '1', null, 'Nazir', 'Clerk', 'nazir@gmail.com', '0214', '02148', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `organization_profile` VALUES ('3', 'ABC', '3', '2584', '', 'abc', '', 'London', 'England', '58855', '021144', '35', '1', '0', '2', '', 'jjjj', 'asdf', 'jsfd@gmail.com', '22148', '221447', '', '', '', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for `organization_types`
-- ----------------------------
DROP TABLE IF EXISTS `organization_types`;
CREATE TABLE `organization_types` (
  `organization_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_type` varchar(100) NOT NULL,
  PRIMARY KEY (`organization_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of organization_types
-- ----------------------------
INSERT INTO `organization_types` VALUES ('1', 'Chambers');
INSERT INTO `organization_types` VALUES ('2', 'Law Firm');

-- ----------------------------
-- Table structure for `previledges`
-- ----------------------------
DROP TABLE IF EXISTS `previledges`;
CREATE TABLE `previledges` (
  `previledge_id` int(11) NOT NULL AUTO_INCREMENT,
  `previledge` varchar(45) NOT NULL,
  PRIMARY KEY (`previledge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of previledges
-- ----------------------------
INSERT INTO `previledges` VALUES ('1', 'Add Hearing');
INSERT INTO `previledges` VALUES ('2', 'Send Message to Open Hearing');

-- ----------------------------
-- Table structure for `previledges_has_user_member_profile`
-- ----------------------------
DROP TABLE IF EXISTS `previledges_has_user_member_profile`;
CREATE TABLE `previledges_has_user_member_profile` (
  `previledges_previledge_id` int(11) NOT NULL,
  `user_member_profile_user_profile_id` int(11) NOT NULL,
  PRIMARY KEY (`previledges_previledge_id`,`user_member_profile_user_profile_id`),
  KEY `fk_previledges_has_user_member_profile_previledges_idx` (`previledges_previledge_id`),
  KEY `fk_previledges_has_user_member_profile_user_member_profile1_idx` (`user_member_profile_user_profile_id`) USING BTREE,
  CONSTRAINT `fk_previledges_has_user_member_profile_user_member_profile1` FOREIGN KEY (`user_member_profile_user_profile_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_previledges_has_user_member_profile_previledges` FOREIGN KEY (`previledges_previledge_id`) REFERENCES `previledges` (`previledge_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of previledges_has_user_member_profile
-- ----------------------------
INSERT INTO `previledges_has_user_member_profile` VALUES ('1', '32');
INSERT INTO `previledges_has_user_member_profile` VALUES ('2', '32');

-- ----------------------------
-- Table structure for `prosecuting_authority_types`
-- ----------------------------
DROP TABLE IF EXISTS `prosecuting_authority_types`;
CREATE TABLE `prosecuting_authority_types` (
  `prosecuting_authority_id` int(11) NOT NULL AUTO_INCREMENT,
  `prosecuting_authority_type` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`prosecuting_authority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of prosecuting_authority_types
-- ----------------------------
INSERT INTO `prosecuting_authority_types` VALUES ('1', 'Normal', '1', '2015-07-01 16:43:55', '1', '2015-07-01 16:44:02', '0');
INSERT INTO `prosecuting_authority_types` VALUES ('2', 'Crown Prosecution Service', '0', '2015-07-01 17:12:28', '0', '2015-07-01 17:12:28', '0');

-- ----------------------------
-- Table structure for `qualifications`
-- ----------------------------
DROP TABLE IF EXISTS `qualifications`;
CREATE TABLE `qualifications` (
  `qualification_id` int(11) NOT NULL AUTO_INCREMENT,
  `qualification` varchar(100) NOT NULL,
  PRIMARY KEY (`qualification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of qualifications
-- ----------------------------
INSERT INTO `qualifications` VALUES ('1', 'Pupilage');
INSERT INTO `qualifications` VALUES ('2', 'Training Contract');

-- ----------------------------
-- Table structure for `regulatory_authority`
-- ----------------------------
DROP TABLE IF EXISTS `regulatory_authority`;
CREATE TABLE `regulatory_authority` (
  `regulatory_authority_id` int(11) NOT NULL AUTO_INCREMENT,
  `authority` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`regulatory_authority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of regulatory_authority
-- ----------------------------
INSERT INTO `regulatory_authority` VALUES ('1', 'Bar Standards Board');
INSERT INTO `regulatory_authority` VALUES ('2', 'Solicitors Regulatory Authority');
INSERT INTO `regulatory_authority` VALUES ('3', 'Chartered Institute of Legal Executives');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `date_create` datetime NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `description_UNIQUE` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Administrators', '2015-02-12 12:46:43', '2015-02-12 12:47:13');
INSERT INTO `roles` VALUES ('2', 'Moderators', '2015-02-12 12:46:51', '2015-02-12 12:47:11');
INSERT INTO `roles` VALUES ('3', 'Users', '2015-02-12 12:47:04', '2015-02-12 12:47:09');

-- ----------------------------
-- Table structure for `tasks`
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(225) DEFAULT NULL,
  `directions` text,
  `status` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`task_id`),
  KEY `fk_tasks_users1_idx` (`users_user_id`),
  CONSTRAINT `fk_tasks_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tasks
-- ----------------------------

-- ----------------------------
-- Table structure for `track`
-- ----------------------------
DROP TABLE IF EXISTS `track`;
CREATE TABLE `track` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `track` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`track_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of track
-- ----------------------------
INSERT INTO `track` VALUES ('1', 'multi');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activated` tinyint(1) NOT NULL,
  `banned` tinyint(1) DEFAULT NULL,
  `ban_reason` varchar(225) DEFAULT NULL,
  `new_password_requested` varchar(225) DEFAULT NULL,
  `new_password_key` varchar(50) DEFAULT NULL,
  `new_email` varchar(100) DEFAULT NULL,
  `new_email_key` varchar(50) DEFAULT NULL,
  `last_ip` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `roles_role_id` int(11) NOT NULL,
  `user_type_user_type_id` int(11) DEFAULT NULL,
  `notificatoin` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`roles_role_id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_roles1_idx` (`roles_role_id`),
  KEY `fk_users_user_type1_idx` (`user_type_user_type_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `fk_users_roles1` FOREIGN KEY (`roles_role_id`) REFERENCES `roles` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_user_type1` FOREIGN KEY (`user_type_user_type_id`) REFERENCES `user_type` (`user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('16', 'bahadur.oad', '$2a$08$d2sIDu3l.H634C3ojqGG.OChxfoNndW6IekV8XgkIOKYwRkxHJYyK', 'bahadur.oad@brickchamber.co.uk', '1', null, null, null, null, null, null, '192.168.0.97', '2015-03-17 09:48:03', '2015-03-20 11:40:03', '2015-07-08 11:42:12', '3', '1', '1');
INSERT INTO `users` VALUES ('17', 'imdad', '$2a$08$rTmCOJyvAAKJ733ZwhDPrureHfmSGQsi5buLSbYk465T/k6iNTCai', 'iadaudpota@gmail.com', '1', null, null, null, null, null, null, '192.168.0.97', '2015-03-17 10:20:18', '2015-07-09 16:23:48', '2015-07-09 16:23:48', '1', '3', '0');
INSERT INTO `users` VALUES ('27', 'bahadur.rai', '$2a$08$A0ots9QqP.alFXDZB6.9JOa4A9OgpR6aUjMTCZCRM6m4RS/leegam', 'bahadur.rai@gmail.com', '0', null, null, null, null, null, null, '192.168.0.97', '2015-04-02 08:28:01', null, '2015-05-28 14:40:57', '3', '3', '0');
INSERT INTO `users` VALUES ('28', 'saqib', '$2a$08$13ImBh7csQlOyasQAQUclOSe9m88yFb1bsqCfEO8g78hj9LGbB6xy', 'saqib@gmail.com', '0', null, null, null, null, null, null, '192.168.0.97', '2015-04-17 13:52:45', null, '2015-04-18 13:09:12', '3', '1', '0');
INSERT INTO `users` VALUES ('31', 'lzlaw', '$2a$08$36L0XX5shCxuWSN8TeN6duQedhUYZGA.6FY2Kyfi5937B3UyuP9TG', 'lzlaw@brickchamber.co.uk', '1', null, null, null, null, null, null, '192.168.0.97', '2015-04-18 13:00:35', null, '2015-04-18 13:09:20', '3', '2', '0');
INSERT INTO `users` VALUES ('32', 'saqibsra', '$2a$08$FK303iz1VX7X62N7keDNIOwLgWOphBvZCDMbWM4Zb6i8wYUjEoPqG', 'saqibsra@gmail.com', '1', null, null, null, null, null, null, '192.168.0.97', '2015-05-01 07:22:12', null, '2015-05-28 14:50:38', '3', '3', '0');
INSERT INTO `users` VALUES ('33', 'bahaduroad', '$2a$08$C3tCHEV.YMvel4D6rjlOceu15bnMkHbtj4Xvo4tLhEf/duvWQ8ZHC', 'bahadur.oad@gmail.com', '0', null, null, null, null, null, 'efff8ba521d4c77bb08e667e2f7a7147', '192.168.0.97', '2015-05-13 10:56:54', null, '2015-05-28 14:35:06', '3', '2', '0');
INSERT INTO `users` VALUES ('35', 'abc', '$2a$08$qFqqZ46F0e/ICmN.4FawOetISjdbUP7rgklLUWviv0ANs5qmwK4pG', 'abc@gmail.com', '0', null, null, null, null, null, '7b5956b7e19b3d5cc82834324e34bda3', '192.168.0.97', '2015-07-01 17:12:28', null, '2015-07-01 17:12:28', '3', '2', '0');

-- ----------------------------
-- Table structure for `user_autologin`
-- ----------------------------
DROP TABLE IF EXISTS `user_autologin`;
CREATE TABLE `user_autologin` (
  `key_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_agent` varchar(150) NOT NULL,
  `last_ip` varchar(40) NOT NULL,
  `last_login` varchar(45) NOT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`key_id`,`users_user_id`),
  KEY `fk_user_autologin_users1_idx` (`users_user_id`),
  CONSTRAINT `fk_user_autologin_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2449 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_autologin
-- ----------------------------
INSERT INTO `user_autologin` VALUES ('2446', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36 OPR/22.0.1471.50', '192.168.0.162', '', '17');

-- ----------------------------
-- Table structure for `user_individual_profile`
-- ----------------------------
DROP TABLE IF EXISTS `user_individual_profile`;
CREATE TABLE `user_individual_profile` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(225) NOT NULL,
  `m_name` varchar(225) NOT NULL,
  `l_name` varchar(225) NOT NULL,
  `legal_qualifications_legal_qualifications_id` int(11) NOT NULL,
  `regulatory_authority_regulatory_authority_id` int(11) NOT NULL,
  `regulatory_authority_no` varchar(100) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `address` text,
  `city` varchar(225) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip_code` varchar(100) DEFAULT NULL,
  `contact_no` varchar(100) DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  `qualifications_qualification_id` int(11) NOT NULL,
  `date_completion` datetime DEFAULT NULL,
  `primary_contact_name` varchar(100) NOT NULL,
  `primary_contact_position` varchar(100) NOT NULL,
  `primary_contact_email` varchar(100) NOT NULL,
  `primary_contact_workphone` varchar(100) NOT NULL,
  `primary_contact_mobile` varchar(100) NOT NULL,
  `secondary_contact_name` varchar(100) NOT NULL,
  `secondary_contact_position` varchar(100) NOT NULL,
  `secondary_contact_email` varchar(100) NOT NULL,
  `secondary_contact_workphone` varchar(100) NOT NULL,
  `secondary_contact_mobile` varchar(100) NOT NULL,
  `alternative_contact_name` varchar(100) DEFAULT NULL,
  `alternative_contact_position` varchar(100) DEFAULT NULL,
  `alternative_contact_email` varchar(100) DEFAULT NULL,
  `alternative_contact_workphone` varchar(100) DEFAULT NULL,
  `alternative_contact_mobile` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_profile_id`,`legal_qualifications_legal_qualifications_id`,`regulatory_authority_regulatory_authority_id`,`qualifications_qualification_id`),
  KEY `fk_user_individual_profile_users1_idx` (`users_user_id`),
  KEY `fk_user_individual_profile_legal_qualifications1_idx` (`legal_qualifications_legal_qualifications_id`),
  KEY `fk_user_individual_profile_regulatory_authority1_idx` (`regulatory_authority_regulatory_authority_id`),
  KEY `fk_user_individual_profile_qualifications1_idx` (`qualifications_qualification_id`),
  CONSTRAINT `fk_user_individual_profile_legal_qualifications1` FOREIGN KEY (`legal_qualifications_legal_qualifications_id`) REFERENCES `legal_qualifications` (`legal_qualifications_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_individual_profile_qualifications1` FOREIGN KEY (`qualifications_qualification_id`) REFERENCES `qualifications` (`qualification_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_individual_profile_regulatory_authority1` FOREIGN KEY (`regulatory_authority_regulatory_authority_id`) REFERENCES `regulatory_authority` (`regulatory_authority_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_individual_profile_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_individual_profile
-- ----------------------------
INSERT INTO `user_individual_profile` VALUES ('7', 'Bahadur', '', 'Rai', '2', '1', '8552', null, 'Gulistan-e-jauhar', 'karachi', 'Pakistan', '14545', '02144888', '27', '1', '2010-02-09 00:00:00', 'Imdad Ali', 'Clerk', 'imdad@gmail.com', '0214888', '034558522', 'Shariq', 'Clerk', 'shariq@gmail.com', '0214488', '255888', 'Saqib', 'Clerk', 'saqib@gmail.com', '021448', '258744');
INSERT INTO `user_individual_profile` VALUES ('8', 'Imdad', 'Ali', 'Daudpota', '1', '1', '5478', null, 'Jouhar', 'Karachi', 'Pakistan', '7889', '0214887', '17', '1', '2010-02-09 00:00:00', 'Saqib', 'Clerk', 'saqib@gmail.com', '021448', '0221448', '', '', '', '', '', null, null, null, null, null);
INSERT INTO `user_individual_profile` VALUES ('10', 'Bahadur', 'Rai', 'Oad', '1', '1', '855', 'www.bahadur.com', 'Gulshan', 'Karachi', 'Pakistan', '8744', '021488', '16', '1', '2015-04-01 15:02:44', 'Imdad Ali', 'Clerk', 'imdad@gmail.com', '021545', '20225', '', '', '', '', '', null, null, null, null, null);
INSERT INTO `user_individual_profile` VALUES ('11', 'Saqib', '', 'Sidique', '2', '2', '5884', null, 'Malir ', 'Karachi', 'Pakistan', '8552', '021148', '28', '2', '2015-04-07 00:00:00', 'Imdad', 'Clerk', 'imdad@gmail.com', '2255887', '221477', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `user_individual_profile` VALUES ('12', 'Bahadur', '', 'Oad', '1', '1', '25845', '', 'Karachi', 'Karachi', 'Pakistan', '214487', '0214488', '33', '1', '2011-03-02 00:00:00', 'Sariq', 'Clerk', 'sariq@gmail.com', '02141488', '034526688', '', '', '', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for `user_individual_profiles__`
-- ----------------------------
DROP TABLE IF EXISTS `user_individual_profiles__`;
CREATE TABLE `user_individual_profiles__` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(225) NOT NULL,
  `m_name` varchar(225) DEFAULT NULL,
  `l_name` varchar(225) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `year_of_call` varchar(10) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `clerk` varchar(225) DEFAULT NULL,
  `silk` varchar(10) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `description` mediumtext,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_profile_id`,`users_user_id`),
  KEY `fk_user_profile_users1_idx` (`users_user_id`),
  CONSTRAINT `user_individual_profiles___ibfk_1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_individual_profiles__
-- ----------------------------
INSERT INTO `user_individual_profiles__` VALUES ('4', 'Bahadur', 'Rai', 'Oad', '1978-10-28', null, '324324', null, null, 'karachi', 'Pakistan', null, '16');
INSERT INTO `user_individual_profiles__` VALUES ('5', 'Imdad', 'Ali', 'Daudpota', '1970-01-01', null, '', null, null, '', '', null, '17');

-- ----------------------------
-- Table structure for `user_member_profile`
-- ----------------------------
DROP TABLE IF EXISTS `user_member_profile`;
CREATE TABLE `user_member_profile` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(225) NOT NULL,
  `m_name` varchar(225) NOT NULL,
  `l_name` varchar(225) NOT NULL,
  `legal_qualifications_legal_qualifications_id` int(11) NOT NULL,
  `regulatory_authority_regulatory_authority_id` int(11) NOT NULL,
  `regulatory_authority_no` varchar(100) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `address` text,
  `city` varchar(225) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `zip_code` varchar(100) DEFAULT NULL,
  `contact_no` varchar(100) DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  `qualifications_qualification_id` int(11) NOT NULL,
  `date_completion` datetime DEFAULT NULL,
  `users_organization_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_profile_id`,`legal_qualifications_legal_qualifications_id`,`regulatory_authority_regulatory_authority_id`,`qualifications_qualification_id`),
  KEY `fk_user_individual_profile_users1_idx` (`users_user_id`),
  KEY `fk_user_individual_profile_legal_qualifications1_idx` (`legal_qualifications_legal_qualifications_id`),
  KEY `fk_user_individual_profile_regulatory_authority1_idx` (`regulatory_authority_regulatory_authority_id`),
  KEY `fk_user_individual_profile_qualifications1_idx` (`qualifications_qualification_id`),
  CONSTRAINT `user_member_profile_ibfk_1` FOREIGN KEY (`legal_qualifications_legal_qualifications_id`) REFERENCES `legal_qualifications` (`legal_qualifications_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_member_profile_ibfk_2` FOREIGN KEY (`qualifications_qualification_id`) REFERENCES `qualifications` (`qualification_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_member_profile_ibfk_3` FOREIGN KEY (`regulatory_authority_regulatory_authority_id`) REFERENCES `regulatory_authority` (`regulatory_authority_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_member_profile_ibfk_4` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_member_profile
-- ----------------------------
INSERT INTO `user_member_profile` VALUES ('8', 'Imdad', 'Ali', 'Daudpota', '1', '1', '5478', null, 'Jouhar', 'Karachi', 'Pakistan', '7889', '0214887', '17', '1', '2010-02-09 00:00:00', '31');
INSERT INTO `user_member_profile` VALUES ('12', 'Saqib', '', 'Ali', '1', '1', '23432', null, 'Karachi', 'Karachi', 'Pakistan', '14885', '25588', '32', '1', '1970-01-01 00:00:00', '31');

-- ----------------------------
-- Table structure for `user_network_profiles`
-- ----------------------------
DROP TABLE IF EXISTS `user_network_profiles`;
CREATE TABLE `user_network_profiles` (
  `user_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(225) NOT NULL,
  `m_name` varchar(225) DEFAULT NULL,
  `l_name` varchar(225) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `year_of_call` varchar(10) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `clerk` varchar(225) DEFAULT NULL,
  `silk` varchar(10) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `description` mediumtext,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_profile_id`,`users_user_id`),
  KEY `fk_user_profile_users1_idx` (`users_user_id`),
  CONSTRAINT `fk_user_profile_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_network_profiles
-- ----------------------------
INSERT INTO `user_network_profiles` VALUES ('4', 'Bahadur', 'Rai', 'Oad', '1978-10-28', null, '324324', null, null, 'karachi', 'Pakistan', null, '16');
INSERT INTO `user_network_profiles` VALUES ('5', 'Imdad', 'Ali', 'Daudpota', '1970-01-01', null, '', null, null, '', '', null, '17');

-- ----------------------------
-- Table structure for `user_type`
-- ----------------------------
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_type
-- ----------------------------
INSERT INTO `user_type` VALUES ('1', 'Network', 'user_individual_profile');
INSERT INTO `user_type` VALUES ('2', 'Organization', 'organization_profile');
INSERT INTO `user_type` VALUES ('3', 'Member', 'user_member_profile');

-- ----------------------------
-- View structure for `view_alerts`
-- ----------------------------
DROP VIEW IF EXISTS `view_alerts`;
CREATE ALGORITHM=UNDEFINED DEFINER=`superadmin`@`%` SQL SECURITY DEFINER VIEW `view_alerts` AS select `alert`.`alert_id` AS `alert_id`,`alert`.`users_user_id` AS `users_user_id`,`alert`.`court_court_id` AS `court_court_id`,`alert`.`date` AS `date`,`users`.`user_id` AS `user_id`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`email` AS `email`,`users`.`activated` AS `activated`,`users`.`banned` AS `banned`,`users`.`ban_reason` AS `ban_reason`,`users`.`new_password_requested` AS `new_password_requested`,`users`.`new_password_key` AS `new_password_key`,`users`.`new_email` AS `new_email`,`users`.`new_email_key` AS `new_email_key`,`users`.`last_ip` AS `last_ip`,`users`.`created` AS `created`,`users`.`last_login` AS `last_login`,`users`.`modified` AS `modified`,`users`.`roles_role_id` AS `roles_role_id`,`users`.`user_type_user_type_id` AS `user_type_user_type_id`,`users`.`notificatoin` AS `notificatoin`,`court`.`court_id` AS `court_id`,`court`.`court_name` AS `court_name`,`court`.`address` AS `address`,`court`.`location` AS `location`,`court`.`court_type_court_type_id` AS `court_type_court_type_id` from ((`alert` join `users` on((`alert`.`users_user_id` = `users`.`user_id`))) join `court` on((`alert`.`court_court_id` = `court`.`court_id`)));
